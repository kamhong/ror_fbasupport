# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#

case @environment
when 'production'
  every 5.minutes do
    runner "PullAsinSellerWorker.perform_async()", :environment => :production
  end

  every 1.day, at: '0:00 am' do
    runner "User.send_notification", :environment => :production
  end

  every 4.hours do
    runner "GlobalRequestReportWorker.perform_async()", :environment => :production
  end

  every 3.hours do
    runner "GlobalRequestListReportWorker.perform_async()", :environment => :production
  end

  # every 4.hours do
  #   runner "GlobalReadReportWorker.perform_async()", :environment => :production
  # end

  every 4.hours do
    runner "PullInventoryOrderWorker.perform_async()", :environment => :production
  end

  every 4.hours do
    rake "re_order:build_inventory", :environment => :production
  end

  every 4.hours do
    runner "UpdateMissingAsinWorker.perform_async()", :environment => :production
  end

  every 15.minutes do
    rake "amazon:sync_shipments"
  end

  every 1.day, at: '8:00 am' do
    runner "PullInboundShipmentWorker.perform_async()", :environment => :production
  end

  every 1.day, at: '9:00 am' do
    runner "UpdateDailyInventoryWorker.perform_async()", :environment => :production
  end

  every 1.day, at: '5:30 am' do
    runner "ClearDatabaseWorker.perform_async()", :environment => :production
  end

when 'development'

  # every 1.hour do
  #   runner "PullAsinSellerWorker.perform_async()", :environment => :development
  # end

  # every 1.day, at: '0:00 am' do
  #   runner "User.send_notification", output: { error: "#{path}/log/error.log", standard: "#{path}/log/cron.log" }
  # end

  # every 1.day, at: '0:30 am' do
  #   runner "GlobalRequestReportWorker.perform_async()", :environment => :development
  # end

  # every 1.day, at: '1:30 am' do
  #   runner "GlobalRequestListReportWorker.perform_async()", :environment => :development
  # end

  # every 1.day, at: '2:00 am' do
  #   runner "GlobalReadReportWorker.perform_async()", :environment => :development
  # end

  # every 1.day, at: '2:30 am' do
  #   runner "PullInventoryOrderWorker.perform_async()", :environment => :development
  # end

  # every 1.day, at: '2:30 am' do
  #   runner "PullInventoryQuantityWorker.perform_async()", :environment => :development
  # end

  # every 1.day do
  #   runner "UpdateMissingAsinWorker.perform_async()", :environment => :development
  # end

  every 1.day, at: '5:30 am' do
    runner "ClearDatabaseWorker.perform_async()", :environment => :production
  end

end

# every 2.minutes do
#   runner "User.send_notification", output: { error: "#{path}/log/error.log", standard: "#{path}/log/cron.log" }
# end

# Learn more: http://github.com/javan/whenever
