Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.	
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries = true
  # config.action_mailer.default :charset => "utf-8"
  # config.action_mailer.smtp_settings = {
  #   :address                => "smtp.gmail.com",
  #   :port                   => 587,
  #   :domain                 => Rails.application.secrets.domain_name,
  #   :authentication         => "plain",
  #   :enable_starttle_auto   => true,
  #   :user_name              => Rails.application.secrets.email_provider_username,
  #   :password               => Rails.application.secrets.email_provider_password
  # }
  #ses-smtp-user.20181218-180040
  config.action_mailer.smtp_settings = {
    :address        => 'email-smtp.us-west-2.amazonaws.com',
    :port           => '587',
    :authentication => :login,
    :user_name      => 'AKIAIUOFHIO5W7XKHGAA',
    :password       => 'BETFCNvyqkv8/I1b/ytP2BR/Df/20vICnupVQr25g0YD5',
    
    :enable_starttls_auto => true
  }

  config.sass.preferred_syntax = :sass
  config.sass.line_comments = false
  config.sass.cache = false

  config.after_initialize do

    Bullet.enable = true
    Bullet.alert = true
    Bullet.bullet_logger = true
    Bullet.console = true
    Bullet.rails_logger = true
    Bullet.unused_eager_loading_enable = false
  end
end
