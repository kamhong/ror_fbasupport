PDFKit.configure do |config|
  config.default_options = {
    page_size: "Letter", #or "Letter" or whatever needed
    page_width: '70',
    page_height: '80'
  }
end
