Rails.application.routes.draw do

  resources :user_table_sessions
  resources :warehouse_vendors do
    collection do
      post  :load_warehouse_vendor
      post :update_fields
      get :list
    end
  end
  resources :customer_invoices do
    member do
      get :print
    end
    collection do
      post :generate_invoice
    end
  end
  resources :invoices,  only: [:index, :show] do
    collection do
      post :list
      post  :updateitem
      post  :set_invoice
    end
  end
  resources :inbound_shipments do
    collection do
      get :create_from_plan
      get :getshipmentofstage
      get :additemfromstage
      get :removeitem
    end
    member do
      get :reset_transport_plan
      get :sync_transport_plan
      get :void_transport_plan
      get :mark_as_shipped
      get :send_invoice
      post :add_invoice
    end
  end
  resources :mws_orders
  resources :user_market_places
  resources :mws_report_requests
  # resources :outbound_shipments
  resources :seller_skus
  get '/sellerskus/load' => 'seller_skus#load'

  devise_scope :user do
    # root to: "skynets#new"
    get 'users/sign_out' => "devise/sessions#destroy"
  end

  resources :pull_inventories
  resources :skynets
  resources :vendors do
    member do
      get :supplier
    end
  end
  resources :vendor_categories
  resources :vendoritems do
    collection do
      get :getvendoritem
      post :check_sku
    end
  end
  resources :subscriptions
  resources :pos
  resources :pending_pos do
    collection do
      post :complete
      post  :receive
      # post :recreate_sku
      get :recreate_sku
      get :remove_sku
      get :check_sku
      get :list_options
      get  :list_all_po
      post  :updateitem
    end
  end
  resources :pending_po_indices do
    collection do
      get :loadsummary
      get :list
      post :updateitem
    end
  end
  resources :ship_cus_invoices do
    collection do
      get :load_invoices
      post :updateitem
    end
    member do
      get :send_invoice
    end
  end
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords',
    confirmations: 'users/confirmations'
  }

  resources :users, only: [:edit, :update, :create, :new, :destroy] do
    collection do
      post :update_warehouse
    end
  end
  get '/profile' => 'users#profile'
  # authenticated :user do
    # root to: "skynets#new"
  # end

  get '/plans' => 'subscriptions#plans'
  get '/stripe_notification' => 'subscriptions#stripe_notification'
  post '/stripe_checkout' => 'subscriptions#stripe_checkout'
  post '/subscription_checkout' => 'subscriptions#subscription_checkout'
  get '/stripe_charge' => 'subscriptions#stripe_charge'
  get '/stripe_mini_charge' => 'subscriptions#stripe_mini_charge'
  post '/stripe_charge_check' => 'subscriptions#stripe_charge_check'

  get '/welcome' => 'subscriptions#welcome'

  resources :companies

  get '/' => 'home#index'
  # defaults to dashboard
  root :to => redirect('/')

  # view routes
  get '/userlist' => 'home#userlist'
  get '/widgets' => 'widgets#index'
  get '/documentation' => 'documentation#index'
  get 'dashboard/dashboard_buyer'
  get 'dashboard/dashboard_manager'
  get 'dashboard/dashboard_admin'
  get 'dashboard/dashboard_h'

  get '/download' => 'skynets#download'
  get '/skynets/history' => 'skynets#history'

  post '/skynets/uploadfile' => 'skynets#uploadfile'
  post 'updateskynetstatus' => 'skynets#updateskynetstatus'
  post 'insertskynet' => 'skynets#insertskynet'
  post 'importxml' => 'skynets#import_xml'
  post 'testxml' => 'skynets#testxml'
  post 'updatemissingasin' => 'skynets#updatemissingasin'

  require 'sidekiq/web'
  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
  end


  post 'updatevendoritem' =>'vendoritems#updatevendoritem'
  post 'deletevendoritem' =>'vendoritems#deletevendoritem'
  post 'addvendoritem' =>'vendoritems#addvendoritem'

  get 'getprofitable' =>'vendoritems#getprofitable'

  namespace :api do
    get 'skynethisotry' => 'skynet#skynethisotry'
    get 'deleteskynet' => 'skynet#deleteskynet'
    get 'getitemlist' => 'vendoritem#getitemlist'
    post 'refreshitems' => 'vendoritem#refreshitems'
    get 'send_to_po' => 'vendoritem#send_to_po'
  end

  post 'po'=> 'po#upload'
  post 'po/uploadfile' => 'po#uploadfile'
  post 'po/send' => 'po#sendpo'
  post 'po/send_pending' => 'po#send_pending'
  get 'po/index' => 'po#index'
  get 'po/list' => 'po#list'
  get 'po/listpo' => 'po#listpo'
  get 'po/load' => 'po#load'
  post 'po/delete' => 'po#delete'
  post 'po/deletebyvendor' => 'po#deletebyvendor'
  post 'po/updateitem' => 'po#updateitem'


  get 'pending_po/load' => 'pending_pos#load'
  get 'pending_po/loadpoindex' => 'pending_pos#loadpoindex'

  # get 'outbound_shipments/list_pending' => 'outbound_shipments/list_pending'
  resources :outbound_shipments do
    collection do
      get :list_pending
      get :list_pending_all
      get :getshipplanfromstage
      get :createshipplan
      get :bagger_list
      get :list_bagger
      get :getbagger
      get :printbagger
      get :get_previous_address

      post  :printsticker
      get :ajax_download
      post :check_shipplan
      post :send_bagger
      post :generate_shipment_hash
      get :get_box_information
      post :print_mix_2d_barcode
    end
    member do
      get :finalize_shipment
    end
  end

  resources :transport_plans do
    collection do
      post :initiate_estimate
      post :estimate
      post :initiate_confirmation
      post :confirm
      get :print_box_labels
      get :print_pallete_labels
      post :remove_package
      post :remove_pallete
      post :remove_tracking
      get :print_bill_of_lading
      get :print_2d_label
      post :submit_box_information
      get :print_mix_labels
    end
    member do
      get :get_estimates
      post :upload_to_amazon
      post :confirm_shipment
      post :add_packages
      get :box_information
      post :update_box_information
      post :update_shipping_fee
    end
  end

  get 'inbound_shipplans/loadshipplan' => 'inbound_shipplans#loadshipplan'

  get 'contact-me', to: 'messages#new', as: 'new_message'
  post 'contact-me', to: 'home#create', as: 'create_message'

  get 'owner_steven' => 'admin#index'


  # get 'elements/button'
  get 'elements/notification'
  get 'elements/spinner'
  get 'elements/animation'
  # get 'elements/dropdown'
  # get 'elements/nestable'
  # get 'elements/sortable'
  # get 'elements/panel'
  # get 'elements/portlet'
  get 'elements/grid'
  # get 'elements/gridmasonry'
  # get 'elements/typography'
  # get 'elements/fonticons'
  # get 'elements/weathericons'
  # get 'elements/colors'
  # get 'elements/buttons'
  # get 'elements/notifications'
  # get 'elements/carousel'
  # get 'elements/tour'
  # get 'elements/sweetalert'
  get 'forms/standard'
  get 'forms/extended'
  get 'forms/validation'
  # get 'forms/wizard'
  # get 'forms/upload'
  # get 'forms/xeditable'
  # get 'forms/imgcrop'
  # get 'multilevel/level1'
  # get 'multilevel/level3'
  # get 'charts/flot'
  # get 'charts/radial'
  # get 'charts/chartjs'
  # get 'charts/chartist'
  # get 'charts/morris'
  # get 'charts/rickshaw'
  # get 'tables/standard'
  # get 'tables/extended'
  get 'tables/datatable'
  get 'tables/jqgrid'
  # get 'maps/google'
  # get 'maps/vector'
  # get 'extras/mailbox'
  # get 'extras/timeline'
  # get 'extras/calendar'
  # get 'extras/invoice'
  # get 'extras/search'
  # get 'extras/todo'
  # get 'extras/profile'
  # get 'extras/contacts'
  # get 'extras/contact_details'
  # get 'extras/projects'
  # get 'extras/project_details'
  # get 'extras/team_viewer'
  # get 'extras/social_board'
  # get 'extras/vote_links'
  # get 'extras/bug_tracker'
  # get 'extras/faq'
  # get 'extras/help_center'
  # get 'extras/followers'
  # get 'extras/settings'
  # get 'extras/plans'
  # get 'extras/file_manager'
  # get 'blog/blog'
  # get 'blog/blog_post'
  # get 'blog/blog_articles'
  # get 'blog/blog_article_view'
  get 'ecommerce/ecommerce_orders'
  # get 'ecommerce/ecommerce_order_view'
  # get 'ecommerce/ecommerce_products'
  # get 'ecommerce/ecommerce_product_view'
  # get 'ecommerce/ecommerce_checkout'
  # get 'forum/forum_categories'
  # get 'forum/forum_topics'
  # get 'forum/forum_discussion'
  # get 'pages/login'
  # get 'pages/register'
  # get 'pages/recover'
  # get 'pages/lock'
  # get 'pages/template'
  # get 'pages/notfound'
  # get 'pages/maintenance'
  # get 'pages/error500'

  # # api routes
  # get '/api/documentation' => 'api#documentation'
  get '/api/datatable' => 'api#datatable'
  # get '/api/skynethisotry' => 'api#skynethisotry'
  get '/api/jqgrid' => 'api#jqgrid'
  get '/api/jqgridtree' => 'api#jqgridtree'

  get '/api/i18n/:locale' => 'api#i18n'

  get '/api/postbanner' => 'api#postbanner'
  get '/users/sendnotification'
  # post '/api/xeditable' => 'api#xeditable'
  # get '/api/xeditable-groups' => 'api#xeditablegroups'

  # # the rest goes to root
  # get '*path' => redirect('/')

end
