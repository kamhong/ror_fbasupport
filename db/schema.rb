# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190612081205) do

  create_table "asin_sellers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "seller_id"
    t.string  "sub_condition"
    t.integer "seller_positive_feedback_rating"
    t.integer "feedback_count"
    t.decimal "listing_price",                   precision: 7, scale: 2
    t.decimal "shipping",                        precision: 7, scale: 2
    t.boolean "is_buy_box_winner"
    t.boolean "is_fulfilled_by_amazon"
    t.boolean "is_featured_merchant"
    t.boolean "ships_domestically"
    t.string  "asin_str",                                                null: false
    t.index ["asin_str"], name: "index_asin_sellers_on_asin_str", using: :btree
    t.index ["seller_id"], name: "index_asin_sellers_on_seller_id", using: :btree
  end

  create_table "boxes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "dimenstion_unit"
    t.float    "length",               limit: 24
    t.float    "width",                limit: 24
    t.float    "height",               limit: 24
    t.string   "weight_unit"
    t.float    "weight",               limit: 24
    t.integer  "transport_content_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "brands", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.index ["name"], name: "index_brands_on_name", unique: true, using: :btree
  end

  create_table "companies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "addressline1"
    t.string "addressline2"
    t.string "city"
    t.string "zipcode"
    t.string "state"
    t.string "aws_api_key"
    t.string "aws_api_secretkey"
    t.string "aws_assoicate_tag"
    t.string "mws_api_key"
    t.index ["name"], name: "index_companies_on_name", unique: true, using: :btree
  end

  create_table "customer_invoices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "vendor_id"
    t.string   "invoice_name",                                                    default: "",    null: false
    t.string   "invoice_link"
    t.string   "po_link"
    t.string   "labels"
    t.date     "vendor_order_placd"
    t.string   "order_no"
    t.string   "payment"
    t.string   "shipment"
    t.date     "order_date"
    t.integer  "received"
    t.integer  "shipped"
    t.decimal  "invoice_sales_order_cost",               precision: 11, scale: 2, default: "0.0"
    t.decimal  "warehouse_cost",                         precision: 11, scale: 2, default: "0.0"
    t.string   "warehouse_inv"
    t.decimal  "total_paid",                             precision: 11, scale: 2, default: "0.0"
    t.decimal  "profit_difference",                      precision: 11, scale: 2, default: "0.0"
    t.decimal  "refund",                                 precision: 11, scale: 2, default: "0.0"
    t.text     "note",                     limit: 65535
    t.datetime "created_at",                                                                      null: false
    t.datetime "updated_at",                                                                      null: false
    t.integer  "pending_po_index_id"
    t.index ["pending_po_index_id"], name: "index_customer_invoices_on_pending_po_index_id", using: :btree
    t.index ["user_id"], name: "index_customer_invoices_on_user_id", using: :btree
    t.index ["vendor_id"], name: "index_customer_invoices_on_vendor_id", using: :btree
  end

  create_table "inbound_shipment_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "inbound_shipment_id"
    t.string   "seller_sku"
    t.integer  "quantity_shipped",    default: 0, null: false
    t.integer  "quantity_in_case",    default: 0, null: false
    t.integer  "quantity_received",   default: 0, null: false
    t.date     "expired_date"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "pending_po_id"
    t.index ["inbound_shipment_id"], name: "index_inbound_shipment_items_on_inbound_shipment_id", using: :btree
    t.index ["pending_po_id"], name: "index_inbound_shipment_items_on_pending_po_id", using: :btree
    t.index ["seller_sku"], name: "index_inbound_shipment_items_on_seller_sku", using: :btree
  end

  create_table "inbound_shipments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "postal_code"
    t.string   "name"
    t.string   "country_code"
    t.string   "state_or_province_code"
    t.string   "address_line2"
    t.string   "address_line1"
    t.string   "city"
    t.boolean  "are_cases_required",                                                    default: false
    t.string   "shipment_id",                                                                               null: false
    t.string   "shipment_name"
    t.string   "destination_fulfillment_center_id"
    t.string   "label_prep_type"
    t.date     "checked_date"
    t.decimal  "partnered_estimate",                           precision: 64, scale: 2, default: "0.0"
    t.integer  "user_id"
    t.datetime "created_at",                                                                                null: false
    t.datetime "updated_at",                                                                                null: false
    t.string   "shipment_status",                                                       default: "WORKING"
    t.string   "invoice_name",                                                          default: ""
    t.boolean  "invoice_status",                                                        default: false
    t.float    "mix_label_charges",                 limit: 24,                          default: 0.0
    t.index ["shipment_id"], name: "index_inbound_shipments_on_shipment_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_inbound_shipments_on_user_id", using: :btree
  end

  create_table "inbound_shipplan_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "outbound_shipment_id"
    t.integer  "inbound_shipplan_id"
    t.integer  "box_qty",              default: 0, null: false
    t.integer  "case_qty",             default: 0, null: false
    t.integer  "ship_qty",             default: 0, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["inbound_shipplan_id"], name: "index_inbound_shipplan_items_on_inbound_shipplan_id", using: :btree
    t.index ["outbound_shipment_id"], name: "index_inbound_shipplan_items_on_outbound_shipment_id", using: :btree
  end

  create_table "inbound_shipplans", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "postal_code"
    t.string   "name"
    t.string   "country_code"
    t.string   "state_or_province_code"
    t.string   "address_line2"
    t.string   "address_line1"
    t.string   "city"
    t.boolean  "are_cases_required",                                         default: false
    t.string   "shipplan_id",                                                                 null: false
    t.string   "shipment_name"
    t.string   "destination_fulfillment_center_id"
    t.string   "label_prep_type"
    t.date     "checked_date"
    t.decimal  "partnered_estimate",                precision: 64, scale: 2, default: "0.0"
    t.string   "shipplan_status",                                            default: "PLAN"
    t.integer  "user_id"
    t.datetime "created_at",                                                                  null: false
    t.datetime "updated_at",                                                                  null: false
    t.index ["shipplan_id"], name: "index_inbound_shipplans_on_shipplan_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_inbound_shipplans_on_user_id", using: :btree
  end

  create_table "inventory_histories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float    "price",                      limit: 24
    t.integer  "total_quantity"
    t.integer  "fulfillable_quantity"
    t.integer  "unsellable_quantity"
    t.integer  "reserved_quantity"
    t.integer  "per_unit_volume"
    t.integer  "inbound_working_quantity"
    t.integer  "inbound_shipped_quantity"
    t.integer  "inbound_receiving_quantity"
    t.integer  "seller_sku_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["seller_sku_id"], name: "index_inventory_histories_on_seller_sku_id", using: :btree
  end

  create_table "invoice_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "invoice_id"
    t.integer  "pending_po_id"
    t.integer  "quantity",                               default: 0,     null: false
    t.decimal  "cost",          precision: 11, scale: 2, default: "0.0"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.index ["invoice_id"], name: "index_invoice_items_on_invoice_id", using: :btree
    t.index ["pending_po_id"], name: "index_invoice_items_on_pending_po_id", using: :btree
  end

  create_table "invoices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "invoice_name"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.integer  "ship_term",                                  default: 0
    t.decimal  "shipping_cost",     precision: 11, scale: 2, default: "0.0"
    t.decimal  "adjustment_cost",   precision: 11, scale: 2, default: "0.0"
    t.string   "adjustment_reason",                          default: ""
    t.date     "check_date"
    t.boolean  "cleared",                                    default: false
    t.boolean  "printed",                                    default: false
    t.string   "check_number",                               default: ""
    t.boolean  "reported",                                   default: false
    t.index ["user_id"], name: "index_invoices_on_user_id", using: :btree
  end

  create_table "mws_order_items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "product_name"
    t.string   "sku"
    t.string   "asin"
    t.integer  "number_of_items"
    t.string   "item_status"
    t.integer  "quantity"
    t.string   "currency"
    t.decimal  "item_price",              precision: 10, scale: 2
    t.decimal  "item_tax",                precision: 10, scale: 2
    t.decimal  "shipping_price",          precision: 10, scale: 2
    t.decimal  "shipping_tax",            precision: 10, scale: 2
    t.decimal  "gift_wrap_price",         precision: 10, scale: 2
    t.decimal  "gift_wrap_tax",           precision: 10, scale: 2
    t.decimal  "item_promotion_discount", precision: 10, scale: 2
    t.decimal  "ship_promotion_discount", precision: 10, scale: 2
    t.string   "promotion_ids"
    t.boolean  "is_business_order"
    t.string   "purchase_order_number"
    t.decimal  "price_designation",       precision: 10, scale: 2
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "mws_order_id"
    t.index ["asin"], name: "index_mws_order_items_on_asin", using: :btree
    t.index ["mws_order_id"], name: "index_mws_order_items_on_mws_order_id", using: :btree
    t.index ["sku"], name: "index_mws_order_items_on_sku", using: :btree
  end

  create_table "mws_orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "amazon_order_id"
    t.string   "merchant_order_id"
    t.datetime "purchase_date"
    t.datetime "last_updated_date"
    t.string   "order_status"
    t.string   "fulfillment_channel"
    t.string   "sales_channel"
    t.string   "order_channel"
    t.string   "url"
    t.string   "ship_service_level"
    t.string   "address_type"
    t.string   "ship_city"
    t.string   "ship_postal_code"
    t.string   "ship_state"
    t.string   "ship_country"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "user_id"
    t.index ["user_id", "amazon_order_id"], name: "index_mws_orders_on_user_id_and_amazon_order_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_mws_orders_on_user_id", using: :btree
  end

  create_table "mws_report_requests", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "report_request_id"
    t.string   "report_id"
    t.string   "report_type"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "report_processing_status"
    t.string   "report_read_status"
    t.integer  "attempt"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "user_id"
    t.integer  "user_market_place_id"
    t.index ["user_id"], name: "index_mws_report_requests_on_user_id", using: :btree
    t.index ["user_market_place_id"], name: "index_mws_report_requests_on_user_market_place_id", using: :btree
  end

  create_table "out_stages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "box_qty"
    t.integer  "case_per_box"
    t.date     "expired_date"
    t.date     "received_date"
    t.string   "status",        default: "RECEIVED"
    t.integer  "pending_po_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["pending_po_id"], name: "index_out_stages_on_pending_po_id", using: :btree
  end

  create_table "outbound_shipments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "box_qty",        default: 1
    t.integer  "show_case_qty",  default: 0
    t.integer  "case_qty",       default: 1
    t.date     "expired_date"
    t.date     "received_date"
    t.boolean  "case",           default: true
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "pending_po_id"
    t.boolean  "bagger",         default: false,      null: false
    t.string   "stage_status",   default: "RECEIVED"
    t.string   "shipment_id"
    t.integer  "boxed_quantity", default: 0
    t.index ["pending_po_id"], name: "index_outbound_shipments_on_pending_po_id", using: :btree
  end

  create_table "package_informations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "asin"
    t.integer  "quantity"
    t.date     "expiry_date"
    t.integer  "package_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "box_id"
  end

  create_table "packages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "dimenstion_unit"
    t.float    "length",               limit: 24
    t.float    "width",                limit: 24
    t.float    "height",               limit: 24
    t.string   "weight_unit"
    t.float    "weight",               limit: 24
    t.integer  "transport_content_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "palletes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "dimension_unit"
    t.float    "length",               limit: 24
    t.float    "width",                limit: 24
    t.float    "height",               limit: 24
    t.string   "weight_unit"
    t.float    "weight",               limit: 24
    t.boolean  "is_stacked",                      default: false
    t.integer  "transport_content_id"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  create_table "pending_po_indices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "po_index"
    t.string   "po_name"
    t.integer  "vendor_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "pending_status", default: 0
    t.index ["vendor_id"], name: "index_pending_po_indices_on_vendor_id", using: :btree
  end

  create_table "pending_pos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "order_quantity",                                         default: 0
    t.integer  "received_quantity",                                      default: 0
    t.integer  "pending_po_index_id"
    t.integer  "vendoritem_id"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.decimal  "cost",                         precision: 64, scale: 12, default: "0.0"
    t.integer  "damaged_qty",                                            default: 0
    t.integer  "mispick_qty",                                            default: 0
    t.integer  "customer_invoice_id"
    t.integer  "confirmed_qty",                                          default: 0
    t.integer  "paid_qty",                                               default: 0
    t.integer  "invoice_id"
    t.decimal  "warehouse_paid",               precision: 11, scale: 2,  default: "0.0"
    t.integer  "warehouse_invoiced_qty",                                 default: 0
    t.decimal  "warehouse_invoiced_unit_cost", precision: 11, scale: 2,  default: "0.0"
    t.datetime "status_date"
    t.integer  "status",                                                 default: 0
    t.index ["customer_invoice_id"], name: "index_pending_pos_on_customer_invoice_id", using: :btree
    t.index ["invoice_id"], name: "index_pending_pos_on_invoice_id", using: :btree
    t.index ["pending_po_index_id"], name: "index_pending_pos_on_pending_po_index_id", using: :btree
    t.index ["vendoritem_id"], name: "index_pending_pos_on_vendoritem_id", using: :btree
  end

  create_table "po_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "po_status"
  end

  create_table "pos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "order_quantity", default: 1
    t.integer  "vendoritem_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "received_qty",   default: 0
    t.integer  "po_status_id",   default: 0
    t.index ["po_status_id"], name: "index_pos_on_po_status_id", using: :btree
    t.index ["vendoritem_id"], name: "index_pos_on_vendoritem_id", using: :btree
  end

  create_table "product_groups", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.index ["name"], name: "index_product_groups_on_name", unique: true, using: :btree
  end

  create_table "product_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.index ["name"], name: "index_product_types_on_name", unique: true, using: :btree
  end

  create_table "pull_inventories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "inputfileurl"
    t.string   "inputfilename"
    t.integer  "idcolumn",          default: -1
    t.integer  "costcolumn",        default: -1
    t.integer  "skucolumn",         default: -1
    t.boolean  "getestimatesales",  default: true
    t.boolean  "getisamazonsale",   default: true
    t.integer  "user_id"
    t.integer  "vendor_id"
    t.integer  "skynet_id_type_id"
    t.integer  "skynet_status_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["inputfileurl"], name: "index_pull_inventories_on_inputfileurl", unique: true, using: :btree
    t.index ["skynet_id_type_id"], name: "index_pull_inventories_on_skynet_id_type_id", using: :btree
    t.index ["skynet_status_id"], name: "index_pull_inventories_on_skynet_status_id", using: :btree
    t.index ["user_id"], name: "index_pull_inventories_on_user_id", using: :btree
    t.index ["vendor_id"], name: "index_pull_inventories_on_vendor_id", using: :btree
  end

  create_table "raked_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.index ["name"], name: "index_raked_categories_on_name", unique: true, using: :btree
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.index ["name"], name: "index_roles_on_name", unique: true, using: :btree
  end

  create_table "seller_skus", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "sku"
    t.decimal  "price",                                 precision: 64, scale: 2
    t.string   "fnsku"
    t.string   "inbound_destination"
    t.string   "prepinstruction"
    t.string   "age"
    t.integer  "age_0_90",                                                       default: 0
    t.integer  "age_91_180",                                                     default: 0
    t.integer  "age_181_270",                                                    default: 0
    t.integer  "age_271_360",                                                    default: 0
    t.integer  "age_365_plus",                                                   default: 0
    t.boolean  "snl",                                                            default: false
    t.integer  "last1daysales",                                                  default: 0
    t.integer  "last7daysales",                                                  default: 0
    t.integer  "last30daysales",                                                 default: 0
    t.integer  "last60daysales",                                                 default: 0
    t.integer  "last90daysales",                                                 default: 0
    t.integer  "last360daysales",                                                default: 0
    t.integer  "item_quantity",                                                  default: 0
    t.integer  "inbound_quantity",                                               default: 0
    t.integer  "reserved_quantity",                                              default: 0
    t.integer  "reserved_fc_processing",                                         default: 0
    t.integer  "reserved_fc_transfers",                                          default: 0
    t.integer  "reserved_customerorders",                                        default: 0
    t.date     "instock_date"
    t.date     "outstock_date"
    t.integer  "days_instock"
    t.integer  "vendorasin_id"
    t.integer  "user_id"
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.text     "item_name",               limit: 65535
    t.text     "item_description",        limit: 65535
    t.date     "open_date"
    t.string   "image_url"
    t.string   "product_id_type"
    t.text     "item_note",               limit: 65535
    t.string   "item_condition"
    t.string   "fulfillment_channel"
    t.string   "listing_id"
    t.boolean  "is_invalid",                                                     default: false, null: false
    t.integer  "bagger_number",                                                  default: 0
    t.string   "sku_status",                                                     default: ""
    t.float    "sales_velocity",          limit: 24
    t.index ["user_id", "sku"], name: "index_seller_skus_on_user_id_and_sku", unique: true, using: :btree
    t.index ["user_id"], name: "index_seller_skus_on_user_id", using: :btree
    t.index ["vendorasin_id"], name: "index_seller_skus_on_vendorasin_id", using: :btree
  end

  create_table "ship_cus_invoices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "inbound_shipment_id"
    t.string   "invoice_name",                               null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.datetime "paid_date"
    t.float    "box_charges",         limit: 24
    t.integer  "new_box_count",                  default: 0
    t.index ["inbound_shipment_id"], name: "index_ship_cus_invoices_on_inbound_shipment_id", using: :btree
  end

  create_table "ship_cust_invoices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "inbound_shipment_id"
    t.string   "invoice_name",        null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["inbound_shipment_id"], name: "index_ship_cust_invoices_on_inbound_shipment_id", using: :btree
  end

  create_table "skynet_id_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
  end

  create_table "skynet_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.index ["name"], name: "index_skynet_statuses_on_name", unique: true, using: :btree
  end

  create_table "skynets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "task_id"
    t.string   "inputfileurl"
    t.string   "inputfilename"
    t.string   "idheadername",      default: "ID"
    t.string   "costheadername",    default: "COST"
    t.string   "skuheadername",     default: "SKU"
    t.boolean  "getestimatesales",  default: true
    t.boolean  "getisamazonsale",   default: true
    t.string   "webhookprogress"
    t.integer  "user_id"
    t.integer  "vendor_id"
    t.integer  "skynet_id_type_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "outputfileurl"
    t.string   "statusmessage"
    t.integer  "skynet_status_id"
    t.integer  "percent",           default: 0
    t.index ["inputfileurl"], name: "index_skynets_on_inputfileurl", unique: true, using: :btree
    t.index ["skynet_id_type_id"], name: "index_skynets_on_skynet_id_type_id", using: :btree
    t.index ["skynet_status_id"], name: "index_skynets_on_skynet_status_id", using: :btree
    t.index ["task_id"], name: "index_skynets_on_task_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_skynets_on_user_id", using: :btree
    t.index ["vendor_id"], name: "index_skynets_on_vendor_id", using: :btree
  end

  create_table "subscriptions", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "subscriptionid"
    t.integer "amount"
    t.string  "interval"
    t.integer "created"
    t.index ["subscriptionid"], name: "index_subscriptions_on_subscriptionid", unique: true, using: :btree
  end

  create_table "trackings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "tracking_number"
    t.integer  "transport_content_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "transport_contents", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "amazon_shipment_id"
    t.string   "shipment_type"
    t.string   "name"
    t.integer  "outbound_shipments_id"
    t.boolean  "delivery_by_amazon",                default: true
    t.string   "carrier"
    t.string   "tracking_id"
    t.string   "email"
    t.string   "phone"
    t.string   "fax"
    t.string   "box_count"
    t.string   "frieght_class"
    t.date     "frieght_read_date"
    t.float    "total_weight",           limit: 24
    t.float    "declared_value",         limit: 24
    t.string   "pro_number"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.boolean  "uploaded_to_amazon",                default: false
    t.string   "status"
    t.float    "amazon_estimate",        limit: 24
    t.integer  "inbound_shipment_id"
    t.boolean  "bill_of_lading_enabled",            default: false
    t.integer  "pallete_count",                     default: 0
    t.datetime "void_deadline"
    t.float    "shipping_fee",           limit: 24
  end

  create_table "user_market_places", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_user_market_places_on_user_id", using: :btree
  end

  create_table "user_table_sessions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "table_name"
    t.text     "state",      limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["user_id", "table_name"], name: "index_user_table_sessions_on_user_id_and_table_name", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_table_sessions_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                            default: "",              null: false
    t.string   "encrypted_password",               default: "",              null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,               null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.boolean  "active",                           default: true
    t.integer  "role_id"
    t.integer  "company_id"
    t.string   "subscription_id"
    t.string   "mws_access_key_id"
    t.string   "mws_access_acces_key"
    t.string   "seller_id"
    t.string   "mws_access_token"
    t.string   "aws_api_key"
    t.string   "aws_api_secret_key"
    t.string   "aws_associate_tag"
    t.boolean  "mws_key_valid",                    default: false
    t.string   "mws_market_place_id",              default: "ATVPDKIKX0DER"
    t.string   "first_name",                       default: "",              null: false
    t.string   "last_name",                        default: "",              null: false
    t.string   "sku_prefix",                       default: "",              null: false
    t.string   "warehouse_name"
    t.string   "warehouse_address_line1"
    t.string   "warehouse_address_line2"
    t.string   "warehouse_city"
    t.string   "warehouse_state_or_province_code"
    t.string   "warehouse_postal_code"
    t.string   "warehouse_country_code"
    t.string   "skynet_key",                       default: ""
    t.boolean  "skynet_api_enabled",               default: false
    t.string   "skynet_api_user_id"
    t.index ["company_id"], name: "index_users_on_company_id", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["role_id"], name: "index_users_on_role_id", using: :btree
  end

  create_table "vendor_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.index ["name"], name: "index_vendor_categories_on_name", unique: true, using: :btree
  end

  create_table "vendorasins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "asin"
    t.integer  "brand_id"
    t.text     "name",                     limit: 65535
    t.integer  "salesrank"
    t.integer  "packagequantity",                                                 default: 1
    t.decimal  "buyboxprice",                            precision: 64, scale: 2, default: "0.0"
    t.decimal  "referenceoffer",                         precision: 64, scale: 2
    t.string   "referenceoffertype"
    t.decimal  "fbafee",                                 precision: 64, scale: 2, default: "0.0"
    t.decimal  "storagefee",                             precision: 64, scale: 2, default: "0.0"
    t.decimal  "variableclosingfee",                     precision: 64, scale: 2, default: "0.0"
    t.decimal  "commissionpct",                          precision: 10,           default: 0
    t.decimal  "commissiionfee",                         precision: 64, scale: 2, default: "0.0"
    t.decimal  "inboundshipping",                        precision: 64, scale: 2
    t.decimal  "salespermonth",                          precision: 10,           default: 0
    t.integer  "totaloffers",                                                     default: 0
    t.integer  "fbaoffers",                                                       default: 0
    t.integer  "fbmoffers",                                                       default: 0
    t.decimal  "lowestfbaoffer",                         precision: 64, scale: 2, default: "0.0"
    t.decimal  "lowestfbmoffer",                         precision: 64, scale: 2, default: "0.0"
    t.boolean  "isbuyboxfba",                                                     default: false
    t.integer  "product_type_id"
    t.integer  "ranked_category_id"
    t.decimal  "weight",                                 precision: 64, scale: 2
    t.decimal  "length",                                 precision: 64, scale: 2
    t.decimal  "width",                                  precision: 64, scale: 2
    t.decimal  "height",                                 precision: 64, scale: 2
    t.datetime "created_at",                                                                      null: false
    t.datetime "updated_at",                                                                      null: false
    t.integer  "product_groups_id"
    t.decimal  "packagewidth",                           precision: 64, scale: 2
    t.decimal  "packageheight",                          precision: 64, scale: 2
    t.decimal  "packagelength",                          precision: 64, scale: 2
    t.decimal  "packageweight",                          precision: 64, scale: 2
    t.text     "notes",                    limit: 65535
    t.decimal  "review",                                 precision: 10,           default: 0
    t.integer  "numreview",                                                       default: 0
    t.boolean  "amazonoffer",                                                     default: false
    t.decimal  "totalfbafee",                            precision: 64, scale: 2, default: "0.0"
    t.string   "mpn",                                                             default: ""
    t.string   "ean",                                                             default: ""
    t.string   "upc",                                                             default: ""
    t.boolean  "invalidid",                                                       default: false
    t.integer  "fetched",                                                         default: 1
    t.boolean  "amazon_selling",                                                  default: false
    t.string   "bbp_seller"
    t.string   "small_image"
    t.string   "medium_image"
    t.string   "large_image"
    t.string   "manufacturer"
    t.integer  "manufacturer_minimum_age"
    t.string   "inner_category"
    t.string   "inner_category_id"
    t.string   "outer_category"
    t.string   "outer_category_id"
    t.index ["asin"], name: "index_vendorasins_on_asin", unique: true, using: :btree
    t.index ["brand_id"], name: "index_vendorasins_on_brand_id", using: :btree
    t.index ["product_groups_id"], name: "index_vendorasins_on_product_groups_id", using: :btree
    t.index ["product_type_id"], name: "index_vendorasins_on_product_type_id", using: :btree
    t.index ["ranked_category_id"], name: "index_vendorasins_on_ranked_category_id", using: :btree
  end

  create_table "vendoritems", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.decimal  "cost",                          precision: 64, scale: 2, default: "0.0", null: false
    t.text     "vendortitle",     limit: 65535
    t.integer  "vendorasin_id"
    t.integer  "vendor_id"
    t.string   "asin"
    t.string   "upc"
    t.string   "isbn"
    t.string   "ean"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.string   "vendorsku",                                              default: ""
    t.integer  "packcount",                                              default: 1
    t.integer  "seller_sku_id"
    t.string   "pre_defined_sku"
    t.decimal  "min_price",                     precision: 10, scale: 2, default: "0.0"
    t.decimal  "max_price",                     precision: 10, scale: 2, default: "0.0"
    t.index ["seller_sku_id"], name: "index_vendoritems_on_seller_sku_id", using: :btree
    t.index ["vendor_id"], name: "index_vendoritems_on_vendor_id", using: :btree
    t.index ["vendorasin_id"], name: "index_vendoritems_on_vendorasin_id", using: :btree
  end

  create_table "vendors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "name"
    t.string  "addressline1"
    t.string  "addressline2"
    t.string  "city"
    t.string  "zipcode"
    t.string  "state"
    t.string  "account_number"
    t.string  "contact"
    t.string  "title"
    t.string  "phone"
    t.string  "email"
    t.string  "website"
    t.boolean "dropship"
    t.boolean "crossdock"
    t.string  "login"
    t.string  "password"
    t.string  "ref_name"
    t.string  "ref_title"
    t.string  "ref_phone"
    t.string  "ref_email"
    t.string  "ref_fax"
    t.string  "ship_fba"
    t.string  "ship_ltl"
    t.string  "sticker_unit"
    t.string  "ship_tier"
    t.string  "tier_price"
    t.string  "cc_tran"
    t.integer "leadtime",           default: 14
    t.integer "user_id"
    t.integer "vendor_category_id"
    t.boolean "inactive",           default: false
    t.string  "supplier_prefix",    default: ""
    t.index ["name"], name: "index_vendors_on_name", unique: true, using: :btree
    t.index ["user_id"], name: "index_vendors_on_user_id", using: :btree
    t.index ["vendor_category_id"], name: "index_vendors_on_vendor_category_id", using: :btree
  end

  create_table "warehouse_vendors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "supplier_code"
    t.string   "day_of_shipping"
    t.decimal  "minimum_order",        precision: 15, scale: 2, default: "0.0"
    t.string   "vendor_phone"
    t.string   "account_number"
    t.integer  "account_holder"
    t.string   "contact_name"
    t.string   "contact_email"
    t.string   "contact_phone_number"
    t.string   "website"
    t.string   "user_name"
    t.string   "password"
    t.integer  "payment_terms"
    t.integer  "shipment_terms"
    t.string   "catalog"
    t.integer  "user_id"
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.index ["name"], name: "index_warehouse_vendors_on_name", unique: true, using: :btree
    t.index ["user_id"], name: "index_warehouse_vendors_on_user_id", using: :btree
  end

end
