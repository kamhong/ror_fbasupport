class AddInboundShipmentToTransportContent < ActiveRecord::Migration[5.0]
  def change
    add_column :transport_contents, :inbound_shipment_id, :integer
  end
end
