class AddInvoiceStatusToInboundShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :inbound_shipments, :invoice_status, :boolean, :default => false
  end
end
