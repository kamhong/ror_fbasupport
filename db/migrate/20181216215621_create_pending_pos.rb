class CreatePendingPos < ActiveRecord::Migration[5.0]
  def change
    create_table :pending_pos do |t|
      t.integer :order_quantity, default: 0
      t.integer :received_quantity, default: 0
      
      t.references :pending_po_index, index: true
      t.references :vendoritem, index: true
      t.timestamps
    end
  end
end
