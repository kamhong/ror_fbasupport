class CreateUserMarketPlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :user_market_places do |t|
      t.references :users, index: true
      t.timestamps
    end
  end
end
