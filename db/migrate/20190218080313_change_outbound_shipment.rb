class ChangeOutboundShipment < ActiveRecord::Migration[5.0]
  def change
    remove_column :outbound_shipments, :vendoritem_id
    add_reference :outbound_shipments, :pending_po, index: true
  end
end
