class CreatePendingPoIndices < ActiveRecord::Migration[5.0]
  def change
    create_table :pending_po_indices do |t|
      t.integer  :po_index
      t.string  :po_name
      
      t.references :vendor, index: true
      t.references :po_status, index: true, default: 1
    end
  end
end
