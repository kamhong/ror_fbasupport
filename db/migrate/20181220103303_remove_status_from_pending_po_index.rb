class RemoveStatusFromPendingPoIndex < ActiveRecord::Migration[5.0]
  def up
    remove_column :pending_po_indices, :po_status_id
    add_column  :pending_po_indices, :pending_status, :string, default: "PENDING"
  end
  def down
    add_reference :pending_po_indices, :po_status, index: true
    remove_column :pending_po_indices, :pending_status
  end
end
