class AddColumnToWarehouseInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :ship_term, :integer, :default=>0
    add_column :invoices, :shipping_cost, :decimal, :precision=>11, :scale=>2, :default=>0
    add_column :invoices, :adjustment_cost, :decimal, :precision=>11, :scale=>2, :default=>0
    add_column :invoices, :adjustment_reason, :string, :default=>""
    add_column :invoices, :check_date, :date
    add_column :invoices, :cleared, :boolean, :default=>false
    add_column :invoices, :printed, :boolean, :default=>false
    add_column :invoices, :check_number, :string, :default=>""
    add_column :invoices, :reported, :boolean, :default=>false
  end
end
