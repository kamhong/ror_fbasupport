class AddSkynetPercentToSkynet < ActiveRecord::Migration[5.0]
  def change
    add_column :skynets, :percent, :integer, :default => 0
  end
end
