class AddStatusToOutbound < ActiveRecord::Migration[5.0]
  def change
    add_column :outbound_shipments, :stage_status, :string, :default => "RECEIVED"
  end
end
