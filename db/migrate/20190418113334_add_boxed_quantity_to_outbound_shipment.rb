class AddBoxedQuantityToOutboundShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :outbound_shipments, :boxed_quantity, :integer, default: 0
  end
end
