class AddShipFromAddressToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :warehouse_name, :string
    add_column :users, :warehouse_address_line1, :string
    add_column :users, :warehouse_address_line2, :string
    add_column :users, :warehouse_city, :string
    add_column :users, :warehouse_state_or_province_code, :string
    add_column :users, :warehouse_postal_code, :string
    add_column :users, :warehouse_country_code, :string
  end
end
