class ChangeSellerSkuIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :seller_skus, :sku
    add_index :seller_skus, [:user_id, :sku], unique: true
  end
end
