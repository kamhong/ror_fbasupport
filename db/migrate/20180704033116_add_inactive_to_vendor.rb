class AddInactiveToVendor < ActiveRecord::Migration[5.0]
  def change
  	add_column :vendors, :inactive, :boolean, default: false
  end
end
