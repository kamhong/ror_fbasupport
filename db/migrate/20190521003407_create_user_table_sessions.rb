class CreateUserTableSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :user_table_sessions do |t|
      t.belongs_to :user, index: true
      t.string  :table_name
      t.text  :state
      t.timestamps
    end
    add_index :user_table_sessions, [:user_id, :table_name], unique: true
  end
end
