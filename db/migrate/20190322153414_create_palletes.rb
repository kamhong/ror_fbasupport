class CreatePalletes < ActiveRecord::Migration[5.0]
  def change
    create_table :palletes do |t|
      t.string  :dimension_unit
      t.float   :length
      t.float   :width
      t.float   :height
      t.string  :weight_unit
      t.float   :weight
      t.boolean :is_stacked, default: false
      t.integer :transport_content_id

      t.timestamps
    end
  end
end
