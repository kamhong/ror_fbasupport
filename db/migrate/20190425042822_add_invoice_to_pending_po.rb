class AddInvoiceToPendingPo < ActiveRecord::Migration[5.0]
  def change
    add_reference :pending_pos, :invoice, index: true
  end
end
