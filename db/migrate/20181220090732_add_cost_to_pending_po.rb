class AddCostToPendingPo < ActiveRecord::Migration[5.0]
  def change
    add_column :pending_pos, :cost, :decimal, :precision=>64, :scale=>12, default: 0
  end
end
