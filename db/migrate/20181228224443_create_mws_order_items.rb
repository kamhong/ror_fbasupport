class CreateMwsOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :mws_order_items do |t|
      t.string  :amazon_order_id
      t.string  :product_name
      t.string  :sku
      t.string  :asin
      t.integer  :number_of_items
      t.string  :item_status
      t.integer  :quantity
      t.string  :currency
      t.decimal  :item_price, precision: 10, scale: 2
      t.decimal  :item_tax, precision: 10, scale: 2
      t.decimal  :shipping_price, precision: 10, scale: 2
      t.decimal  :shipping_tax, precision: 10, scale: 2
      t.decimal  :gift_wrap_price, precision: 10, scale: 2
      t.decimal  :gift_wrap_tax, precision: 10, scale: 2
      t.decimal  :item_promotion_discount, precision: 10, scale: 2
      t.decimal  :ship_promotion_discount, precision: 10, scale: 2
      t.string  :promotion_ids
      t.boolean  :is_business_order
      t.string  :purchase_order_number
      t.decimal  :price_designation, precision: 10, scale: 2


      t.timestamps
    end
    add_index :mws_order_items, :amazon_order_id
    add_index :mws_order_items, :sku
    add_index :mws_order_items, :asin
  end
end
