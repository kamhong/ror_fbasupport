class AdStatusAndDateToPendingPoItem < ActiveRecord::Migration[5.0]
  def change
    remove_column :pending_pos, :po_status_id

    add_column :pending_pos, :status_date, :datetime
    add_column :pending_pos, :status, :integer, :default => 0
  end
end
