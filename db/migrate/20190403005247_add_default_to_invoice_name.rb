class AddDefaultToInvoiceName < ActiveRecord::Migration[5.0]
  def change
    change_column :customer_invoices, :invoice_name, :string, default: ""
  end
end
