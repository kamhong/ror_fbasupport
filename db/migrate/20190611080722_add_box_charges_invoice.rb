class AddBoxChargesInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :ship_cus_invoices, :box_charges, :float
  end
end
