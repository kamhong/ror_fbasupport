class CreatePackageInformations < ActiveRecord::Migration[5.0]
  def change
    create_table :package_informations do |t|
      t.string    :asin
      t.integer   :quantity
      t.date      :expiry_date
      t.integer   :package_id
      t.timestamps
    end
  end
end
