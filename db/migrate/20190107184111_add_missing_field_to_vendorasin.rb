class AddMissingFieldToVendorasin < ActiveRecord::Migration[5.0]
  def change
    add_column :vendorasins, :small_image, :string
    add_column :vendorasins, :medium_image, :string
    add_column :vendorasins, :large_image, :string
    add_column :vendorasins, :manufacturer, :string
    add_column :vendorasins, :manufacturer_minimum_age, :integer
  end
end
