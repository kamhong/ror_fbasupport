class ChangeDefaultOfSellerSku < ActiveRecord::Migration[5.0]
  def change
    change_column_default(:seller_skus, :age_0_90, 0)
    change_column_default(:seller_skus, :age_91_180, 0)
    change_column_default(:seller_skus, :age_181_270, 0)
    change_column_default(:seller_skus, :age_271_360, 0)
    change_column_default(:seller_skus, :age_365_plus, 0)
    change_column_default(:seller_skus, :last1daysales, 0)
    change_column_default(:seller_skus, :last7daysales, 0)
    change_column_default(:seller_skus, :last30daysales, 0)
    change_column_default(:seller_skus, :last60daysales, 0)
    change_column_default(:seller_skus, :last90daysales, 0)
    change_column_default(:seller_skus, :last360daysales, 0)
    change_column_default(:seller_skus, :item_quantity, 0)
    change_column_default(:seller_skus, :inbound_quantity, 0)
    change_column_default(:seller_skus, :reserved_quantity, 0)
    change_column_default(:seller_skus, :reserved_fc_processing, 0)
    change_column_default(:seller_skus, :reserved_fc_transfers, 0)
    change_column_default(:seller_skus, :reserved_customerorders, 0)
    change_column_default(:seller_skus, :snl, false)

    change_column_default(:vendorasins, :packagequantity, 1)
    change_column_default(:vendorasins, :buyboxprice, 0)
    
  end
end
