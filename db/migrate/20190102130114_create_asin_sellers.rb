class CreateAsinSellers < ActiveRecord::Migration[5.0]
  def change
    create_table :asin_sellers do |t|
      t.string  :seller_id
      t.string  :sub_condition
      t.integer :seller_positive_feedback_rating
      t.integer :feedback_count
      t.decimal  :listing_price, :precision=>7, :scale=>2
      t.decimal  :shipping, :precision=>7, :scale=>2
      t.boolean :is_buy_box_winner
      t.boolean :is_fulfilled_by_amazon
      t.boolean :is_featured_merchant
      t.boolean :ships_domestically
      t.references :vendorasin, index: true
    end
    add_index :asin_sellers, :seller_id
  end
end
