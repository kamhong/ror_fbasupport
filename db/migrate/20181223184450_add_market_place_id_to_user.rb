class AddMarketPlaceIdToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :mws_market_place_id, :string
  end
end
