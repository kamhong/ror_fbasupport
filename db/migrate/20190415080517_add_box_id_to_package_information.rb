class AddBoxIdToPackageInformation < ActiveRecord::Migration[5.0]
  def change
    add_column :package_informations, :box_id, :integer
  end
end
