class AddDamageAndMisPickToPendingPo < ActiveRecord::Migration[5.0]
  def change
    add_column :pending_pos, :damaged_qty, :integer, default: 0
    add_column :pending_pos, :mispick_qty, :integer, default: 0
  end
end
