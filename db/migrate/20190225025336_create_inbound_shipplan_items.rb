class CreateInboundShipplanItems < ActiveRecord::Migration[5.0]
  def change
    create_table :inbound_shipplan_items do |t|
      t.references :outbound_shipment, index: true
      t.references :inbound_shipplan, index: true
      
      t.integer :box_qty, default: 0, null: false
      t.integer :case_qty, default: 0, null: false
      t.integer :ship_qty, default: 0, null: false
      t.timestamps
    end


  end
end
