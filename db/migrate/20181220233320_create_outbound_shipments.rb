class CreateOutboundShipments < ActiveRecord::Migration[5.0]
  def change
    create_table :outbound_shipments do |t|
      t.integer :box_qty, default: 1
      t.integer :show_case_qty, default: 0
      t.integer :case_qty, default: 1
      t.date  :expired_date, :null => :true
      t.date  :received_date
      t.boolean :case, default: true
      t.references  :vendoritem, index: true
      t.timestamps
    end
  end
end
