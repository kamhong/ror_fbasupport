class AddAmazonSellingToVendorAsin < ActiveRecord::Migration[5.0]
  def change
    add_column :vendorasins, :amazon_selling, :boolean, default: false
  end
end
