class AddPrecisionToDecimal < ActiveRecord::Migration[5.0]
  def change
  	change_column :vendorasins, :buyboxprice, :float, :precision=>64, :scale=>12
  end
end
