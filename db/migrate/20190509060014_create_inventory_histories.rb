class CreateInventoryHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :inventory_histories do |t|
      t.float     :price
      t.integer   :total_quantity
      t.integer   :fulfillable_quantity
      t.integer   :unsellable_quantity
      t.integer   :reserved_quantity
      t.integer   :per_unit_volume
      t.integer   :inbound_working_quantity
      t.integer   :inbound_shipped_quantity
      t.integer   :inbound_receiving_quantity
      
      t.references   :seller_sku

      t.timestamps
    end
  end
end
