class AddInvoiceSentToInboundShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :inbound_shipments, :invoice_sent, :boolean, default: false
  end
end
