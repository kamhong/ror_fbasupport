class CreateInvoiceItems < ActiveRecord::Migration[5.0]
  def change
    create_table :invoice_items do |t|
      t.references :invoice, index:true
      t.references :pending_po, index:true

      t.integer :quantity, default: 0, null: false
      t.decimal :cost, :precision=>11, :scale=>2, :default=>0
      t.timestamps
    end
  end
end
