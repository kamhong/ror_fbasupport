class CreateInboundShipplans < ActiveRecord::Migration[5.0]
  def change
    create_table :inbound_shipplans do |t|
      t.string :postal_code
      t.string :name
      t.string :country_code
      t.string :state_or_province_code
      t.string :address_line2
      t.string :address_line1
      t.string :city
      t.boolean :are_cases_required, default: false
      t.string :shipplan_id, null: false
      t.string :shipment_name
      t.string :destination_fulfillment_center_id
      t.string :label_prep_type
      t.date :checked_date
      t.decimal :partnered_estimate, :precision=>64, :scale=>2, default: 0
      t.string :shipplan_status, default: "PLAN"
      t.references :user, index: true

      t.timestamps
    end

    add_index :inbound_shipplans, :shipplan_id, unique: true
  end
end
