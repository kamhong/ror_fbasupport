class RemoveRefFromMwsReport < ActiveRecord::Migration[5.0]
  def up
    remove_column :mws_report_requests, :users_id
    remove_column :mws_report_requests, :user_market_places_id
    remove_column :user_market_places, :users_id

    add_reference :mws_report_requests, :user, index: true
    add_reference :mws_report_requests, :user_market_place, index: true
    add_reference :user_market_places, :user, index: true
  end
  def down
    add_reference :mws_report_requests, :users, index: true
    add_reference :mws_report_requests, :user_market_place, index: true
    add_reference :user_market_places, :users, index: true

    remove_column :mws_report_requests, :user_id
    remove_column :mws_report_requests, :user_market_place_id
    remove_column :user_market_places, :user_id
  end
end
