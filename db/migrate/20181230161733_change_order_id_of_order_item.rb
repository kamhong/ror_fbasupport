class ChangeOrderIdOfOrderItem < ActiveRecord::Migration[5.0]
  def change
    remove_column :mws_order_items, :amazon_order_id

    add_reference :mws_order_items, :mws_order_item, index: true
  end
end
