class AddBaggerNumberToSellerSku < ActiveRecord::Migration[5.0]
  def change
    add_column :seller_skus, :bagger_number, :integer, :default => 0
  end
end
