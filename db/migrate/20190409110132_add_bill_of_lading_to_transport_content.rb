class AddBillOfLadingToTransportContent < ActiveRecord::Migration[5.0]
  def change
    add_column :transport_contents, :bill_of_lading_enabled, :boolean, default: false
  end
end
