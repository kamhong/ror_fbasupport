class AddMinMaxPriceToVendorItem < ActiveRecord::Migration[5.0]
  def change
    add_column :vendoritems, :min_price, :decimal, :precision=>10, :scale=>2, :default => 0
    add_column :vendoritems, :max_price, :decimal, :precision=>10, :scale=>2, :default => 0
  end
end
