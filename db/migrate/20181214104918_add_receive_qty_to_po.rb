class AddReceiveQtyToPo < ActiveRecord::Migration[5.0]
    def change
      add_column :pos, :received_qty, :integer, default: 0
    end
end
