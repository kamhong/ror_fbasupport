class AddQtiesToPendingPo < ActiveRecord::Migration[5.0]
  def change
    add_column :pending_pos, :confirmed_qty, :integer, :default => 0
    add_column :pending_pos, :paid_qty, :integer, :default => 0
  end
end
