class ChangeNotNullCostOfVendorItem < ActiveRecord::Migration[5.0]
  def change
    change_column_null :vendoritems, :cost, false, 0
  end
end
