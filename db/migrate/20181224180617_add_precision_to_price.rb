class AddPrecisionToPrice < ActiveRecord::Migration[5.0]
  def change
    change_column :seller_skus, :price, :float, :precision=>64, :scale=>2
  end
end
