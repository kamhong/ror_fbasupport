class AddPendingPoIndexToCustomerInvoice < ActiveRecord::Migration[5.0]
  def change
    add_reference :customer_invoices, :pending_po_index, index: true
  end
end
