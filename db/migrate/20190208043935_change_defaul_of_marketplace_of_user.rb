class ChangeDefaulOfMarketplaceOfUser < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :mws_market_place_id, :string, :default => 'ATVPDKIKX0DER'
  end
end
