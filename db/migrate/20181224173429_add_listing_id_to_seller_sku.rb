class AddListingIdToSellerSku < ActiveRecord::Migration[5.0]
  def change
    add_column :seller_skus, :listing_id, :string
  end
end
