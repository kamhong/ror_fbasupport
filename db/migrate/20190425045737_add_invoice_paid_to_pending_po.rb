class AddInvoicePaidToPendingPo < ActiveRecord::Migration[5.0]
  def change
    add_column :pending_pos, :warehouse_paid, :decimal, :precision=>11, :scale=>2, :default=>0
  end
end
