class AddShippingFeeToTransportPlan < ActiveRecord::Migration[5.0]
  def change
    add_column :transport_contents, :shipping_fee, :float
  end
end
