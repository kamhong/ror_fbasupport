class AddIndexToAsinSeller < ActiveRecord::Migration[5.0]
  def change
    add_index :asin_sellers, :asin_str
  end
end
