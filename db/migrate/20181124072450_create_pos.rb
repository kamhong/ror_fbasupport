class CreatePos < ActiveRecord::Migration[5.0]
  def change
    create_table :pos do |t|
    	t.integer :order_quantity, default: 1
    	t.references :vendoritem, index: true
      
      t.references :po_status, index: true, default: 1
      t.timestamps
    end
  end
  
end
