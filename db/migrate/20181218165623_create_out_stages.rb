class CreateOutStages < ActiveRecord::Migration[5.0]
  def change
    create_table :out_stages do |t|
      t.integer :box_qty
      t.integer :case_per_box
      t.date  :expired_date
      t.date  :received_date
      t.string  :status, default: 'RECEIVED'
      t.references  :pending_po, index: true
      
      t.timestamps
    end
  end
end
