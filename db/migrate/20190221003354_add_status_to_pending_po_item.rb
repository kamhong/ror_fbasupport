class AddStatusToPendingPoItem < ActiveRecord::Migration[5.0]
  def change
    add_reference :pending_pos, :po_status, index: true, :default => 2
  end
end
