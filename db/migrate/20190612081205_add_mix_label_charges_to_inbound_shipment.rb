class AddMixLabelChargesToInboundShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :inbound_shipments, :mix_label_charges, :float, default: 0.0
  end
end
