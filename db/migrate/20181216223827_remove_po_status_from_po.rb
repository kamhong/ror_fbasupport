class RemovePoStatusFromPo < ActiveRecord::Migration[5.0]
  def up
    remove_column :pos, :po_status_id
  end
  def down
    add_reference :pos, :po_status, index: true
  end
end
