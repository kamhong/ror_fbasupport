class CreateTrackings < ActiveRecord::Migration[5.0]
  def change
    create_table :trackings do |t|
      t.string    :tracking_number
      t.integer   :transport_content_id
      t.timestamps
    end
  end
end
