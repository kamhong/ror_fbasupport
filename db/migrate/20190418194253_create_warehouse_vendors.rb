class CreateWarehouseVendors < ActiveRecord::Migration[5.0]
  def change
    create_table :warehouse_vendors do |t|
      t.string  :name
      t.string  :supplier_code  
      t.string  :day_of_shipping
      t.decimal :paid_amount, :default => 0.0, :precision=>15, :scale=>2
      t.string  :vendor_phone
      t.string  :account_number
      t.integer  :account_holder
      t.string  :contact_name
      t.string  :contact_email
      t.string  :contact_phone_number
      t.string  :website
      t.string  :user_name
      t.string  :password
      t.integer  :payment_terms
      t.integer :shipment_terms
      t.string  :catalog

      t.references :user, index: true
      t.timestamps
    end
    add_index :warehouse_vendors, :name, unique: true
  end
end
