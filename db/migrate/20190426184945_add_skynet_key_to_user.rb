class AddSkynetKeyToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :skynet_key, :string, :default=>""
  end
end
