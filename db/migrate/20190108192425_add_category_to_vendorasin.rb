class AddCategoryToVendorasin < ActiveRecord::Migration[5.0]
  def change
    add_column :vendorasins, :inner_category, :string
    add_column :vendorasins, :inner_category_id, :string
    add_column :vendorasins, :outer_category, :string
    add_column :vendorasins, :outer_category_id, :string
  end
end
