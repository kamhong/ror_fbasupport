class AddMwsValidToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :mws_key_valid, :boolean, default: false
  end
end
