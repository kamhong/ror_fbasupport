class ChangeWarehouseVendorColumn < ActiveRecord::Migration[5.0]
  def change
    rename_column :warehouse_vendors, :paid_amount, :minimum_order
  end
end
