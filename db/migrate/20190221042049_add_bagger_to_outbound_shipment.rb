class AddBaggerToOutboundShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :outbound_shipments, :bagger, :boolean, default: false, null: false
  end
end
