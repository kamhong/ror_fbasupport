class AddSalesVelocityToSellerSku < ActiveRecord::Migration[5.0]
  def change
    add_column :seller_skus, :sales_velocity, :float
  end
end
