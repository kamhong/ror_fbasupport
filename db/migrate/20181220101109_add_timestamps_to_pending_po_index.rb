class AddTimestampsToPendingPoIndex < ActiveRecord::Migration[5.0]
  def change
    # add new column but allow null values
    add_timestamps :pending_po_indices, null: true 

    # backfill existing record with created_at and updated_at
    # values making clear that the records are faked
    long_ago = DateTime.new(2000, 1, 1)
    PendingPoIndex.update_all(created_at: long_ago, updated_at: long_ago)

    # change not null constraints
    change_column_null :pending_po_indices, :created_at, false
    change_column_null :pending_po_indices, :updated_at, false
  end
end
