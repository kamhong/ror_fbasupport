class AddSuppplierPrefixToVendor < ActiveRecord::Migration[5.0]
  def change
    add_column :vendors, :supplier_prefix, :string, :default => ""
  end
end
