class CreateShipCusInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :ship_cus_invoices do |t|
      t.references :inbound_shipment, index:true
      t.string :invoice_name, null: false
      t.timestamps
    end
  end
end
