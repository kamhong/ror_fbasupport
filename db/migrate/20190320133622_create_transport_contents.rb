class CreateTransportContents < ActiveRecord::Migration[5.0]
  def change
    create_table :transport_contents do |t|
      t.string  :amazon_shipment_id
      t.string  :shipment_type
      t.string  :name
      t.integer :outbound_shipments_id
      t.boolean :delivery_by_amazon, default: true
      t.string  :carrier
      t.string  :tracking_id
      t.string  :email
      t.string  :phone
      t.string  :email
      t.string  :fax
      t.string  :box_count
      t.string  :frieght_class
      t.date    :frieght_read_date
      t.float   :total_weight
      t.float   :declared_value
      t.string  :pro_number
      t.timestamps
    end
  end
end
