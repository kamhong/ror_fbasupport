class ChangeOrderItems < ActiveRecord::Migration[5.0]
  def change
    remove_column :mws_order_items, :mws_order_item_id

    add_reference :mws_order_items, :mws_order, index: true
  end
end
