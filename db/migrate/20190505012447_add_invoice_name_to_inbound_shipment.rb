class AddInvoiceNameToInboundShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :inbound_shipments, :invoice_name, :string, :default=>""
  end
end
