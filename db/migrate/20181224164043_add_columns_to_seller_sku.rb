class AddColumnsToSellerSku < ActiveRecord::Migration[5.0]
  def change
    add_column  :vendorasins, :fetched, :integer, default: 1

    add_column :seller_skus, :item_name, :text
    add_column :seller_skus, :item_description, :text
    add_column :seller_skus, :open_date, :date
    add_column :seller_skus, :image_url, :string
    add_column :seller_skus, :product_id_type, :string
    add_column :seller_skus, :item_note, :text
    add_column :seller_skus, :item_condition, :string
    add_column :seller_skus, :fulfillment_channel, :string
  end
end
