class AddVendoInfoToShipmentItem < ActiveRecord::Migration[5.0]
  def change
    add_reference :inbound_shipment_items, :pending_po, index: true
  end
end
