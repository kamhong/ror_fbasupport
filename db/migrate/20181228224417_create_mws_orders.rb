class CreateMwsOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :mws_orders do |t|
      t.string  :amazon_order_id
      t.string  :merchant_order_id
      t.datetime  :purchase_date
      t.datetime  :last_updated_date
      t.string  :order_status
      t.string  :fulfillment_channel
      t.string  :sales_channel
      t.string  :order_channel
      t.string  :url
      t.string  :ship_service_level
      t.string  :address_type
      t.string  :ship_city
      t.string  :ship_postal_code
      t.string  :ship_state
      t.string  :ship_country

      t.references :users, index: true
      t.timestamps
    end
    add_index :mws_orders, :amazon_order_id, unique: true
    
  end
end
