class ChangeTheStatusofPendingPoIndexToInteger < ActiveRecord::Migration[5.0]
  def change
    remove_column :pending_po_indices, :pending_status
    add_column :pending_po_indices, :pending_status, :integer, :default => 0
  end
end
