class CreateMwsReportRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :mws_report_requests do |t|
      t.string  :report_request_id
      t.string  :report_id
      t.string  :report_type
      t.date  :start_date
      t.date  :end_date
      t.string  :report_processing_status
      t.string  :report_read_status
      t.integer :attempt
      t.references :user_market_places, index: true
      t.references :users, index: true
      t.timestamps
    end
  end
end
