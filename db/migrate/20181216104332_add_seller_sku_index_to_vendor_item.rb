class AddSellerSkuIndexToVendorItem < ActiveRecord::Migration[5.0]
  def change
    add_reference :vendoritems, :seller_sku, index: true
  end
end
