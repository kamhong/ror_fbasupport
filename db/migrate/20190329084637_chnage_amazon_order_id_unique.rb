class ChnageAmazonOrderIdUnique < ActiveRecord::Migration[5.0]
  def change
    remove_index :mws_orders, :amazon_order_id
    add_index :mws_orders, [:user_id, :amazon_order_id], unique: true
  end
end
