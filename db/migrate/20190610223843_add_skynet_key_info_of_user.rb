class AddSkynetKeyInfoOfUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :skynet_api_enabled, :boolean, :default => false
    add_column :users, :skynet_api_user_id, :string 
  end
end
