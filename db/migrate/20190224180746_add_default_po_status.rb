class AddDefaultPoStatus < ActiveRecord::Migration[5.0]
  def change
    add_column :pos, :po_status_id, :integer, :default => 0
  end
end
