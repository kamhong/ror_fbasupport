class CreatePackages < ActiveRecord::Migration[5.0]
  def change
    create_table :packages do |t|
      t.string  :dimenstion_unit
      t.float   :length
      t.float   :width
      t.float   :height
      t.string  :weight_unit
      t.float   :weight
      t.integer :transport_content_id
      t.timestamps
    end
  end
end
