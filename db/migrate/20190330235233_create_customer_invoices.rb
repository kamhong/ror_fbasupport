class CreateCustomerInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :customer_invoices do |t|
      t.references :user, index: true
      t.references :vendor, index: true
      
      t.string  :invoice_name, null: false
      t.string  :invoice_link
      t.string  :po_link
      t.string  :labels
      t.date    :vendor_order_placd, null: true
      t.string  :order_no
      t.string  :payment
      t.string  :shipment
      t.date  :order_date, null: true
      t.integer :received
      t.integer :shipped
      t.decimal :invoice_sales_order_cost, :precision=>11, :scale=>2, :default=>0
      t.decimal :warehouse_cost, :precision=>11, :scale=>2, :default=>0
      t.string  :warehouse_inv
      t.decimal :total_paid, :precision=>11, :scale=>2, :default=>0
      t.decimal :profit_difference, :precision=>11, :scale=>2, :default=>0
      t.decimal :refund, :precision=>11, :scale=>2, :default=>0
      t.text    :note
      t.timestamps
    end
    add_reference :pending_pos, :customer_invoice, index: true
  end
end
