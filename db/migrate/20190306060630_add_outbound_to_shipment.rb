class AddOutboundToShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :outbound_shipments, :shipment_id, :string
  end
end
