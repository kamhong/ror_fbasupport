class ChangeUserOfMwsOrder < ActiveRecord::Migration[5.0]
  def change
    remove_column :mws_orders, :users_id

    add_reference :mws_orders, :user, index: true
  end
end
