class AddUploadedFieldsToTransportContent < ActiveRecord::Migration[5.0]
  def change
    add_column :transport_contents, :uploaded_to_amazon, :boolean, default: false
    add_column :transport_contents, :status, :string
    add_column :transport_contents, :amazon_estimate, :float
  end
end
