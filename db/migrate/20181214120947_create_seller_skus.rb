class CreateSellerSkus < ActiveRecord::Migration[5.0]
  def change
    create_table :seller_skus do |t|
      t.string  :sku
      t.decimal :price
      t.string  :fnsku
      t.string  :inbound_destination
      t.string  :prepinstruction
      
      t.string  :age
      t.integer :age_0_90
      t.integer :age_91_180
      t.integer :age_181_270
      t.integer :age_271_360
      t.integer :age_365_plus

      t.boolean :snl
      
      t.integer :last1daysales
      t.integer :last7daysales
      t.integer :last30daysales
      t.integer :last60daysales
      t.integer :last90daysales
      t.integer :last360daysales

      t.integer :item_quantity
      t.integer :inbound_quantity
      t.integer :reserved_quantity
      t.integer :reserved_fc_processing
      t.integer :reserved_fc_transfers
      t.integer :reserved_customerorders

      t.date :instock_date
      t.date :outstock_date
      t.integer :days_instock

      t.references :vendorasin, index:true
      t.references :user, index:true


      t.timestamps
    end
    add_index :seller_skus, :sku,                unique: true
  end
end
