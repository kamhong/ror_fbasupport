class AddPaidDateToShipCusInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :ship_cus_invoices, :paid_date, :datetime
  end
end
