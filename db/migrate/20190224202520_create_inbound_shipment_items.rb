class CreateInboundShipmentItems < ActiveRecord::Migration[5.0]
  def change
    create_table :inbound_shipment_items do |t|
      t.references :inbound_shipment, index:true

      t.string :seller_sku, index: true
      t.integer :quantity_shipped, default: 0, null: false
      t.integer :quantity_in_case, default: 0, null: false
      t.integer :quantity_received	, default: 0, null: false
      t.date :expired_date, null: true	
      t.timestamps
    end

    add_column :inbound_shipments, :shipment_status, :string, default: "WORKING"

  end
end
