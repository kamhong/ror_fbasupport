class AddAmazonKeyToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :mws_access_key_id, :string
    add_column :users, :mws_access_acces_key, :string
    add_column :users, :seller_id, :string
    add_column :users, :mws_access_token, :string
    add_column :users, :aws_api_key, :string
    add_column :users, :aws_api_secret_key, :string
    add_column :users, :aws_associate_tag, :string
  end
end
