class AddWarehouseCostToPendingPo < ActiveRecord::Migration[5.0]
  def change
    add_column :pending_pos, :warehouse_invoiced_qty, :integer, :default=>0
    add_column :pending_pos, :warehouse_invoiced_unit_cost, :decimal, :precision=>11, :scale=>2, :default=>0
  end
end
