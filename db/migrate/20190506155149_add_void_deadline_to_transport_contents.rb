class AddVoidDeadlineToTransportContents < ActiveRecord::Migration[5.0]
  def change
    add_column :transport_contents, :void_deadline, :datetime
  end
end
