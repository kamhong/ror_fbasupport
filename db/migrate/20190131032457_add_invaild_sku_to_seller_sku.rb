class AddInvaildSkuToSellerSku < ActiveRecord::Migration[5.0]
  def change
    add_column :seller_skus, :is_invalid, :boolean, default: false, null: false
  end
end
