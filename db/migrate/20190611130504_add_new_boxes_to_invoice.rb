class AddNewBoxesToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :ship_cus_invoices, :new_box_count, :integer, default: 0
  end
end
