class CreatePoStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :po_statuses do |t|
      t.string :po_status
    end
  end
end
