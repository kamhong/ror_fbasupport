class AddStatusToSellerSku < ActiveRecord::Migration[5.0]
  def change
    add_column :seller_skus, :sku_status, :string, :default => ""
  end
end
