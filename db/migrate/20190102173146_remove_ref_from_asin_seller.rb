class RemoveRefFromAsinSeller < ActiveRecord::Migration[5.0]
  def change
    remove_column :asin_sellers, :vendorasin_id
    add_column :asin_sellers, :asin_str, :string, null: false
  end
end
