class CreateInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :invoices do |t|
      t.references :user, index: true
      t.string  :invoice_name
      t.timestamps
    end
  end
end
