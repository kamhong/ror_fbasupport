namespace :amazon do
  desc "TODO"
  task sync_shipments: :environment do
    shipment_ids = InboundShipment.where.not(shipment_status: "CLOSED").map(&:shipment_id)
    shipment_ids.each do |shipment_id|
      TransportContentService.new(nil, shipment_id).sync_shipment
    end
  end
end
