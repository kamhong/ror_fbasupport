namespace :re_order do
  desc "Pulls daily inventory and syncs it with SKUs"
  task build_inventory: :environment do
    PullInventoryQuantityWorker.perform_async
  end
end
