module MwsCall
    def self.get(mws_key, api, version, params=[])
        params = {
            'AWSAccessKeyId' => mws_key[:mws_access_key_id],        
            'MWSAuthToken'=>mws_key[:mws_access_token],
            'seller_id' => mws_key[:seller_id],
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> version,       
            'MarketplaceId' => mws_key[:mws_market_place_id]   
        }
    end
end