# == Schema Information
#
# Table name: customer_invoices
#
#  id                       :integer          not null, primary key
#  user_id                  :integer
#  vendor_id                :integer
#  invoice_name             :string(255)      default(""), not null
#  invoice_link             :string(255)
#  po_link                  :string(255)
#  labels                   :string(255)
#  vendor_order_placd       :date
#  order_no                 :string(255)
#  payment                  :string(255)
#  shipment                 :string(255)
#  order_date               :date
#  received                 :integer
#  shipped                  :integer
#  invoice_sales_order_cost :decimal(11, 2)   default(0.0)
#  warehouse_cost           :decimal(11, 2)   default(0.0)
#  warehouse_inv            :string(255)
#  total_paid               :decimal(11, 2)   default(0.0)
#  profit_difference        :decimal(11, 2)   default(0.0)
#  refund                   :decimal(11, 2)   default(0.0)
#  note                     :text(65535)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  pending_po_index_id      :integer
#
# Indexes
#
#  index_customer_invoices_on_pending_po_index_id  (pending_po_index_id)
#  index_customer_invoices_on_user_id              (user_id)
#  index_customer_invoices_on_vendor_id            (vendor_id)
#

require 'test_helper'

class CustomerInvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @customer_invoice = customer_invoices(:one)
  end

  test "should get index" do
    get customer_invoices_url
    assert_response :success
  end

  test "should get new" do
    get new_customer_invoice_url
    assert_response :success
  end

  test "should create customer_invoice" do
    assert_difference('CustomerInvoice.count') do
      post customer_invoices_url, params: { customer_invoice: {  } }
    end

    assert_redirected_to customer_invoice_url(CustomerInvoice.last)
  end

  test "should show customer_invoice" do
    get customer_invoice_url(@customer_invoice)
    assert_response :success
  end

  test "should get edit" do
    get edit_customer_invoice_url(@customer_invoice)
    assert_response :success
  end

  test "should update customer_invoice" do
    patch customer_invoice_url(@customer_invoice), params: { customer_invoice: {  } }
    assert_redirected_to customer_invoice_url(@customer_invoice)
  end

  test "should destroy customer_invoice" do
    assert_difference('CustomerInvoice.count', -1) do
      delete customer_invoice_url(@customer_invoice)
    end

    assert_redirected_to customer_invoices_url
  end
end
