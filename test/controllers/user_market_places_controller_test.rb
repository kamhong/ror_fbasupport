# == Schema Information
#
# Table name: user_market_places
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_user_market_places_on_user_id  (user_id)
#

require 'test_helper'

class UserMarketPlacesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_market_place = user_market_places(:one)
  end

  test "should get index" do
    get user_market_places_url
    assert_response :success
  end

  test "should get new" do
    get new_user_market_place_url
    assert_response :success
  end

  test "should create user_market_place" do
    assert_difference('UserMarketPlace.count') do
      post user_market_places_url, params: { user_market_place: {  } }
    end

    assert_redirected_to user_market_place_url(UserMarketPlace.last)
  end

  test "should show user_market_place" do
    get user_market_place_url(@user_market_place)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_market_place_url(@user_market_place)
    assert_response :success
  end

  test "should update user_market_place" do
    patch user_market_place_url(@user_market_place), params: { user_market_place: {  } }
    assert_redirected_to user_market_place_url(@user_market_place)
  end

  test "should destroy user_market_place" do
    assert_difference('UserMarketPlace.count', -1) do
      delete user_market_place_url(@user_market_place)
    end

    assert_redirected_to user_market_places_url
  end
end
