# == Schema Information
#
# Table name: mws_orders
#
#  id                  :integer          not null, primary key
#  amazon_order_id     :string(255)
#  merchant_order_id   :string(255)
#  purchase_date       :datetime
#  last_updated_date   :datetime
#  order_status        :string(255)
#  fulfillment_channel :string(255)
#  sales_channel       :string(255)
#  order_channel       :string(255)
#  url                 :string(255)
#  ship_service_level  :string(255)
#  address_type        :string(255)
#  ship_city           :string(255)
#  ship_postal_code    :string(255)
#  ship_state          :string(255)
#  ship_country        :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  user_id             :integer
#
# Indexes
#
#  index_mws_orders_on_user_id                      (user_id)
#  index_mws_orders_on_user_id_and_amazon_order_id  (user_id,amazon_order_id) UNIQUE
#

require 'test_helper'

class MwsOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mws_order = mws_orders(:one)
  end

  test "should get index" do
    get mws_orders_url
    assert_response :success
  end

  test "should get new" do
    get new_mws_order_url
    assert_response :success
  end

  test "should create mws_order" do
    assert_difference('MwsOrder.count') do
      post mws_orders_url, params: { mws_order: {  } }
    end

    assert_redirected_to mws_order_url(MwsOrder.last)
  end

  test "should show mws_order" do
    get mws_order_url(@mws_order)
    assert_response :success
  end

  test "should get edit" do
    get edit_mws_order_url(@mws_order)
    assert_response :success
  end

  test "should update mws_order" do
    patch mws_order_url(@mws_order), params: { mws_order: {  } }
    assert_redirected_to mws_order_url(@mws_order)
  end

  test "should destroy mws_order" do
    assert_difference('MwsOrder.count', -1) do
      delete mws_order_url(@mws_order)
    end

    assert_redirected_to mws_orders_url
  end
end
