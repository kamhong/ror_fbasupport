# == Schema Information
#
# Table name: mws_report_requests
#
#  id                       :integer          not null, primary key
#  report_request_id        :string(255)
#  report_id                :string(255)
#  report_type              :string(255)
#  start_date               :date
#  end_date                 :date
#  report_processing_status :string(255)
#  report_read_status       :string(255)
#  attempt                  :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  user_id                  :integer
#  user_market_place_id     :integer
#
# Indexes
#
#  index_mws_report_requests_on_user_id               (user_id)
#  index_mws_report_requests_on_user_market_place_id  (user_market_place_id)
#

require 'test_helper'

class MwsReportRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mws_report_request = mws_report_requests(:one)
  end

  test "should get index" do
    get mws_report_requests_url
    assert_response :success
  end

  test "should get new" do
    get new_mws_report_request_url
    assert_response :success
  end

  test "should create mws_report_request" do
    assert_difference('MwsReportRequest.count') do
      post mws_report_requests_url, params: { mws_report_request: {  } }
    end

    assert_redirected_to mws_report_request_url(MwsReportRequest.last)
  end

  test "should show mws_report_request" do
    get mws_report_request_url(@mws_report_request)
    assert_response :success
  end

  test "should get edit" do
    get edit_mws_report_request_url(@mws_report_request)
    assert_response :success
  end

  test "should update mws_report_request" do
    patch mws_report_request_url(@mws_report_request), params: { mws_report_request: {  } }
    assert_redirected_to mws_report_request_url(@mws_report_request)
  end

  test "should destroy mws_report_request" do
    assert_difference('MwsReportRequest.count', -1) do
      delete mws_report_request_url(@mws_report_request)
    end

    assert_redirected_to mws_report_requests_url
  end
end
