# == Schema Information
#
# Table name: pending_po_indices
#
#  id             :integer          not null, primary key
#  po_index       :integer
#  po_name        :string(255)
#  vendor_id      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  pending_status :integer          default("")
#
# Indexes
#
#  index_pending_po_indices_on_vendor_id  (vendor_id)
#

require 'test_helper'

class PendingPoIndicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pending_po_index = pending_po_indices(:one)
  end

  test "should get index" do
    get pending_po_indices_url
    assert_response :success
  end

  test "should get new" do
    get new_pending_po_index_url
    assert_response :success
  end

  test "should create pending_po_index" do
    assert_difference('PendingPoIndex.count') do
      post pending_po_indices_url, params: { pending_po_index: {  } }
    end

    assert_redirected_to pending_po_index_url(PendingPoIndex.last)
  end

  test "should show pending_po_index" do
    get pending_po_index_url(@pending_po_index)
    assert_response :success
  end

  test "should get edit" do
    get edit_pending_po_index_url(@pending_po_index)
    assert_response :success
  end

  test "should update pending_po_index" do
    patch pending_po_index_url(@pending_po_index), params: { pending_po_index: {  } }
    assert_redirected_to pending_po_index_url(@pending_po_index)
  end

  test "should destroy pending_po_index" do
    assert_difference('PendingPoIndex.count', -1) do
      delete pending_po_index_url(@pending_po_index)
    end

    assert_redirected_to pending_po_indices_url
  end
end
