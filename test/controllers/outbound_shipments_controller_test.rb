# == Schema Information
#
# Table name: outbound_shipments
#
#  id             :integer          not null, primary key
#  box_qty        :integer          default(1)
#  show_case_qty  :integer          default(0)
#  case_qty       :integer          default(1)
#  expired_date   :date
#  received_date  :date
#  case           :boolean          default(TRUE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  pending_po_id  :integer
#  bagger         :boolean          default(FALSE), not null
#  stage_status   :string(255)      default("RECEIVED")
#  shipment_id    :string(255)
#  boxed_quantity :integer          default(0)
#
# Indexes
#
#  index_outbound_shipments_on_pending_po_id  (pending_po_id)
#

require 'test_helper'

class OutboundShipmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @outbound_shipment = outbound_shipments(:one)
  end

  test "should get index" do
    get outbound_shipments_url
    assert_response :success
  end

  test "should get new" do
    get new_outbound_shipment_url
    assert_response :success
  end

  test "should create outbound_shipment" do
    assert_difference('OutboundShipment.count') do
      post outbound_shipments_url, params: { outbound_shipment: {  } }
    end

    assert_redirected_to outbound_shipment_url(OutboundShipment.last)
  end

  test "should show outbound_shipment" do
    get outbound_shipment_url(@outbound_shipment)
    assert_response :success
  end

  test "should get edit" do
    get edit_outbound_shipment_url(@outbound_shipment)
    assert_response :success
  end

  test "should update outbound_shipment" do
    patch outbound_shipment_url(@outbound_shipment), params: { outbound_shipment: {  } }
    assert_redirected_to outbound_shipment_url(@outbound_shipment)
  end

  test "should destroy outbound_shipment" do
    assert_difference('OutboundShipment.count', -1) do
      delete outbound_shipment_url(@outbound_shipment)
    end

    assert_redirected_to outbound_shipments_url
  end
end
