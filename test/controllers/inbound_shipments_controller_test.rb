# == Schema Information
#
# Table name: inbound_shipments
#
#  id                                :integer          not null, primary key
#  postal_code                       :string(255)
#  name                              :string(255)
#  country_code                      :string(255)
#  state_or_province_code            :string(255)
#  address_line2                     :string(255)
#  address_line1                     :string(255)
#  city                              :string(255)
#  are_cases_required                :boolean          default(FALSE)
#  shipment_id                       :string(255)      not null
#  shipment_name                     :string(255)
#  destination_fulfillment_center_id :string(255)
#  label_prep_type                   :string(255)
#  checked_date                      :date
#  partnered_estimate                :decimal(64, 2)   default(0.0)
#  user_id                           :integer
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  shipment_status                   :string(255)      default("WORKING")
#  invoice_name                      :string(255)      default("")
#  invoice_status                    :boolean          default(FALSE)
#  mix_label_charges                 :float(24)        default(0.0)
#  invoice_sent                      :boolean          default(FALSE)
#
# Indexes
#
#  index_inbound_shipments_on_shipment_id  (shipment_id) UNIQUE
#  index_inbound_shipments_on_user_id      (user_id)
#

require 'test_helper'

class InboundShipmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inbound_shipment = inbound_shipments(:one)
  end

  test "should get index" do
    get inbound_shipments_url
    assert_response :success
  end

  test "should get new" do
    get new_inbound_shipment_url
    assert_response :success
  end

  test "should create inbound_shipment" do
    assert_difference('InboundShipment.count') do
      post inbound_shipments_url, params: { inbound_shipment: {  } }
    end

    assert_redirected_to inbound_shipment_url(InboundShipment.last)
  end

  test "should show inbound_shipment" do
    get inbound_shipment_url(@inbound_shipment)
    assert_response :success
  end

  test "should get edit" do
    get edit_inbound_shipment_url(@inbound_shipment)
    assert_response :success
  end

  test "should update inbound_shipment" do
    patch inbound_shipment_url(@inbound_shipment), params: { inbound_shipment: {  } }
    assert_redirected_to inbound_shipment_url(@inbound_shipment)
  end

  test "should destroy inbound_shipment" do
    assert_difference('InboundShipment.count', -1) do
      delete inbound_shipment_url(@inbound_shipment)
    end

    assert_redirected_to inbound_shipments_url
  end
end
