# == Schema Information
#
# Table name: ship_cus_invoices
#
#  id                  :integer          not null, primary key
#  inbound_shipment_id :integer
#  invoice_name        :string(255)      not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  paid_date           :datetime
#  box_charges         :float(24)
#  new_box_count       :integer          default(0)
#
# Indexes
#
#  index_ship_cus_invoices_on_inbound_shipment_id  (inbound_shipment_id)
#

require 'test_helper'

class ShipCusInvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ship_cus_invoice = ship_cus_invoices(:one)
  end

  test "should get index" do
    get ship_cus_invoices_url
    assert_response :success
  end

  test "should get new" do
    get new_ship_cus_invoice_url
    assert_response :success
  end

  test "should create ship_cus_invoice" do
    assert_difference('ShipCusInvoice.count') do
      post ship_cus_invoices_url, params: { ship_cus_invoice: {  } }
    end

    assert_redirected_to ship_cus_invoice_url(ShipCusInvoice.last)
  end

  test "should show ship_cus_invoice" do
    get ship_cus_invoice_url(@ship_cus_invoice)
    assert_response :success
  end

  test "should get edit" do
    get edit_ship_cus_invoice_url(@ship_cus_invoice)
    assert_response :success
  end

  test "should update ship_cus_invoice" do
    patch ship_cus_invoice_url(@ship_cus_invoice), params: { ship_cus_invoice: {  } }
    assert_redirected_to ship_cus_invoice_url(@ship_cus_invoice)
  end

  test "should destroy ship_cus_invoice" do
    assert_difference('ShipCusInvoice.count', -1) do
      delete ship_cus_invoice_url(@ship_cus_invoice)
    end

    assert_redirected_to ship_cus_invoices_url
  end
end
