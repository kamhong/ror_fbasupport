# == Schema Information
#
# Table name: pos
#
#  id             :integer          not null, primary key
#  order_quantity :integer          default(1)
#  vendoritem_id  :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  received_qty   :integer          default(0)
#  po_status_id   :integer          default(0)
#
# Indexes
#
#  index_pos_on_po_status_id   (po_status_id)
#  index_pos_on_vendoritem_id  (vendoritem_id)
#

require 'test_helper'

class PosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @po = pos(:one)
  end

  test "should get index" do
    get pos_url
    assert_response :success
  end

  test "should get new" do
    get new_po_url
    assert_response :success
  end

  test "should create po" do
    assert_difference('Po.count') do
      post pos_url, params: { po: {  } }
    end

    assert_redirected_to po_url(Po.last)
  end

  test "should show po" do
    get po_url(@po)
    assert_response :success
  end

  test "should get edit" do
    get edit_po_url(@po)
    assert_response :success
  end

  test "should update po" do
    patch po_url(@po), params: { po: {  } }
    assert_redirected_to po_url(@po)
  end

  test "should destroy po" do
    assert_difference('Po.count', -1) do
      delete po_url(@po)
    end

    assert_redirected_to pos_url
  end
end
