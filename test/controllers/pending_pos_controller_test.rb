# == Schema Information
#
# Table name: pending_pos
#
#  id                           :integer          not null, primary key
#  order_quantity               :integer          default(0)
#  received_quantity            :integer          default(0)
#  pending_po_index_id          :integer
#  vendoritem_id                :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  cost                         :decimal(64, 12)  default(0.0)
#  damaged_qty                  :integer          default(0)
#  mispick_qty                  :integer          default(0)
#  customer_invoice_id          :integer
#  confirmed_qty                :integer          default(0)
#  paid_qty                     :integer          default(0)
#  invoice_id                   :integer
#  warehouse_paid               :decimal(11, 2)   default(0.0)
#  warehouse_invoiced_qty       :integer          default(0)
#  warehouse_invoiced_unit_cost :decimal(11, 2)   default(0.0)
#  status_date                  :datetime
#  status                       :integer          default("")
#
# Indexes
#
#  index_pending_pos_on_customer_invoice_id  (customer_invoice_id)
#  index_pending_pos_on_invoice_id           (invoice_id)
#  index_pending_pos_on_pending_po_index_id  (pending_po_index_id)
#  index_pending_pos_on_vendoritem_id        (vendoritem_id)
#

require 'test_helper'

class PendingPosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pending_po = pending_pos(:one)
  end

  test "should get index" do
    get pending_pos_url
    assert_response :success
  end

  test "should get new" do
    get new_pending_po_url
    assert_response :success
  end

  test "should create pending_po" do
    assert_difference('PendingPo.count') do
      post pending_pos_url, params: { pending_po: {  } }
    end

    assert_redirected_to pending_po_url(PendingPo.last)
  end

  test "should show pending_po" do
    get pending_po_url(@pending_po)
    assert_response :success
  end

  test "should get edit" do
    get edit_pending_po_url(@pending_po)
    assert_response :success
  end

  test "should update pending_po" do
    patch pending_po_url(@pending_po), params: { pending_po: {  } }
    assert_redirected_to pending_po_url(@pending_po)
  end

  test "should destroy pending_po" do
    assert_difference('PendingPo.count', -1) do
      delete pending_po_url(@pending_po)
    end

    assert_redirected_to pending_pos_url
  end
end
