# == Schema Information
#
# Table name: seller_skus
#
#  id                      :integer          not null, primary key
#  sku                     :string(255)
#  price                   :decimal(64, 2)
#  fnsku                   :string(255)
#  inbound_destination     :string(255)
#  prepinstruction         :string(255)
#  age                     :string(255)
#  age_0_90                :integer          default(0)
#  age_91_180              :integer          default(0)
#  age_181_270             :integer          default(0)
#  age_271_360             :integer          default(0)
#  age_365_plus            :integer          default(0)
#  snl                     :boolean          default(FALSE)
#  last1daysales           :integer          default(0)
#  last7daysales           :integer          default(0)
#  last30daysales          :integer          default(0)
#  last60daysales          :integer          default(0)
#  last90daysales          :integer          default(0)
#  last360daysales         :integer          default(0)
#  item_quantity           :integer          default(0)
#  inbound_quantity        :integer          default(0)
#  reserved_quantity       :integer          default(0)
#  reserved_fc_processing  :integer          default(0)
#  reserved_fc_transfers   :integer          default(0)
#  reserved_customerorders :integer          default(0)
#  instock_date            :date
#  outstock_date           :date
#  days_instock            :integer
#  vendorasin_id           :integer
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  item_name               :text(65535)
#  item_description        :text(65535)
#  open_date               :date
#  image_url               :string(255)
#  product_id_type         :string(255)
#  item_note               :text(65535)
#  item_condition          :string(255)
#  fulfillment_channel     :string(255)
#  listing_id              :string(255)
#  is_invalid              :boolean          default(FALSE), not null
#  bagger_number           :integer          default(0)
#  sku_status              :string(255)      default("")
#  sales_velocity          :float(24)
#
# Indexes
#
#  index_seller_skus_on_user_id          (user_id)
#  index_seller_skus_on_user_id_and_sku  (user_id,sku) UNIQUE
#  index_seller_skus_on_vendorasin_id    (vendorasin_id)
#

require 'test_helper'

class SellerSkusControllerTest < ActionDispatch::IntegrationTest
  setup do
    @seller_sku = seller_skus(:one)
  end

  test "should get index" do
    get seller_skus_url
    assert_response :success
  end

  test "should get new" do
    get new_seller_sku_url
    assert_response :success
  end

  test "should create seller_sku" do
    assert_difference('SellerSku.count') do
      post seller_skus_url, params: { seller_sku: {  } }
    end

    assert_redirected_to seller_sku_url(SellerSku.last)
  end

  test "should show seller_sku" do
    get seller_sku_url(@seller_sku)
    assert_response :success
  end

  test "should get edit" do
    get edit_seller_sku_url(@seller_sku)
    assert_response :success
  end

  test "should update seller_sku" do
    patch seller_sku_url(@seller_sku), params: { seller_sku: {  } }
    assert_redirected_to seller_sku_url(@seller_sku)
  end

  test "should destroy seller_sku" do
    assert_difference('SellerSku.count', -1) do
      delete seller_sku_url(@seller_sku)
    end

    assert_redirected_to seller_skus_url
  end
end
