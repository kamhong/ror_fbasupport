# == Schema Information
#
# Table name: warehouse_vendors
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  supplier_code        :string(255)
#  day_of_shipping      :string(255)
#  minimum_order        :decimal(15, 2)   default(0.0)
#  vendor_phone         :string(255)
#  account_number       :string(255)
#  account_holder       :integer
#  contact_name         :string(255)
#  contact_email        :string(255)
#  contact_phone_number :string(255)
#  website              :string(255)
#  user_name            :string(255)
#  password             :string(255)
#  payment_terms        :integer
#  shipment_terms       :integer
#  catalog              :string(255)
#  user_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_warehouse_vendors_on_name     (name) UNIQUE
#  index_warehouse_vendors_on_user_id  (user_id)
#

require 'test_helper'

class WarehouseVendorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @warehouse_vendor = warehouse_vendors(:one)
  end

  test "should get index" do
    get warehouse_vendors_url
    assert_response :success
  end

  test "should get new" do
    get new_warehouse_vendor_url
    assert_response :success
  end

  test "should create warehouse_vendor" do
    assert_difference('WarehouseVendor.count') do
      post warehouse_vendors_url, params: { warehouse_vendor: {  } }
    end

    assert_redirected_to warehouse_vendor_url(WarehouseVendor.last)
  end

  test "should show warehouse_vendor" do
    get warehouse_vendor_url(@warehouse_vendor)
    assert_response :success
  end

  test "should get edit" do
    get edit_warehouse_vendor_url(@warehouse_vendor)
    assert_response :success
  end

  test "should update warehouse_vendor" do
    patch warehouse_vendor_url(@warehouse_vendor), params: { warehouse_vendor: {  } }
    assert_redirected_to warehouse_vendor_url(@warehouse_vendor)
  end

  test "should destroy warehouse_vendor" do
    assert_difference('WarehouseVendor.count', -1) do
      delete warehouse_vendor_url(@warehouse_vendor)
    end

    assert_redirected_to warehouse_vendors_url
  end
end
