# == Schema Information
#
# Table name: invoices
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  invoice_name      :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  ship_term         :integer          default("")
#  shipping_cost     :decimal(11, 2)   default(0.0)
#  adjustment_cost   :decimal(11, 2)   default(0.0)
#  adjustment_reason :string(255)      default("")
#  check_date        :date
#  cleared           :boolean          default(FALSE)
#  printed           :boolean          default(FALSE)
#  check_number      :string(255)      default("")
#  reported          :boolean          default(FALSE)
#
# Indexes
#
#  index_invoices_on_user_id  (user_id)
#

require 'test_helper'

class InvoicesControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
end
