require 'test_helper'

class SellerSkUsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @seller_sku = seller_skus(:one)
  end

  test "should get index" do
    get seller_skus_url
    assert_response :success
  end

  test "should get new" do
    get new_seller_sku_url
    assert_response :success
  end

  test "should create seller_sku" do
    assert_difference('SellerSku.count') do
      post seller_skus_url, params: { seller_sku: {  } }
    end

    assert_redirected_to seller_sku_url(SellerSku.last)
  end

  test "should show seller_sku" do
    get seller_sku_url(@seller_sku)
    assert_response :success
  end

  test "should get edit" do
    get edit_seller_sku_url(@seller_sku)
    assert_response :success
  end

  test "should update seller_sku" do
    patch seller_sku_url(@seller_sku), params: { seller_sku: {  } }
    assert_redirected_to seller_sku_url(@seller_sku)
  end

  test "should destroy seller_sku" do
    assert_difference('SellerSku.count', -1) do
      delete seller_sku_url(@seller_sku)
    end

    assert_redirected_to seller_skus_url
  end
end
