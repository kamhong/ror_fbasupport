# == Schema Information
#
# Table name: mws_order_items
#
#  id                      :integer          not null, primary key
#  product_name            :string(255)
#  sku                     :string(255)
#  asin                    :string(255)
#  number_of_items         :integer
#  item_status             :string(255)
#  quantity                :integer
#  currency                :string(255)
#  item_price              :decimal(10, 2)
#  item_tax                :decimal(10, 2)
#  shipping_price          :decimal(10, 2)
#  shipping_tax            :decimal(10, 2)
#  gift_wrap_price         :decimal(10, 2)
#  gift_wrap_tax           :decimal(10, 2)
#  item_promotion_discount :decimal(10, 2)
#  ship_promotion_discount :decimal(10, 2)
#  promotion_ids           :string(255)
#  is_business_order       :boolean
#  purchase_order_number   :string(255)
#  price_designation       :decimal(10, 2)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  mws_order_id            :integer
#
# Indexes
#
#  index_mws_order_items_on_asin          (asin)
#  index_mws_order_items_on_mws_order_id  (mws_order_id)
#  index_mws_order_items_on_sku           (sku)
#

require 'test_helper'

class MwsOrderItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
