# == Schema Information
#
# Table name: warehouse_vendors
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  supplier_code        :string(255)
#  day_of_shipping      :string(255)
#  minimum_order        :decimal(15, 2)   default(0.0)
#  vendor_phone         :string(255)
#  account_number       :string(255)
#  account_holder       :integer
#  contact_name         :string(255)
#  contact_email        :string(255)
#  contact_phone_number :string(255)
#  website              :string(255)
#  user_name            :string(255)
#  password             :string(255)
#  payment_terms        :integer
#  shipment_terms       :integer
#  catalog              :string(255)
#  user_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_warehouse_vendors_on_name     (name) UNIQUE
#  index_warehouse_vendors_on_user_id  (user_id)
#

require 'test_helper'

class WarehouseVendorTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
