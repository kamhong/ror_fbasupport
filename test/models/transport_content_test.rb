# == Schema Information
#
# Table name: transport_contents
#
#  id                     :integer          not null, primary key
#  amazon_shipment_id     :string(255)
#  shipment_type          :string(255)
#  name                   :string(255)
#  outbound_shipments_id  :integer
#  delivery_by_amazon     :boolean          default(TRUE)
#  carrier                :string(255)
#  tracking_id            :string(255)
#  email                  :string(255)
#  phone                  :string(255)
#  fax                    :string(255)
#  box_count              :string(255)
#  frieght_class          :string(255)
#  frieght_read_date      :date
#  total_weight           :float(24)
#  declared_value         :float(24)
#  pro_number             :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  uploaded_to_amazon     :boolean          default(FALSE)
#  status                 :string(255)
#  amazon_estimate        :float(24)
#  inbound_shipment_id    :integer
#  bill_of_lading_enabled :boolean          default(FALSE)
#  pallete_count          :integer          default(0)
#  void_deadline          :datetime
#  shipping_fee           :float(24)
#

#

require 'test_helper'

class TransportContentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
