# == Schema Information
#
# Table name: users
#
#  id                               :integer          not null, primary key
#  email                            :string(255)      default(""), not null
#  encrypted_password               :string(255)      default(""), not null
#  reset_password_token             :string(255)
#  reset_password_sent_at           :datetime
#  remember_created_at              :datetime
#  sign_in_count                    :integer          default(0), not null
#  current_sign_in_at               :datetime
#  last_sign_in_at                  :datetime
#  current_sign_in_ip               :string(255)
#  last_sign_in_ip                  :string(255)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  active                           :boolean          default(TRUE)
#  role_id                          :integer
#  company_id                       :integer
#  subscription_id                  :string(255)
#  mws_access_key_id                :string(255)
#  mws_access_acces_key             :string(255)
#  seller_id                        :string(255)
#  mws_access_token                 :string(255)
#  aws_api_key                      :string(255)
#  aws_api_secret_key               :string(255)
#  aws_associate_tag                :string(255)
#  mws_key_valid                    :boolean          default(FALSE)
#  mws_market_place_id              :string(255)      default("ATVPDKIKX0DER")
#  first_name                       :string(255)      default(""), not null
#  last_name                        :string(255)      default(""), not null
#  sku_prefix                       :string(255)      default(""), not null
#  warehouse_name                   :string(255)
#  warehouse_address_line1          :string(255)
#  warehouse_address_line2          :string(255)
#  warehouse_city                   :string(255)
#  warehouse_state_or_province_code :string(255)
#  warehouse_postal_code            :string(255)
#  warehouse_country_code           :string(255)
#  skynet_key                       :string(255)      default("")
#  skynet_api_enabled               :boolean          default(FALSE)
#  skynet_api_user_id               :string(255)
#
# Indexes
#
#  index_users_on_company_id            (company_id)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_role_id               (role_id)
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
