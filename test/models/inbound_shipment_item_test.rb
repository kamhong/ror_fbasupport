# == Schema Information
#
# Table name: inbound_shipment_items
#
#  id                  :integer          not null, primary key
#  inbound_shipment_id :integer
#  seller_sku          :string(255)
#  quantity_shipped    :integer          default(0), not null
#  quantity_in_case    :integer          default(0), not null
#  quantity_received   :integer          default(0), not null
#  expired_date        :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  pending_po_id       :integer
#
# Indexes
#
#  index_inbound_shipment_items_on_inbound_shipment_id  (inbound_shipment_id)
#  index_inbound_shipment_items_on_pending_po_id        (pending_po_id)
#  index_inbound_shipment_items_on_seller_sku           (seller_sku)
#

require 'test_helper'

class InboundShipmentItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
