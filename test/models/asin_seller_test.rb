# == Schema Information
#
# Table name: asin_sellers
#
#  id                              :integer          not null, primary key
#  seller_id                       :string(255)
#  sub_condition                   :string(255)
#  seller_positive_feedback_rating :integer
#  feedback_count                  :integer
#  listing_price                   :decimal(7, 2)
#  shipping                        :decimal(7, 2)
#  is_buy_box_winner               :boolean
#  is_fulfilled_by_amazon          :boolean
#  is_featured_merchant            :boolean
#  ships_domestically              :boolean
#  asin_str                        :string(255)      not null
#
# Indexes
#
#  index_asin_sellers_on_asin_str   (asin_str)
#  index_asin_sellers_on_seller_id  (seller_id)
#

require 'test_helper'

class AsinSellerTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
