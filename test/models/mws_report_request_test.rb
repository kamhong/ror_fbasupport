# == Schema Information
#
# Table name: mws_report_requests
#
#  id                       :integer          not null, primary key
#  report_request_id        :string(255)
#  report_id                :string(255)
#  report_type              :string(255)
#  start_date               :date
#  end_date                 :date
#  report_processing_status :string(255)
#  report_read_status       :string(255)
#  attempt                  :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  user_id                  :integer
#  user_market_place_id     :integer
#
# Indexes
#
#  index_mws_report_requests_on_user_id               (user_id)
#  index_mws_report_requests_on_user_market_place_id  (user_market_place_id)
#

require 'test_helper'

class MwsReportRequestTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
