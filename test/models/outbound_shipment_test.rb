# == Schema Information
#
# Table name: outbound_shipments
#
#  id             :integer          not null, primary key
#  box_qty        :integer          default(1)
#  show_case_qty  :integer          default(0)
#  case_qty       :integer          default(1)
#  expired_date   :date
#  received_date  :date
#  case           :boolean          default(TRUE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  pending_po_id  :integer
#  bagger         :boolean          default(FALSE), not null
#  stage_status   :string(255)      default("RECEIVED")
#  shipment_id    :string(255)
#  boxed_quantity :integer          default(0)
#
# Indexes
#
#  index_outbound_shipments_on_pending_po_id  (pending_po_id)
#

require 'test_helper'

class OutboundShipmentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
