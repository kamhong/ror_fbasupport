# == Schema Information
#
# Table name: inventory_histories
#
#  id                         :integer          not null, primary key
#  price                      :float(24)
#  total_quantity             :integer
#  fulfillable_quantity       :integer
#  unsellable_quantity        :integer
#  reserved_quantity          :integer
#  per_unit_volume            :integer
#  inbound_working_quantity   :integer
#  inbound_shipped_quantity   :integer
#  inbound_receiving_quantity :integer
#  seller_sku_id              :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#
# Indexes
#
#  index_inventory_histories_on_seller_sku_id  (seller_sku_id)
#

require 'test_helper'

class InventoryHistoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
