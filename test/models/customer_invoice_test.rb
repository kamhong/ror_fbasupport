# == Schema Information
#
# Table name: customer_invoices
#
#  id                       :integer          not null, primary key
#  user_id                  :integer
#  vendor_id                :integer
#  invoice_name             :string(255)      default(""), not null
#  invoice_link             :string(255)
#  po_link                  :string(255)
#  labels                   :string(255)
#  vendor_order_placd       :date
#  order_no                 :string(255)
#  payment                  :string(255)
#  shipment                 :string(255)
#  order_date               :date
#  received                 :integer
#  shipped                  :integer
#  invoice_sales_order_cost :decimal(11, 2)   default(0.0)
#  warehouse_cost           :decimal(11, 2)   default(0.0)
#  warehouse_inv            :string(255)
#  total_paid               :decimal(11, 2)   default(0.0)
#  profit_difference        :decimal(11, 2)   default(0.0)
#  refund                   :decimal(11, 2)   default(0.0)
#  note                     :text(65535)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  pending_po_index_id      :integer
#
# Indexes
#
#  index_customer_invoices_on_pending_po_index_id  (pending_po_index_id)
#  index_customer_invoices_on_user_id              (user_id)
#  index_customer_invoices_on_vendor_id            (vendor_id)
#

require 'test_helper'

class CustomerInvoiceTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
