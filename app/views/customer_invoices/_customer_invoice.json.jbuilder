json.extract! customer_invoice, :id, :created_at, :updated_at
json.url customer_invoice_url(customer_invoice, format: :json)
