json.extract! mws_order, :id, :created_at, :updated_at
json.url mws_order_url(mws_order, format: :json)
