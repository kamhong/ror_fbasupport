json.extract! warehouse_vendor, :id, :created_at, :updated_at
json.url warehouse_vendor_url(warehouse_vendor, format: :json)
