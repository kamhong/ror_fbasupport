json.extract! mws_report_request, :id, :created_at, :updated_at
json.url mws_report_request_url(mws_report_request, format: :json)
