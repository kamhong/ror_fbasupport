json.extract! pending_po, :id, :created_at, :updated_at
json.url pending_po_url(pending_po, format: :json)
