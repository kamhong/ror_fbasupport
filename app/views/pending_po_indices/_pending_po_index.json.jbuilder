json.extract! pending_po_index, :id, :created_at, :updated_at
json.url pending_po_index_url(pending_po_index, format: :json)
