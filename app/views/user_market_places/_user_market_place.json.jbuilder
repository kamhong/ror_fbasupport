json.extract! user_market_place, :id, :created_at, :updated_at
json.url user_market_place_url(user_market_place, format: :json)
