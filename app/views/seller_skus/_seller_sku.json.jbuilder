json.extract! seller_sku, :id, :created_at, :updated_at
json.url seller_sku_url(seller_sku, format: :json)
