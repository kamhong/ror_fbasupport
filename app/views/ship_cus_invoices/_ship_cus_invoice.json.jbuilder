json.extract! ship_cus_invoice, :id, :created_at, :updated_at
json.url ship_cus_invoice_url(ship_cus_invoice, format: :json)
