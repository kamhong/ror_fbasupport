json.extract! inbound_shipment, :id, :created_at, :updated_at
json.url inbound_shipment_url(inbound_shipment, format: :json)
