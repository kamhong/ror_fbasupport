json.extract! outbound_shipment, :id, :created_at, :updated_at
json.url outbound_shipment_url(outbound_shipment, format: :json)
