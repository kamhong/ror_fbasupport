class TransportContentService
  def initialize(transport_content_id = nil, shipment_id = nil, user = nil)
    @transport_content = TransportContent.find_by(id: transport_content_id)
    @shipment_id = shipment_id.presence || @transport_content.inbound_shipment.shipment_id
    @user = InboundShipment.find_by(shipment_id: @shipment_id).user
    @client = TransportContentService.set_client @user
  end

  def upload_to_amazon
    request_hash = {}
    if @transport_content.delivery_by_amazon and @transport_content.shipment_type == "LTL"
      partnered_ltl_data = request_hash["PartneredLtlData"] = {}
      partnered_ltl_data["BoxCount"] = @transport_content.box_count
      partnered_ltl_data["FreightReadyDate"] = @transport_content.frieght_read_date if @transport_content.frieght_read_date.present?
      partnered_ltl_data["Contact"] = {}
      partnered_ltl_data["Contact"]["Name"] = @transport_content.name
      partnered_ltl_data["Contact"]["Phone"] = @transport_content.phone
      partnered_ltl_data["Contact"]["Email"] = @transport_content.email
      partnered_ltl_data["Contact"]["Fax"] = @transport_content.fax
      partnered_ltl_data["PalletList"] = {}
      partnered_ltl_data["PalletList"]["member"] = {}
      if @transport_content.palletes.present?
        @transport_content.palletes.each_with_index do |pallete, index|
          partnered_ltl_data["PalletList"]["member"][index + 1] = {}
          partnered_ltl_data["PalletList"]["member"][index + 1]["Dimensions"] = {}
          partnered_ltl_data["PalletList"]["member"][index + 1]["Dimensions"]["Unit"] = pallete.dimension_unit == "IN" ? "inches" : pallete.dimension_unit
          partnered_ltl_data["PalletList"]["member"][index + 1]["Dimensions"]["Length"] = pallete.length
          partnered_ltl_data["PalletList"]["member"][index + 1]["Dimensions"]["Width"] = pallete.width
          partnered_ltl_data["PalletList"]["member"][index + 1]["Dimensions"]["Height"] = pallete.width

          partnered_ltl_data["PalletList"]["member"][index + 1]["Weight"] = {}
          partnered_ltl_data["PalletList"]["member"][index + 1]["Weight"]["Unit"] = pallete.weight_unit
          partnered_ltl_data["PalletList"]["member"][index + 1]["Weight"]["Value"] = pallete.weight
        end
      else
        partnered_ltl_data["PalletList"]["member"][1] = {}
        partnered_ltl_data["PalletList"]["member"][1]["Dimensions"] = {}
        partnered_ltl_data["PalletList"]["member"][1]["Dimensions"]["Unit"] = "inches"
        partnered_ltl_data["PalletList"]["member"][1]["Dimensions"]["Length"] = 25
        partnered_ltl_data["PalletList"]["member"][1]["Dimensions"]["Width"] = 25
        partnered_ltl_data["PalletList"]["member"][1]["Dimensions"]["Height"] = 25

        partnered_ltl_data["PalletList"]["member"][1]["Weight"] = {}
        partnered_ltl_data["PalletList"]["member"][1]["Weight"]["Unit"] = "pounds"
        partnered_ltl_data["PalletList"]["member"][1]["Weight"]["Value"] = 25
      end
    elsif !(@transport_content.delivery_by_amazon) and @transport_content.shipment_type == "LTL"
      non_partnered_ltl_data = request_hash["NonPartneredLtlData"] = {}
      non_partnered_ltl_data["CarrierName"] = @transport_content.carrier
      non_partnered_ltl_data["ProNumber"] = @transport_content.pro_number

    elsif @transport_content.delivery_by_amazon and @transport_content.shipment_type == "SP"
      partnered_sp_data = request_hash["PartneredSmallParcelData"] = {}
      partnered_sp_data["CarrierName"] = @transport_content.carrier
      partnered_sp_data["PackageList"] = {}
      partnered_sp_data["PackageList"]["member"] = {}
      @transport_content.packages.each_with_index do |package, index|
        partnered_sp_data["PackageList"]["member"][index + 1] = {}
        partnered_sp_data["PackageList"]["member"][index + 1]["Weight"] = {}
        partnered_sp_data["PackageList"]["member"][index + 1]["Weight"]["Unit"] = package.weight_unit
        partnered_sp_data["PackageList"]["member"][index + 1]["Weight"]["Value"] = package.weight
        partnered_sp_data["PackageList"]["member"][index + 1]["Dimensions"] = {}
        partnered_sp_data["PackageList"]["member"][index + 1]["Dimensions"]["Unit"] = package.dimenstion_unit
        partnered_sp_data["PackageList"]["member"][index + 1]["Dimensions"]["Length"] = package.length
        partnered_sp_data["PackageList"]["member"][index + 1]["Dimensions"]["Width"] = package.width
        partnered_sp_data["PackageList"]["member"][index + 1]["Dimensions"]["Height"] = package.height
      end
    elsif !(@transport_content.delivery_by_amazon) and @transport_content.shipment_type == "SP"
      non_partnered_sp_data = request_hash["NonPartneredSmallParcelData"] = {}
      non_partnered_sp_data["CarrierName"] = @transport_content.carrier
      non_partnered_sp_data["PackageList"] = {}
      non_partnered_sp_data["PackageList"]["member"] = {}
      @transport_content.trackings.each_with_index do |tracking, index|
        non_partnered_sp_data["PackageList"]["member"][index + 1] = {}
        non_partnered_sp_data["PackageList"]["member"][index + 1]["TrackingId"] = tracking.tracking_number
      end
    end
    request_hash = JSON.parse(request_hash.to_json)
    begin
      res = @client.put_transport_content(@transport_content.inbound_shipment.shipment_id, @transport_content.delivery_by_amazon, @transport_content.shipment_type, request_hash)
      if (res.data["TransportResult"]["TransportStatus"] rescue false)
        @transport_content.update(status: res.data["TransportResult"]["TransportStatus"])
        TransportContentService.new(@transport_content.id).get_estimate if @transport_content.delivery_by_amazon
        {success: true}
      else
        {success: false}
      end
    rescue => e
      {success: false, error: e}
    end
  end

  def get_transport_content
    shipment_id = @shipment_id.presence || @transport_content.inbound_shipment.shipment_id
    response = @client.get_transport_content(shipment_id)
    response
  end

  def get_estimate
    begin
      shipment_id = @transport_content.inbound_shipment.shipment_id
      response = @client.estimate_transport_request(shipment_id)
      @transport_content.update(status: response.data["TransportResult"]["TransportStatus"])
      TransportContentService.new(nil, shipment_id).sync_transport_with_amazon @transport_content.inbound_shipment.user
      {success: true}
    rescue => e
      TransportContentService.new(nil, shipment_id).sync_transport_with_amazon @transport_content.inbound_shipment.user
      {success: false, error: e}
    end
  end

  def sync_transport_with_amazon user = nil
    begin
      response = get_transport_content
      user = user.presence || @user
      shipment = user.inbound_shipments.find_or_create_by(shipment_id: response.data["TransportContent"]["TransportHeader"]["ShipmentId"])
      transport_content = shipment.transport_contents.find_or_create_by(shipment_type: response.data["TransportContent"]["TransportHeader"]["ShipmentType"])
      transport_info = response.data["TransportContent"]
      transport_content.update(
                                delivery_by_amazon: (transport_info["TransportHeader"]["IsPartnered"] == "true"),
                                status: transport_info["TransportResult"]["TransportStatus"]
                              )
      if transport_info["TransportDetails"]["PartneredLtlData"]
        transport_content.update(uploaded_to_amazon: true)
        partnered_data = transport_info["TransportDetails"]["PartneredLtlData"]
        void_deadline = partnered_data["PartneredEstimate"]["VoidDeadline"].to_datetime rescue nil
        bill_of_lading_enabled = partnered_data["IsBillOfLadingAvailable"] == "true"
        transport_content.update(
                                 frieght_read_date: partnered_data["FreightReadyDate"],
                                 frieght_class: partnered_data["PreviewFreightClass"],
                                 amazon_estimate: (partnered_data["PartneredEstimate"]["Amount"]["Value"] rescue nil),
                                 box_count: partnered_data["BoxCount"],
                                 name: partnered_data["Contact"]["Name"],
                                 fax: partnered_data["Contact"]["Fax"],
                                 phone: partnered_data["Contact"]["Phone"],
                                 email: partnered_data["Contact"]["Email"],
                                 bill_of_lading_enabled: bill_of_lading_enabled
                                )
        transport_content.palletes.destroy_all
        unless partnered_data["PalletList"].nil?
          if partnered_data["PalletList"]["member"].class != Array
            pallete_information = [partnered_data["PalletList"]["member"]]
          else
            pallete_information = partnered_data["PalletList"]["member"]
          end
          pallete_information.each do |pallete_info|
            next if dummy_pallete? pallete_info
            pallate = transport_content.palletes.create(weight_unit: pallete_info["Weight"]["Unit"],
                                                        weight: pallete_info["Weight"]["Value"],
                                                        dimension_unit: (pallete_info["Dimensions"]["Unit"] == "IN" ? "inches" : pallete_info["Dimensions"]["Unit"]),
                                                        width: pallete_info["Dimensions"]["Width"],
                                                        height: pallete_info["Dimensions"]["Height"],
                                                        length: pallete_info["Dimensions"]["Length"])
          end
        end
      elsif transport_info["TransportDetails"]["PartneredSmallParcelData"]
        transport_content.packages.destroy_all
        partnered_data = transport_info["TransportDetails"]["PartneredSmallParcelData"]
        void_deadline = partnered_data["PartneredEstimate"]["VoidDeadline"].to_datetime rescue nil
        transport_content.update(uploaded_to_amazon: true, amazon_estimate: (partnered_data["PartneredEstimate"]["Amount"]["Value"] rescue nil), void_deadline: void_deadline)
        unless partnered_data["PackageList"].nil?
          if partnered_data["PackageList"]["member"].class != Array
            package_information = [partnered_data["PackageList"]["member"]]
          else
            package_information = partnered_data["PackageList"]["member"]
          end
          package_information.each do |pkg|
            transport_content.update(carrier: pkg["CarrierName"])
            package = transport_content.packages.create(
                                                        dimenstion_unit: ((pkg["Dimensions"]["Unit"] == "IN" ? "inches" : pkg["Dimensions"]["Unit"]) rescue nil),
                                                        length: (pkg["Dimensions"]["Length"] rescue nil),
                                                        width: (pkg["Dimensions"]["Width"] rescue nil),
                                                        height: (pkg["Dimensions"]["Height"] rescue nil),
                                                        weight_unit: (pkg["Weight"]["Unit"] rescue nil),
                                                        weight: (pkg["Weight"]["Value"] rescue nil)
                                                      )
          end
        end
      elsif transport_info["TransportDetails"]["NonPartneredLtlData"]
        transport_content.update(uploaded_to_amazon: true)
        transport_content.update(pro_number: transport_info["TransportDetails"]["NonPartneredLtlData"]["ProNumber"])
      elsif transport_info["TransportDetails"]["NonPartneredSmallParcelData"]
        transport_content.update(uploaded_to_amazon: true)
        partnered_data = transport_info["TransportDetails"]["NonPartneredSmallParcelData"]
        if partnered_data["PackageList"]["member"].class != Array
          package_information = [partnered_data["PackageList"]["member"]]
        else
          package_information = partnered_data["PackageList"]["member"]
        end
        transport_content.update(box_count: package_information.count)
        package_information.each do |pkg|
          next if !(pkg["Dimensions"].present?) or !(pkg["Weight"].present?)
          transport_content.update(carrier: pkg["CarrierName"])
          package = transport_content.packages.create(
                                                      dimenstion_unit: pkg["Dimensions"]["Unit"],
                                                      length: pkg["Dimensions"]["Length"],
                                                      width: pkg["Dimensions"]["Width"],
                                                      height: pkg["Dimensions"]["Height"],
                                                      weight_unit: pkg["Weight"]["Unit"],
                                                      weight: pkg["Weight"]["Value"]
                                                    )
        end
      end
      transport_info
      {success: true}
    rescue => e
      {success: false, error: e}
    end
  end

  def update_transport_status
    begin
      response = get_transport_content
      status = response.data["TransportContent"]["TransportResult"]["TransportStatus"]
      estimate = response.data["TransportContent"]["TransportDetails"]["PartneredLtlData"]["PartneredEstimate"]["Amount"]["Value"] rescue nil
      if estimate.blank?
        estimate = response.data["TransportContent"]["TransportDetails"]["PartneredSmallParcelData"]["PartneredEstimate"]["Amount"]["Value"] rescue nil
      end
      if TransportContent::STATUS_ARRAY.include?(status)
        @transport_content.update(status: status, amazon_estimate: estimate)
        {success: true}
      end
    rescue => e
      {success: false, error: e}
    end
  end

  def confirm_on_amazon
    begin
      shipment_id = @transport_content.inbound_shipment.shipment_id
      response = @client.confirm_transport_request(shipment_id)
      status = response.data["TransportContent"]["TransportResult"]["TransportStatus"]
      if TransportContent::STATUS_ARRAY.include?(status)
        @transport_content.update(status: status)
        TransportContentService.new(nil, shipment_id).sync_shipment
        {success: true}
      end
    rescue => e
      TransportContentService.new(nil, shipment_id).sync_shipment
      {success: false, error: e}
    end
  end

  def get_pallete_labels(number_of_palletes = 1, label_type = "PackageLabel_Plain_Paper")
    begin
      shipment_id = @transport_content.inbound_shipment.shipment_id
      response = @client.get_pallet_labels(shipment_id, label_type, number_of_palletes )
      @transport_content.update(pallete_count: number_of_palletes)
      return {success: true, base64_pdf: response.data["TransportDocument"]["PdfDocument"]}
    rescue => e
      {success: false, error: e}
    end
  end

  def get_package_labels(label_type = "PackageLabel_Plain_Paper")
    begin
      shipment_id = @transport_content.inbound_shipment.shipment_id
      response = @client.get_package_labels(shipment_id, label_type)
      return {success: true, base64_pdf: response.data["TransportDocument"]["PdfDocument"]}
    rescue => e
      {success: false, error: e}
    end
  end

  def get_bill_of_lading
    begin
      shipment_id = @shipment_id.presence || @transport_content.inbound_shipment.shipment_id
      response = @client.get_bill_of_lading(shipment_id)
      return {success: true, base64_pdf: response.data["TransportDocument"]["PdfDocument"]}
    rescue => e
      {success: false, error: e}
    end
  end

  def sync_shipment
    if @shipment_id.present?
      response = @client.list_inbound_shipments(shipment_id_list: [@shipment_id])
      shipment = response.data["ShipmentData"]["member"] rescue nil
      p response.data, @shipment_id
      return unless shipment
      inbound_shipment = @user.inbound_shipments.find_or_create_by(shipment_id: shipment["ShipmentId"])
      inbound_shipment.update(label_prep_type: shipment["LabelPrepType"],
                              destination_fulfillment_center_id: shipment["DestinationFulfillmentCenterId"],
                              state_or_province_code: shipment["ShipFromAddress"]["StateOrProvinceCode"],
                              postal_code: shipment["ShipFromAddress"]["PostalCode"],
                              shipment_status: shipment["ShipmentStatus"],
                              city: shipment["ShipFromAddress"]["City"],
                              name: shipment["ShipmentName"])
      TransportContentService.new(nil, @shipment_id).sync_transport_with_amazon
      inbound_shipment
    end
  end

  def void_transport_plan
    begin
      shipment_id = @shipment_id.presence || @transport_content.inbound_shipment.shipment_id
      response = @client.void_transport_request(shipment_id)
      TransportContentService.new(nil, @shipment_id).sync_transport_with_amazon
      {success: true}
    rescue => e
      {success: false, error: e}
    end
  end

  def self.generate_shipments_from_amazon user = nil
    client = TransportContentService.set_client user
    response = client.list_inbound_shipments(shipment_status_list: ["WORKING"])
    shipments = response.data["ShipmentData"]["member"]
    p shipments
    if shipments.class == Hash
      inbound_shipment = user.inbound_shipments.find_or_create_by(shipment_id: shipments["ShipmentId"])
      inbound_shipment.update(label_prep_type: shipments["LabelPrepType"],
                              destination_fulfillment_center_id: shipments["DestinationFulfillmentCenterId"],
                              state_or_province_code: shipments["ShipFromAddress"]["StateOrProvinceCode"],
                              postal_code: shipments["ShipFromAddress"]["PostalCode"],
                              shipment_status: shipments["ShipmentStatus"],
                              city: shipments["ShipFromAddress"]["City"],
                              name: shipments["ShipmentName"])
    else
      shipments.each do |shipment|
        begin
          inbound_shipment = user.inbound_shipments.find_or_create_by(shipment_id: shipment["ShipmentId"])
          inbound_shipment.update(label_prep_type: shipment["LabelPrepType"],
                                  destination_fulfillment_center_id: shipment["DestinationFulfillmentCenterId"],
                                  state_or_province_code: shipment["ShipFromAddress"]["StateOrProvinceCode"],
                                  postal_code: shipment["ShipFromAddress"]["PostalCode"],
                                  shipment_status: shipment["ShipmentStatus"],
                                  city: shipment["ShipFromAddress"]["City"],
                                  name: shipment["ShipmentName"])
        rescue => e
        end
      end
    end
  end

  def self.sync_all_shipments_with_amazon user = nil
    client = TransportContentService.set_client user
    shipment_ids = InboundShipment.all.map(&:shipment_id)
    shipment_ids.each do |shipment_id|
      TransportContentService.new(nil, shipment_id).sync_transport_with_amazon user
    end
  end

  def mark_as_shipped
    begin
      shipment = InboundShipment.find_by(shipment_id: @shipment_id)
      TransportContentService.new(nil, @shipment_id).sync_shipment
      return { success: false, error: "Shipment cannot be marked as SHIPPED. Current Status #{shipment.shipment_status}"} if shipment.shipment_status != "WORKING"
      adderss = {
        name: shipment.name,
        address_line1: shipment.address_line1,
        address_line2: shipment.address_line2,
        city: shipment.city,
        state_or_province_code: shipment.state_or_province_code,
        postal_code: shipment.postal_code,
        country_code: shipment.country_code
      }
      inbound_shipment_header = {
        shipment_name: shipment.shipment_name,
        ship_from_address: adderss,
        destination_fulfillment_center_id: shipment.destination_fulfillment_center_id,
        are_cases_required: shipment.are_cases_required,
        shipment_status: "SHIPPED",
      }
      response = @client.update_inbound_shipment(shipment.shipment_id, inbound_shipment_header, []).parse
      TransportContentService.new(nil, @shipment_id).sync_shipment
      {success: true}
    rescue => e
      { success: false, error: e.to_s}
    end
  end

  private

  def self.set_client user = nil
    merchant_id = user.try(:seller_id).try(:presence)
    mws_access_token = user.try(:mws_access_token).try(:presence)
    @client = MWS.fulfillment_inbound_shipment(
      merchant_id: merchant_id,
      aws_secret_access_key: "L2P4I5DPK99Ta/xWYgmZ4nnZvzE960FQbAZyIxjd",
      marketplace: "ATVPDKIKX0DER",
      aws_access_key_id: "AKIAIB5ZWGDEZQZFMISA",
      auth_token: mws_access_token)
  end

  def dummy_pallete? pallete_info
    [pallete_info["Weight"]["Value"], pallete_info["Dimensions"]["Width"], pallete_info["Dimensions"]["Height"], pallete_info["Dimensions"]["Length"]].uniq[0].to_i == 25
  end
end
