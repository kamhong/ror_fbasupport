

class ProcessDeletevendoritemWorker
  include Sidekiq::Worker
  
  sidekiq_options :retry => false, :backtrace => true, :queue => :default

  def perform(vendor_id)

    puts "Processing delete vendor item"
    vendor_item = Vendoritem.where(:vendor_id => vendor_id)
    vendor_item.destroy_all         
  end
end

  