require 'net/http'
require 'peddler'

class CheckUserMwsValidWorker
  include Sidekiq::Worker
  
  sidekiq_options :retry => false, :backtrace => true, :queue => :critical

  def perform(user_id)
    mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
    mws_developer_secret = "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"

    user = User.find(user_id)
    puts(user_id)
    begin
      client = MWS.products(marketplace: user.mws_market_place_id,
        merchant_id: user.seller_id,
        auth_token: user.mws_access_token,
        aws_access_key_id: mws_developer_id,
        aws_secret_access_key: mws_developer_secret)
      report = client.get_service_status().parse
      puts report["Status"].to_s
      if report["Status"].to_s == "GREEN"
        user.mws_key_valid = true
      else
        user.mws_key_valid = false
      end    
      
    rescue Exception => error
      user.mws_key_valid = false
      puts error
    end
    user.save
    if user.mws_key_valid
      subscription = MWS.subscriptions(marketplace: user.mws_market_place_id,
        merchant_id: user.seller_id,
        auth_token: user.mws_access_token,
        aws_access_key_id: mws_developer_id,
        aws_secret_access_key: mws_developer_secret)
      begin
        subscription.register_destination("https://sqs.us-west-2.amazonaws.com/568804649181/fba_support_mws_change_offer",  user.mws_market_place_id)
      rescue Exception=>error
      end

      begin
        result = subscription.create_subscription("AnyOfferChanged",  "https://sqs.us-west-2.amazonaws.com/568804649181/fba_support_mws_change_offer", user.mws_market_place_id)
      rescue Exception=>error
      end

      begin
        params = {
          "SellerId": user.seller_id,
          "MarketplaceId": "ATVPDKIKX0DER",
          "AccountName": user.full_name,
          "AuthenticationToken": user.mws_access_token,
          "UserId": user.id,
          "MWSKey": "",
          "MWSSecret": "",
          "AWSKey": "",
          "AWSSecret": ""
        }
        created_api = HTTParty.post(
          'http://skynet2.azurewebsites.net/api/Admin/CreateApiAccount',
          :body => params.to_json,
          :headers => {
            'Content-Type' => 'application/json',
            'apikey' => '9A38277F-721A-4473-90B9-9780BC02996A',
          }
        )
        api_key = created_api['UserApiKey']
        user.skynet_key = api_key
        user.save
      rescue Exception => error
      end
    end
  end
end
