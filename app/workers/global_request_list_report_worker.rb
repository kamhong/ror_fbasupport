require 'peddler'
class GlobalRequestListReportWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :default
  def perform()
    mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
    mws_developer_secret = "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"

    User.where(mws_key_valid: true).find_each do |user|
      user.mws_report_requests.not_listed.each do |report|
        begin
          client = MWS.reports(marketplace: user.mws_market_place_id,
            merchant_id: user.seller_id,
            aws_access_key_id: mws_developer_id,
            auth_token: user.mws_access_token,
            aws_secret_access_key: mws_developer_secret)
          
          opt = {
            report_request_id_list: [report.report_request_id],
          }

          response = client.get_report_request_list(opt).parse
          response = response["ReportRequestInfo"]

          report.attempt = report.attempt.to_i + 1
          unless response["GeneratedReportId"].blank?
            report.report_id = response["GeneratedReportId"]
            report.report_read_status = "ready"
          end
          if response["ReportProcessingStatus"].to_s == "_CANCELLED_" || response["ReportProcessingStatus"].to_s == "_DONE_NO_DATA_"
            report.report_read_status = "read"
          end
          report.save
        rescue Exception => error
          puts "Error to request report_list"
          puts error
        end
      end
    end
  end
end
