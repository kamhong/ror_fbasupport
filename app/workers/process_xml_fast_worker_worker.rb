class ProcessXmlFastWorkerWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :default
  
  def perform(xml_str)
    puts "Processing xml data in ProcessXmlWorker"

    begin
      parser = Saxerator.parser(xml_str)
      vendor_id = 0;
      asd = "Start"
      parser.for_tag("VendorId").each do |items|
        vendor_id = items.to_i
      end

      # Much faster way to find the VendorId 
      # output = `"grep #{xml_str} " | awk -F">" '{print $2}' | awk -F"<" '{print $1}'` 
      # output = system "grep #{xml_str} 'VendorId' | awk -F\">\" '{print $2}' | awk -F\"<\" '{print $1}'"
      # output = output.to_i
      # vendor_id = output.to_id
      # asd = asd + vendor_id.to_s

      puts "Found vendor id #{vendor_id} from xml file"
    

      if vendor_id > 0

        puts "Finding required vendor"
        vendor = Vendor.find(vendor_id)

        if !vendor.blank?
          user = vendor.user
          asin_columns = [:asin, :brand_id, :name, :salesrank, :packagequantity, :buyboxprice, :amazonoffer,  :totalfbafee, :fbafee, :storagefee, :variableclosingfee, :commissionpct, :commissiionfee, :salespermonth, :totaloffers, :fbaoffers, :fbmoffers, :lowestfbaoffer, :lowestfbmoffer, :product_type_id, :ranked_category_id, :product_groups_id, :weight, :length, :width, :height, :packageweight, :packageheight, :packagewidth, :packagelength, :notes, :review, :numreview, :small_image, :large_image, :ean, :mpn, :isbuyboxfba, :invalidid]
          vendor_column = [:asin, :vendorasin_id, :vendortitle, :upc, :cost, :vendorsku, :packcount, :vendor_id]
          asin_values = []
          vendor_values = []
          i = 0
          parser.for_tag("OutputRowInProcess").each do |item|
            
            i += 1
            asintext = item['ASIN'].to_s
            if asintext.empty?
              next
            end

            brandtext = item['Brand'].to_s
            mpn = item['MPN'].to_s
            ean = item['EAN'].to_s
            itemname = item['ItemName'].to_s
            rank = item['SalesRank'].to_s.to_i
            rankcategorytext = item['SalesRankCategory'].to_s
            packagequantity = item['PackageQuantity'].to_s.to_i
            buyboxprice = item['BuyBoxPrice'].to_s.to_f
            amazonoffers = item['AmazonOffers'].to_s.to_i
            productgroup_text = item['ProductGroup'].to_s
            producttype_text = item['ProductTypeName'].to_s
            itemweight = item['ItemWeight'].to_s.to_f
            itemheight = item['ItemHeight'].to_s.to_f
            itemwidth = item['ItemWidth'].to_s.to_f
            itemlength = item['ItemLength'].to_s.to_f
            packageweight = item['PackageWeight'].to_s.to_f
            packagelength = item['PackageLength'].to_s.to_f
            packagewidth = item['PackageWidth'].to_s.to_f
            packageheight = item['PackageHeight'].to_s.to_f
            notes = item['Notes'].to_s
            fbafee = item['FBAFee'].to_s.to_s.to_f
            storagefee = item['StorageFee'].to_s.to_f
            variableclosingfee = item['VariableClosingFee'].to_s.to_f
            commissionfee = item['CommissionFee'].to_s.to_f
            commissionpct = item['CommissionPct'].to_s.to_f
            totalfbafee = item['TotalFBAFee'].to_s.to_f
            numtotaloffers = item['NumTotalOffers'].to_s.to_i
            numfbaoffer = item['NumFBAOffers'].to_s.to_i
            numfbmoffer = item['NumMFNOffers'].to_s.to_i
            lowestfbaoffer = item['LowestFBAOffer'].to_s.to_f
            lowestfbmoffer = item['LowestMFNOffer'].to_s.to_f
            purchaseprice = item['PurchasePrice'].to_s.to_f
            invalidid = item['InvalidId'].to_s
            estsalespermonth = item['EstSalesPerMonth'].to_s.to_i
            isbuyboxfba = item['IsBuyboxFBA'].to_s
            reviewrating = item['ReviewRating'].to_s.to_f
            numreview = item['NumReviews'].to_s.to_i
            small_image = item['SmallImage'].to_s
            large_image = small_image.gsub('._SL75', '')

            upc = item['UPC'].to_s
            vendorsku =item['VendorSKU'].to_s

            rankcategory = RakedCategory.where(:name => rankcategorytext).first_or_create
            brand = Brand.where(:name => brandtext).first_or_create
            productgroup = ProductGroup.where(:name => productgroup_text).first_or_create
            producttype = ProductType.where(:name => producttype_text).first_or_create

            if isbuyboxfba == 'true'
              isbuyboxfba = true
            else
              isbuyboxfba = false
            end

            if invalidid == 'true'
              invalidid = true
            else
              invalidid = false
            end

            asin_values << [asintext, brand.id, itemname, rank, packagequantity, buyboxprice, amazonoffers,  totalfbafee, fbafee, storagefee, variableclosingfee, commissionpct, commissionfee, estsalespermonth, numtotaloffers, numfbaoffer, numfbmoffer, lowestfbaoffer, lowestfbmoffer, producttype.id, rankcategory.id, productgroup.id, itemweight, itemlength, itemwidth, itemheight, packageweight, packageheight, packagewidth, packagelength, notes, reviewrating, numreview, small_image, large_image, ean, mpn, isbuyboxfba, invalidid]

            vendor_values << [asintext, 0, itemname, upc, purchaseprice, vendorsku, packagequantity, vendor.id]
            # vendoritem = vendor.vendoritems.where(:asin => asintext).first_or_create
            # vendoritem.vendorasin_id = asin.id                      # vendoritem id : ID
            # vendoritem.vendortitle = itemname
            # vendoritem.upc = upc                                    # vendoritem upc : UPC
            # vendoritem.cost = purchaseprice.to_f                                  # vendoritem cost : COST
            # vendoritem.vendorsku = vendorsku                        # vendoritem vendorsku : VendorSKU
            # vendoritem.packcount = packageqty.to_i

            # sku = user.seller_skus.where(:vendorasin_id => asin.id).first
            # if sku.nil?
            #   vendoritem.seller_sku_id = nil
            # else
            #   puts "updated sellersku"
            #   puts sku.sku
            #   vendoritem.seller_sku_id = sku.id
            # end
            # vendoritem.save!
          end
          Vendorasin.import asin_columns, asin_values, on_duplicate_key_update: :all
          Vendoritem.import vendor_column, vendor_values, on_duplicate_key_update: :all

          sql = "UPDATE vendoritems INNER JOIN vendorasins on vendoritems.asin = vendorasins.asin SET vendoritems.vendorasin_id = vendorasins.id where vendoritems.vendor_id = #{vendor.id} and vendoritems.vendorasin_id = 0 "
          ActiveRecord::Base.connection.execute(sql)

          sql = "UPDATE vendoritems LEFT JOIN (SELECT * from seller_skus where user_id = #{user.id} ) as SKU on SKU.vendorasin_id = vendoritems.vendorasin_id SET vendoritems.seller_sku_id = SKU.id where vendoritems.vendor_id = #{vendor.id}"
          ActiveRecord::Base.connection.execute(sql)

          if vendor.supplier_prefix.blank?
            first_item = vendor.vendoritems.where("LENGTH(vendorsku) > 5").first
            unless first_item.nil?
              vendor.supplier_prefix = first_item.vendorsku[0...4]
              vendor.save
            end
          end
        else 
          puts "Vendor not found in database"
        end
      end
    rescue Exception => e

      if Rails.env.production?
        S3_BUCKET.put_object({
          acl: 'public-read',
          body: 'Error Occur',
          key: "uploads/local/import_xml.txt"
        })
      end
      puts "Failed worker with exception #{e}"
    end
  end
end
