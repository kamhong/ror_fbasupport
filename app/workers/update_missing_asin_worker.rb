require 'net/http'
include Rails.application.routes.url_helpers

class UpdateMissingAsinWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :default
  def perform()
    puts "UpdateMissingAsinWorker started"
    missings = Vendorasin.needed_updated
    if missings.load.count == 0
      return
    end
    file_csv = CSV.generate do |csv|
      csv << ["ASIN","SKU","COST","ID"]
      missings.needed_updated.each do |asin|
        csv << [asin.asin, asin.asin, 0, asin.id]
      end
    end

    missings.update_all(fetched: 2)
    
    restored_filename = Digest::SHA1.hexdigest Time.now.to_s
    obj = S3_BUCKET.put_object({
        key: "uploads/#{restored_filename}",
        body: file_csv,
        acl: 'public-read'
    })
    puts restored_filename
    server_url = "http://http://54.184.212.98/"
    if Rails.env.production?
      server_url = "https://fba.support/"
    end
    skynetparams = {
      "InputfileUrl"                => "https://fba-skynets-csvs.s3.amazonaws.com/uploads/#{restored_filename}",
      "InputfileName"               => "MissingAsin.csv", 
      "IdType"                      => 0,  #asin
      "IdHeaderName"                => "ASIN", 
      "CostHeaderName"              => "COST", 
      "VendorSKUHeaderName"         => "SKU", 
      "ShippingCostPerLb"           => 0,
      "VendorId"                    => -1,
      "VendorName"                  => "Missing",
      "GetEstimatedSales"           => true,
      "GetIsAmazonSelling"          => true,
      "WebhookForProgress"          => "#{server_url}updateskynetstatus", 
      "NeedBuyboxEligibleOffers"    => true, 
      "WebhookForComplete"          => "#{server_url}updatemissingasin"
    }
    result = HTTParty.post(
      'http://skynet2.azurewebsites.net/api/ProfitSourcing/CreateTask', 
      :body => skynetparams.to_json,
      :headers => {
        'Content-Type' => 'application/json',
        'apikey'       => 'BC6418E5-D86D-4822-BFEE-E154CB145EB5'
      }
    )
    puts result['Message']

    #To update the supplier code
    Vendor.where("supplier_prefix = '' or supplier_prefix is null").each do |vendor|
      if vendor.supplier_prefix.blank?
        first_item = vendor.vendoritems.where("LENGTH(vendorsku) > 5").first
        unless first_item.nil?
          vendor.supplier_prefix = first_item.vendorsku[0...4]
          vendor.save
          puts vendor.supplier_prefix
        end
      end
    end
  end
end
