class PullInventoryQuantityWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :default

  def perform()
    mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
    mws_developer_secret = "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"

    User.where(mws_key_valid: true).find_each do |user|
      user.mws_report_requests.readable_listed.where(report_type: '_GET_FBA_MYI_ALL_INVENTORY_DATA_').each do |report|
        begin
          
          client = MWS.reports(marketplace: user.mws_market_place_id,
            merchant_id: user.seller_id,
            aws_access_key_id: mws_developer_id,
            auth_token: user.mws_access_token,
            aws_secret_access_key: mws_developer_secret)
         
          response = client.get_report(report.report_id).parse
          response.each  do |line|
            sku = line[0].to_s
            # seller_sku = user.seller_skus.where(:sku => sku).first
            seller_sku = user.seller_skus.where(:sku => sku).first

            newly_created = false
            if seller_sku.nil?
              newly_created = true
              seller_sku = user.seller_skus.create(:sku => sku)
            else
              if seller_sku.user_id != user.id
                next
              end
            end
            seller_sku.fnsku = line[1].to_s

            asin1=line[2].to_s
            asin =  Vendorasin.where(:asin => asin1).first_or_create
            if asin.name.nil? || asin.name.empty?
              asin.fetched = 0
              asin.save
            end
            seller_sku.vendorasin_id = asin.id

            seller_sku.price = line[5].to_f
            seller_sku.reserved_quantity = line[12].to_i
            seller_sku.item_quantity = line[10].to_i
            seller_sku.inbound_quantity = line[15].to_i + line[16].to_i + line[17].to_i

            seller_sku.save

            if newly_created
              puts "#{seller_sku.sku} newly added"
              user.vendoritems.where(:asin => asin1).update_all(:seller_sku_id => seller_sku.id)
            end
            puts seller_sku.id
          end
          report.report_read_status = "read"
          report.save
        rescue Exception => error
          puts "_GET_FBA_MYI_ALL_INVENTORY_DATA_: #{report.id}"
          puts "pull inventory quantity"
          puts error
        end
      end
    end
  end
end
