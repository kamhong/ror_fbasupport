class UpdateDailyInventoryWorker
  include Sidekiq::Worker
  
  sidekiq_options :retry => true, :backtrace => true, :queue => :default
  

  def perform
    User.where(mws_key_valid: true).find_each do |user|
      mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
      mws_developer_secret = "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"
      user.mws_report_requests.readable_listed.where(report_type: '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_').each do |report|
        p user.as_json, report.as_json
        begin
          client = MWS.reports(marketplace: user.mws_market_place_id,
            merchant_id: user.seller_id,
            aws_access_key_id: mws_developer_id,
            auth_token: user.mws_access_token,
            aws_secret_access_key: mws_developer_secret)
          response = client.get_report(report.report_id).parse
          response.each  do |row|
            seller_sku = user.seller_skus.where(sku: row[0]).first_or_create
            inventory_history = seller_sku.inventory_histories.create(
                                            price: row[5],
                                            fulfillable_quantity: row[10],
                                            unsellable_quantity: row[11],
                                            reserved_quantity: row[12],
                                            total_quantity: row[13],
                                            per_unit_volume: row[14],
                                            inbound_working_quantity: row[15],
                                            inbound_shipped_quantity: row[16],
                                            inbound_receiving_quantity: row[17]
                                            )
            seller_sku.update(sales_velocity: (seller_sku.formatted_sales_velocity))
          end
          report.report_read_status = "read"
          report.save
        rescue Exception => error
          puts error
        end
      end
    end
  end
end
