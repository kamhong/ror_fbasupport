require 'aws-sdk'
require 'nokogiri'
class PullOfferChangedWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :critical
  def perform()
    begin
      client = Aws::SQS::Client.new(
        region: 'us-west-2',
        credentials: Aws::Credentials.new('AKIAJN3UNSPYMYI2462Q', 'wrLrMXIvkJEt/Wb7sOGNUin0+gc9ZG+L43fATekw'),
      )

      resp = client.receive_message(queue_url: "https://sqs.us-west-2.amazonaws.com/568804649181/fba_support_mws_change_offer", max_number_of_messages: 10)

      resp.messages.each do |m|
        doc = Nokogiri::XML(m.body)
        
        doc.xpath("//AnyOfferChangedNotification").each do |any_offer_change_notification|
          offer_change_trigger = any_offer_change_notification.xpath("OfferChangeTrigger")
          asin_string = offer_change_trigger.xpath("ASIN").text
          amazon_selling = false
          bbp_seller = ""
          fba_offer = 0
          fbm_offer = 0
          bbp = 0
          rank = 0

          summary = any_offer_change_notification.xpath("Summary")
          number_of_offers = summary.xpath("NumberOfOffers")
          
          number_of_offers.xpath("OfferCount").each do |offer_count|
            fulfillment_channel = offer_count.attribute("fulfillmentChannel").to_s
            
            condition = offer_count.attribute("condition").to_s
            if condition != "new"
              next
            end
            
            if fulfillment_channel == "Amazon"
              fba_offer = offer_count.text.to_i
            elsif fulfillment_channel == "Merchant"
              fbm_offer = offer_count.text.to_i
            end
          end

          summary.xpath("//BuyBoxPrice").each do |buy_box_price|
            condition = buy_box_price.attribute("condition").to_s
            if condition != "new"
              next
            end
            bbp = buy_box_price.xpath("LandedPrice").xpath("Amount").text.to_f
          end

          summary.xpath("//SalesRank").each do |sales_rank|
            if rank < sales_rank.xpath("Rank").text.to_i
              rank = sales_rank.xpath("Rank").text.to_i
            end
          end

          offers = any_offer_change_notification.xpath("Offers")
          offers.xpath("Offer").each do |offer|
            if offer.xpath("IsFulfilledByAmazon").text == "true"
              is_fulfilled_by_amazon = true
            else
              is_fulfilled_by_amazon =  false
            end
            if offer.xpath("IsBuyBoxWinner").text == "true"
              is_buy_box_winner = true
            else
              is_buy_box_winner =  false
            end
            if offer.xpath("IsFeaturedMerchant").text == "true"
              is_featured_merchant = true
            else
              is_featured_merchant =  false
            end
            if offer.xpath("ShipsDomestically").text == "true"
              ships_domestically = true
            else
              ships_domestically =  false
            end
            item = {
              seller_id: offer.xpath("SellerId").text,
              sub_condition: offer.xpath("SubCondition").text,
              seller_positive_feedback_rating: offer.xpath("SellerFeedbackRating").xpath("SellerPositiveFeedbackRating").text.to_i,
              feedback_count: offer.xpath("SellerFeedbackRating").xpath("FeedbackCount").text.to_i,
              listing_price: offer.xpath("ListingPrice").xpath("Amount").text.to_f,
              shipping: offer.xpath("Shipping").xpath("Amount").text.to_f,
              is_buy_box_winner: is_buy_box_winner,
              is_fulfilled_by_amazon: is_fulfilled_by_amazon,
              is_featured_merchant: is_featured_merchant,
              ships_domestically: ships_domestically
            }
            
            asin_seller = AsinSeller.where(:seller_id => item[:seller_id]).where(:asin_str => asin_string).first_or_create
            asin_seller.update(item)

            if is_buy_box_winner
              bbp_seller = item[:seller_id]
              if item[:seller_id] == "ATVPDKIKX0DER"
                amazon_selling = true
              end
            end
          end

          asin = Vendorasin.where(:asin => asin_string).first_or_create
          asin.buyboxprice = bbp
          asin.amazon_selling = amazon_selling
          asin.salesrank = rank
          asin.bbp_seller = bbp_seller
          asin.save

          puts asin.id
        end
        
      end
    rescue Exception => e
      puts e
    end
  end
end
