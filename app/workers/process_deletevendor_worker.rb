

class ProcessDeletevendorWorker
  include Sidekiq::Worker
  
  sidekiq_options :retry => false, :backtrace => true, :queue => :default

  def perform(vendor_id)
    puts "Processing delete vendor"
    @vendor = Vendor.find(vendor_id)
    if @vendor.present?
      # Remove InboundSipplan Item also
      OutboundShipment.joins(pending_po: :pending_po_index).where(:pending_po_indices => {:vendor_id => @vendor.id}).destroy_all
      puts "outbound shipments destoryed"
      PendingPo.joins(:pending_po_index).where(:pending_po_indices => {:vendor_id => @vendor.id}).delete_all
      puts "PendingPO deleted"
      Po.joins(:vendoritem).where(:vendoritems => {:vendor_id => @vendor.id}).delete_all
      puts "PO deleted"
      
      @vendor.delete
    end
  end
end

  