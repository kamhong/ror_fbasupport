require 'peddler'
class GlobalRequestReportWorker
  @@report_list = [
    # "_GET_MERCHANT_LISTINGS_DATA_",
    "_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_",
    "_GET_FBA_MYI_ALL_INVENTORY_DATA_",
    "_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_"
  ]
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :critical
  def perform()
    mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
    mws_developer_secret = "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"

    User.where(mws_key_valid: true).find_each do |user|
      begin
        client = MWS.reports(marketplace: user.mws_market_place_id,
          merchant_id: user.seller_id,
          aws_access_key_id: mws_developer_id,
          auth_token: user.mws_access_token,
          aws_secret_access_key: mws_developer_secret)
        start_date =  1.day.ago
        end_date =  Time.current
        
        opt = {
          marketplace_id_list: user.mws_market_place_id,
          start_date: start_date.iso8601,
          end_date: end_date.iso8601,
        }
        @@report_list.each do |report|
          response = client.request_report(report, opt).parse
          response = response["ReportRequestInfo"]
          
          unless response["ReportRequestId"].empty?
            user.mws_report_requests.create([{report_request_id: response["ReportRequestId"], report_type: report, start_date: start_date, end_date: end_date, report_read_status: 'not_yet', report_processing_status: response["ReportProcessingStatus"], attempt:0}])
            puts response["ReportRequestId"]
          end
        end
        # response = client.request_report(@@report_list[0], opt).parse
        # response = response["ReportRequestInfo"]
        
        # unless response["ReportRequestId"].empty?
        #   user.mws_report_requests.create([{report_request_id: response["ReportRequestId"], report_type: @@report_list[0], start_date: start_date, end_date: end_date, report_read_status: 'not_yet', report_processing_status: response["ReportProcessingStatus"], attempt:0}])
        #   puts response["ReportRequestId"]
        # end
      rescue Exception => error
        puts error
      end
    end
  end
end
