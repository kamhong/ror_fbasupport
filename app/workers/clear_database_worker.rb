class ClearDatabaseWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :default
  def perform()
    puts "#{Vendor.count}"
    Vendor.all.each do |vendor|
      puts "ClearDatabaseWorker #{vendor.id}_Start"
      ids = Vendoritem.where(vendor_id: vendor.id).select("MIN(id) as id").group(:vendor_id,:vendorasin_id).collect(&:id)
      Vendoritem.where(vendor_id: vendor.id).where.not(id: ids).delete_all
      puts "ClearDatabaseWorker #{vendor.id}_End"
    end
    puts "ClearDatabaseWorker End"
  end
end
