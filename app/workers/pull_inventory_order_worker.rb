class PullInventoryOrderWorker
  include Sidekiq::Worker

  sidekiq_options :retry => false, :backtrace => true, :queue => :default

  def perform()
    mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
    mws_developer_secret = "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"

    User.where(mws_key_valid: true).find_each do |user|
      user.mws_report_requests.readable_listed.where(report_type: '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_').each do |report|
        begin
          client = MWS.reports(marketplace: user.mws_market_place_id,
            merchant_id: user.seller_id,
            aws_access_key_id: mws_developer_id,
            auth_token: user.mws_access_token,
            aws_secret_access_key: mws_developer_secret)

          response = client.get_report(report.report_id).parse
          response.each  do |line|
            amazon_order_id = line["amazon-order-id"].to_s
            merchant_order_id = line["merchant-order-id"].to_s
            purchase_date = Time.parse(line["purchase-date"])
            last_updated_date = Time.parse(line["last-updated-date"])
            order_status = line["order-status"].to_s
            unless order_status == 'Shipped' || order_status == 'Pending'
              next
            end
            
            fulfillment_channel = line["fulfillment-channel"].to_s
            sales_channel = line["sales-channel"].to_s
            order_channel = line["order-channel"].to_s
            url = line["url"].to_s
            ship_service_level = line["ship-service-level"].to_s
            product_name = line["product-name"].to_s
            sku = line["sku"].to_s
            asin = line["asin"].to_s
            item_status = line["item-status"].to_s
            
            quantity = line["quantity"].to_i
            currency = line["currency"].to_s
            item_price = line["item-price"].to_f
            item_tax = line["item-tax"].to_f
            shipping_price = line["shipping-price"].to_f
            shipping_tax = line["shipping-tax"].to_f
            gift_wrap_price = line["gift-wrap-price"].to_f
            gift_wrap_tax = line["gift-wrap-tax"].to_f
            item_promotion_discount = line["item-promotion-discount"].to_f
            ship_promotion_discount = line["ship-promotion-discount"].to_f
            ship_city = line[26].to_s
            ship_state = line[27].to_s
            ship_postal_code = line[28].to_s
            ship_country = line[29].to_s
            promotion_ids = line[30].to_s

            order = user.mws_orders.where(:amazon_order_id => amazon_order_id).first_or_create
            order.merchant_order_id = merchant_order_id
            order.purchase_date = purchase_date
            order.last_updated_date = last_updated_date
            order.order_status = order_status
            order.fulfillment_channel = fulfillment_channel
            order.sales_channel = sales_channel
            order.order_channel = order_channel
            order.url = url
            order.ship_service_level = ship_service_level
            order.ship_city = ship_city
            order.ship_postal_code = ship_postal_code
            order.ship_state = ship_state
            order.ship_country = ship_country

            unless !order.save || order.nil?
              order_item = order.mws_order_items.where(:sku => sku).first_or_create

              order_item.product_name = product_name
              order_item.sku = sku
              order_item.asin = asin
              order_item.item_status = item_status
              order_item.quantity = quantity
              order_item.currency = currency
              order_item.item_price = item_price
              order_item.item_tax = item_tax
              order_item.shipping_price = shipping_price
              order_item.shipping_tax = shipping_tax
              order_item.gift_wrap_price = gift_wrap_price
              order_item.gift_wrap_tax = gift_wrap_tax
              order_item.item_promotion_discount = item_promotion_discount
              order_item.ship_promotion_discount = ship_promotion_discount
              order_item.promotion_ids = promotion_ids

              order_item.save

              puts order_item.id
            end
          end
          report.report_read_status = "read"
          report.save
        rescue Exception => error
          puts "PullInventoryOrderWorker_ERROR_#{report.report_id}"
          puts error
          report.report_read_status = "read"
          report.save
        end
      end
    end
  end
end
