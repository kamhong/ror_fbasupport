
require 'aws-sdk'
require 'saxerator'
require 'nokogiri'
class PullAsinSellerWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :critical
  def perform()
    begin
      client = Aws::SQS::Client.new(
        region: 'us-west-2',
        credentials: Aws::Credentials.new('AKIAJN3UNSPYMYI2462Q', 'wrLrMXIvkJEt/Wb7sOGNUin0+gc9ZG+L43fATekw'),
      )

      resp = client.receive_message(queue_url: "https://sqs.us-west-2.amazonaws.com/568804649181/fba_support_mws_change_offer", max_number_of_messages: 10)

      resp.messages.each do |m|
        doc = Nokogiri::XML(m.body)
        
        doc.xpath("//AnyOfferChangedNotification").each do |any_offer_change_notification|
          offer_change_trigger = any_offer_change_notification.xpath("OfferChangeTrigger")
          asin_string = offer_change_trigger.xpath("ASIN").text
          amazon_selling = false
          bbp_seller = ""
          fba_offer = 0
          fbm_offer = 0
          bbp = 0
          rank = 0

          summary = any_offer_change_notification.xpath("Summary")
          number_of_offers = summary.xpath("NumberOfOffers")
          
          number_of_offers.xpath("OfferCount").each do |offer_count|
            fulfillment_channel = offer_count.attribute("fulfillmentChannel").to_s
            
            condition = offer_count.attribute("condition").to_s
            if condition != "new"
              next
            end
            
            if fulfillment_channel == "Amazon"
              fba_offer = offer_count.text.to_i
            elsif fulfillment_channel == "Merchant"
              fbm_offer = offer_count.text.to_i
            end
          end

          summary.xpath("//BuyBoxPrice").each do |buy_box_price|
            condition = buy_box_price.attribute("condition").to_s
            if condition != "new"
              next
            end
            bbp = buy_box_price.xpath("LandedPrice").xpath("Amount").text.to_f
          end

          summary.xpath("//SalesRank").each do |sales_rank|
            if rank < sales_rank.xpath("Rank").text.to_i
              rank = sales_rank.xpath("Rank").text.to_i
            end
          end

          offers = any_offer_change_notification.xpath("Offers")
          offers.xpath("Offer").each do |offer|
            if offer.xpath("IsFulfilledByAmazon").text == "true"
              is_fulfilled_by_amazon = true
            else
              is_fulfilled_by_amazon =  false
            end
            if offer.xpath("IsBuyBoxWinner").text == "true"
              is_buy_box_winner = true
            else
              is_buy_box_winner =  false
            end
            if offer.xpath("IsFeaturedMerchant").text == "true"
              is_featured_merchant = true
            else
              is_featured_merchant =  false
            end
            if offer.xpath("ShipsDomestically").text == "true"
              ships_domestically = true
            else
              ships_domestically =  false
            end
            item = {
              seller_id: offer.xpath("SellerId").text,
              sub_condition: offer.xpath("SubCondition").text,
              seller_positive_feedback_rating: offer.xpath("SellerFeedbackRating").xpath("SellerPositiveFeedbackRating").text.to_i,
              feedback_count: offer.xpath("SellerFeedbackRating").xpath("FeedbackCount").text.to_i,
              listing_price: offer.xpath("ListingPrice").xpath("Amount").text.to_f,
              shipping: offer.xpath("Shipping").xpath("Amount").text.to_f,
              is_buy_box_winner: is_buy_box_winner,
              is_fulfilled_by_amazon: is_fulfilled_by_amazon,
              is_featured_merchant: is_featured_merchant,
              ships_domestically: ships_domestically
            }
            
            asin_seller = AsinSeller.where(:seller_id => item[:seller_id]).where(:asin_str => asin_string).first_or_create
            asin_seller.update(item)

            if is_buy_box_winner
              bbp_seller = item[:seller_id]
              if item[:seller_id] == "ATVPDKIKX0DER"
                amazon_selling = true
              end
            end
          end

          asin = Vendorasin.where(:asin => asin_string).first_or_create
          asin.buyboxprice = bbp
          asin.amazon_selling = amazon_selling
          asin.salesrank = rank
          asin.bbp_seller = bbp_seller
          asin.fbaoffers = fba_offer
          asin.fbmoffers = fbm_offer
          asin.save

          puts asin.id
        end
        
      end
    rescue Exception => e
      puts e
    end
  end

  # def perform()
  #   begin
  #     client = Aws::SQS::Client.new(
  #       region: 'us-west-2',
  #       credentials: Aws::Credentials.new('AKIAJN3UNSPYMYI2462Q', 'wrLrMXIvkJEt/Wb7sOGNUin0+gc9ZG+L43fATekw'),
  #     )

  #     resp = client.receive_message(queue_url: "https://sqs.us-west-2.amazonaws.com/568804649181/fba_support_mws_change_offer", max_number_of_messages: 10)

  #     resp.messages.each do |m|
        
  #       parser = Saxerator.parser(m.body)
  #       parser.for_tag("AnyOfferChangedNotification").each do |any_offer_changed_notification|
  #         offer_change_trigger = any_offer_changed_notification["OfferChangeTrigger"]
  #         asin_string = offer_change_trigger["ASIN"].to_s
  #         bbp = 0
  #         rank = 0
  #         amazon_selling = false
  #         bbp_seller = ""
  #         items = []

  #         summary = any_offer_changed_notification["Summary"]
  #         if summary.nil?
  #           next
  #         end

  #         if summary["BuyBoxPrices"].nil?
  #           next
  #         end

  #         buy_box_prices = summary["BuyBoxPrices"]["BuyBoxPrice"]
  #         unless buy_box_prices.nil?
            
  #           if buy_box_prices.kind_of?(Saxerator::Builder::ArrayElement)
  #             buy_box_prices.each do |buy_box_price|
  #               if buy_box_price["LandedPrice"]["Amount"].to_f > bbp
  #                 bbp = buy_box_price["LandedPrice"]["Amount"].to_f
  #               end
  #             end
  #           else
  #             if buy_box_prices["LandedPrice"]["Amount"].to_f > bbp
  #               bbp = buy_box_prices["LandedPrice"]["Amount"].to_f
  #             end
  #           end
  #         end

  #         sales_rankings = summary["SalesRankings"]["SalesRank"]
  #         unless sales_rankings.nil?
  #           if sales_rankings.kind_of?(Saxerator::Builder::ArrayElement)
  #             sales_rankings.each do |sales_rank|
  #               if sales_rank["Rank"].to_i > rank
  #                 rank = sales_rank["Rank"].to_i
  #               end
  #             end
  #           else
  #             sales_rank = sales_rankings
  #             if sales_rankings["Rank"].to_i > rank
  #               rank = sales_rankings["Rank"].to_i
  #             end
  #           end
  #         end

  #         offers = any_offer_changed_notification["Offers"]["Offer"]
  #         unless offers.nil?
  #           if offers.kind_of?(Saxerator::Builder::ArrayElement)
  #             offers.each do |offer|
                
  #               if offer["IsBuyBoxWinner"] == "true"
  #                 if offer["SellerId"] == "ATVPDKIKX0DER"
  #                   amazon_selling = true
  #                 end
  #                 bbp_seller = offer["SellerId"].to_s
  #               end
  #               ships_domestically = false
  #               is_buy_box_winner = false
  #               is_fulfilled_by_amazon = false
  #               is_featured_merchant = false

  #               if offer["IsBuyBoxWinner"].to_s == "true"
  #                 is_buy_box_winner = true
  #               end
  #               if offer["IsFulfilledByAmazon"].to_s == "true"
  #                 is_fulfilled_by_amazon = true
  #               end
  #               if offer["IsFeaturedMerchant"].to_s == "true"
  #                 is_featured_merchant = true
  #               end
  #               if offer["ShipsDomestically"].to_s == "true"
  #                 ships_domestically = true
  #               end

  #               item = {
  #                 seller_id: offer["SellerId"].to_s,
  #                 asin_str: asin_string,
  #                 sub_condition: offer["SubCondition"].to_s,
  #                 seller_positive_feedback_rating: offer["SellerFeedbackRating"]["SellerPositiveFeedbackRating"].to_i,
  #                 feedback_count: offer["SellerFeedbackRating"]["FeedbackCount"].to_i,
  #                 listing_price: offer["ListingPrice"]["Amount"].to_f,
  #                 shipping: offer["Shipping"]["Amount"].to_f,
  #                 is_buy_box_winner: is_buy_box_winner,
  #                 is_fulfilled_by_amazon: is_fulfilled_by_amazon,
  #                 is_featured_merchant: is_featured_merchant,
  #                 ships_domestically: ships_domestically
  #               }
                
  #               items << item
  #               asin_seller = AsinSeller.where(:seller_id => offer["SellerId"].to_s).where(:asin_str => asin_string).first_or_create
  #               asin_seller.update(item)
  #             end
  #           else
              
  #             offer = offers
  #             if offer["IsBuyBoxWinner"] == "true"
  #               if offer["SellerId"] == "ATVPDKIKX0DER"
  #                 amazon_selling = true
  #               end
  #               bbp_seller = offer["SellerId"].to_s
  #             end

  #             ships_domestically = false
  #             is_buy_box_winner = false
  #             is_fulfilled_by_amazon = false
  #             is_featured_merchant = false

  #             if offer["IsBuyBoxWinner"].to_s == "true"
  #               is_buy_box_winner = true
  #             end
  #             if offer["IsFulfilledByAmazon"].to_s == "true"
  #               is_fulfilled_by_amazon = true
  #             end
  #             if offer["IsFeaturedMerchant"].to_s == "true"
  #               is_featured_merchant = true
  #             end
  #             if offer["ShipsDomestically"].to_s == "true"
  #               ships_domestically = true
  #             end
  #             item = {
  #               seller_id: offer["SellerId"].to_s,
  #               asin_str: asin_string,
  #               sub_condition: offer["SubCondition"].to_s,
  #               seller_positive_feedback_rating: offer["SellerFeedbackRating"]["SellerPositiveFeedbackRating"].to_i,
  #               feedback_count: offer["SellerFeedbackRating"]["FeedbackCount"].to_i,
  #               listing_price: offer["ListingPrice"]["Amount"].to_f,
  #               shipping: offer["Shipping"]["Amount"].to_f,
  #               is_buy_box_winner: is_buy_box_winner,
  #               is_fulfilled_by_amazon: is_fulfilled_by_amazon,
  #               is_featured_merchant: is_featured_merchant,
  #               ships_domestically: ships_domestically
  #             }
  #             items << item
  #             asin_seller = AsinSeller.where(:seller_id => offer["SellerId"].to_s).where(:asin_str => asin_string).first_or_create
  #             asin_seller.update(item)
  #           end
  #         end
          

  #         asin = Vendorasin.where(:asin => asin_string).first_or_create
  #         asin.buyboxprice = bbp
  #         asin.amazon_selling = amazon_selling
  #         asin.salesrank = rank
  #         asin.bbp_seller = bbp_seller
  #         asin.save

  #         puts bbp_seller
  #         # AsinSeller.create(items)
  #       end
  #     end
  #   rescue Exception => e
  #     puts e
  #   end
  # end
end
