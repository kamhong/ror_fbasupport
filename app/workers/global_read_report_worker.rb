require 'peddler'
class GlobalReadReportWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :default
  def perform()
    mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
    mws_developer_secret = "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"
    return
    User.where(mws_key_valid: true).find_each do |user|
      user.mws_report_requests.readable_listed.where(report_type: '_GET_MERCHANT_LISTINGS_DATA_').each do |report|
        begin
          puts "Repport ID: #{report.id}"
          client = MWS.reports(marketplace: user.mws_market_place_id,
            merchant_id: user.seller_id,
            aws_access_key_id: mws_developer_id,
            auth_token: user.mws_access_token,
            aws_secret_access_key: mws_developer_secret)
          # client = MWS.reports(marketplace: user.mws_market_place_id,

          #   merchant_id: user.seller_id,
          #   aws_access_key_id: user.mws_access_key_id,
          #   auth_token: user.mws_access_token,
          #   aws_secret_access_key: user.mws_access_acces_key)
         
          response = client.get_report(report.report_id).parse
          if report.report_type == '_GET_MERCHANT_LISTINGS_DATA_'
            
            response.each  do |line|
              
              seller_sku=line[3].to_s
              # new_sku = user.seller_skus.where(:sku => seller_sku).first
              new_sku = user.seller_skus.where(:sku => seller_sku).first
              newly_created = false
              if new_sku.nil?
                puts "new SKU #{seller_sku}"
                newly_created = true
                new_sku = user.seller_skus.create(:sku => seller_sku)
              else
                if new_sku.user_id != user.id
                  puts "Dup_SKU #{user.id} #{seller_sku}"
                  next
                end
              end
              
              #update price, qty
              #   new_sku.vendorasin.nil? 
              #   next
              # end
              item_name=line[0].to_s
              new_sku.item_name = item_name

              item_description=line[1].to_s
              new_sku.item_description = item_description
              
              listing_id=line[2].to_s
              new_sku.listing_id = listing_id
              
              # seller_sku=line[3].to_s
              
              price=line[4].to_f
              new_sku.price = price
              
              quantity=line[5].to_i
              new_sku.item_quantity = quantity
              
              open_date=Time.parse(line[6])
              new_sku.open_date = open_date
              
              image_url=line[7].to_s
              new_sku.item_name = item_name
              
              # item_is_marketplace=line[8]
              product_id_type=line[9].to_s
              new_sku.product_id_type = product_id_type
              
              # zshop_shipping_fee=line[10]
              item_note=line[11].to_s
              new_sku.item_note = item_note
              
              item_condition=line[12].to_s
              new_sku.item_condition = item_condition
              
              # zshop_category1=line[13]
              # zshop_browse_path=line[14]
              # zshop_storefront_feature=line[15]
              asin1=line[16].to_s
              asin =  Vendorasin.where(:asin => asin1).first_or_create
              if asin.name.nil? || asin.name.empty?
                asin.fetched = 0
                asin.save
              end
              new_sku.vendorasin_id = asin.id
              # asin2=line[17]
              # asin3=line[18]
              # will_ship_internationally=line[19]
              # expedited_shipping=line[20]
              # zshop_boldface=line[21]
              # product_id=line[22]
              # bid_for_featured_placement=line[23]
              # add_delete=line[24]
              # pending_quantity=line[25]
              fulfillment_channel=line[26].to_s
              new_sku.fulfillment_channel = fulfillment_channel
              
              # merchant_shipping_group=line[27]
              
              new_sku.save

              if newly_created
                puts "#{new_sku.sku} newly added"
                user.vendoritems.where(:asin => asin1).update_all(:seller_sku_id => new_sku.id)
              end
              
              # puts new_sku.id
            end
            report.report_read_status = "read"
            report.save
        end
          
        rescue Exception => error
          puts error
        end
      end
    end
  end
end
