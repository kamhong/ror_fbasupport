class PullInboundShipmentWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false, :backtrace => true, :queue => :critical
  def perform()
    mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
    mws_developer_secret = "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"
    User.where(mws_key_valid: true).find_each do |user|
      begin
        client = MWS.fulfillment_inbound_shipment(marketplace: user.mws_market_place_id,
          merchant_id: user.seller_id,
          aws_access_key_id: mws_developer_id,
          auth_token: user.mws_access_token,
          aws_secret_access_key: mws_developer_secret)

        start_date =  1.day.ago.iso8601
        end_date =  Time.current.iso8601

        response = client.list_inbound_shipments({
          :shipment_status_list => ['CLOSED','DELETED','RECEIVING','WORKING','SHIPPED','IN_TRANSIT','CHECKED_IN','DELIVERED','ERROR','CANCELLED'],
          :last_updated_after => start_date,
          :last_updated_before => end_date
        }).parse
        unless response["ShipmentData"].nil?
          parse_shipment(response["ShipmentData"], user.id)  
        end
        token = response["NextToken"]
        while token
          response = client.list_inbound_shipments_by_next_token(token).parse
          unless response["ShipmentData"].nil?
            parse_shipment(response["ShipmentData"], user.id)  
          end
          
          token = response["NextToken"]
        end
      rescue Exception => exception
        puts exception
      end
      
    end
  end

  def parse_shipment(shipment_data, user_id)
    user = User.find(user_id)
    members = []
    if shipment_data["member"].kind_of?(Array)
      members = shipment_data["member"]
    else
      members << shipment_data["member"]
    end
    members.each do |member|
      shipment_id = member["ShipmentId"]
      if shipment_id.blank?
        next
      end
      inbound_shipment = user.inbound_shipments.where(:shipment_id => shipment_id).first_or_create
      inbound_shipment.label_prep_type = member["LabelPrepType"]
      inbound_shipment.destination_fulfillment_center_id = member["DestinationFulfillmentCenterId"]
      inbound_shipment.shipment_name = member["ShipmentName"]
      inbound_shipment.shipment_status = member["ShipmentStatus"]
      inbound_shipment.are_cases_required = member["AreCasesRequired"].to_s == "true"
      inbound_shipment.label_prep_type = member["LabelPrepType"]
      inbound_shipment.city = member["ShipFromAddress"]["City"]
      inbound_shipment.country_code = member["ShipFromAddress"]["CountryCode"]
      inbound_shipment.postal_code = member["ShipFromAddress"]["PostalCode"]
      inbound_shipment.name = member["ShipFromAddress"]["Name"]
      inbound_shipment.address_line1 = member["ShipFromAddress"]["AddressLine1"]
      inbound_shipment.address_line2 = member["ShipFromAddress"]["AddressLine2"] || ""
      inbound_shipment.state_or_province_code = member["ShipFromAddress"]["StateOrProvinceCode"]

      if inbound_shipment.save
        puts shipment_id
        add_inbound_shipment_item(inbound_shipment, user)
      end
    end
  end

  def add_inbound_shipment_item(inbound_shipment, user)
    mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
    mws_developer_secret = "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"

    client = MWS.fulfillment_inbound_shipment(marketplace: user.mws_market_place_id,
      merchant_id: user.seller_id,
      aws_access_key_id: mws_developer_id,
      auth_token: user.mws_access_token,
      aws_secret_access_key: mws_developer_secret)
    start_date =  1.day.ago.iso8601
    end_date =  Time.current.iso8601

    response = client.list_inbound_shipment_items({
      :shipment_id => inbound_shipment.shipment_id,
      :last_updated_after => start_date,
      :last_updated_before => end_date
    }).parse
    unless response["ItemData"].nil?
      add_item(response["ItemData"],inbound_shipment)
    end
    token = response["NextToken"]
    while token
      response = client.list_inbound_shipment_items_by_next_token(token).parse
      unless response["ItemData"].nil?
        add_item(response["ItemData"],inbound_shipment)
      end
      token = response["NextToken"]
    end
  end

  def add_item(item_data,shipment)
    members = []
    if item_data["member"].kind_of?(Array)
      members = item_data["member"]
    else
      members << item_data["member"]
    end

    members.each do |member|
      shipment_id = member["ShipmentId"]
      unless shipment_id == shipment.shipment_id
        next
      end
      seller_sku = member["SellerSKU"]
      item = shipment.inbound_shipment_items.where(:seller_sku => seller_sku).first_or_create
      # item.fnsku = member["FulfillmentNetworkSKU"].to_i
      item.quantity_shipped = member["QuantityShipped"].to_i
      item.quantity_received = member["QuantityReceived"].to_i
      item.quantity_in_case = member["QuantityInCase"].to_i
      item.save
    end
  end
end
