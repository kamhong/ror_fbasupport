# == Schema Information
#
# Table name: inbound_shipplans
#
#  id                                :integer          not null, primary key
#  postal_code                       :string(255)
#  name                              :string(255)
#  country_code                      :string(255)
#  state_or_province_code            :string(255)
#  address_line2                     :string(255)
#  address_line1                     :string(255)
#  city                              :string(255)
#  are_cases_required                :boolean          default(FALSE)
#  shipplan_id                       :string(255)      not null
#  shipment_name                     :string(255)
#  destination_fulfillment_center_id :string(255)
#  label_prep_type                   :string(255)
#  checked_date                      :date
#  partnered_estimate                :decimal(64, 2)   default(0.0)
#  shipplan_status                   :string(255)      default("PLAN")
#  user_id                           :integer
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#
# Indexes
#
#  index_inbound_shipplans_on_shipplan_id  (shipplan_id) UNIQUE
#  index_inbound_shipplans_on_user_id      (user_id)
#

class InboundShipplan < ApplicationRecord
    has_many    :inbound_shipplan_items, dependent: :destroy
    belongs_to  :user

    scope :search_on, -> (txt) { joins(:inbound_shipplan_items).where('inbound_shipplans.destination_fulfillment_center_id LIKE ? OR inbound_shipplans.shipplan_id LIKE ? ', "%#{txt}%", "%#{txt}%") }

    def as_json(option={})
    {
        shipplan_id: shipplan_id,
        destination_fulfillment_center_id: destination_fulfillment_center_id,
        sku_count: sku_count,
        ship_qty: ship_qty
    }
    end

    def sku_count
        inbound_shipplan_items.count
    end

    def ship_qty
        inbound_shipplan_items.sum(:ship_qty)
    end
end
