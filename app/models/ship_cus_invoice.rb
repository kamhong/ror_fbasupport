# == Schema Information
#
# Table name: ship_cus_invoices
#
#  id                  :integer          not null, primary key
#  inbound_shipment_id :integer
#  invoice_name        :string(255)      not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  paid_date           :datetime
#  box_charges         :float(24)
#  new_box_count       :integer          default(0)
#
# Indexes
#
#  index_ship_cus_invoices_on_inbound_shipment_id  (inbound_shipment_id)
#

class ShipCusInvoice < ApplicationRecord
  default_scope { where("ship_cus_invoices.inbound_shipment_id is not null") }

  belongs_to :inbound_shipment
  validates :invoice_name, presence: true

  # accepts_nested_attributes_for :inbound_shipment

  def as_json(option={})
    super.except("created_at", "updated_at","paid_date").tap do |hash|
      # hash[:inbound_shipment] = inbound_shipment
      hash["created_at"] = created_at.strftime("%B %d, %Y")
      hash["paid_date"] = paid_date&.strftime("%B %d, %Y")
    end
  end

  def new_box_total
    new_box_count.to_i * 2
  end
end
