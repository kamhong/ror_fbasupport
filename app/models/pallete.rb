# == Schema Information
#
# Table name: palletes
#
#  id                   :integer          not null, primary key
#  dimension_unit       :string(255)
#  length               :float(24)
#  width                :float(24)
#  height               :float(24)
#  weight_unit          :string(255)
#  weight               :float(24)
#  is_stacked           :boolean          default(FALSE)
#  transport_content_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Pallete < ApplicationRecord
  belongs_to :transport_content

  # validates :dimension_unit, presence: true
  # validates :length, presence: true
  # validates :width, presence: true
  # validates :dimension_unit, presence: true
  # validates :height, presence: true
  # validates :weight_unit, presence: true
  # validates :weight, presence: true

  scope :skip_dummy, -> {where.not(length: 25, width: 25, height: 25, weight: 25)}

  
end
