# == Schema Information
#
# Table name: pending_pos
#
#  id                           :integer          not null, primary key
#  order_quantity               :integer          default(0)
#  received_quantity            :integer          default(0)
#  pending_po_index_id          :integer
#  vendoritem_id                :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  cost                         :decimal(64, 12)  default(0.0)
#  damaged_qty                  :integer          default(0)
#  mispick_qty                  :integer          default(0)
#  customer_invoice_id          :integer
#  confirmed_qty                :integer          default(0)
#  paid_qty                     :integer          default(0)
#  invoice_id                   :integer
#  warehouse_paid               :decimal(11, 2)   default(0.0)
#  warehouse_invoiced_qty       :integer          default(0)
#  warehouse_invoiced_unit_cost :decimal(11, 2)   default(0.0)
#  status_date                  :datetime
#  status                       :integer          default("")
#
# Indexes
#
#  index_pending_pos_on_customer_invoice_id  (customer_invoice_id)
#  index_pending_pos_on_invoice_id           (invoice_id)
#  index_pending_pos_on_pending_po_index_id  (pending_po_index_id)
#  index_pending_pos_on_vendoritem_id        (vendoritem_id)
#

class PendingPo < ApplicationRecord
  belongs_to  :pending_po_index
  belongs_to  :vendoritem
  belongs_to  :customer_invoice

  # changed to through pending_po
  # belongs_to  :invoice 

  has_many  :invoice_items
  has_many  :invoices, through: :invoice_items

  has_many  :outbound_shipments, dependent: :destroy

  enum status: ['', "Backorder", "Mismatch", "Confirmed", "Discontinued", "Completed","All"]
  # belongs_to  :seller_sku
  # Item.where('game_name LIKE :search OR genre LIKE :search OR console LIKE :search', search: "%#{search}%")
  scope :search_on, -> (title) { joins(vendoritem: :seller_sku).where('vendoritems.asin LIKE ? OR vendoritems.upc LIKE ? OR vendoritems.vendorsku like ? OR  seller_skus.sku LIKE ?', "%#{title}%", "%#{title}%", "%#{title}%",  "%#{title}%") }

  scope :search_fnsku, ->(fnsku) {joins(vendoritem: :seller_sku).where('seller_skus.fnsku LIKE ?', "%#{fnsku}%")}

  scope :search_all, -> (vendor, name) { joins(vendoritem: :seller_sku).joins("INNER JOIN vendors on vendors.id = vendoritems.vendor_id").where("vendors.name LIKE ?", "%#{vendor}%").where('vendoritems.vendortitle LIKE ?', "%#{name}%") }

  scope :per_status, ->(status) {
    if status == ""
      where(:status => 0).or(where(:status => nil))
    elsif status == "All"
    else
      where(:status => PendingPo.statuses[status.to_sym])
    end
  }
  def order_cost
    order_quantity * cost
  end


  def shipped_packs
    (received_quantity / vendoritem.packcount).to_i
  end

  def confirmed_cost
    confirmed_qty * cost
  end

  def confiremd_pack_qty
    (confirmed_qty / vendoritem.packcount).to_i
  end

  def received_cost
    received_quantity * cost
  end

  def remained
    order_quantity - received_quantity
  end

  def fee_per_label(box_label)
    if vendoritem.seller_sku.nil?
      return ''
    end
    if vendoritem.seller_sku.is_type_of_prep(box_label) == "Yes"
      case box_label
      when "Labeling"
        (shipped_packs * 0.18).round(2)
      when "Bubble"
        (shipped_packs * 0.18).round(2)
      when "Polybag"
        (shipped_packs * 0.18).round(2)
      when "Bundle"
        (shipped_packs * 0.79).round(2)
      when "Box"
        1.5
      else
        0
      end
    else
      0
    end
  end

  def labeling_fee
    fee_per_label("Labeling")
  end

  def bubble_fee
    fee_per_label("Bubble")
  end

  def polybag_fee
    fee_per_label("Polybag")
  end

  def bundle_fee
    fee_per_label("Bundle")
  end

  def box_fee
    fee_per_label("Box")
  end


  def as_json(*)
    super.except("created_at", "updated_at", "status_date").tap do |hash|
      # hash["vendoritem"] = vendoritem
      # hash["order_cost"] = order_cost
      hash["remained"] = remained
      hash["created_at"] = created_at.strftime("%B %d, %Y")
      # hash["labeling_fee"] = fee_per_label("Labeling")
      # hash["bubble_fee"] = fee_per_label("Bundle")
      # hash["polybag_fee"] = fee_per_label("Polybug")
      # hash["bundle_fee"] = fee_per_label("Bundle")
      # hash["box_fee"] = fee_per_label("Box")
      # hash["item_id"] =  vendoritem.id
      # hash["missing"] = missing
      # hash["shipped_packs"] = shipped_packs
      # hash["confirmed_cost"] = confirmed_cost
      # hash["confiremd_pack_qty"] = confiremd_pack_qty

      #for OutboundStage Page
      hash["datatype"] = "pending_po_item"
      hash["stage_status"] = ""
      hash["shipment_id"] = ""
      #fro all po
      # hash["invoice_id"] = invoices&.last&.id
      # hash["invoice_names"] = invoice_names
      # hash["invoice_date"] = invoices&.last&.created_at&.strftime("%B %-d, %Y")
      # hash["invoice"] = invoices&.last

      # hash["invoice_item"] = invoice_items&.last

      
      # hash["status_date"] = status == "Backorder" || status == "Confirmed"? status_date&.strftime("%B %d, %Y") : ""
      # hash["customer_invoice"] = customer_invoice
      # hash["pending_po_index"] = pending_po_index.po_name
    end
  end

  def status_date_string
    status == "Backorder" || status == "Confirmed"? status_date&.strftime("%B %d, %Y") : ""
  end

  def invoice_names
    invoice_str = ""
    invoices.each do |i|
      invoice_str = invoice_str + "#{i.invoice_name}, "
    end
    invoice_str.chomp(", ")
  end

  def missing
    if received_quantity > order_quantity
      0
    else
      received_quantity - order_quantity
    end
  end

  def self.number_compare(op, value, field)
    if field == 'profit'
      where("(vendorasins.buyboxprice-pending_pos.cost*packcount-vendorasins.totalfbafee) #{op} #{value}")
    elsif field == 'margin'
      where("(vendorasins.buyboxprice-pending_pos.cost*packcount-vendorasins.totalfbafee)/pending_pos.cost*100 #{op} #{value}")
    else
      where("#{field} #{op} #{value}")
    end
  end

  def warehouse_invoiced_costs
    warehouse_invoiced_qty * warehouse_invoiced_unit_cost
  end

  def self.status_list
    [
      {'name'=>'', 'value' => ""},
      {'name'=>'Backorder', 'value' => "Backorder"},
      {'name'=>'Mismatch', 'value' => "Mismatch"},
      {'name'=>'Confirmed', 'value' => "Confirmed"},
      {'name'=>'Discontinued', 'value' => "Discontinued"},
      {'name'=>'Completed', 'value' => "Completed"},
    ]
  end
end
