# == Schema Information
#
# Table name: user_table_sessions
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  table_name :string(255)
#  state      :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_user_table_sessions_on_user_id                 (user_id)
#  index_user_table_sessions_on_user_id_and_table_name  (user_id,table_name) UNIQUE
#

class UserTableSession < ApplicationRecord
  belongs_to :user_id

  validates :user, presence: true
  validates :name, uniqueness: { scope: :table_name,    message: "Table session can't be duplicated" }
  validates :table_name, length: { in: 1..70, too_long: "%{count} characters is the maximum allowed" }
  validates :state, length: { in: 1..10000, too_long: "%{count} characters is the maximum allowed" }
end
