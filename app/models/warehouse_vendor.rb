# == Schema Information
#
# Table name: warehouse_vendors
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  supplier_code        :string(255)
#  day_of_shipping      :string(255)
#  minimum_order        :decimal(15, 2)   default(0.0)
#  vendor_phone         :string(255)
#  account_number       :string(255)
#  account_holder       :integer
#  contact_name         :string(255)
#  contact_email        :string(255)
#  contact_phone_number :string(255)
#  website              :string(255)
#  user_name            :string(255)
#  password             :string(255)
#  payment_terms        :integer
#  shipment_terms       :integer
#  catalog              :string(255)
#  user_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_warehouse_vendors_on_name     (name) UNIQUE
#  index_warehouse_vendors_on_user_id  (user_id)
#

class WarehouseVendor < ApplicationRecord
  belongs_to  :user
  has_many    :vendors, foreign_key: :supplier_prefix, primary_key: :supplier_code
  
  enum payment_terms: ['', "CC", "Net_15"]

  def payment_term_msg
    if payment_terms == "default"
      ""
    else
      payment_terms
    end
  end

  def as_json(*)
    super.except("created_at", "updated_at").tap do |hash|
      hash["buyer_name"] = user&.full_name
    end
  end

  def self.payment_term_list
    [
      {'name'=>'', 'value' => ""},
      {'name'=>'CC', 'value' => "CC"},
      {'name'=>'Net_15', 'value' => "Net_15"},
    ]
  end
end
