# == Schema Information
#
# Table name: po_statuses
#
#  id        :integer          not null, primary key
#  po_status :string(255)
#

class PoStatus < ApplicationRecord
    has_many :pos
end
