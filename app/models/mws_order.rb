# == Schema Information
#
# Table name: mws_orders
#
#  id                  :integer          not null, primary key
#  amazon_order_id     :string(255)
#  merchant_order_id   :string(255)
#  purchase_date       :datetime
#  last_updated_date   :datetime
#  order_status        :string(255)
#  fulfillment_channel :string(255)
#  sales_channel       :string(255)
#  order_channel       :string(255)
#  url                 :string(255)
#  ship_service_level  :string(255)
#  address_type        :string(255)
#  ship_city           :string(255)
#  ship_postal_code    :string(255)
#  ship_state          :string(255)
#  ship_country        :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  user_id             :integer
#
# Indexes
#
#  index_mws_orders_on_user_id                      (user_id)
#  index_mws_orders_on_user_id_and_amazon_order_id  (user_id,amazon_order_id) UNIQUE
#

class MwsOrder < ApplicationRecord
    belongs_to :user
    has_many :mws_order_items
end
