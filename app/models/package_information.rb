# == Schema Information
#
# Table name: package_informations
#
#  id          :integer          not null, primary key
#  asin        :string(255)
#  quantity    :integer
#  expiry_date :date
#  package_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  box_id      :integer
#

class PackageInformation < ApplicationRecord
  belongs_to :box
end
