# == Schema Information
#
# Table name: out_stages
#
#  id            :integer          not null, primary key
#  box_qty       :integer
#  case_per_box  :integer
#  expired_date  :date
#  received_date :date
#  status        :string(255)      default("RECEIVED")
#  pending_po_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_out_stages_on_pending_po_id  (pending_po_id)
#

class OutStage < ApplicationRecord
end
