# == Schema Information
#
# Table name: pending_po_indices
#
#  id             :integer          not null, primary key
#  po_index       :integer
#  po_name        :string(255)
#  vendor_id      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  pending_status :integer          default("")
#
# Indexes
#
#  index_pending_po_indices_on_vendor_id  (vendor_id)
#

class PendingPoIndex < ApplicationRecord
  belongs_to  :vendor
  has_many    :pending_pos, dependent: :delete_all
  has_many    :customer_invoices, dependent: :delete_all
  after_create :set_po_index

  enum pending_status: ["","Sent", "Confirmed", "Payment", "Inbound", "Invoice","Rep Payout", "Completed","All"]
  
  scope :per_status, ->(status) {
    if status == "" || status == nil
      where(:pending_status => 0).or(where(:pending_status => nil))
    elsif status == "All"
    else
      where(:pending_status => PendingPoIndex.pending_statuses[status.to_sym])
    end
  }

  def set_po_index
    self.po_index = (PendingPoIndex.where(:vendor_id => vendor_id).maximum('po_index') || 9999) + 1
    self.po_name = "#{vendor.name}_#{po_index}" 
    self.save
  end

  def as_json(*)
    super.except("created_at","updated_at").tap do |hash|
      # hash[:vendor] = vendor
      # hash[:order_qty] = order_qty
      # hash[:order_e_cost] = order_e_cost
      # hash[:confirmed_qty] = confirmed_qty
      # hash[:confiremd_e_cost] = confiremd_e_cost
      # hash[:received_quantity] = received_quantity
      # hash[:total_shipped_pack] = total_shipped_pack
      # hash[:sku_count] = sku_count
      # hash[:missing_quantity] = missing_quantity
      hash[:created_at] = created_at.strftime("%B %d, %Y")
      # hash[:invoices] = invoices
    end
  end

  def order_qty
    pending_pos.sum(:order_quantity)
  end

  def order_e_cost
    pending_pos.sum("order_quantity * cost")
  end

  def confirmed_qty
    pending_pos.sum(:confirmed_qty)
  end

  def confiremd_e_cost
    pending_pos.sum("confirmed_qty * cost")
  end

  def received_quantity
    pending_pos.sum(:received_quantity)
  end

  def total_shipped_pack
    pending_pos.sum(&:shipped_packs)
  end

  def missing_quantity
    # pending_pos.sum(&:missing)
    received_quantity  > order_qty ? 0 : order_qty -  received_quantity
  end

  def sku_count
    pending_pos.count(:vendoritem_id)
  end

  def invoices
    invoices = ""
    customer_invoices.each do |invoice|
      invoices = invoices + invoice.invoice_name + ", "
    end

    invoices.chomp(', ')
  end

  def self.status_list
    [
      {'name'=>'', 'value' => ""},
      {'name'=>'Sent', 'value' => "Sent"},
      {'name'=>'Confirmed', 'value' => "Confirmed"},
      {'name'=>'Payment', 'value' => "Payment"},
      {'name'=>'Inbound', 'value' => "Inbound"},
      {'name'=>'Invoice', 'value' => "Invoice"},
      {'name'=>'Rep Payout', 'value' => "Rep Payout"},
      {'name'=>'Completed', 'value' => "Completed"},
    ]
  end
end
