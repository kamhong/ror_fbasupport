# == Schema Information
#
# Table name: inbound_shipments
#
#  id                                :integer          not null, primary key
#  postal_code                       :string(255)
#  name                              :string(255)
#  country_code                      :string(255)
#  state_or_province_code            :string(255)
#  address_line2                     :string(255)
#  address_line1                     :string(255)
#  city                              :string(255)
#  are_cases_required                :boolean          default(FALSE)
#  shipment_id                       :string(255)      not null
#  shipment_name                     :string(255)
#  destination_fulfillment_center_id :string(255)
#  label_prep_type                   :string(255)
#  checked_date                      :date
#  partnered_estimate                :decimal(64, 2)   default(0.0)
#  user_id                           :integer
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  shipment_status                   :string(255)      default("WORKING")
#  invoice_name                      :string(255)      default("")
#  invoice_status                    :boolean          default(FALSE)
#  mix_label_charges                 :float(24)        default(0.0)
#  invoice_sent                      :boolean          default(FALSE)
#
# Indexes
#
#  index_inbound_shipments_on_shipment_id  (shipment_id) UNIQUE
#  index_inbound_shipments_on_user_id      (user_id)
#

class InboundShipment < ApplicationRecord
  belongs_to  :user
  has_many    :inbound_shipment_items

  has_many    :transport_contents
  has_one   :ship_cus_invoice
  accepts_nested_attributes_for :ship_cus_invoice


  # scope :search_on, -> (txt) { joins(:inbound_shipment_items).where('inbound_shipments.destination_fulfillment_center_id LIKE ? OR inbound_shipments.shipment_id LIKE ? ', "%#{txt}%", "%#{txt}%") }
  scope :search_on, -> (txt) { where('inbound_shipments.destination_fulfillment_center_id LIKE ? OR inbound_shipments.shipment_id LIKE ? ', "%#{txt}%", "%#{txt}%") }
  scope :working, ->  { where( shipment_status: ["WORKING", "ESTIMATED", "ESTIMATING"] ) }
  scope :invoiced, ->  { where( invoice_status: true ) }

  SHIPMENT_STATUSES = ["WORKING", "SHIPPED", "IN_TRANSIT", "DELIVERED", "CHECKED_IN", "RECEIVING", "CANCELLED", "DELETED", "CLOSED"]

  after_save :set_invoice_status, on: [:update]

  # def as_json(options={})
  # {
  #   id: id,
  #   shipment_id: shipment_id,
  #   destination_fulfillment_center_id: destination_fulfillment_center_id,
  #   sku_count: sku_count,
  #   ship_qty: ship_qty
  #   super.except("created_at", "updated_at").tap do |hash|
  #     hash[:user] = user
  #     hash[:warehouse_vendor] = warehouse_vendor
  #   end
  # }
  # end

  def as_json(option={})
    super.except("created_at", "updated_at").tap do |hash|
      hash[:sku_count] = sku_count
      hash[:ship_qty] = ship_qty
      hash[:ship_cost] = ship_cost
      hash[:user] = user
    end
  end

  def set_invoice_status
    unless self.invoice_name.blank? || self.invoice_status
      self.invoice_status = true
      self.save
    end
  end
  def self.by_status keyword
    keyword.present? ? where(shipment_status: keyword) : all
  end

  def sku_count
    inbound_shipment_items.count
  end

  def ship_qty
    inbound_shipment_items.sum(:quantity_shipped)
  end

  def ship_cost
    inbound_shipment_items.sum(&:item_cost)
  end

  def copy_from_plan(plan_data)
    self.postal_code = plan_data.postal_code
    self.name = plan_data.name
    self.country_code = plan_data.country_code
    self.state_or_province_code = plan_data.state_or_province_code
    self.address_line2 = plan_data.address_line2
    self.address_line1 = plan_data.address_line1
    self.city = plan_data.city
    self.are_cases_required = plan_data.are_cases_required
    self.shipment_name = plan_data.shipment_name
    self.destination_fulfillment_center_id = plan_data.destination_fulfillment_center_id
    self.label_prep_type = plan_data.label_prep_type
    self.partnered_estimate = plan_data.partnered_estimate

    self.save
  end

  def latest_transport_plan
    transport_contents.where.not(id: nil).last
  end

  def resetable?
    transport_content = transport_contents.where.not(shipment_type: nil).try(:last)
    (transport_content.try(:shipment_type).present?) and transport_content.try(:status) != "CONFIRMED"
  end

  def total_box_count
    box_count = nil
    return box_count unless latest_transport_plan.present?
    if latest_transport_plan.small_package_and_partnered?
        box_count = latest_transport_plan.packages.count
    elsif latest_transport_plan.truck_and_partnered? or latest_transport_plan.small_package_and_not_partnered?
        box_count = latest_transport_plan.box_count.to_i
    end
    box_count
  end

  def total_pallete_count
    pallete_count = 0
    return pallete_count unless latest_transport_plan.present?
    pallete_count = latest_transport_plan.truck_and_partnered? ? latest_transport_plan.palletes.count : latest_transport_plan.pallete_count.to_i
    pallete_count.to_i
  end

  def pallete_charges
    total_pallete_count * 15
  end
end
