# == Schema Information
#
# Table name: mws_order_items
#
#  id                      :integer          not null, primary key
#  product_name            :string(255)
#  sku                     :string(255)
#  asin                    :string(255)
#  number_of_items         :integer
#  item_status             :string(255)
#  quantity                :integer
#  currency                :string(255)
#  item_price              :decimal(10, 2)
#  item_tax                :decimal(10, 2)
#  shipping_price          :decimal(10, 2)
#  shipping_tax            :decimal(10, 2)
#  gift_wrap_price         :decimal(10, 2)
#  gift_wrap_tax           :decimal(10, 2)
#  item_promotion_discount :decimal(10, 2)
#  ship_promotion_discount :decimal(10, 2)
#  promotion_ids           :string(255)
#  is_business_order       :boolean
#  purchase_order_number   :string(255)
#  price_designation       :decimal(10, 2)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  mws_order_id            :integer
#
# Indexes
#
#  index_mws_order_items_on_asin          (asin)
#  index_mws_order_items_on_mws_order_id  (mws_order_id)
#  index_mws_order_items_on_sku           (sku)
#

class MwsOrderItem < ApplicationRecord
    belongs_to :mws_order
    belongs_to  :seller_sku, foreign_key: :sku, primary_key: :sku

    scope :last_day_sales, -> (n) {joins(:mws_order).where(mws_orders: {purchase_date: n.day.ago..Date.today}).sum(:quantity)}
    scope :within_month, -> { joins(:mws_order).where(mws_orders: {purchase_date: 30.day.ago..Date.today}) }
    after_save  :set_sku_sales

    def set_sku_sales
        
        self.seller_sku.last1daysales = self.seller_sku.mws_order_items.last_day_sales(1)
        self.seller_sku.last7daysales = self.seller_sku.mws_order_items.last_day_sales(7)
        self.seller_sku.last30daysales = self.seller_sku.mws_order_items.last_day_sales(30)
        self.seller_sku.last60daysales = self.seller_sku.mws_order_items.last_day_sales(60)
        self.seller_sku.last90daysales = self.seller_sku.mws_order_items.last_day_sales(90)
        self.seller_sku.last360daysales = self.seller_sku.mws_order_items.last_day_sales(360)

        self.seller_sku.save
    end

    
end
