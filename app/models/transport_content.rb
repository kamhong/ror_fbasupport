# == Schema Information
#
# Table name: transport_contents
#
#  id                     :integer          not null, primary key
#  amazon_shipment_id     :string(255)
#  shipment_type          :string(255)
#  name                   :string(255)
#  outbound_shipments_id  :integer
#  delivery_by_amazon     :boolean          default(TRUE)
#  carrier                :string(255)
#  tracking_id            :string(255)
#  email                  :string(255)
#  phone                  :string(255)
#  fax                    :string(255)
#  box_count              :string(255)
#  frieght_class          :string(255)
#  frieght_read_date      :date
#  total_weight           :float(24)
#  declared_value         :float(24)
#  pro_number             :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  uploaded_to_amazon     :boolean          default(FALSE)
#  status                 :string(255)
#  amazon_estimate        :float(24)
#  inbound_shipment_id    :integer
#  bill_of_lading_enabled :boolean          default(FALSE)
#  pallete_count          :integer          default(0)
#  void_deadline          :datetime
#  shipping_fee           :float(24)
#

#

class TransportContent < ApplicationRecord
  belongs_to :inbound_shipment

  has_many :trackings
  has_many :packages
  has_many :palletes
  has_many :boxes

  accepts_nested_attributes_for :trackings
  accepts_nested_attributes_for :packages
  accepts_nested_attributes_for :palletes
  accepts_nested_attributes_for :boxes



  after_save :remove_invalid_packages, :set_estimated, :remove_invalid_trackings#, :create_palletes_for_LTL

  scope :not_confirmed, -> { where.not(status: "Confirmed").or(TransportContent.where(status: nil)) }

  PARTNER_OPTIONS = [["Yes", true], ["No", false]]
  SHIPMENT_TYPE_OPTIONS = [["Small Parcel", "SP"], ["Less Than Truckload/Full Truckload ", "LTL"]]

  DIMESION_UNITS = ['inches', 'centimeters']
  WEIGHT_UNITS = ['pounds', 'kilograms']

  CARRIER_FOR_NON_PARTNERED_SMALL_PARCEL = ["DHL_EXPRESS_USA_INC", "FEDERAL_EXPRESS_CORP", "UNITED_STATES_POSTAL_SERVICE", "UNITED_PARCEL_SERVICE_INC", "OTHER"]
  CARRIER_FOR_PARTNERED_SMALL_PARCEL = ["DHL_STANDARD", "UNITED_PARCEL_SERVICE_INC"]

  CARRIER_FOR_NON_PARTNERED_LTL_PACKAGE = ["DHL_EXPRESS_USA_INC", "FEDERAL_EXPRESS_CORP", "UNITED_STATES_POSTAL_SERVICE", "UNITED_PARCEL_SERVICE_INC", "OTHER"]

  STATUS_ARRAY = ["WORKING", "ESTIMATING", "ERROR_ON_ESTIMATING", "ESTIMATED", "ERROR_ON_CONFIRMING", "CONFIRMING", "CONFIRMED"]
  SHIPMENT_STATUSES = ["WORKING", "SHIPPED", "IN_TRANSIT", "DELIVERED", "CHECKED_IN", "RECEIVING", "ERROR"]

  # validate :check_plan_structure

  validates :box_count, :email, :name, :phone, :fax, presence: true, if: :truck_and_partnered?

  validates :pro_number, length: { maximum: 10, minimum: 7 }, if: :truck_and_not_partnered?

  after_save :remove_empty_palletes

  def remove_empty_palletes
    palletes.where(length: nil, width: nil, height: nil, weight: nil).destroy_all
  end

  def remove_invalid_trackings
    trackings.where(tracking_number: "").destroy_all
  end

  def check_plan_structure
    if small_package_and_partnered? and packages.count == 0
      errors.add(:packages, 'Packages Should be present for Small Package and Partnered!')
    elsif small_package_and_not_partnered? and trackings.count == 0
      errors.add(:trackings, 'At least 1 tracking shoule be present for Small Package and Non Partnered!')
    end
  end

  def remove_invalid_packages
    packages.where(length: nil, width: nil, height: nil, weight: nil).destroy_all unless truck_and_partnered?
  end

  def set_estimated
    update(status: "ESTIMATED") if amazon_estimate.present? and status == "ESTIMATING"
  end

  def create_palletes_for_LTL
    self.palletes.create(weight_unit: "kilograms", weight: 25.0, dimension_unit: "inches", length: 25, width: 25, height: 25) if ltl_partnered_with_no_palletes
  end

  def ltl_partnered_with_no_palletes
    truck_and_partnered? and !(palletes.present?)
  end

  def full_shipment_type
    shipment_type == "SP" ? "Small Parcel" : "Less Than Truckload/Full Truckload"
  end

  def small_package_and_partnered?
    delivery_by_amazon == true and shipment_type == "SP"
  end

  def small_package_and_not_partnered?
    delivery_by_amazon == false and shipment_type == "SP"
  end

  def truck_and_partnered?
    delivery_by_amazon == true and truck_content?
  end

  def truck_and_not_partnered?
    delivery_by_amazon == false and truck_content?
  end

  def truck_content?
    shipment_type == "LTL"
  end

  def barcode_data
    barcode_data = ""
    barcode_data += "AMZN,PO:#{inbound_shipment.shipment_id},"
    packages.each do |package|
      package.package_informations.each do |pkg_info|
        if pkg_info.quantity > 0
          barcode_data += "ASIN:#{pkg_info.asin},QTY:#{pkg_info.quantity}"
          barcode_data += ",EXP:#{pkg_info.expiry_date.strftime('%Y%m%d')}" if pkg_info.expiry_date.present?
        end
      end
    end
  end

  def shipable?
    shipable = delivery_by_amazon ? (status == "CONFIRMED" and inbound_shipment.shipment_status == "WORKING") : (status == "WORKING")
    return shipable
  end

  def formatted_estimate
    return "$#{amazon_estimate}" if amazon_estimate
  end

end
