# == Schema Information
#
# Table name: vendoritems
#
#  id              :integer          not null, primary key
#  cost            :decimal(64, 2)   default(0.0), not null
#  vendortitle     :text(65535)
#  vendorasin_id   :integer
#  vendor_id       :integer
#  asin            :string(255)
#  upc             :string(255)
#  isbn            :string(255)
#  ean             :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  vendorsku       :string(255)      default("")
#  packcount       :integer          default(1)
#  seller_sku_id   :integer
#  pre_defined_sku :string(255)
#  min_price       :decimal(10, 2)   default(0.0)
#  max_price       :decimal(10, 2)   default(0.0)
#
# Indexes
#
#  index_vendoritems_on_seller_sku_id  (seller_sku_id)
#  index_vendoritems_on_vendor_id      (vendor_id)
#  index_vendoritems_on_vendorasin_id  (vendorasin_id)
#

class Vendoritem < ApplicationRecord
  belongs_to :vendorasin
  belongs_to :user
  belongs_to :seller_sku
  has_one :po, dependent: :destroy
  has_many  :pending_pos, dependent: :destroy
  has_many  :outbound_shipments, through:  :pending_pos
  accepts_nested_attributes_for :vendorasin, reject_if: proc { |attributes| attributes['name'].blank? }

  

  belongs_to :vendor

  scope :profitable, -> {joins(:vendorasin).where("vendorasins.buyboxprice-cost-vendorasins.totalfbafee > 0")}

  scope :find_sellersku, -> (column, value) {joins(:seller_sku).where("seller_skus.#{column} LIKE ?", "%#{value}%")}

  scope :missing_asin, -> {where("vendorasin_id = 0")}
  def profit
    if vendorasin.buyboxprice.nil? || cost.nil? || vendorasin.totalfbafee.nil?
      return 0      
    else
      return (vendorasin.buyboxprice-cost*packcount-vendorasin.totalfbafee - prep_fee).to_f.round(2)
    end
  end

  def margin
    if vendorasin.buyboxprice.nil? || cost.nil? || vendorasin.totalfbafee.nil?
      return 0      
    else
      return (profit / cost / packcount * 100).to_f.round(2)
    end
  end

  def packcost
    (packcount || 1) *cost  
  end

  def self.sorted_by_margin
    all.order(:cost)
  end

  def title
    title = vendortitle || vendorasin.name
    unless title.nil?
      title
    else
      "Missing Name"
    end
  end

  def as_json(options={})
    {
      id: id,
      asin: asin,
      vendortitle: vendortitle,
      vendor: vendor&.name,
      vendor_id: vendor_id,
      upc: upc,
      cost: cost,
      buyboxprice: vendorasin.buyboxprice,
      fbafee: vendorasin.fbafee,
      storagefee: vendorasin.storagefee,
      variableclosingfee: vendorasin.variableclosingfee,
      commissionpct: vendorasin.commissionpct,
      commissiionfee: vendorasin.commissiionfee,
      salespermonth: vendorasin.salespermonth,
      review: vendorasin.review,
      packagequantity: vendorasin.packagequantity,
      totaloffers: vendorasin.totaloffers,
      fbaoffers: vendorasin.fbaoffers,
      fbmoffers: vendorasin.fbmoffers,
      salesrank: vendorasin.salesrank,
      margin: margin,
      profit: profit,
      packcount: packcount,
      packcost: packcost,
      vendorsku: vendorsku,
      product_groups_id: vendorasin.product_groups_id,
      sku: sellersku,
      bbs: bbs,
      vendorasin_id: vendorasin_id,

      vendorasin: vendorasin,
      seller_sku: seller_sku,
      full_name: vendor.user.full_name
    }
  end

  def sellersku
    unless seller_sku.nil?
      seller_sku.sku
    end
  end

  def bbs
    if vendorasin.amazon_selling
        "AMZ"
    elsif vendorasin.fbaoffers > 0
        "FBA"
    else
        "FBM"
    end
  end

  def self.bbs_search(bbs)
      if bbs == "AMZ"
          joins(:vendorasin).where("vendorasins.amazon_selling = true")
      elsif bbs == "FBA"
          joins(:vendorasin).where("vendorasins.amazon_selling = false and vendorasins.fbaoffers > 0")
      else
          joins(:vendorasin).where("vendorasins.amazon_selling = false and vendorasins.fbaoffers = 0 and vendorasins.fbmoffers > 0")
      end 
  end

  def self.profit()
    # if op == '='
    #   where("(vendorasins.buyboxprice-cost*packcount-vendorasins.totalfbafee) = #{value}")
    # elsif op == '<'
      
    #   where("(vendorasins.buyboxprice-cost*packcount-vendorasins.totalfbafee) < #{value}")
    # end
    # if vendorasin.buyboxprice.nil? || cost.nil? || vendorasin.totalfbafee.nil?
    #   return 0      
    # else
    #   return (profit / cost / packcount * 100).to_f.round(2)
    # end
    0
  end

  def self.number_compare(op, value, field)
    if field == 'profit'
      where("(vendorasins.buyboxprice-cost*packcount-vendorasins.totalfbafee) #{op} #{value}")
    elsif field == 'margin'
      where("(vendorasins.buyboxprice-cost*packcount-vendorasins.totalfbafee)/cost*100 #{op} #{value}")
    else
        where("#{field} #{op} #{value}")
    end
  end

  def prep_fee
    fee = 0.0
    unless seller_sku.nil?
      unless seller_sku.prepinstruction.nil?
        if seller_sku.prepinstruction.downcase.include? "Labeling".downcase
          fee += 0.18
        end
        if seller_sku.prepinstruction.downcase.include? "Bubble".downcase
          fee += 0.18
        end
        if seller_sku.prepinstruction.downcase.include? "Polybag".downcase
          fee += 0.18
        end
        if seller_sku.prepinstruction.downcase.include? "Box".downcase
          fee += 1.5
        end
        if packcount > 1
          fee += 0.48
        end
      end
    end  
    fee
  end  
end
