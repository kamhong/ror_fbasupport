# == Schema Information
#
# Table name: outbound_shipments
#
#  id             :integer          not null, primary key
#  box_qty        :integer          default(1)
#  show_case_qty  :integer          default(0)
#  case_qty       :integer          default(1)
#  expired_date   :date
#  received_date  :date
#  case           :boolean          default(TRUE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  pending_po_id  :integer
#  bagger         :boolean          default(FALSE), not null
#  stage_status   :string(255)      default("RECEIVED")
#  shipment_id    :string(255)
#  boxed_quantity :integer          default(0)
#
# Indexes
#
#  index_outbound_shipments_on_pending_po_id  (pending_po_id)
#

class OutboundShipment < ApplicationRecord
    belongs_to  :pending_po
    has_many    :inbound_shipplan_items, dependent: :delete_all
    scope :search_fnsku, ->(fnsku) {joins(:pending_po).joins("INNER JOIN vendoritems on pending_pos.vendoritem_id = vendoritems.id").joins("INNER JOIN seller_skus on vendoritems.seller_sku_id = seller_skus.id").where('seller_skus.fnsku LIKE ?', "%#{fnsku}%")}

    scope :search_shipment_sku, ->(shipment_id, sku) {joins(:pending_po).joins("INNER JOIN vendoritems on pending_pos.vendoritem_id = vendoritems.id").joins("INNER JOIN seller_skus on vendoritems.seller_sku_id = seller_skus.id").where('seller_skus.sku = ? AND outbound_shipments.shipment_id = ?', sku, shipment_id)}

    scope :search_bagger, ->(search) {joins(:pending_po).joins("INNER JOIN vendoritems on pending_pos.vendoritem_id = vendoritems.id").joins("INNER JOIN seller_skus on vendoritems.seller_sku_id = seller_skus.id").where('outbound_shipments.bagger = 1').where('seller_skus.fnsku LIKE ? or seller_skus.sku = ? ', "%#{search}%", "%#{search}%")}

    scope :search_exact_fnsku, ->(fnsku) {joins(:pending_po).joins("INNER JOIN vendoritems on pending_pos.vendoritem_id = vendoritems.id").joins("INNER JOIN seller_skus on vendoritems.seller_sku_id = seller_skus.id").where('seller_skus.fnsku = ?', "#{fnsku}")}

    def shipplan_creatable
        if stage_status == "SHIPMENT"
            false
        else
            true
        end
    end

    def received_quantity
        box_qty * case_qty
    end

    def received_quantity_pack
        (box_qty * case_qty / pending_po.vendoritem.packcount).to_i
    end

    def update_pending_po_receive(changed_qty)
        self.pending_po.received_quantity = self.pending_po.received_quantity + (changed_qty * self.case_qty / self.pending_po.vendoritem.packcount).to_i
        self.pending_po.save
    end

    def as_json(*)
        super.except("created_at", "updated_at").tap do |hash|
            # hash["pending_po"] = pending_po
            # hash["pending_po_index"] = pending_po.pending_po_index
            # hash["vendoritem"] = pending_po.vendoritem

            # hash["received_quantity"] = received_quantity
            # hash["shipped_packs"] = received_quantity_pack

            # hash["mispick_qty"] = pending_po.mispick_qty
            # hash["damaged_qty"] = pending_po.damaged_qty
            # hash["confirmed_qty"] = pending_po.confirmed_qty
            # hash["order_quantity"] = pending_po.order_quantity

            # hash["vendoritem"] = pending_po.vendoritem
            hash["datatype"] =  'stage_item'
            # hash["item_id"] =  pending_po.vendoritem.id
        end
    end

    def boxable_quantity
      [received_quantity - boxed_quantity, 0].max
    end

    def self.pending_box_info search_term
      fnskus = OutboundShipment.make_fnsku_array search_term
      data_array = []
      fnskus.each do |fnsku|
        shipments = JSON.parse(OutboundShipment.all.search_exact_fnsku(fnsku).to_json)
        next if shipments.count == 0
        shipments.each do |shipment|
            outbound_shipment = OutboundShipment.find_by(shipment_id: shipment["shipment_id"])
            next if (data_array.select{|elem| elem[:asin] == shipment["vendoritem"]["asin"] }) != []
            data_array << {shipment_id: shipment["shipment_id"].presence || "No Shipment Assosiated!",
                        asin: shipment["vendoritem"]["asin"],
                        boxable_quantity: outbound_shipment.boxable_quantity,
                        received_quantity: shipment["received_quantity"],
                        packed_quantity: shipment["vendoritem"]["packcount"],
                        case_quantity: shipment["shipped_packs"]}
        end
      end
      data_array
    end

    def self.make_fnsku_array fnskus
        formatted_fnsku = fnskus.gsub(/\s+/, "").split(",")
    end
    # def as_json(options={})
    # {
    #     id: id,
    #     box_qty: box_qty,

    #     asin: pending_po.vendoritem.vendorasin.asin,
    #     vendortitle: pending_po.vendoritem.vendortitle,
    #     vendor: pending_po.vendoritem.vendor.name,
    #     upc: pending_po.vendoritem.upc,
    #     cost: pending_po.vendoritem.cost,
    #     buyboxprice: pending_po.vendoritem.vendorasin.buyboxprice,
    #     fbafee: pending_po.vendoritem.vendorasin.fbafee,
    #     storagefee: pending_po.vendoritem.vendorasin.storagefee,
    #     variableclosingfee: pending_po.vendoritem.vendorasin.variableclosingfee,
    #     commissionpct: pending_po.vendoritem.vendorasin.commissionpct,
    #     commissiionfee: pending_po.vendoritem.vendorasin.commissiionfee,
    #     salespermonth: pending_po.vendoritem.vendorasin.salespermonth,
    #     packagequantity: pending_po.vendoritem.vendorasin.packagequantity,
    #     totaloffers: pending_po.vendoritem.vendorasin.totaloffers,
    #     fbaoffers: pending_po.vendoritem.vendorasin.fbaoffers,
    #     fbmoffers: pending_po.vendoritem.vendorasin.fbmoffers,
    #     margin: pending_po.vendoritem.margin,
    #     profit: pending_po.vendoritem.profit,
    #     packcount: pending_po.vendoritem.packcount,
    #     packcost: pending_po.vendoritem.packcost,
    #     vendorsku: pending_po.vendoritem.vendorsku,
    #     salesrank: pending_po.vendoritem.vendorasin.salesrank,
    #     product_groups_id: pending_po.vendoritem.vendorasin.product_groups_id,
    #     item_id: pending_po.vendoritem.id,
    #     order_quantity: pending_po.order_quantity,
    #     single: pending_po.order_quantity * pending_po.vendoritem.packcount,
    #     missing: pending_po.missing,
    #     order_cost: pending_po.order_cost,
    #     received_quantity:  received_quantity_pack,
    #     single_pack_received_quantity: received_quantity,
    #     prepinstruction: pending_po.vendoritem.seller_sku.prepinstruction,
    #     sellersku: pending_po.vendoritem.sellersku,
    #     fnsku: pending_po.vendoritem.seller_sku.fnsku,
    #     inbound_destination: pending_po.vendoritem.seller_sku.inbound_destination,
    #     po_name: pending_po.pending_po_index.po_name,
    #     user: pending_po.vendoritem.vendor.user.full_name,
    #     mispick_qty: pending_po.mispick_qty,
    #     damaged_qty: pending_po.damaged_qty,
    #     stage_status: stage_status,
    #     shipment_id: shipment_id,
    #     datatype: 'stage_item',

    #     large_image: pending_po.vendoritem.vendorasin.large_image,
    #     name: pending_po.vendoritem.vendorasin.name,
    #     bagger_number: pending_po.vendoritem.seller_sku.bagger_number,
    #     snl: pending_po.vendoritem.seller_sku.snl
    #     }
    # end
end
