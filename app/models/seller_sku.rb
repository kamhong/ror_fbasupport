# == Schema Information
#
# Table name: seller_skus
#
#  id                      :integer          not null, primary key
#  sku                     :string(255)
#  price                   :decimal(64, 2)
#  fnsku                   :string(255)
#  inbound_destination     :string(255)
#  prepinstruction         :string(255)
#  age                     :string(255)
#  age_0_90                :integer          default(0)
#  age_91_180              :integer          default(0)
#  age_181_270             :integer          default(0)
#  age_271_360             :integer          default(0)
#  age_365_plus            :integer          default(0)
#  snl                     :boolean          default(FALSE)
#  last1daysales           :integer          default(0)
#  last7daysales           :integer          default(0)
#  last30daysales          :integer          default(0)
#  last60daysales          :integer          default(0)
#  last90daysales          :integer          default(0)
#  last360daysales         :integer          default(0)
#  item_quantity           :integer          default(0)
#  inbound_quantity        :integer          default(0)
#  reserved_quantity       :integer          default(0)
#  reserved_fc_processing  :integer          default(0)
#  reserved_fc_transfers   :integer          default(0)
#  reserved_customerorders :integer          default(0)
#  instock_date            :date
#  outstock_date           :date
#  days_instock            :integer
#  vendorasin_id           :integer
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  item_name               :text(65535)
#  item_description        :text(65535)
#  open_date               :date
#  image_url               :string(255)
#  product_id_type         :string(255)
#  item_note               :text(65535)
#  item_condition          :string(255)
#  fulfillment_channel     :string(255)
#  listing_id              :string(255)
#  is_invalid              :boolean          default(FALSE), not null
#  bagger_number           :integer          default(0)
#  sku_status              :string(255)      default("")
#  sales_velocity          :float(24)
#
# Indexes
#
#  index_seller_skus_on_user_id          (user_id)
#  index_seller_skus_on_user_id_and_sku  (user_id,sku) UNIQUE
#  index_seller_skus_on_vendorasin_id    (vendorasin_id)
#

class SellerSku < ApplicationRecord
    belongs_to  :user
    belongs_to  :vendorasin
    has_many    :vendoritems
    has_many    :pending_pos, through: :vendoritems
    has_many    :mws_order_items, foreign_key: :sku, primary_key: :sku
    has_many    :inventory_histories, dependent: :destroy

    VALID_DAYS = [1, 7, 30, 60, 90, 360]

    # has_and_belongs_to_many :vendoritems, association_foreign_key: 'vendorasin_id', join_table: 'vendorasins'
    # has_many    :vendoritems, -> { joins(:vendors).where('vendors.user_id = ?', 2 )}, through: :vendorasin

    # def vendoritems
    #     user.vendoritems.where('vendorasin_id =?', vendorasin.id)
    #     # vendorasin.vendorasins
    # end


    # def as_json(options={})
    # {
    #     id: id,
    #     sku: sku,
    #     price: price,
    #     item_quantity: item_quantity,
    #     inbound_quantity: inbound_quantity,
    #     reserved_quantity: reserved_quantity,
    #     last1daysales: last1daysales,
    #     last30daysales: last30daysales,
    #     last60daysales: last60daysales,
    #     last90daysales: last90daysales,
    #     last360daysales: last360daysales,
    #     asin: vendorasin.asin,
    #     amazon_selling: vendorasin.amazon_selling,
    #     item_name: item_name || vendorasin.name,
    #     bbs: bbs,
    #     vendorasin_id: vendorasin_id,
    #     prepinstruction: prepinstruction,
    # }
    # end

    def as_json(*)
      super.tap do |hash|
        hash["asin"] = vendorasin.asin
        hash["amazon_selling"] = vendorasin.amazon_selling
        hash["item_name"] = item_name || vendorasin.name
        hash["labeling"] = is_type_of_prep("labeling")
        hash["bubble"] = is_type_of_prep("Bubble")
        hash["polybag"] = is_type_of_prep("Polybag")
        hash["bundle"] = is_type_of_prep("Bundle")
        hash["box"] = is_type_of_prep("Box")
        hash["bbs"] = bbs
        hash["days_of_sale"] = days_of_sale
        hash["sales_velocity_for"] = (sales_velocity_for 30).round(2)
        hash["reorder_quantity"] = re_order_amount - already_ordered_amount
        hash["already_ordered_amount"] = already_ordered_amount
      end
    end

    def already_ordered_amount
      return 0 if vendoritems.count == 0
      pos = Po.where(vendoritem_id: vendoritems.ids)
      pos.sum(:order_quantity)
    end

    def is_type_of_prep (type)
      unless prepinstruction.nil?
        if prepinstruction.downcase.include? type.downcase
          "Yes"
        else
          ""
        end
      else
        ""
      end
    end


    def needed_refresh_prep
      prepinstruction.nil?
    end

    def bbs
      if vendorasin.amazon_selling
        "AMZ"
      elsif vendorasin.fbaoffers > 0
        "FBA"
      else
        "FBM"
      end
    end

    def self.bbs_search(bbs)
      if bbs == "AMZ"
        joins(:vendorasin).where("vendorasins.amazon_selling = true")
      elsif bbs == "FBA"
        joins(:vendorasin).where("vendorasins.amazon_selling = false and vendorasins.fbaoffers > 0")
      else
        joins(:vendorasin).where("vendorasins.amazon_selling = false and vendorasins.fbaoffers = 0 and vendorasins.fbmoffers > 0")
      end
    end

  def is_valid_fnsku
    !(vendorasin.asin == fnsku)
  end

  def sales_velocity_for days = 30
    return unless VALID_DAYS.include? days
    (send("last#{days}daysales").to_f) / (days - (30 - days_of_sale))
  end

  def formatted_sales_velocity
    current_sales_velocity = sales_velocity_for 30
    ((current_sales_velocity).nan? or (current_sales_velocity).infinite?) ? 0 : (current_sales_velocity)
  end

  def re_order_quantity_for days = 30
    ((sales_velocity_for 30) * days) - item_quantity
  end

  def calculate_quantity_and_days
    # Calculates quantities and on days on which orders were made for last 30 days
    quantity = 0
    days = 0
    item_hash = mws_order_items.within_month.map{
                                      |item| {date: item.mws_order.last_updated_date.to_date, quantity: item.quantity}
                                    }.group_by{|h| h[:date]}
    item_hash.keys.each do |key|
      days += 1
      quantity += item_hash[key].map{|h| h[:quantity]}.sum
    end
    p quantity, days
  end

  def days_of_sale
    30 - (inventory_histories.with_stock.within_one_month
                                        .order("DATE(created_at)")
                                        .group("DATE(created_at)").count.count)
  end

  def re_order_amount
    re_order_amount = re_order_quantity_for 30
    ((re_order_amount).nan? or (re_order_amount).infinite?) ? 0 : (re_order_amount).to_i
  end
end
