# == Schema Information
#
# Table name: invoices
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  invoice_name      :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  ship_term         :integer          default("")
#  shipping_cost     :decimal(11, 2)   default(0.0)
#  adjustment_cost   :decimal(11, 2)   default(0.0)
#  adjustment_reason :string(255)      default("")
#  check_date        :date
#  cleared           :boolean          default(FALSE)
#  printed           :boolean          default(FALSE)
#  check_number      :string(255)      default("")
#  reported          :boolean          default(FALSE)
#
# Indexes
#
#  index_invoices_on_user_id  (user_id)
#

class Invoice < ApplicationRecord
  # invoice can include multi user's Po items.
  # belongs_to  :user
  has_many  :invoice_items
  has_many  :pending_pos, through: :invoice_items

  enum ship_term: ['', "CC", "Net_15"]

  def self.filter_user(user_name)
    Invoice.all.select { |c| c.user_list.include? user_name}
  end
  def pending_po_index
    pending_pos.first&.pending_po_index
  end

  def pending_po_with_user(user_id = nil)
    unless user_id.nil?
      ppis = pending_pos.joins(vendoritem: :vendor).where("vendors.user_id = ?", user_id)
    else
      ppis = pending_pos
    end
    ppis
  end

  def pending_po_list(user_id = nil)
    po_names = ""
    pending_po_ids_with_invoice = pending_pos.pluck(:pending_po_index_id).uniq

    unless user_id.nil?
      ppis = PendingPoIndex.joins(:vendor).where(vendors: {user_id: user_id}).where(:id =>pending_po_ids_with_invoice)
    else
      ppis = PendingPoIndex.where(:id => pending_po_ids_with_invoice)
    end
    unless ppis.nil?
      ppis.each do |ppi|
        po_names = po_names + "#{ppi.po_name}, "
      end
    end
    po_names.chomp(", ")
  end

  def user_list
    names = ""
    pending_po_ids_with_invoice = pending_pos.pluck(:pending_po_index_id).uniq
    user_id = PendingPoIndex.joins(vendor: :user).where(pending_po_indices: {id: pending_po_ids_with_invoice}).pluck("users.id").uniq
    User.where(:id => user_id).each do |user|
      names = names + "#{user.full_name}, "
    end
    names.chomp(", ")
  end

  def vendor_list
    vendors = ""
    pending_po_ids_with_invoice = pending_pos.pluck(:pending_po_index_id).uniq
    vendor_id = PendingPoIndex.where(pending_po_indices: {id: pending_po_ids_with_invoice}).pluck("vendor_id").uniq
    Vendor.where(:id => vendor_id).each do |vendor|
      vendors = vendors + "#{vendor.name}, "
    end
    vendors.chomp(", ")
  end

  def as_json(*)
    super.except("created_at", "updated_at").tap do |hash|
      hash["created_at"] = created_at.strftime("%B %d, %Y")
      hash["item_count"] = item_count
      hash["confirmed_qty"] = confirmed_qty
      hash["confirmed_cost"] = confirmed_cost
      hash["pending_po_index"] = pending_po_index
      hash["invoiced_qty"] = invoiced_qty
      hash["invoiced_cost"] = invoiced_cost
      hash["pending_po_list"] = pending_po_list
      hash["user_list"] = user_list
      hash["vendor_list"] = vendor_list
    end
  end

  def item_count
    invoice_items&.count
  end

  def confirmed_qty(user_id=nil)
    unless user_id.nil?
      pending_pos.joins(vendoritem: :vendor).where(vendors: {user_id: user_id}).sum(:confirmed_qty)
    else
      pending_pos.sum(:confirmed_qty)
    end
  end

  def confirmed_cost(user_id=nil)
    unless user_id.nil?
      pending_pos.joins(vendoritem: :vendor).where(vendors: {user_id: user_id}).sum(&:confirmed_cost)
    else
      pending_pos.sum(&:confirmed_cost)
    end
  end

  def invoiced_qty(user_id=nil)
    unless user_id.nil?
      invoice_items.joins(pending_po: :vendoritem).joins("INNER JOIN vendors on vendors.id = vendoritems.vendor_id").where(vendors: {user_id: user_id}).sum(:quantity)
    else
      invoice_items.sum(:quantity)
    end
  end

  def invoiced_cost(user_id=nil)
    unless user_id.nil?
      invoice_items.joins(pending_po: :vendoritem).joins("INNER JOIN vendors on vendors.id = vendoritems.vendor_id").where(vendors: {user_id: user_id}).sum(&:invoice_cost)
    else
      invoice_items.sum(&:invoice_cost)
    end
  end

  def received_qty(user_id=nil)
    unless user_id.nil?
      pending_pos.joins(vendoritem: :vendor).where(vendors: {user_id: user_id}).sum(:received_quantity)
    else
      pending_pos.sum(:received_quantity)
    end
  end

  def received_cost(user_id=nil)
    unless user_id.nil?
      pending_pos.joins(vendoritem: :vendor).where(vendors: {user_id: user_id}).sum(&:received_cost)
    else
      pending_pos.sum(&:received_cost)
    end
  end

  def sku_count(user_id=nil)
    unless user_id.nil?
      pending_pos.joins(vendoritem: :vendor).where(vendors: {user_id: user_id}).count
    else
      pending_pos.count
    end
  end
end
