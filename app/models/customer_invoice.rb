# == Schema Information
#
# Table name: customer_invoices
#
#  id                       :integer          not null, primary key
#  user_id                  :integer
#  vendor_id                :integer
#  invoice_name             :string(255)      default(""), not null
#  invoice_link             :string(255)
#  po_link                  :string(255)
#  labels                   :string(255)
#  vendor_order_placd       :date
#  order_no                 :string(255)
#  payment                  :string(255)
#  shipment                 :string(255)
#  order_date               :date
#  received                 :integer
#  shipped                  :integer
#  invoice_sales_order_cost :decimal(11, 2)   default(0.0)
#  warehouse_cost           :decimal(11, 2)   default(0.0)
#  warehouse_inv            :string(255)
#  total_paid               :decimal(11, 2)   default(0.0)
#  profit_difference        :decimal(11, 2)   default(0.0)
#  refund                   :decimal(11, 2)   default(0.0)
#  note                     :text(65535)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  pending_po_index_id      :integer
#
# Indexes
#
#  index_customer_invoices_on_pending_po_index_id  (pending_po_index_id)
#  index_customer_invoices_on_user_id              (user_id)
#  index_customer_invoices_on_vendor_id            (vendor_id)
#

class CustomerInvoice < ApplicationRecord
    belongs_to  :vendor
    belongs_to  :user
    belongs_to  :pending_po_index

    has_many    :pending_pos
    after_create :set_invoice_name

    def set_invoice_name
        failed = true
        while failed
            invoice_name = "DD#{self.user_id}U#{self.vendor_id}V#{self.pending_po_index_id}#{('0'..'9').to_a.shuffle[0,4].join}#{self.id}"
            if CustomerInvoice.where(:invoice_name => invoice_name).count == 0
                self.invoice_name = invoice_name
                self.save
                failed = false
            end
        end
    end
end
