# == Schema Information
#
# Table name: pos
#
#  id             :integer          not null, primary key
#  order_quantity :integer          default(1)
#  vendoritem_id  :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  received_qty   :integer          default(0)
#  po_status_id   :integer          default(0)
#
# Indexes
#
#  index_pos_on_po_status_id   (po_status_id)
#  index_pos_on_vendoritem_id  (vendoritem_id)
#

class Po < ApplicationRecord
    belongs_to :vendoritem
    belongs_to :vendorasin

    def order_cost
      order_quantity * vendoritem.cost
    end

    def order_profit
      order_quantity * vendoritem.profit
    end
    
    # def as_json(*)
    #   super.except("created_at", "updated_at").tap do |hash|
    #     hash["item_id"] = vendoritem.id
    #     hash["vendoritem"] = vendoritem
    #   end
    # end
    def as_json(options={})
    {
      id: id,
      created_at: created_at.strftime("%B %d, %Y"),


      asin: vendoritem.vendorasin.asin,
      vendortitle: vendoritem.title,
      vendor: vendoritem.vendor.name,
      upc: vendoritem.upc,
      cost: vendoritem.cost,
      buyboxprice: vendoritem.vendorasin.buyboxprice,
      fbafee: vendoritem.vendorasin.fbafee,
      storagefee: vendoritem.vendorasin.storagefee,
      variableclosingfee: vendoritem.vendorasin.variableclosingfee,
      commissionpct: vendoritem.vendorasin.commissionpct,
      commissiionfee: vendoritem.vendorasin.commissiionfee,
      salespermonth: vendoritem.vendorasin.salespermonth,
      packagequantity: vendoritem.vendorasin.packagequantity,
      totaloffers: vendoritem.vendorasin.totaloffers,
      fbaoffers: vendoritem.vendorasin.fbaoffers,
      fbmoffers: vendoritem.vendorasin.fbmoffers,
      margin: vendoritem.margin,
      profit: vendoritem.profit,
      packcount: vendoritem.packcount,
      packcost: vendoritem.packcost,
      vendorsku: vendoritem.vendorsku,
      salesrank: vendoritem.vendorasin.salesrank,
      product_groups_id: vendoritem.vendorasin.product_groups_id,
      item_id: vendoritem.id,
      order_quantity: order_quantity,
      order_cost: order_cost,
      received_qty: received_qty,
      sku: vendoritem.try(:seller_sku).try(:sku),
      sku_status: vendoritem.try(:seller_sku).try(:sku_status),
      vendorasin_id: vendoritem.vendorasin_id
    }
  end


  def self.profit(op, value)
    if op == '='
      where("(vendorasins.buyboxprice-cost*vendoritems.packcount-vendorasins.totalfbafee) = #{value}")
    elsif op == '<'
      
      where("(vendorasins.buyboxprice-cost*vendoritems.packcount-vendorasins.totalfbafee) < #{value}")
    end
  end

  def self.number_compare(op, value, field)
    if field == 'profit'
      where("(vendorasins.buyboxprice-cost*vendoritems.packcount-vendorasins.totalfbafee) #{op} #{value}")
    elsif field == 'margin'
      where("(vendorasins.buyboxprice-cost*vendoritems.packcount-vendorasins.totalfbafee)/cost/vendoritems.packcount*100 #{op} #{value}")
    else
        where("#{field} #{op} #{value}")
    end
  end
end
