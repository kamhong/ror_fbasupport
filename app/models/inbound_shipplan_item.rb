# == Schema Information
#
# Table name: inbound_shipplan_items
#
#  id                   :integer          not null, primary key
#  outbound_shipment_id :integer
#  inbound_shipplan_id  :integer
#  box_qty              :integer          default(0), not null
#  case_qty             :integer          default(0), not null
#  ship_qty             :integer          default(0), not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_inbound_shipplan_items_on_inbound_shipplan_id   (inbound_shipplan_id)
#  index_inbound_shipplan_items_on_outbound_shipment_id  (outbound_shipment_id)
#

class InboundShipplanItem < ApplicationRecord
    belongs_to :inbound_shipplan
    belongs_to :outbound_shipment
    
    def as_json(options={})
    {
        plan_id: inbound_shipplan.id,
        plan_shipment_id: inbound_shipplan.shipplan_id
    }
    end
end
