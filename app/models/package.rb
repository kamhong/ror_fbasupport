# == Schema Information
#
# Table name: packages
#
#  id                   :integer          not null, primary key
#  dimenstion_unit      :string(255)
#  length               :float(24)
#  width                :float(24)
#  height               :float(24)
#  weight_unit          :string(255)
#  weight               :float(24)
#  transport_content_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Package < ApplicationRecord
  belongs_to :transport_content
  has_many :package_informations, dependent: :destroy

  accepts_nested_attributes_for :package_informations, :allow_destroy => true

  def barcode_data
    barcode_data = ""
    barcode_data += "AMZN,PO:#{transport_content.inbound_shipment.shipment_id},"
    package_informations.each do |pkg_info|
      if pkg_info.quantity > 0
        barcode_data += "ASIN:#{pkg_info.asin},QTY:#{pkg_info.quantity}"
        barcode_data += ",EXP:#{pkg_info.expiry_date.strftime('%Y%m%d')}" if pkg_info.expiry_date.present?
      end
    end
    barcode_data
  end
end
