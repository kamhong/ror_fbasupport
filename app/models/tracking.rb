# == Schema Information
#
# Table name: trackings
#
#  id                   :integer          not null, primary key
#  tracking_number      :string(255)
#  transport_content_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Tracking < ApplicationRecord
end
