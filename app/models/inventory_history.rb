# == Schema Information
#
# Table name: inventory_histories
#
#  id                         :integer          not null, primary key
#  price                      :float(24)
#  total_quantity             :integer
#  fulfillable_quantity       :integer
#  unsellable_quantity        :integer
#  reserved_quantity          :integer
#  per_unit_volume            :integer
#  inbound_working_quantity   :integer
#  inbound_shipped_quantity   :integer
#  inbound_receiving_quantity :integer
#  seller_sku_id              :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#
# Indexes
#
#  index_inventory_histories_on_seller_sku_id  (seller_sku_id)
#

class InventoryHistory < ApplicationRecord
  belongs_to :seller_sku

  scope :within_one_month, -> { where(created_at: (Time.now - 30.days)..(Time.now)) }
  scope :with_stock, -> { where.not(total_quantity: 0) }

  def self.generate_dummy_data
    days = []
    inventory_dataset = [0, 1, 3]
    price_dataset = [12.00, 13.00, 14.00]
    SellerSku.last(500).each do |sku|
      Date.new(2019, 04, 10).upto(Date.new(2019, 05, 10)) do |date|
        sku.inventory_histories.create(total_quantity: inventory_dataset.sample, price: price_dataset.sample, inbound_shipped_quantity: 987, updated_at: date)
      end
    end
  end
end
