# == Schema Information
#
# Table name: user_market_places
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_user_market_places_on_user_id  (user_id)
#

class UserMarketPlace < ApplicationRecord
end
