# == Schema Information
#
# Table name: invoice_items
#
#  id            :integer          not null, primary key
#  invoice_id    :integer
#  pending_po_id :integer
#  quantity      :integer          default(0), not null
#  cost          :decimal(11, 2)   default(0.0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_invoice_items_on_invoice_id     (invoice_id)
#  index_invoice_items_on_pending_po_id  (pending_po_id)
#

class InvoiceItem < ApplicationRecord
  belongs_to  :invoice
  belongs_to  :pending_po

  def invoice_cost
    quantity * cost
  end
end
