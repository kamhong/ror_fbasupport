# == Schema Information
#
# Table name: users
#
#  id                               :integer          not null, primary key
#  email                            :string(255)      default(""), not null
#  encrypted_password               :string(255)      default(""), not null
#  reset_password_token             :string(255)
#  reset_password_sent_at           :datetime
#  remember_created_at              :datetime
#  sign_in_count                    :integer          default(0), not null
#  current_sign_in_at               :datetime
#  last_sign_in_at                  :datetime
#  current_sign_in_ip               :string(255)
#  last_sign_in_ip                  :string(255)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  active                           :boolean          default(TRUE)
#  role_id                          :integer
#  company_id                       :integer
#  subscription_id                  :string(255)
#  mws_access_key_id                :string(255)
#  mws_access_acces_key             :string(255)
#  seller_id                        :string(255)
#  mws_access_token                 :string(255)
#  aws_api_key                      :string(255)
#  aws_api_secret_key               :string(255)
#  aws_associate_tag                :string(255)
#  mws_key_valid                    :boolean          default(FALSE)
#  mws_market_place_id              :string(255)      default("ATVPDKIKX0DER")
#  first_name                       :string(255)      default(""), not null
#  last_name                        :string(255)      default(""), not null
#  sku_prefix                       :string(255)      default(""), not null
#  warehouse_name                   :string(255)
#  warehouse_address_line1          :string(255)
#  warehouse_address_line2          :string(255)
#  warehouse_city                   :string(255)
#  warehouse_state_or_province_code :string(255)
#  warehouse_postal_code            :string(255)
#  warehouse_country_code           :string(255)
#  skynet_key                       :string(255)      default("")
#  skynet_api_enabled               :boolean          default(FALSE)
#  skynet_api_user_id               :string(255)
#
# Indexes
#
#  index_users_on_company_id            (company_id)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_role_id               (role_id)
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :role
  belongs_to :company
  belongs_to :subscription
  has_many :vendors, dependent: :destroy
  has_many :skynets, dependent: :destroy
  has_many :vendoritems, through: :vendors
  has_many  :seller_skus
  has_many  :pos, through:  :vendors
  has_many  :pending_pos, through:  :pending_po_indices
  has_many  :outbound_shipments, through: :pending_pos
  has_many  :pending_po_indices, through:  :vendors
  has_many  :mws_orders
  has_many  :mws_report_requests
  has_many  :customer_invoices
  has_many  :inbound_shipplans
  has_many  :inbound_shipments
  validates :email, presence: true, uniqueness: true
  # validates :company_exists
  # validates_presence_of :company
  before_save :strip_whitespace, on: [:create, :update]
  attr_accessor :current_password

  def strip_whitespace
    self.first_name = self.first_name.strip unless self.first_name.nil?
    self.last_name = self.last_name.strip unless self.last_name.nil?
    self.email = self.email.strip unless self.email.nil?
    self.mws_access_token = self.mws_access_token.strip unless self.mws_access_token.nil?
    self.mws_market_place_id = self.mws_market_place_id.strip unless self.mws_market_place_id.nil?
    self.seller_id = self.seller_id.strip unless self.seller_id.nil?
  end

  def key_changed(user)
    if self.seller_id != user[:seller_id]
      return true
    elsif self.mws_access_token != user[:mws_access_token]
      return true
    elsif self.mws_access_acces_key != user[:mws_access_acces_key]
      return true
    elsif self.mws_access_key_id != user[:mws_access_acces_key]
      return true
    end
    return false
  end

  def is_warehouse
    # if self.role.name == "Warehouse"
    #   true
    # else
    #   false
    # end
    if self.role_id == 3
      true
    else
      false
    end
  end
  def get_vendors
    if self.role.name == 'Manager'
      return self.company.vendors
    else
      return self.vendors
    end
  end

  def get_skynets
    if self.role.name == 'Manager'
      return self.company.skynets
    else
      return self.skynets
    end
  end

  def get_weeks_last_sign
    timeDiff = Time.current - self.last_sign_in_at
    days =   (timeDiff / 1.day).round
    if days  == 7
      # return (days / 7).round
      return 1
    else
      return 0
    end
  end

  def get_days_last_sign
    timeDiff = Time.current - self.last_sign_in_at
    days =   (timeDiff / 1.day).round
    return days
  end

  def self.send_notification
    User.all.each do |user|
      weeks = user.get_weeks_last_sign()
      if weeks > 0
        @email = user.email
        UserMailer.send_signin_notification(@email).deliver_now
      end

      if user.active  && user.subscription_id.nil? && user.get_days_last_sign() > 29
        user.active = false
        user.save
      end

    end


    # UserMailer.send_signin_notification('daniel.parker989@gmail.com').deliver_later
  end

  def self.test_worker
    CheckUserMwsValidWorker.perform_async(1)
  end

  def company_exists
    if company_id.nil?
      errors.add(:email, "Company is required")
    end
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def label
    ("#{first_name.presence || 'User'} - #{email}").truncate(30)
  end

  def admin?
    role.name == "Manager" or role.name == "Warehouse"
  end

  def as_json(option={})
    super.except("created_at", "updated_at").tap do |hash|
      hash[:full_name] = full_name
    end
  end

  def formatted_state_code
    warehouse_state_or_province_code.present? ? warehouse_state_or_province_code.gsub(/[a-z]+/, "").gsub(" ", "") : ""
  end

  def warehouse_address
    if [warehouse_name,warehouse_address_line1,warehouse_city,warehouse_state_or_province_code,warehouse_postal_code,warehouse_country_code].all?
      {
        name: warehouse_name,
        address_line1: warehouse_address_line1,
        address_line2: warehouse_address_line2,
        city: warehouse_city,
        state_or_province_code: formatted_state_code,
        postal_code: warehouse_postal_code,
        country_code: warehouse_country_code
      }
    else
      {
        name: "FBA.support Prep & Pack",
        address_line1: "36-16 29th street",
        address_line2: "",
        city: "Long Island City",
        state_or_province_code: "NY",
        postal_code: "11106",
        country_code: "US"
      }
    end
  end

  def total_inbound_shipment_boxes
    inbound_shipments.map(&:total_box_count).reject(&:nil?).sum
  end

  def total_inbound_shipment_palletes
    inbound_shipments.map(&:total_pallete_count).reject(&:nil?).sum
  end

end
