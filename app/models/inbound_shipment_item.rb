# == Schema Information
#
# Table name: inbound_shipment_items
#
#  id                  :integer          not null, primary key
#  inbound_shipment_id :integer
#  seller_sku          :string(255)
#  quantity_shipped    :integer          default(0), not null
#  quantity_in_case    :integer          default(0), not null
#  quantity_received   :integer          default(0), not null
#  expired_date        :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  pending_po_id       :integer
#
# Indexes
#
#  index_inbound_shipment_items_on_inbound_shipment_id  (inbound_shipment_id)
#  index_inbound_shipment_items_on_pending_po_id        (pending_po_id)
#  index_inbound_shipment_items_on_seller_sku           (seller_sku)
#

class InboundShipmentItem < ApplicationRecord
  belongs_to  :inbound_shipment
  belongs_to  :pending_po
  
  def prep_fee_summary
    prep_fee_summary = ""
    sku = pending_po&.vendoritem&.seller_sku
    unless sku&.prepinstruction.nil?
      if sku&.prepinstruction.downcase.include? "Labeling".downcase
        prep_fee_summary = prep_fee_summary + "Labeling $#{number_to_currency(0.18 * quantity_shipped)}, "
      end
      if sku&.prepinstruction.downcase.include? "Bubble".downcase
        prep_fee_summary = prep_fee_summary + "Bubble $#{number_to_currency(0.18 * quantity_shipped)}, "
      end
      if sku&.prepinstruction.downcase.include? "Polybag".downcase
        prep_fee_summary = prep_fee_summary + "Polybag $#{number_to_currency(0.18 * quantity_shipped)}, "
      end
      if sku&.prepinstruction.downcase.include? "Box".downcase
        prep_fee_summary = prep_fee_summary + "Box $#{number_to_currency(1.5 * quantity_shipped)}, "
      end
      if pending_po&.vendoritem&.packcount > 1
        prep_fee_summary = prep_fee_summary + "Pack $#{number_to_currency(0.48 * quantity_shipped)}, "
      end
    end

    prep_fee_summary.chomp(", ")
  end

  def prep_fee_total
    prep_fee_total = 0
    sku = pending_po&.vendoritem&.seller_sku

    unless sku&.prepinstruction.nil?
      if sku&.prepinstruction.downcase.include? "Labeling".downcase
        prep_fee_total += 0.18 * quantity_shipped
      end
      if sku&.prepinstruction.downcase.include? "Bubble".downcase
        prep_fee_total += 0.18 * quantity_shipped
      end
      if sku&.prepinstruction.downcase.include? "Polybag".downcase
        prep_fee_total += 0.18 * quantity_shipped
      end
      if sku&.prepinstruction.downcase.include? "Box".downcase
        prep_fee_total += 1.5 * quantity_shipped
      end
      if pending_po&.vendoritem&.packcount > 1
        prep_fee_total += 0.48 * quantity_shipped
      end
    end
    prep_fee_total
  end

  def item_cost
    unless pending_po&.vendoritem.nil?
      quantity_shipped * pending_po.vendoritem.packcount * pending_po.cost
    else
      0
    end
  end

  private
  def number_to_currency(str)
    ActionController::Base.helpers.number_to_currency(str, delimiter: "", unit: "").to_f
  end
end
