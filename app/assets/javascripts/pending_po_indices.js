
var pending_po_editor; // use a global for the submit and return data rendering in the examples
var pending_po_index_editor;
var deletor;
var pending_po_table;
var pending_po_summary_table;
var pending_po_id;
var buttonCommon
var select_option_status_list = [];
var status_class = "number_cell";
var filter_status = "";
var datable_pending_po_search = "";
function showfilter(column) {
    // $("#dlg_set_filter").modal();
    $('#dlg_set_filter_'+column).modal();
}


$("#datable_pending_po thead :input:not(:checkbox)").on( 'keyup change', function () {
    console.log("ASD");
    pending_po_table
        .column( $(this).parent().index()+':visible' )
        .search( this.value )
        .draw();
    // console.log("AD");
    // var _this = $(this);
    //     clearTimeout(input_timer);
    //     input_timer = setTimeout(function() {
    //         if (datable_pending_po_search == _this.val())
    //             return;
    //         console.log("ASD");
    //         datable_pending_po_search = _this.val();
    //         pending_po_table
    //             .column( _this.parent().index()+':visible' )
    //             .search( _this.val() )
    //             .draw();
    //     }, input_delay );
} );
function loadpendingpo_summary_table(){
    if ( $.fn.dataTable.isDataTable( '#datatable_pending_po_summary' ) ) {
        pending_po_summary_table = $('#datatable_pending_po_summary').dataTable();
        pending_po_summary_table.fnDestroy();
    }

    pending_po_summary_table = $('#datatable_pending_po_summary').DataTable({
        paging:   true,  // Table pagination
        ordering: true,  // Column ordering
        info:     true,  // Bottom left status text
        processing: true,
        serverSide: true,
        iDisplayLength : 25,
        scrollY:        'calc(100vh - 300px)',
        scrollX: true,
        scrollCollapse: true,
        deferRender: true,
        orderCellsTop: true,  
        sAjaxSource: '/pending_po_indices/loadsummary?filter_status=' + filter_status,
        aoColumns: [
            { mData: 'po_name', render: function (data, type, full, meta) {
                return '<a href="/pending_po_indices/'+full.id+'">' + data + '</a>';
            } },
            { mData: 'created_at'},
            { mData: 'vendor.user.full_name'},
            { mData: 'vendor.name', render: function (data, type, full, meta) {
                return '<a href="/pending_po_indices/'+full.id+'">' + data + '</a>';
            } },
            { mData: 'vendor.warehouse_vendor.name', 'className':' number_cell', render: function (data, type, full, meta) {
                var warehouse_vendor = full.vendor.warehouse_vendor;
                if (warehouse_vendor != null){
                    return '<a href="/pending_pos?warehouse_vendor='+ warehouse_vendor.id +'">' + data + '</a>';
                }else{
                    return   data ;
                }
            } },
            { mData: 'pending_status', className: status_class },
            { mData: 'order_qty' },
            { mData: 'confirmed_qty' },
            { mData: 'received_quantity' },
            { mData: 'total_shipped_pack' },
            { mData: 'missing_quantity' },
            { mData: 'order_e_cost' },
            { mData: 'confiremd_e_cost' },
        ],
        rowId: 'id',
        aoColumnDefs: [ 
          { className:"number_cell", "targets": "_all"} 
        ],
        initComplete: function () {
            this.api().columns().every( function (i) {
                var column = this;
                var option_value = "";
                if(i == 0){
                    option_value = $('#po_option').html();
                }else if (i == 2){
                    option_value = $('#user_option').html();
                }else if (i == 3){
                    option_value = $('#vendor_option').html();
                }else if(i == 4){
                    var select = $('<span>' + 'Reset' + ' <i class="fa fa-times"></i></span>')
                        .appendTo(  $("#datatable_pending_po_summary_wrapper thead tr:eq(1) th").eq(column.index()).empty() )
                        .on( 'click', function () {
                            
                            var val = $(this).val();
                            column
                                .search( val ? '' : '', true, false )
                                .draw();
                        } );
                }else if (i == 5){
                    /*option_value = $('#sel_pending_po_index_status').html();*/
                }
                if (option_value != ""){
                    var select = $('<select><option value=""></option></select>')
                    .appendTo(  $("#datatable_pending_po_summary_wrapper thead tr:eq(1) th").eq(column.index()).empty() )
                    .on( 'change', function () {
                        var val = $(this).val();

                        column
                            .search( val ? val : '', true, false )
                            .draw();
                    } );


                    select.append( option_value );
                }
                
                
            } );
        },
        // "createdRow": function ( row, data, index ) {
        // },
        // dom: '<"bottom"l>rtip',
        dom: 'rt<"bottom"lp><"clear"><"bottom"i>',
        // buttons: [
           
        // ],
        // order: [[1, 'asc']]
    });
    $('#datatable_pending_po_summary_wrapper thead tr#filterrow th').each( function (i) {
        // if(i!=0){
          var title = $(this).text();
          $(this).html('' );    
        // }
    } );
}

function loadpendingpotable(pending_po_id){
    if ( $.fn.dataTable.isDataTable( '#datable_pending_po' ) ) {
        pending_po_table = $('#datable_pending_po').dataTable();
        pending_po_table.fnDestroy();
    }

    pending_po_table = $('#datable_pending_po').DataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
        "processing": true,
        "serverSide": true,
        "iDisplayLength" : 25,
        deferRender: true,
        orderCellsTop: true,  
        sAjaxSource: '/pending_po/load?pending_po='+pending_po_id,
        aoColumns: [
          // { mData: null},
          {
                mData: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
           } ,
          { mData: 'id' },
          { mData: 'vendoritem.vendorasin.name', render: function (data, type, full, meta) {
                if (full.vendoritem.vendortitle == null){
                    return '<span data-toggle="tooltip" title="' + data + '">' + data + '</span>';
                }else{
                    return '<span data-toggle="tooltip" title="' + full.vendoritem.vendortitle + '">' + full.vendoritem.vendortitle + '</span>';
                }
            } },
            { mData: 'vendoritem.seller_sku.sku' },
          { mData: 'vendoritem.asin' },
          { mData: 'vendoritem.vendorsku' },
          { mData: 'vendoritem.upc' },
          { mData: 'order_quantity', 'className': status_class},
          { mData: 'received_quantity','className': status_class},
          { mData: 'confirmed_qty','className': status_class},
          { mData: 'order_cost' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'confirmed_cost' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'cost' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'vendoritem.packcount' },
          { mData: 'vendoritem.packcost', render: $.fn.dataTable.render.number( ',', '.', 2, '$' ) },
          { mData: 'status', 'className': status_class },
          { mData: 'status_date_string' },
          { mData: 'vendoritem.vendorasin.buyboxprice', render: $.fn.dataTable.render.number( ',', '.', 2, '$' ) },
          { mData: 'vendoritem.vendorasin.salesrank' },
          { mData: 'vendoritem.profit', render: $.fn.dataTable.render.number( ',', '.', 2,'$')},
          { mData: 'vendoritem.margin',render: $.fn.dataTable.render.number( ',', '.', 2, '','%' )},
          { mData: 'vendoritem.vendorasin.fbafee' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'vendoritem.vendorasin.commissionpct' },
          { mData: 'vendoritem.vendorasin.commissiionfee' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'vendoritem.vendorasin.salespermonth' },
          { mData: 'vendoritem.vendorasin.totaloffers' },
          { mData: 'vendoritem.vendorasin.fbaoffers' },
          { mData: 'vendoritem.vendorasin.fbmoffers' },
          {mData: 'item_id'},
        ],
        rowId: 'id',
        aoColumnDefs: [ 
          {
              targets: [24],
              "visible": false,
          },
          { className:"number_cell", "targets": [1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]} 
        ],
        "createdRow": function ( row, data, index ) {
            if ( data['vendoritem']['profit'] > 0 ) {
                $('td', row).eq(19).addClass('positive_profit');
            }else{
                $('td', row).eq(19).addClass('negative_profit');
            }
            $('td', row).eq(2).addClass('abbr_cell');
            
        },
        dom: 'Blfrtip',
        // select: true,
        select:{
                style: 'multi'
            },
         buttons: [
            // { extend: "edit",   editor: editor },
            // { extend: "remove", editor: deletor },
            {
                text: 'Refresh',
                action: function ( e, dt, node, config ) {
                    
                    var sData = pending_po_table.rows( { selected: true } ).data();
                    if(sData.length == 0){
                      swal("Please select the Items to refresh");
                      return;
                    }
                    var sIds = []; 
                    for (var i = 0; i < sData.length; i++) {
                      sIds.push(sData[i].vendorasin_id);
                    }
                    console.log(sIds);
                    $.ajax({
                      url: '/api/refreshitems', 
                      dataType: 'json',  
                      data: {'ids': sIds},
                      type: 'post',
                      beforeSend: function(){
                      },
                      success: function(data){
                        if(data.result == true){
                            pending_po_table.ajax.reload();
                            swal("Items are successfully refreshed");
                        }else{
                            swal("Error occured while refreshing the items");
                        }
                      }
                   });
                }
                
            },
           
           'selectAll',
           'selectNone',
           {    extend: 'collection',
                text: 'Export Selected',
                // buttons: ['copy','csv','excel','pdf','print'],
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Excel',
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        }
                    },
                ],
           },
            
           
        ],
        order: [[1, 'asc']]
    });

}




function initComplete () {
    $('#datable_pending_po thead tr#filterrow th').each( function (i) {
        var title = $(this).text();
        if(title == '' ){

        }
        else if(title == 'EST' || title == 'Profit' || title == 'Margin' || title == 'SalesRank'){
            $(this).html( '<input type="hidden" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" name="'+title+'" id="txt_'+title+'"/><a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Search '+title+'" href="javascript:showfilter(\''+title+'\')">'+'<em class="fa fa-search"></em>'+'</a>' );
        }else{
            $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );  
        }
    } );
}


$(document).ready(function(){
    $('.btn-filter-set').click(function () {
        var column = $(this).data('column');
        var search_string = '';
        $(".element_"+column).each(function(){
            var id = this.id;
            var split_id = id.split("_");
            var column = split_id[1];
            var index = split_id[2];

            var operation = $("#slt_expression_"+column+"_"+index).val();
            var value = $("#txt_comparevalue_"+column+"_"+index).val();
            if(isNaN(value) === false){
                search_string = search_string + operation + "$$" + value + "##";
            }
        });
        if(search_string != ''){
            $('#txt_'+column).attr('value',search_string);
            $('#txt_'+column).trigger("change");
        }
        
    })

    $('.btn-filter-reset').click(function () {
        var column = $(this).data('column');
        $('#txt_'+column).attr('value', '');
        $('#txt_'+column).trigger("change");
    })

    $('.filter-status').click(function() {
        filter_status = $(this).find('input').val();
        
        pending_po_summary_table.ajax.url('/pending_po_indices/loadsummary?filter_status=' + filter_status).load();
    })

    $(".add-skill").click(function(){
        var column = $(this).data('column');
        
        // Finding total number of elements added
        
        // last <div> with element class id
        var lastid = $(".element_"+column+":last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[2]) + 1;

        // Check total number elements
        // Adding new div container after last occurance of element class
        $(".element_"+column+":last").after("<div class='element_"+column+"' id='div_"+column+"_"+ nextindex +"'></div>");
        
        // Adding element to <div>
        $("#div_" +column+"_"+ nextindex).append("<select name='slt_expression_"+column+"_"+nextindex+"' id='slt_expression_"+column+"_"+nextindex+"'> \
            <option value='eq'>=</option>\
            <option value='lg'>></option>\
            <option value='el'>>=</option>\
            <option value='ls'><</option>\
            <option value='es'><=</option>\
        </select>\
      <input type='number' name='txt_comparevalue_"+column+"_"+nextindex+"' id='txt_comparevalue_"+column+"_"+nextindex+"'>\
      <a id='remove_" +column+'_'+ nextindex + "' class='btn btn-primary btn-sm editable-submit remove_filter' ><i class='fa fa-fw fa-remove'></i></a>");
        
    });

        // Remove element
    $('.multiple-filter-dlg').on('click','.remove_filter',function(){
        var id = this.id;
        var split_id = id.split("_");
        var column = split_id[1];
        var deleteindex = split_id[2];
        // Remove <div> with id
        $("#div_"+column+"_" + deleteindex).remove();
    });

    $('#datatable_pending_po_summary').on('click', '.supplier_prefix', function(){
        var val = $(this).text();
        pending_po_summary_table
            .column( $(this).index()+':visible' )
            .search( val)
            .draw();
    });
});

function initPendigPoEditor(){
    pending_po_index_editor = new $.fn.DataTable.Editor( {
        ajax: "/pending_po_indices/updateitem",
        table: "#datatable_pending_po_summary",
        idSrc:  'id',
        fields: [
            {
                label: "Status",
                name: "pending_status",
                type: "select",
                ipOpts: select_option_status_list
            },
        ]
    } );
}

function initPendigPoItemEditor(){
    pending_po_editor = new $.fn.DataTable.Editor( {
        ajax: "/pending_pos/updateitem",
        table: "#datable_pending_po",
        idSrc:  'id',
        fields: [
            {
                label: "Status",
                name: "status",
                type: "select",
                ipOpts: select_option_status_list
            },
            {
                label: "Received",
                name: "received_quantity",
            },
            {
                label: "Order",
                name: "order_quantity",
            },
            {
                label: "Confirmed",
                name: "confirmed_qty",
            },
            
            
        ]
    } );
}

$('#datatable_pending_po_summary').on( 'click', 'tbody td.editable', function (e) {
    pending_po_index_editor.inline( pending_po_summary_table.cell(this).index(), {
        onBlur: 'submit'
    });
} );

$('#datable_pending_po').on( 'click', 'tbody td.editable', function (e) {
    pending_po_editor.inline( this);
} );