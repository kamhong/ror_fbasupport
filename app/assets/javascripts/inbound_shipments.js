$( document ).ready(function() {
  $('.frieght-date-field').datepicker({ format: 'yyyy/mm/dd' });

  $(document).on('click', '.package-wrapper .clone-package', function(){
    $("[data-blueprint-id=packages_fields_blueprint]").click();
    length        = $(this).parents(".package-wrapper").find(".package-length").val()
    width         = $(this).parents(".package-wrapper").find(".package-width").val()
    height        = $(this).parents(".package-wrapper").find(".package-height").val()
    weight        = $(this).parents(".package-wrapper").find(".package-weight").val()
    dimensionUnit = $(this).parents(".package-wrapper").find(".package-dimension-unit").val()
    weightUnit    = $(this).parents(".package-wrapper").find(".package-weight-unit").val()

    fieldView = $(".package-wrapper").last();

    fieldView.find(".package-length").val(length)
    fieldView.find(".package-width").val(width)
    fieldView.find(".package-height").val(height)
    fieldView.find(".package-weight").val(weight)
    fieldView.find(".package-dimension-unit").val(dimensionUnit)
    fieldView.find(".package-weight-unit").val(weightUnit)
  });

});

$('.shipment-type-radio, .partner-radio').change(function(){
  amazon_carrier = $(".partner-radio:checked").val()
  shipment_type = $('.shipment-type-radio:checked').val()

  $(".transport-info-section").addClass("hidden")
  $(".submit-btn").addClass("hidden")
  $(".required").prop('required', false)
  if (amazon_carrier == "true" && shipment_type == "SP") {
    $(".partnered-small-parcel").removeClass("hidden")
    $(".partnered-small-parcel .required").prop('required', true)
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "false" && shipment_type == "SP") {
    $(".non-partnered-small-parcel").removeClass("hidden")
    $(".non-partnered-small-parcel .required").prop('required', true)
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "true" && shipment_type == "LTL") {
    $(".partnered-ltl-parcel").removeClass("hidden")
    $(".partnered-ltl-parcel .required").prop('required', true)
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "false" && shipment_type == "LTL") {
    $(".non-partnered-ltl-parcel").removeClass("hidden")
    $(".non-partnered-ltl-parcel .required").prop('required', true)
    $(".submit-btn").removeClass("hidden")
  }
});

$(".show-submit-btn").click(function(){
	$(".submit-button").removeClass("hidden")
});

jQuery(function ($) {
  $(document).on('nested:fieldRemoved', function (event) {
    $('[required]', event.field).removeAttr('required');
    event.field.find('input').val("");
  });
});


$('.send_email').bind('ajax:success', function() {
  alert("Email was sent successfully");
});