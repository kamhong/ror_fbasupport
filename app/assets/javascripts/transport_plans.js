//= require select2/dist/js/select2
$(document).ready(function() { $('.user-search-select').select2(); });
$(".shipment-selects select").on('change', function() {
  amazon_carrier = $("#transport_content_delivery_by_amazon").val()
  shipment_type = $("#transport_content_shipment_type").val()
  $(".transport-info-section").addClass("hidden")
  $(".submit-btn").addClass("hidden")
  if (amazon_carrier == "true" && shipment_type == "SP") {
    $(".partnered-small-parcel").removeClass("hidden")
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "false" && shipment_type == "SP") {
    $(".non-partnered-small-parcel").removeClass("hidden")
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "true" && shipment_type == "LTL") {
    $(".partnered-ltl-parcel").removeClass("hidden")
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "false" && shipment_type == "LTL") {
    $(".non-partnered-ltl-parcel").removeClass("hidden")
    $(".submit-btn").removeClass("hidden")
  }
});
