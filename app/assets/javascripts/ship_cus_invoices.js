var invoice_table;
var editor
function load_invoice_table(){
    editor = new $.fn.DataTable.Editor( {
        ajax: "/ship_cus_invoices/updateitem",
        table: "#datatable_ship_customer_invoice",
        idSrc:  'id',
        fields: [
            {
                label: "Date",
                name: "paid_date",
                type:   'datetime',
                def:    function () { return new Date(); },
                format: 'YYYY/MM/DD',
                // fieldInfo: 'US style m/d/y format'
            }
        ]
    } );

    if ( $.fn.dataTable.isDataTable( '#datatable_ship_customer_invoice' ) ) {
        invoice_table = $('#datatable_ship_customer_invoice').dataTable();
        invoice_table.fnDestroy();
    }
  
    invoice_table = $('#datatable_ship_customer_invoice').DataTable({
            'paging':   true,  // Table pagination
            'ordering': false,  // Column ordering
            'info':     true,  // Bottom left status text
            // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
            "processing": true,
            "serverSide": true,
            "iDisplayLength" : 25,
            scrollY:        'calc(100vh - 300px)',
            scrollX: true,
            scrollCollapse: true,
            deferRender: true,
            orderCellsTop: true,  
            sAjaxSource: '/ship_cus_invoices/load_invoices',
            // sServerMethod: "POST",
            aoColumns: [
                { mData: 'invoice_name', render: function (data, type, full, meta) {
                        var invoice_id = full.id;
                        return '<a class="btn btn-primary btn-sm" href="/ship_cus_invoices/'+invoice_id+'.pdf">' + data + '</a>';
                    } 
                },
                { mData: 'created_at' },
                { mData: 'inbound_shipment.user.full_name' },
                { mData: 'inbound_shipment.shipment_id' },
                { mData: 'inbound_shipment.shipment_name' },
                { mData: 'paid_date', 'className': 'editable number_cell' },
                { mData: 'inbound_shipment.sku_count'},
                { mData: 'inbound_shipment.ship_cost'},
                { mData: 'inbound_shipment.ship_qty' },
            ],
            rowId: 'id',
            aoColumnDefs: [ 
                { className:"number_cell", "targets": "_all"} ,
            ],
            // order: [[1, 'asc']],
            initComplete: function () {
                this.api().columns().every( function (i) {
                    var column = this;
                    var option_value = "";
                    if (i == 2){
                        option_value = $('#user_id_option').html();
                    }
                    if (option_value != ""){
                        var select = $('<select><option value=""></option>'+option_value+'</select>')
                            .appendTo(  $("#datatable_ship_customer_invoice_wrapper thead tr:eq(1) th").eq(column.index()).empty() )
                            .on( 'change', function () {
                                var val = $(this).val();
                                column
                                    .search( val ? val : '', true, false )
                                    .draw();
                            } );
                    }
                });
            }
      });
  
    initComplete();
  
    $("#datatable_ship_customer_invoice_wrapper thead :input:not(:checkbox)").on( 'keyup change', function () {
        invoice_table
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );
}

function initComplete(){
    $('#datatable_ship_customer_invoice_wrapper thead tr#filterrow th').each( function (i) {
        var title = $(this).text();
        if(title == '' ){

        }
        else if(i == 0 || i == 2 || i == 3 || i == 4){
            $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );  
        }else{
            $(this).html('');        
        }

    } );
}
$('#datatable_ship_customer_invoice').on( 'click', 'tbody td.editable', function (e) {
    editor.inline( invoice_table.cell(this).index(), {
        onBlur: 'submit'
    });
} );