

var editor; // use a global for the submit and return data rendering in the examples
var deletor;
var table;
var buttonCommon

function showfilter(column) {
    // $("#dlg_set_filter").modal();
    $('#dlg_set_filter_'+column).modal();
}
(function(window, document, $, undefined){

    deletor = new $.fn.DataTable.Editor( {
        ajax: "/po/delete",
        table: "#datable_sellersku",
        idSrc:  'id',
    } );
    editor = new $.fn.DataTable.Editor( {
        ajax: "/po/updateitem",
        table: "#datable_sellersku",
        idSrc:  'id',
        fields: [ {
                label: "Order",
                name: "order_quantity"
            }, {
                label: "COST",
                name: "cost"
            }, {
                label: "VendorSKU",
                name: "vendorsku"
            }, {
                label: "Received",
                name: "received_qty"
            }
        ]
    } );

  buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, row, column, node ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
                        data.replace( /[$,]/g, '' ) :
                        data;
                }
            }
        }
    };




  loaddatatable();


})(window, document, window.jQuery);







function loaddatatable(){


  if ( $.fn.dataTable.isDataTable( '#datable_sellersku' ) ) {
      table = $('#datable_sellersku').dataTable();
      table.fnDestroy();
  }

  table = $('#datable_sellersku').DataTable({
        paging:   true,  // Table pagination
        ordering: true,  // Column ordering
        info:     true,  // Bottom left status text
        // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
        processing: true,
        pagingType: 'input',

        serverSide: true,
        iDisplayLength : 25,
        scrollY:        'calc(100vh - 500px)',
        scrollX: true,
        scrollCollapse: true,
        deferRender: true,
        orderCellsTop: true,
        language: {
            oPaginate: {
              sNext: '<i class="fa fa-forward"></i>',
              sPrevious: '<i class="fa fa-backward"></i>',
              sFirst: '<i class="fa fa-step-backward"></i>',
              sLast: '<i class="fa fa-step-forward"></i>'
            }
        },
        sAjaxSource: '/sellerskus/load',
        aoColumns: [
          // { mData: null},
          {
                mData: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
           } ,
           { mData: 'id' },
           { mData: 'item_name', render: function (data, type, full, meta) {
                                 return '<span data-toggle="tooltip" title="' + data + '">' + data + '</span>';
                             } },
           { mData: 'sku' },
           { mData: 'asin' },
           { mData: 'price' },
           { mData: 'bbs' , orderable: false},
           { mData: 'item_quantity', 'className':'editable number_cell'},
           { mData: 'inbound_quantity','className':'editable number_cell'},
           { mData: 'reserved_quantity'},
           { mData: 'last1daysales'},
           { mData: 'last30daysales' },
           { mData: 'last60daysales'},
           { mData: 'last90daysales'},
           { mData: 'last360daysales' },
           { mData: 'days_of_sale' },
           { mData: 'sales_velocity_for' },
           { mData: 'already_ordered_amount' },
           { mData: 'reorder_quantity', render: function (data, type, full, meta) {
                return '<span title="' + data + '" class = "' + (data < 0 ? "negative" : "positive") + '">' + data + '</span>';
           } },

        ],
        rowId: 'id',
        aoColumnDefs: [

          { className:"number_cell", "targets": "_all"}
        ],
        "createdRow": function ( row, data, index ) {
            $('td', row).eq(2).addClass('abbr_cell');

        },
        dom: 'Blfrtip',
        // select: true,
        select:{
                style: 'multi'
            },
         buttons: [
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: deletor },
            {
                text: 'Refresh',
                action: function ( e, dt, node, config ) {

                    // alert( table.rows('.selected').data().length +' row(s) selected' );
                    // var sData = this.fnGetTableData(config);
                    var sData = table.rows( { selected: true } ).data();
                    if(sData.length == 0){
                      swal("Please select the Items to refresh");
                      return;
                    }
                    var sIds = [];
                    for (var i = 0; i < sData.length; i++) {
                      sIds.push(sData[i].vendorasin_id);
                    }
                    $.ajax({
                      url: '/api/refreshitems',
                      dataType: 'json',
                      data: {'ids': sIds},
                      type: 'post',
                      beforeSend: function(){
                      },
                      success: function(data){
                        if(data.result == true){
                          table.ajax.reload();
                          swal("Items are successfully refreshed");
                        }else{
                          swal("Error occured while refreshing the items");
                        }
                      }
                   });
                }

            },
            {
                text: 'Send to PO',
                action: function ( e, dt, node, config ) {

                    // alert( table.rows('.selected').data().length +' row(s) selected' );
                    // var sData = this.fnGetTableData(config);
                    var sData = table.rows( { selected: true } ).data();
                    if(sData.length == 0){
                      swal("Please select valid items to send to PO");
                      return;
                    }
                    var sIds = [];
                    for (var i = 0; i < sData.length; i++) {
                      if (sData[i].reorder_quantity < 0) {
                        swal("Cannot add negative re-order quantity amount to PO. Please refer to [ReOrder] column and de-select to continue!")
                        return
                      } else if (sData[i].already_ordered_amount > 0) {
                        swal("Some Items are already added to PO. Please refer to [Amount Already in PO] column and de-select to continue!")
                        return
                      }
                      else {
                        sIds.push([sData[i].vendorasin_id, sData[i].sku]);
                      }
                    }
                    var data = {sku_data: sIds};
                    window.location.href = "/api/send_to_po?" + $.param(data)

                }

            },

           'selectAll',
           'selectNone',
           {    extend: 'collection',
                text: 'Export Selected',
                // buttons: ['copy','csv','excel','pdf','print'],
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Excel',
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        }
                    },
                ],
           },

        ],
        order: [[10, 'desc']],
        initComplete: function () {
            this.api().columns().every( function (i) {
              if(i==6){
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo(  $("#datable_sellersku_wrapper thead tr:eq(1) th").eq(column.index()).empty() )
                    .on( 'change', function () {
                        var val = $(this).val();

                        column
                            .search( val ? val : '', true, false )
                            .draw();
                    } );


                    select.append( '<option value="AMZ">AMZ</option>' );
                    select.append( '<option value="FBA">FBA</option>' );
                    select.append( '<option value="FBM">FBM</option>' );
              }

            } );
        },
    });

  initComplete();

    $("#datable_sellersku_wrapper thead :input:not(:checkbox)").on( 'keyup change', function () {
        table
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );
}

function initComplete () {

   $('#datable_sellersku_wrapper thead tr#seller_sku_filterrow th').each( function (i) {
        var title = $(this).text();
        if(title == '' ){

        }
        else if(title == 'EST' || title == 'Profit' || title == 'Margin' || title == 'SalesRank'){
            $(this).html( '<input type="hidden" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" name="'+title+'" id="txt_'+title+'"/><a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Search '+title+'" href="javascript:showfilter(\''+title+'\')">'+'<em class="fa fa-search"></em>'+'</a>' );
        }else{
            $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );
        }

        //$(this).html( '<input type="text" onclick="stopPropagation(event);" placeholder="Search '+title+'" />' );
    } );

  // Activate an inline edit on click of a table cell

}





$(document).ready(function(){
    $('.btn-filter-set').click(function () {
        var column = $(this).data('column');
        var search_string = '';
        $(".element_"+column).each(function(){
            var id = this.id;
            var split_id = id.split("_");
            var column = split_id[1];
            var index = split_id[2];

            var operation = $("#slt_expression_"+column+"_"+index).val();
            var value = $("#txt_comparevalue_"+column+"_"+index).val();
            if(isNaN(value) === false){
                search_string = search_string + operation + "$$" + value + "##";
            }
        });
        if(search_string != ''){
            $('#txt_'+column).attr('value',search_string);
            $('#txt_'+column).trigger("change");
        }

    })

    $('.btn-filter-reset').click(function () {
        var column = $(this).data('column');
        $('#txt_'+column).attr('value', '');
        $('#txt_'+column).trigger("change");
    })

    $(".add-skill").click(function(){
        var column = $(this).data('column');

        // Finding total number of elements added

        // last <div> with element class id
        var lastid = $(".element_"+column+":last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[2]) + 1;

        // Check total number elements
        // Adding new div container after last occurance of element class
        $(".element_"+column+":last").after("<div class='element_"+column+"' id='div_"+column+"_"+ nextindex +"'></div>");

        // Adding element to <div>
        $("#div_" +column+"_"+ nextindex).append("<select name='slt_expression_"+column+"_"+nextindex+"' id='slt_expression_"+column+"_"+nextindex+"'> \
            <option value='eq'>=</option>\
            <option value='lg'>></option>\
            <option value='el'>>=</option>\
            <option value='ls'><</option>\
            <option value='es'><=</option>\
        </select>\
      <input type='number' name='txt_comparevalue_"+column+"_"+nextindex+"' id='txt_comparevalue_"+column+"_"+nextindex+"'>\
      <a id='remove_" +column+'_'+ nextindex + "' class='btn btn-primary btn-sm editable-submit remove_filter' ><i class='fa fa-fw fa-remove'></i></a>");

    });

        // Remove element
    $('.multiple-filter-dlg').on('click','.remove_filter',function(){
        var id = this.id;
        var split_id = id.split("_");
        var column = split_id[1];
        var deleteindex = split_id[2];
        // Remove <div> with id
        $("#div_"+column+"_" + deleteindex).remove();
    });

    $('#datable_sellersku').on( 'click', 'tbody td.editable', function (e) {
      editor.inline( this);
    } );
});