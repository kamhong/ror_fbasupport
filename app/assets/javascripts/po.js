var editor; // use a global for the submit and return data rendering in the examples
var deletor;
var table;
var pending_po_table;
var buttonCommon

function showfilter(column) {
    // $("#dlg_set_filter").modal();
    $('#dlg_set_filter_'+column).modal();
}
(function(window, document, $, undefined){
  
  deletor = new $.fn.DataTable.Editor( {
      ajax: "/po/delete",
      table: "#datable_po",
      idSrc:  'id',
  } );
  editor = editor = new $.fn.DataTable.Editor( {
    ajax: "/po/updateitem",
    table: "#datable_po",
    idSrc:  'id',
    fields: [ {
            label: "Order",
            name: "order_quantity"
        }, {
            label: "COST",
            name: "cost"
        }, {
            label: "VendorSKU",
            name: "vendorsku"
        }, {
            label: "Received",
            name: "received_qty"
        }
    ]
} );

  buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, row, column, node ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
                        data.replace( /[$,]/g, '' ) :
                        data;
                }
            }
        }
    };

 

 
    

})(window, document, window.jQuery);






$(document).on('change', '#versionsDiv select', function() {
    var po_id = $(this).val();
    if(po_id == ""){
        return;
    }
    loadpendingpotable(po_id);
});

function loadpendingpotable(pending_po_id){
    if ( $.fn.dataTable.isDataTable( '#datable_pending_po' ) ) {
        pending_po_table = $('#datable_pending_po').dataTable();
        pending_po_table.fnDestroy();
    }

    pending_po_table = $('#datable_pending_po').DataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
        "processing": true,
        "serverSide": true,
        "iDisplayLength" : 25,
        deferRender: true,
        orderCellsTop: true,  
        sAjaxSource: '/pending_po/load?pending_po='+pending_po_id,
        aoColumns: [
          // { mData: null},
          {
                mData: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
           } ,
          { mData: 'id' },
          { mData: 'vendortitle', render: function (data, type, full, meta) {
                return '<span data-toggle="tooltip" title="' + data + '">' + data + '</span>';
            } },
            { mData: 'sku' },
          { mData: 'asin' },
          { mData: 'vendorsku' },
          { mData: 'upc' },
          { mData: 'order_quantity', 'className':'editable number_cell'},
          { mData: 'received_quantity','className':'editable number_cell'},
          { mData: 'order_cost' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'cost' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'packcount' },
          { mData: 'packcost', render: $.fn.dataTable.render.number( ',', '.', 2, '$' ) },
          { mData: 'buyboxprice', render: $.fn.dataTable.render.number( ',', '.', 2, '$' ) },
          { mData: 'salesrank' },
          { mData: 'profit', render: $.fn.dataTable.render.number( ',', '.', 2,'$')},
          // { mData: 'profit', render: function(obj){

          // }},
          { mData: 'margin',render: $.fn.dataTable.render.number( ',', '.', 2, '','%' )},
          { mData: 'fbafee' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'commissionpct' },
          { mData: 'commissiionfee' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
          { mData: 'salespermonth' },
          { mData: 'totaloffers' },
          { mData: 'fbaoffers' },
          { mData: 'fbmoffers' },
          {mData: 'item_id'},
        ],
        rowId: 'id',
        aoColumnDefs: [ 
          {
              targets: [24],
              "visible": false,
          },
          { className:"number_cell", "targets": [1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]} 
        ],
        "createdRow": function ( row, data, index ) {
            if ( data['profit'] > 0 ) {
                $('td', row).eq(15).addClass('positive_profit');
            }else{
                $('td', row).eq(15).addClass('negative_profit');
            }
            $('td', row).eq(2).addClass('abbr_cell');
            
        },
        dom: 'Blfrtip',
        // select: true,
        select:{
                style: 'multi'
            },
         buttons: [
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: deletor },
            {
                text: 'Refresh',
                action: function ( e, dt, node, config ) {
                    
                    var sData = table.rows( { selected: true } ).data();
                    if(sData.length == 0){
                      swal("Please select the Items to refresh");
                      return;
                    }
                    var sIds = []; 
                    for (var i = 0; i < sData.length; i++) {
                      sIds.push(sData[i].vendorasin_id);
                    }
                    $.ajax({
                      url: '/api/refreshitems', 
                      dataType: 'json',  
                      data: {'ids': sIds},
                      type: 'post',
                      beforeSend: function(){
                      },
                      success: function(data){
                        if(data.result == true){
                          table.ajax.reload();
                          swal("Items are successfully refreshed");
                        }else{
                          swal("Error occured while refreshing the items");
                        }
                      }
                   });
                }
                
            },
           
           'selectAll',
           'selectNone',
           {    extend: 'collection',
                text: 'Export Selected',
                // buttons: ['copy','csv','excel','pdf','print'],
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Excel',
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        }
                    },
                ],
           },
            
           
        ],
        order: [[1, 'asc']]
    });

}

$("#datable_po thead :input:not(:checkbox)").on( 'keyup change', function () {
    table
        .column( $(this).parent().index()+':visible' )
        .search( this.value )
        .draw();
} );

function initComplete () {
 
  
   $('#datable_po thead tr#filterrow th').each( function (i) {
        var title = $(this).text();
        if(title == '' ){

        }
        else if(title == 'EST' || title == 'Profit' || title == 'Margin' || title == 'SalesRank'){
            $(this).html( '<input type="hidden" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" name="'+title+'" id="txt_'+title+'"/><a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Search '+title+'" href="javascript:showfilter(\''+title+'\')">'+'<em class="fa fa-search"></em>'+'</a>' );
        }else{
            $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );  
        }
    } );  

    $('#datable_pending_po thead tr#filterrow th').each( function (i) {
        var title = $(this).text();
        if(title == '' ){

        }
        else if(title == 'EST' || title == 'Profit' || title == 'Margin' || title == 'SalesRank'){
            $(this).html( '<input type="hidden" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" name="'+title+'" id="txt_'+title+'"/><a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Search '+title+'" href="javascript:showfilter(\''+title+'\')">'+'<em class="fa fa-search"></em>'+'</a>' );
        }else{
            $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );  
        }
    } );
}


function deletevendoritems(){
  var vendor = $("#grid_vendor_id").val();
    $.ajax({
        url: '/po/deletebyvendor', 
        dataType: 'json',  
        data: {'vendor_id': vendor},
        type: 'post',
        beforeSend: function(){
        },
        success: function(data){
            table.ajax.reload();
            swal("Items are removed");
           
        },
        error: function(jqXHR){
            swal("Error occured while remove vendor items.");
        }
    });
}

$(document).ready(function(){
    $('.btn-filter-set').click(function () {
        var column = $(this).data('column');
        var search_string = '';
        $(".element_"+column).each(function(){
            var id = this.id;
            var split_id = id.split("_");
            var column = split_id[1];
            var index = split_id[2];

            var operation = $("#slt_expression_"+column+"_"+index).val();
            var value = $("#txt_comparevalue_"+column+"_"+index).val();
            if(isNaN(value) === false){
                search_string = search_string + operation + "$$" + value + "##";
            }
        });
        if(search_string != ''){
            $('#txt_'+column).attr('value',search_string);
            $('#txt_'+column).trigger("change");
        }
        
    })

    $('.btn-filter-reset').click(function () {
        var column = $(this).data('column');
        $('#txt_'+column).attr('value', '');
        $('#txt_'+column).trigger("change");
    })

    $(".add-skill").click(function(){
        var column = $(this).data('column');
        
        // Finding total number of elements added
        
        // last <div> with element class id
        var lastid = $(".element_"+column+":last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[2]) + 1;

        // Check total number elements
        // Adding new div container after last occurance of element class
        $(".element_"+column+":last").after("<div class='element_"+column+"' id='div_"+column+"_"+ nextindex +"'></div>");
        
        // Adding element to <div>
        $("#div_" +column+"_"+ nextindex).append("<select name='slt_expression_"+column+"_"+nextindex+"' id='slt_expression_"+column+"_"+nextindex+"'> \
            <option value='eq'>=</option>\
            <option value='lg'>></option>\
            <option value='el'>>=</option>\
            <option value='ls'><</option>\
            <option value='es'><=</option>\
        </select>\
      <input type='number' name='txt_comparevalue_"+column+"_"+nextindex+"' id='txt_comparevalue_"+column+"_"+nextindex+"'>\
      <a id='remove_" +column+'_'+ nextindex + "' class='btn btn-primary btn-sm editable-submit remove_filter' ><i class='fa fa-fw fa-remove'></i></a>");
        
    });

        // Remove element
    $('.multiple-filter-dlg').on('click','.remove_filter',function(){
        var id = this.id;
        var split_id = id.split("_");
        var column = split_id[1];
        var deleteindex = split_id[2];
        // Remove <div> with id
        $("#div_"+column+"_" + deleteindex).remove();
    });

    $('#datable_po').on( 'click', 'tbody td.editable', function (e) {
      editor.inline( this);
    } );

    $("#file_field").change(function(){
        // document.getElementById("filefeed").submit();
        // return;
        var file_data = $('#file_field').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: '/po/uploadfile', // point to server-side PHP script
            dataType: 'json',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            beforeSend: function(){
               // $("#loading").dialog('open').html();
            },
            success: function(data){
              // $('#loading').dialog('close');
              console.log(data);
              if(data.result){
                swal("File is successfully uploaded.");
                swal('File is uploaded.');
                $(".csvheader option").remove();
               
                for (var i = 0; i< data.header.length ; i++) {
                    $(".csvheader ").append($('<option>', {
                        value: i-1,
                        text: data.header[i],
                    }));
                }
                $("#file_name").val(data.file_name);
                $("#file_path").val(data.temp_path);
                $("#restored_filename").val(data.restored_filename);
              }else{
                $("#file_name").val('');
                $("#file_path").val('');
                $("#restored_filename").val('');
                swal(data.msg)
              }
              
            }
         });
    });
});