/// All PO Page
var editor;
var warehouse_invoice_table;


function load_warehouse_invoice(){
    if ( $.fn.dataTable.isDataTable( '#datatable_warehouse_invoice' ) ) {
        warehouse_invoice_table = $('#datatable_warehouse_invoice').dataTable();
        warehouse_invoice_table.fnDestroy();
    }

    warehouse_invoice_table = $('#datatable_warehouse_invoice').DataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
        "processing": true,
        "serverSide": true,
        "iDisplayLength" : 25,
        deferRender: true,
        orderCellsTop: true, 
        sAjaxSource: '/invoices/list',
        sServerMethod: 'POST',
        aoColumns: [
            { mData: 'invoice_name', render: function (data, type, full, meta) {
                    return '<a class="btn btn-primary btn-sm" style="width:100%" target = "_blank" href="/invoices/'+full.id+'.pdf">' + data + '</a>';
                },
            },
            { mData: 'created_at' },
            { mData: 'pending_po_list'},
            { mData: 'ship_term'},
            { mData: 'vendor_list'},
            { mData: 'user_list'},
            { mData: 'item_count' },
            { mData: 'confirmed_qty' },
            { mData: 'confirmed_cost' },
            { mData: 'invoiced_qty' },
            { mData: 'invoiced_cost' },
            { mData: 'shipping_cost', className: "number_cell editable" },
            { mData: 'adjustment_cost', className: "number_cell editable"  },
            { mData: 'adjustment_reason', className: "number_cell editable"  },
        ],
        rowId: 'id',
        aoColumnDefs: [ 
          { className:"number_cell", "targets": "_all"} 
        ],
        select:{
            style: 'multi'
        },
        buttons: [
        ],
        dom: 'Blfrtip',
        // order: [[1, 'asc']]
        initComplete: function () {
            this.api().columns().every( function (i) {
                var column = this;
                var option_value = "";
                if (i == 5){
                    option_value = $('#user_id_option').html();
                }else if( i== 2){
                    option_value = $('#po_option').html();
                }else if( i== 1){
                    var time_div = $('<input id="time_div" class="time_div">').appendTo($("#datatable_warehouse_invoice thead tr:eq(1) th").eq(column.index()).empty());;
                    $("#time_div").datepicker(
                        { 
                            onSelect: function () { 
                                var val = $(this).val();
                                column
                                    .search( val ? val : '', true, false )
                                    .draw();
                             }, 
                            changeMonth: true, 
                            changeYear: true }
                        ).on("change", function() {
                            // console.log($(this).val());
                          });
                }else if (i == 4){
                    option_value = $('#vendor_id_option').html();
                }
                if (option_value != ""){
                    var select = $('<select><option value=""></option>'+option_value+'</select>')
                        .appendTo(  $("#datatable_warehouse_invoice thead tr:eq(1) th").eq(column.index()).empty() )
                        .on( 'change', function () {
                            // var val = $.fn.dataTable.util.escapeRegex(
                            //     $(this).val()
                            // );
                            var val = $(this).val();
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
                }
                
                
            });
        },
    });

    initComplete();

    $("#datatable_warehouse_invoice thead :input:not(:checkbox)").on( 'keyup change', function () {
        pending_po_table
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );
}

function initComplete () {
 
  
    $('#datatable_warehouse_invoice thead tr#filterrow th').each( function (i) {
         var title = $(this).text();
         if(title == '' ){
 
         }
         else if(title == 'EST' || title == 'Profit' || title == 'Margin' || title == 'SalesRank'){
             $(this).html( '<input type="hidden" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" name="'+title+'" id="txt_'+title+'"/><a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Search '+title+'" href="javascript:showfilter(\''+title+'\')">'+'<em class="fa fa-search"></em>'+'</a>' );
         }else{
             $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );  
         }
 
         //$(this).html( '<input type="text" onclick="stopPropagation(event);" placeholder="Search '+title+'" />' );
     } );
 
   // Activate an inline edit on click of a table cell
    editor = new $.fn.DataTable.Editor( {
        ajax: "/invoices/updateitem",
        table: "#datatable_warehouse_invoice",
        idSrc:  'id',
        fields: [{
                label: "Shipping",
                name: "shipping_cost"
            },
            {
                label: "Adjustment",
                name: "adjustment_cost"
            },
            {
                label: "Reason",
                name: "adjustment_reason"
            }
        ]
    } );
 }

$('#datatable_warehouse_invoice').on( 'click', 'tbody td.editable', function (e) {
    editor.inline( this);
} );


$('.time_div').change(function() {
    var val = $(this).val();
    console.log(val);
    pending_po_summary_table
        .search( val ? val : '', true, false )
        .draw();
});