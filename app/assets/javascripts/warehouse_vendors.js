var warehouse_table;
var editor;
function load_warhouse_table(){
    if ( $.fn.dataTable.isDataTable( '#dataable_warehouse_vendor' ) ) {
        warehouse_table = $('#dataable_warehouse_vendor').dataTable();
        warehouse_table.fnDestroy();
    }
  
    warehouse_table = $('#dataable_warehouse_vendor').DataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
            "processing": true,
            "serverSide": true,
            "iDisplayLength" : 25,
            scrollY:        'calc(100vh - 300px)',
            scrollX: true,
            scrollCollapse: true,
            deferRender: true,
            orderCellsTop: true,  
            sAjaxSource: '/warehouse_vendors/load_warehouse_vendor',
            sServerMethod: "POST",
            aoColumns: [
                { mData: 'name' },
                { mData: 'supplier_code' },
                { mData: 'day_of_shipping'},
                { mData: 'minimum_order' },
                { mData: 'buyer_name'},
                { mData: 'vendor_phone' },
                { mData: 'account_number'},
                { mData: 'account_holder' },
                { mData: 'contact_name'},
                { mData: 'contact_email'},
                { mData: 'contact_phone_number'},
                { mData: 'website'},
                { mData: 'user_name'},
                { mData: 'password'},
                { mData: 'payment_terms'},
                { mData: 'shipment_terms' },
                { mData: 'catalog'},
            ],
            rowId: 'id',
            aoColumnDefs: [ 
                { className:"number_cell editable", "targets": "_all"} ,
            ],
            "createdRow": function ( row, data, index ) {
            },         
            order: [[1, 'asc']],
            initComplete: function () {
                this.api().columns().every( function (i) {
                    var column = this;
                    var option_value = "";
                    if(i == 0){
                        option_value = $('#warehouse_vendor_option').html();
                    }else if (i == 4){
                        option_value = $('#user_option').html();
                    }
                    if (option_value != ""){
                        var select = $('<select><option value=""></option>'+option_value+'</select>')
                            .appendTo(  $("#dataable_warehouse_vendor_wrapper thead tr:eq(1) th").eq(column.index()).empty() )
                            .on( 'change', function () {
                                // var val = $.fn.dataTable.util.escapeRegex(
                                //     $(this).val()
                                // );
                                var val = $(this).val();
                                column
                                    .search( val ? val : '', true, false )
                                    .draw();
                            } );
                    }
                    
                    
                } );
            },
      });
  
    initComplete();
  
    $("#dataable_warehouse_vendor_wrapper thead :input:not(:checkbox)").on( 'keyup change', function () {
          table
              .column( $(this).parent().index()+':visible' )
              .search( this.value )
              .draw();
      } );
}

function initComplete(){
    $('#dataable_warehouse_vendor_wrapper thead tr#filterrow th').each( function (i) {
        var title = $(this).text();
        if(title == '' ){

        }
        else if(i == 0 || i == 4){
            $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );  
        }else{
            $(this).html('');        
        }

    } );
}

function initEditor(){
    var default_provider_options = [];
    var default_provider_payment = [];
    $.getJSON('/warehouse_vendors/list?type=3', function (data) {
        var user = data.user
        var payment = data.payment
        $.each(user, function (index) {
            default_provider_options.push({
                value: user[index].id,
                label: user[index].full_name
            });
        });
        $.each(payment, function (index) {
            default_provider_payment.push({
                value: payment[index].value,
                label: payment[index].name
            });
        });
        editor = new $.fn.DataTable.Editor( {
            ajax: "/warehouse_vendors/update_fields",
            table: "#dataable_warehouse_vendor",
            idSrc:  'id',
            fields: [ 
                {
                    label: "Name",
                    name: "name",
                },
                {
                    label: "Supplier Code",
                    name: "supplier_code",
                },
                {
                    label: "Day of Shipping",
                    name: "day_of_shipping",
                },
                {
                    label: "Minimum Order",
                    name: "minimum_order",
                },
                {
                    label: "User",
                    name: "buyer_name",
                    type: "select",
                    ipOpts: default_provider_options
                },
                {
                    label: "Phone",
                    name: "vendor_phone",
                },
                {
                    label: "Account #",
                    name: "account_number",
                },
                {
                    label: "Account Holder",
                    name: "account_holder",
                },
                {
                    label: "Contact Name",
                    name: "contact_name",
                },
                {
                    label: "Contact Email",
                    name: "contact_email",
                },
                {
                    label: "Name",
                    name: "contact_phone_number",
                },
                {
                    label: "Website",
                    name: "website",
                },
                {
                    label: "User name",
                    name: "user_name",
                },
                {
                    label: "Password",
                    name: "password",
                },
                {
                    label: "Payment Terms",
                    name: "payment_terms",
                    type: "select",
                    ipOpts: default_provider_payment,
                },
                {
                    label: "Shipment Terms",
                    name: "shipment_terms",
                },
                {
                    label: "Catalog",
                    name: "catalog",
                }
            ],
        } );
        load_warhouse_table();
    });
}

$(document).ready(function(){
    $('#dataable_warehouse_vendor').on( 'click', 'tbody td.editable', function (e) {
        editor.inline( warehouse_table.cell(this).index(), {
            // submit: 'allIfChanged',
            onBlur: 'submit'
        } );
    } );
    
});

