var vendor_list;

function load_vendor_prefixtable(){
    if ( $.fn.dataTable.isDataTable( '#datable_vendor_supplier_list' ) ) {
        vendor_list = $('#datable_vendor_supplier_list').dataTable();
        vendor_list.fnDestroy();
    }

    vendor_list = $('#datable_vendor_supplier_list').DataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
        "processing": true,
        "serverSide": true,
        "iDisplayLength" : 25,
        deferRender: true,
        orderCellsTop: true, 
        sAjaxSource: '/vendors/load_supplier_list',
        // sServerMethod: 'GET',
        aoColumns: [
            { mData: 'user.full_name' },
            { mData: 'name' },
            { mData: 'supplier_prefix','className': 'editable number_cell' },
        ],
        rowId: 'id',
        aoColumnDefs: [ 
          { className:"number_cell", "targets": "_all"} 
        ],
        initComplete: function () {
            this.api().columns().every( function (i) {
                var column = this;
                var option_value = "";
                if (i == 0){
                    option_value = $('#user_option').html();
                }
                if (option_value != ""){
                    var select = $('<select><option value=""></option>'+option_value+'</select>')
                        .appendTo(  $("#datable_vendor_supplier_list thead tr:eq(1) th").eq(column.index()).empty() )
                        .on( 'change', function () {
                            var val = $(this).val();
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
                }
                
                
            });
        },
        // dom: 'Blfrtip'
        // order: [[1, 'asc']]
    });

    initComplete();

    $("#datable_vendor_supplier_list thead :input:not(:checkbox)").on( 'keyup change', function () {
        vendor_list
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );
}

function initComplete () {
    $('#datable_vendor_supplier_list thead tr#filterrow th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );
    } );
}

$('#datable_vendor_supplier_list').on( 'click', 'tbody td.editable', function (e) {
    editor.inline( this );
} );

