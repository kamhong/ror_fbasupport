

var editor; // use a global for the submit and return data rendering in the examples
var deletor;
var creator;
var table;
var buttonCommon;

function showfilter(column) {
  $('#dlg_set_filter_'+column).modal();

}

(function(window, document, $, undefined){
  creator =  new $.fn.DataTable.Editor( {
    ajax: "/addvendoritem",
    table: "#datatable4",
    idSrc:  'id',
    fields: [
      {
        label: "ASIN",
        name: "asin"
      },
      {
        label: "Vendor SKU",
        name: "vendorsku"
      },
      {
        label: "Cost",
        name: "cost"
      }
    ]
  });

  deletor = new $.fn.DataTable.Editor({
    ajax: "/deletevendoritem",
    table: "#datatable4",
    idSrc:  'id',
  });

  editor = new $.fn.DataTable.Editor({
    ajax: "/updatevendoritem",
    table: "#datatable4",
    idSrc:  'id',
    fields: [
      {
        label: "Packcount",
        name: "packcount",
      },
      {
        label: "ASIN",
        name: "asin",
      },
      {
        label: "COST",
        name: "cost"
      },
      {
        label: "Name",
        name: "vendortitle"
      },
      {
        label: "UPC",
        name: "upc"
      },
      {
        label: "BBP",
        name: "buyboxprice"
      },
      {
        label: "Est",
        name: "salespermonth"
      },
      {
        label: "VendorSKU",
        name: "vendorsku"
      },
      {
        label: "FBAFee",
        name: "fbafee"
      },
      {
        label: "SalesRank",
        name: "salesrank"
      },
      {
        label: "BBS",
        name: "bbs"
      }
    ]
  });

  buttonCommon = {
    exportOptions: {
      format: {
        body: function ( data, row, column, node ) {
          // Strip $ from salary column to make it numeric
          return column === 5 ?
          data.replace( /[$,]/g, '' ) :
          data;
        }
      }
    }
  };



  $(function(){
    $('#datatable_skynethistory').dataTable({
      "sDom": '<"top"f<"clear">>rt<"bottom"ipl<"clear">>',
      'ordering': true,  // Column ordering
      'paging':   true,  // Table pagination
      'info':     true,  // Bottom left status text
      'responsive': true, // https://datatables.net/extensions/responsive/examples/
      "lengthMenu": [[10, 25, 50], ['10 by page', '25 by page', '50 by page']],
      "pagingType": "numbers",
      "processing": true,
      "serverSide": true,
      "searching": true,
      language: {
        "sLengthMenu": "_MENU_",
        search: "",
        searchPlaceholder: "Search"
      },
      "fnDrawCallback": function(oSettings) {
        skynethisotry_table = $('#datatable_skynethistory').DataTable();
        page_number = skynethisotry_table.page.info()
          if ($('#datatable_skynethistory tr').length < 10 && page_number["page"] == 0) {
        }
      },
      "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', aData.id);
      },
      sAjaxSource: '/api/skynethisotry',
      aoColumns: [
        { mData: 'inputfilename',
          "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a href='https://fba-skynets-csvs.s3.amazonaws.com/uploads/"+oData.inputfileurl+"' target=\"_blank\">"+oData.inputfilename+"</a>");
          }
        },
        { mData: 'name' },
        { mData: 'statusmessage' },
        { mData: 'outputfileurl' },
        { mData: 'created_at' },
        {
          mdata: null,
          className: "center",
          defaultContent: '<a href="javascript:void(0)"><i class="fa fa-download download-link" style="font-size: 18px; color: #444;"></i></a> &nbsp;&nbsp;&nbsp; <i class="fa fa-trash-o delete-skynet" style="font-size: 18px; color: #444; href="#""></i>'
        }
      ],
      "columnDefs": [
        {
          "targets": 3,//index of column starting from 0
          "data": "outputurl", //this name should exist in your JSON response
          "render": function ( data, type, full, meta ) {
            if(data != '' && data != null){
              return "<a href=\""+data+"\">Download</a>";
            }else{
              return '';
            }
          }
        },
      ],
      "order": [[ 3, "desc" ]],
    });
    $(".dataTables_length").addClass("listing-dropdown");
  });

  $.ajax({
    method: 'GET',
    datatype: "json",
    url: '/getprofitable?vendor='+$("#grid_vendor_id").val(),
    success: function(data) {

      $("#profitrows").html(data.count);
      $("#avgprofit").html(data.avg_profit);
      $("#avgmargin").html(data.avg_margin);
    },
    error: function(jqXHR) {
      //alert('Oops connection error!');
    }
  });
  loaddatatable($("#grid_vendor_id").val());
})(window, document, window.jQuery);



function outputdownload (cellvalue, options, rowObject) {
  if(cellvalue == null)
     return "In Progress";
  return "<a href='"+cellvalue+"'><em class='fa fa-download'>"+"Download"+"</em></a>";
}

$("#file_field").change(function(){
  var file_data = $('#file_field').prop('files')[0];
  var form_data = new FormData();
  form_data.append('file', file_data);
  $.ajax({
    url: '/skynets/uploadfile', // point to server-side PHP script
    dataType: 'json',  // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    beforeSend: function(){
      $("#uploading-image").show()
      $(".files_field").hide()
    },
    success: function(data){
      $("#uploading-finish").prop( "disabled", false );
      $("#uploading-image").hide()
      $("#display_successfull").show();
      $("#uploaded-file").prepend(data.file_name)
      console.log(data);
      if(data.result){
        $(".csvheader option")[1].remove();
        $(".csvheader option")[2].remove();
        $(".csvheader option")[3].remove();
        for (var i = 0; i< data.header.length ; i++) {
          $(".csvheader ").append($('<option>', {
            value: data.header[i],
            text: data.header[i],
          }));
        }
        $("#file_name").val(data.file_name);
        $("#file_path").val(data.restored_filename);
      }else{
        $("#file_name").val('');
        $("#file_path").val('');
        $("#uploading-finish").attr("disabled", true);
        $("#display_successfull").hide();
        $("#uploading-image").hide()
        $("#display_warning").show()
      }
    }
  });
});

$("#grid_vendor_id").change(function(){
  $.ajax({
    method: 'GET',
    datatype: "json",
    url: '/getprofitable?vendor='+$(this).val(),
    success: function(data) {
      $("#profitrows").html(data.count);
      $("#avgprofit").html(data.avg_profit);
      $("#avgmargin").html(data.avg_margin);
    },
    error: function(jqXHR) {
    }
  });

  loaddatatable($(this).val());
});

$all_vendors = $("#all_vendors")
function loaddatatable(vendor_id){
  if ( $.fn.dataTable.isDataTable( '#datatable4' ) ) {
    table = $('#datatable4').dataTable();
    table.fnDestroy();
  }

  table = $('#datatable4').DataTable({
    'paging':   true,  // Table pagination
    'ordering': true,  // Column ordering
    'info':     true,  // Bottom left status text
    // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
    "processing": true,
    "pagingType": "input",
    "serverSide": true,
    stateSave: true,
    scrollY:        'calc(100vh - 250px)',
    scrollX: true,
    scrollCollapse: true,
    "iDisplayLength" : 25,
    "lengthMenu": [[10, 25, 50], ['10 by page', '25 by page', '50 by page']],
    language: {
      "sLengthMenu": "_MENU_",
      search: "",
      searchPlaceholder: "Search",
      oPaginate: {
        sNext: '<i class="fa fa-forward"></i>',
        sPrevious: '<i class="fa fa-backward"></i>',
        sFirst: '<i class="fa fa-step-backward"></i>',
        sLast: '<i class="fa fa-step-forward"></i>'
     }
    },
    "sDom": '<"top"fBr<"clear">>rt<"bottom"ipl<"clear">>',
    deferRender: true,
    orderCellsTop: true,
    "fnDrawCallback": function(oSettings) {

        page_number = table.page.info()
      if ($('#datatable4 tr').length < 25 && page_number["page"] == 0) {
        $('#datatable4_paginate').hide();
      }
    },
    sAjaxSource: '/api/getitemlist?vendor='+vendor_id,
    "fnInitComplete": function(oSettings, json) {
      $listing_text = $("<span id=listing-id style=font-size:22px;><span id=new-listing-id>Listings</span></span>");
      datatable4_top = $(".top")[1]
      $(datatable4_top).prepend($listing_text);
      vendore = window.$all_vendors[0]
      $(".btn-group").append(vendore);
      $margintext = $("<span class=profitable-rows><span>Profitable rows:</span> <span class=popspan id=profitrows></span>&nbsp;&nbsp;&nbsp;&nbsp;Average Profit: <span id=avgprofit class=popspan></span>&nbsp;&nbsp;&nbsp;&nbsp;Average Margin: <span id=avgmargin class=popspan></span></span>")
      margin = (window.$listing_text[0].id)
      $("#"+margin).append($margintext);
      refresh_button = $('.refresh-class')[0]
      $store_link = $('.refresh-class')[0].parentElement.parentElement
      remove_button = refresh_button.parentElement.parentElement.removeAttribute('class')
      get_button_group = refresh_button.parentElement.parentElement.parentElement.children[0].remove()
      $("#new-listing-id").append($store_link);
      $("#datatable4-paginations").append($("#datatable4_length"));
      $("#datatable4_length").append($("#datatable4_paginate"))
      $("#skynet-paginations").append($("#datatable_skynethistory_length"));
      hide_listing = $(".paging_numbers")[2]
      $(hide_listing).hide()
      hide_pagination = $(".dataTables_length")[2]
      $(hide_pagination).hide()
      $(".buttons-select-all").click(function(){
      $(".buttons-select-none").show();
      $(".profitable-rows").hide();
      })
      $(vendore).change(function(){
        loaddatatable($("#grid_vendor_id").val());
        $("#datatable4_length").hide()
        $("#datatable4_paginate").hide()
      });
      $.ajax({
        method: 'GET',
        datatype: "json",
        url: '/getprofitable?vendor='+$("#grid_vendor_id").val(),
        success: function(data) {
          $("#profitrows").html(data.count);
          $("#avgprofit").html(data.avg_profit);
          $("#avgmargin").html(data.avg_margin);
        },
        error: function(jqXHR) {
        }
      });
    },
    aoColumns: [
      {
        mData: null,
        defaultContent: '',
        className: 'select-checkbox',
        orderable: false
      },
      { mData: 'id' },
      { mData: 'vendortitle', className: 'abbr_cell', render: function (data, type, full, meta) {
          return '<span data-toggle="tooltip" title="Edit">' + data + '</span>';
        }, className: 'editable'
      },
      { mData: 'sku', className: 'editable number_cell' },
      { mData: 'asin', className: 'number_cell' , render: function (data, type, full, meta) {
        return asin_link(data);
      } },
      { mData: 'vendorsku', className: 'editable number_cell' },
      { mData: 'upc', className: 'editable number_cell' },
      { mData: 'cost', className: 'editable number_cell' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' ), 'className':'editable number_cell'},
      { mData: 'packcount' , 'className':'editable number_cell'},
      { mData: 'packcost', className: 'editable number_cell', render: $.fn.dataTable.render.number( ',', '.', 2, '$' ) },
      { mData: 'buyboxprice', render: $.fn.dataTable.render.number( ',', '.', 2, '$' ), 'className':'editable number_cell' },
      { mData: 'salesrank', 'className':'editable number_cell number_cell' },
      { mData: 'bbs', className: 'editable number_cell', orderable: false},
      { mData: 'profit', render: $.fn.dataTable.render.number( ',', '.', 2,'$')},
      { mData: 'margin',render: $.fn.dataTable.render.number( ',', '.', 2, '','%' )},
      { mData: 'fbafee' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' ), 'className':'editable number_cell'},
      { mData: 'commissionpct' },
      { mData: 'commissiionfee' ,render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
      { mData: 'salespermonth', 'className':'editable number_cell'},
      { mData: 'review' },
    ],
    rowId: 'id',
    aoColumnDefs: [
      { "searchable": false, "targets": 0 },
      { className:"number_cell", "targets": [1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]} ,
    ],
    "createdRow": function ( row, data, index ) {
      if ( data['profit'] > 0 ) {
        $('td', row).eq(13).addClass('positive_profit');
      }else{
        $('td', row).eq(13).addClass('negative_profit');
      }
      $('td', row).eq(2).addClass('abbr_cell');
      $(row).addClass("created-row");
    },
    dom: 'Blfrtip',
    select:{
      style: 'multi'
    },
    buttons: [
      {
        text: '<i class="fa fa-refresh refresh-class" style= "margin-left: 12px;color:black;"></i>',
        className: 'refresh',
        action: function ( e, dt, node, config ) {
          var sData = table.rows( { selected: true } ).data();
          if(sData.length == 0){
            swal("Please select the Items to refresh");
            return;
          }
          var sIds = [];
          for (var i = 0; i < sData.length; i++) {
            sIds.push(sData[i].id);
          }
          $.ajax({
            url: '/api/refreshitems',
            dataType: 'json',
            data: {'ids': sIds},
            type: 'post',
            beforeSend: function(){
            },
            success: function(data){
              console.log(data);
              if(data.result == true){
                table.ajax.reload();
                swal("Items are successfully refreshed");
              }else{
                  console.log(data.msg);
                swal("Error occured while refreshing the items");
              }
            }
         });
        }
      },
      { extend: "create", editor: creator  },
      // { extend: "edit",   editor: editor, className: 'hidden-button' },
      { extend: "remove", editor: deletor, className: 'hidden-button' },
      'selectAll',
      'selectNone',
      {
        extend: 'collection',
        text: 'Export Selected',
        className: 'hidden-button',
        // buttons: ['copy','csv','excel','pdf','print'],
        buttons: [
          {
            extend: 'excel',
            text: 'Excel',
            exportOptions: {
              modifier: {
                selected: true
              }
            }
          },
        ],
      },
      {
        text: 'Send to PO',
        className: 'hidden-button',
        action: function ( e, dt, node, config ) {
          var sData = table.rows( { selected: true } ).data();
          if(sData.length == 0){
            swal("Please select the Items to add to warehouse");
            return;
          }
          var sIds = [];
          for (var i = 0; i < sData.length; i++) {
            sIds.push(sData[i].id);
          }
          $.ajax({
            url: '/po/send',
            dataType: 'json',
            data: {'ids': sIds},
            type: 'post',
            beforeSend: function(){
              $("body").addClass("loading");
              $("#loading").dialog('open').html();
            },
            success: function(data){
              $('#loading').dialog('close');
              $("body").removeClass("loading");
              if(data.result == true){
                swal(data.msg);
                table.ajax.reload();
              }else{
                  alert(data.msg);
              }
            }
         });
        }
      }
    ],
  order: [[1, 'asc']],
    initComplete: function () {

    },
  });
  $(".buttons-select-none").addClass("hidden-button");
  initComplete();

  $("#datatable4_wrapper thead :input:not(:checkbox)").on( 'keyup change', function () {
    table.column( $(this).parent().index()+':visible' ).search( this.value ).draw();
  });
}

function initComplete () {
 $('#datatable4_wrapper thead tr#filterrow th').each( function (i) {
    var title = $(this).text();
    if(title == '' ){
    }
    else if(title == 'EST' || title == 'Profit' || title == 'Margin' || title == 'SalesRank'){
      $(this).html( '<input type="hidden" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" name="'+title+'" id="txt_'+title+'"/><a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Search '+title+'" href="javascript:showfilter(\''+title+'\')">'+'<em class="fa fa-search"></em>'+'</a>' );
    }else{
      $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );
    }
  });
}



function deletevendoritems(){
  var vendor = $("#grid_vendor_id").val();
  var href =  '/skynets/new?delete_vendor='+vendor;
  window.location = href;
}

$(document).ready(function(){
  $("#datatable_skynethistory_length").append($("#datatable_skynethistory_paginate"))

  // Upload text in skynet datatable
  $upload_text = '<h3 style="color: #3a3b3c;float: left;margin-left: -946px;">Upload</h3>'
  $("#datatable_skynethistory_filter").prepend($upload_text);

  // download-link
  $('#datatable_skynethistory').on('click', '.download-link', function (e) {
  e.preventDefault();
  link = this.parentElement.parentElement.parentElement.cells[0].childNodes[0].href
  this.parentElement.href = link
  this.parentElement.click()
  } );

  // Delete row in Uploading section
  $('#datatable_skynethistory').on('click', '.delete-skynet', function (e) {
  e.preventDefault();
  id = this.parentElement.parentElement.id
    $.ajax({
      url: '/api/deleteskynet',
      method: 'GET',
      data: { id: id  },
      success: function( data, status, xhr ) {
        $('#datatable_skynethistory').DataTable().row(id).remove().draw();
      }
    });
  } );

  // Drag and drop file
  var holder = document.getElementById('holder');
  holder.ondragover = function () {
    return false; };
  holder.ondragend = function () { return false; };
  holder.ondrop = function (e) {
    e.preventDefault();
    var file_data = e.dataTransfer.files[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
      $.ajax({
        url: '/skynets/uploadfile', // point to server-side PHP script
        dataType: 'json',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        beforeSend: function(){
           $("#uploading-image").show()
           $(".files_field").hide()
        },
        success: function(data){
          $("#uploading-finish").prop( "disabled", false );
          $("#uploading-image").hide()
          $("#display_successfull").show();
          $("#uploaded-file").prepend(data.file_name)
          console.log(data);
          if(data.result){
            $(".csvheader option")[1].remove();
            $(".csvheader option")[2].remove();
            $(".csvheader option")[3].remove();

            for (var i = 0; i< data.header.length ; i++) {
              $(".csvheader ").append($('<option>', {
                value: data.header[i],
                text: data.header[i],
              }));
            }

            $("#file_name").val(data.file_name);
            $("#file_path").val(data.restored_filename);
          }else{
            $("#file_name").val('');
            $("#file_path").val('');
            // swal(data.msg)
            $("#uploading-finish").attr("disabled", true);
            $("#display_successfull").hide();
            $("#uploading-image").hide()
            $("#display_warning").show()
          }
        }
      })
    }
    $("#datatable4").hover(function() {
      $(this).css('cursor','pointer').attr('title', 'Edit');
    }, function() {
      $(this).css('cursor','auto');
    });

    $('#datatable4').on( 'click', 'tbody td.editable', function (e) {
      editor.inline( this );
    } );

    // $(".buttons-select-all").click(function(){
    //   $(".buttons-select-none").show();
    //   $(".profitable-rows").hide();
    // })

    $('#datatable4 tbody').on('click', 'tr', function () {
      if ($(this).hasClass('selected'))
      {
        $(this).removeClass('selected')
        $(".hidden-button").hide()
        $(".profitable-rows").show()
      }
      else {
        $(this).addClass('selected')
        $(".hidden-button").show()
        $(".profitable-rows").hide()
      }
    });

    $("#uploaded-another").click(function(){
        $("#file_field").click()
    });

    $("#cancle-button").click(function(){
     $("#myModal").modal('show');
    })

    $("#no-modal-button").click(function(){
      $("#myModal").modal('hide')
    })

    $("#yes-modal-button").click(function(){
      $("#myModal").modal('hide')
      $(".files_field").show()
      $("#display_warning").hide()
    })

    $("#retry-button").click(function(){
      $("#file_field").click()
      $("#display_warning").hide()
    })

    $(".skyent-id-types .tab1").addClass('active');
    $(".skyent-id-types .tabs").click(function(e) {
      var tabs = $(".skyent-id-types .tabs");
      for (i = 0; i < tabs.length; i++) {
        tabs[i].className = tabs[i].className.replace(" active", "");
      }
      e.currentTarget.className += " active";
      alert(e.currentTarget.innerText+" is selected");
    });

    $('#index_header').on('change', function (e) {
      var index_header_val = $("#index_header option:selected").val();
      if(index_header_val.toLowerCase().match(/asin/i) == 'asin'){
        $("#skynet_id_type_id").val('0');
      }else if(index_header_val.toLowerCase().match(/upc/i) == 'upc'){
        $("#skynet_id_type_id").val('1');
      }else if(index_header_val.toLowerCase().match(/isbn/i) == 'isbn'){
        $("#skynet_id_type_id").val('2');
      }else if(index_header_val.toLowerCase().match(/ean/i) == 'ean'){
        $("#skynet_id_type_id").val('3');
      }
    });

    $('.btn-filter-set').click(function () {
      var column = $(this).data('column');
      var search_string = '';
      $(".element_"+column).each(function(){
        var id = this.id;
        var split_id = id.split("_");
        var column = split_id[1];
        var index = split_id[2];
        var operation = $("#slt_expression_"+column+"_"+index).val();
        var value = $("#txt_comparevalue_"+column+"_"+index).val();
        if(isNaN(value) === false){
          search_string = search_string + operation + "$$" + value + "##";
        }
      });
      if(search_string != ''){
        $('#txt_'+column).attr('value',search_string);
        $('#txt_'+column).trigger("change");
      }
    })

    $('.btn-filter-reset').click(function () {
      var column = $(this).data('column');
      $('#txt_'+column).attr('value', '');
      $('#txt_'+column).trigger("change");
    })

    $('#datatable4').on( 'click', 'tbody td.editable', function (e) {
      editor.inline( this);
    } );

    $(".add-skill").click(function(){
      var column = $(this).data('column');
      var lastid = $(".element_"+column+":last").attr("id");
      var split_id = lastid.split("_");
      var nextindex = Number(split_id[2]) + 1;
      // Check total number elements
      // Adding new div container after last occurance of element class
      $(".element_"+column+":last").after("<div class='element_"+column+"' id='div_"+column+"_"+ nextindex +"'></div>");
      // Adding element to <div>
      $("#div_" +column+"_"+ nextindex).append("<select name='slt_expression_"+column+"_"+nextindex+"' id='slt_expression_"+column+"_"+nextindex+"'> \
         <option value='eq'>=</option>\
        <option value='lg'>></option>\
        <option value='el'>>=</option>\
        <option value='ls'><</option>\
        <option value='es'><=</option>\
      </select>\
      <input type='number' name='txt_comparevalue_"+column+"_"+nextindex+"' id='txt_comparevalue_"+column+"_"+nextindex+"'>\
      <a id='remove_" +column+'_'+ nextindex + "' class='btn btn-primary btn-sm editable-submit remove_filter' ><i class='fa fa-fw fa-remove'></i></a>");
    });

    // Remove element
    $('.multiple-filter-dlg').on('click','.remove_filter',function(){
      var id = this.id;
      var split_id = id.split("_");
      var column = split_id[1];
      var deleteindex = split_id[2];
      // Remove <div> with id
      $("#div_"+column+"_" + deleteindex).remove();
    });

    $( window ).on( "load", function(){
      $("#datatable_skynethistory_filter :input").addClass('empty');
      $("#datatable4_length select").addClass('listing-select');
      $("#datatable_skynethistory_length select").addClass('listing-select');
    } );
});