/// All PO Page
var editor;
var selected_pending_po_item_id;
var invoice_name;
var prep_array = ['Labeling','BubbleWrapping', 'Polybagging', 'Bundle','Box'];

(function(window, document, $, undefined){
    
})(window, document, window.jQuery);

function load_all_pendingpotable(){
    if ( $.fn.dataTable.isDataTable( '#datable_pending_all_po_list' ) ) {
        pending_po_table = $('#datable_pending_all_po_list').dataTable();
        pending_po_table.fnDestroy();
    }

    pending_po_table = $('#datable_pending_all_po_list').DataTable({
        paging:   true,  // Table pagination
        ordering: true,  // Column ordering
        info:     true,  // Bottom left status text
        // 'responsive': true, // https://datatables.net/extensions/responsive/examples/
        processing: true,
        serverSide: true,
        iDisplayLength : 25,
        deferRender: true,
        orderCellsTop: true,
        scrollY:        'calc(100vh - 300px)',
        scrollX: true,
        scrollCollapse: true, 
        'sAjaxSource': '/pending_pos/list_all_po',
        // 'sServerMethod': 'POST',
        'fnServerParams': function ( aoData ) {
            aoData.push( { "name": "invoice","value": invoice});
            aoData.push( { "name": "ware","value": warehouse_vendor});
            // aoData.push( { "name": "authenticity_token", "value": window._token} );
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        aoColumns: [
            {
                mData: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
           } ,
            { mData: 'vendoritem.seller_sku.user.full_name' },
            { mData: 'pending_po_index.po_name' },
            { mData: 'created_at' },
            { mData: 'vendoritem.vendor.name' },
            { mData: 'vendoritem.vendorasin.name', className: 'abbr_cell', render: function (data, type, full, meta) {
                if (full.vendoritem.vendortitle == null){
                    return '<span data-toggle="tooltip" title="' + data + '">' + data + '</span>';
                }else{
                    return '<span data-toggle="tooltip" title="' + full.vendoritem.vendortitle + '">' + full.vendoritem.vendortitle + '</span>';
                }
                } },
            { mData: 'cost','className': 'editable number_cell'},
            { mData: 'order_cost' },
            { mData: 'vendoritem.packcount', 'className': 'editable number_cell' },
            { mData: 'confirmed_qty', 'className': 'editable number_cell'},
            { mData: 'confiremd_pack_qty'},
            { mData: 'status','className': 'editable number_cell'},
            { mData: 'status_date_string', 'className': 'number_cell'},
            { mData: 'order_quantity' },
            { mData: 'received_quantity'},
            { mData: 'paid_qty'},
            { mData: 'shipped_packs'},
            { mData: null},
            { mData: null},
            { mData: 'vendoritem.seller_sku.sku' },
            { mData: 'vendoritem.asin'},
            { mData: 'vendoritem.vendorsku'},
            { mData: 'labeling_fee'},
            { mData: 'bubble_fee'},
            { mData: 'polybag_fee'},
            { mData: 'bundle_fee'},
            { mData: 'box_fee'},
            { mData: 'customer_invoice.invoice_name', render: function (data, type, full, meta) {
                    if(data == null){
                        return '';
                    }else{
                        var invoice_id = full.customer_invoice.id;
                        return '<a class="btn btn-primary btn-sm" href="/customer_invoices/'+invoice_id+'.pdf">' + data + '</a>';
                    }
                } 
            },
            { mData: 'customer_invoice.invoice_name', render: function (data, type, full, meta) {
                    if(data == null){
                        return '';
                    }else{
                        var invoice_id = full.customer_invoice.id;
                        return '<a class="btn btn-danger btn-sm" data-confirm="Are you sure?" data-method="delete"  href="javascript:reset_customer_invoice('+invoice_id+');">' + 'RESET INVOICE  <i class="fa fa-trash"></i>' + '</a>';
                    }
                } 
            },
            { mData: 'invoice_names', render: function (data, type, full, meta) {
                if(data == ""){
                    return '<button class="btn btn-info btn-sm set_invoice" >' + 'Input Invoice' + '</button>';
                }else{
                    return '<button class="btn btn-primary btn-sm set_invoice" >' + data + '</button>';
                }
                
            } }, //Invoice
            
            { mData: null}, //Balance Due
            { mData: null}, //Over Size
            { mData: 'invoice_date'}, //Date Invoiced
            { mData: null}, //Item Label 
            { mData: 'warehouse_paid'}, //Paid 
            { mData: null}, //Date
            { mData: 'remained'},
            { mData: null}, //RMA
            { mData: null}, //Shpped
        ],
        "createdRow": function ( row, data, index ) {
            if ( data['status'] == "Backorder" ||  data['status'] == "Confirmed" ) {
                $('td', row).eq(28).addClass('editable');
            }
            
        },
        rowId: 'id',
        aoColumnDefs: [ 
          { className:"number_cell", "targets": "_all"} 
        ],
        select:{
            style: 'multi'
        },
        buttons: [
            {
                text: 'Generate Invoice',
                action: function ( e, dt, node, config ) {
                    var sData = pending_po_table.rows( { selected: true } ).data();
                    if(sData.length == 0){
                    swal("Please select the Items to refresh");
                    return;
                    }
                    var sIds = []; 
                    for (var i = 0; i < sData.length; i++) {
                        sIds.push(sData[i].id);
                    }
                    $.ajax({
                        url: '/customer_invoices/generate_invoice', 
                        dataType: 'json',  
                        data: {'ids': sIds},
                        type: 'post',
                        beforeSend: function(){
                            $("body").addClass("loading");
                            $("#loading").dialog('open').html();
                        },
                        success: function(data){
                            $('#loading').dialog('close');
                            $("body").removeClass("loading");
                            if(data.status){
                                pending_po_table.ajax.reload();
                                window.open('/customer_invoices/'+data.id+'.pdf',    '_blank');
                            }else{
                                alert(data.error_message);
                            }
                        },
                        error: function(jqXHR){
                            $('#loading').dialog('close');
                            $("body").removeClass("loading");
                        }
                    });
                }
            },
            {
                extend: 'excel',
                text: 'Export All',
                exportOptions: {
                    columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
                }
            },
            {
                extend: 'excel',
                text: 'Export Selected',
                exportOptions: {
                    modifier: {
                        selected: true
                    },
                    columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
                }
            },
            {
                text: 'Export Invoice',
                action: function ( e, dt, node, config ) {
                    var sData = pending_po_table.rows( { selected: true } ).data();
                    if(sData.length == 0){
                    swal("Please select the Items to refresh");
                    return;
                    }
                    var sIds = []; 
                    for (var i = 0; i < sData.length; i++) {
                        sIds.push(sData[i].id);
                    }
                    $.ajax({
                        url: '/customer_invoices/export', 
                        dataType: 'json',  
                        data: {'ids': sIds},
                        type: 'post',
                        beforeSend: function(){
                            $("body").addClass("loading");
                            $("#loading").dialog('open').html();
                        },
                        success: function(data){
                            $('#loading').dialog('close');
                            $("body").removeClass("loading");
                        },
                        error: function(jqXHR){
                            $('#loading').dialog('close');
                            $("body").removeClass("loading");
                        }
                    });
                }
            },
        ],
        dom: 'Blfrtip',
        // order: [[1, 'asc']]
        initComplete: function () {
            this.api().columns().every( function (i) {
                var column = this;
                var option_value = "";
                if (i == 1){
                    option_value = $('#user_option').html();
                }else if( i== 2){
                    option_value = $('#po_option').html();
                }else if( i== 3){
                    var time_div = $('<input id="time_div" class="time_div">').appendTo($("#datable_pending_all_po_list_wrapper thead tr:eq(1) th").eq(column.index()).empty());;
                    $("#time_div").datepicker(
                        { 
                            onSelect: function () { 
                                var val = $(this).val();
                                column
                                    .search( val ? val : '', true, false )
                                    .draw();
                             }, 
                            changeMonth: true, 
                            changeYear: true }
                        ).on("change", function() {
                          });
                }else if (i == 4){
                    option_value = $('#vendor_option').html();
                }else if(i == 11){
                    // option_value = $('#sel_pending_po_status').html();
                }else if (i == 27 || i == 29){
                    option_value = '<option value="Yes">Yes</option><option value="No">No</option>';
                }else if (i > 21 && i < 27){
                    option_value =  '<option value="'+prep_array[i-22]+'">Yes</option>';
                }
                if (option_value != ""){
                    var select = $('<select><option value=""></option>'+option_value+'</select>')
                        .appendTo(  $("#datable_pending_all_po_list_wrapper thead tr:eq(1) th").eq(column.index()).empty() )
                        .on( 'change', function () {
                            // var val = $.fn.dataTable.util.escapeRegex(
                            //     $(this).val()
                            // );
                            var val = $(this).val();
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
                }
                
                
            });
        },
    });

    initComplete();

    // $("#datable_pending_all_po_list_wrapper thead :input:not(:checkbox)").on( 'keyup change', function () {
    //     pending_po_table
    //         .column( $(this).parent().index()+':visible' )
    //         .search( this.value )
    //         .draw();
    // } );
    var datable_pending_all_po_list_search = "";
    $("#datable_pending_all_po_list_wrapper thead :input:not(:checkbox)").bind('keydown blur change', function(e) {
        var _this = $(this);
        clearTimeout(input_timer);
        input_timer = setTimeout(function() {
            if (datable_pending_all_po_list_search == _this.val())
                return;
            
            datable_pending_all_po_list_search = _this.val();
            pending_po_table
                .column( _this.parent().index()+':visible' )
                .search( _this.val() )
                .draw();
        }, input_delay );
    });
}

function initComplete () {
 
  
    $('#datable_pending_all_po_list_wrapper thead tr#filterrow th').each( function (i) {
        var title = $(this).text();
        if(i!=0 && (i < 18 || i > 21) && i != 5){
        $(this).html('' );    
        }else{
            $(this).html( '<input type="text" onclick="stopPropagation(event);" data-toggle="tooltip" data-placement="top" title="Search '+title+'" data-index="'+i+'" />' );  
        }
     } );
 
    
 }

 $('#datable_pending_all_po_list').on( 'click', 'tbody td.editable', function (e) {
    editor.inline( pending_po_table.cell(this).index(), {
        onBlur: 'submit'
    });
  } );
  $('#datable_pending_all_po_list tbody').on( 'click', '.set_invoice', function (e) {
    var data = pending_po_table.row( $(this).parents('tr') ).data();
    
    selected_pending_po_item_id = data['id'];
    if (false){
        editor.inline( this);
    }else{
        if (data['invoice'] != null){
            $("#txt_invoice_name").val(data['invoice']['invoice_name']);
        }else{
            $("#txt_invoice_name").val("");
        }
        
        if (data['invoice_item'] != null){
            $("#txt_invoice_qty").val(data['invoice_item']['quantity']);
            $("#txt_invoice_cost").val(data['invoice_item']['cost']);
        }else{
            $("#txt_invoice_qty").val(data['received_quantity']);
            $("#txt_invoice_cost").val(data['cost']);
        }
        $("#dlg_edit_invoice").modal();
    }
    
} );

$("#btn_edit_invoice").click(function(){
    invoice_name = $("#txt_invoice_name").val();
    invoice_qty = $("#txt_invoice_qty").val();
    invoice_cost = $("#txt_invoice_cost").val();
    $.ajax({
        method: 'POST',
        url: '/invoices/set_invoice',
        data: {
            po_id: selected_pending_po_item_id,
            invoice_name: invoice_name,
            invoice_cost: invoice_cost,
            invoice_qty: invoice_qty
        },
        datatype: "json",
        beforeSend: function(){
          $("body").addClass("loading");
          $("#loading").dialog('open').html();
        },
    
        success: function(data) {
          $('#loading').dialog('close');
          $("body").removeClass("loading");
          if(data.status == true){
              pending_po_table.ajax.reload();
          }else{
              alert(data.error_message);
          }
        },
        error: function(jqXHR) {
          //alert('Oops connection error!');
          $('#loading').dialog('close');
          $("body").removeClass("loading");
        }
    });
});

$(".clickable-row").click(function() {
    window.location = $(this).data("href");
});

$('.time_div').change(function() {
    var val = $(this).val();
    pending_po_summary_table
        .search( val ? val : '', true, false )
        .draw();
});

function reset_customer_invoice(customer_invoice_id){
    $.ajax({
        url: '/customer_invoices/'+customer_invoice_id,
        dataType: 'json',  
        type: 'post',
        data: {"_method":"delete"},
        beforeSend: function(){
            $("body").addClass("loading");
            $("#loading").dialog('open').html();
        },
        success: function(data){
            $('#loading').dialog('close');
            $("body").removeClass("loading");
            pending_po_table.ajax.reload();
        },
        error: function(jqXHR){
            alert("Failed to reset the Customer Invoice.");
            $('#loading').dialog('close');
            $("body").removeClass("loading");
        }
    });    
}
function initEditor(){
    var default_provider_status_list = [];
    $.getJSON('/pending_pos/list_options', function (data) {
        var status_list = data.status_list
        $.each(status_list, function (index) {
            default_provider_status_list.push({
                value: status_list[index].value,
                label: status_list[index].name
            });
        });
        editor = new $.fn.DataTable.Editor( {
            ajax: "/pending_pos/updateitem",
            table: "#datable_pending_all_po_list",
            idSrc:  'id',
            fields: [{
                    label: "Confirmed",
                    name: "confirmed_qty"
                },
                {
                    label: "cost",
                    name: "cost"
                },
                {
                    label: "Status",
                    name: "status",
                    type: "select",
                    ipOpts: default_provider_status_list
                },
                {
                    label: "PACK",
                    name: "vendoritem.packcount",
                },
                {
                    label: "Date",
                    name: "status_date",
                    type:   'datetime',
                    def:    function () { return new Date(); },
                    format: 'YYYY/MM/DD',
                    // fieldInfo: 'US style m/d/y format'
                }
            ]
        } );
        load_all_pendingpotable();
    });
}

$('.filter-status').click(function() {
    filter_status = $(this).find('input').val();
    
    pending_po_table.ajax.url('/pending_pos/list_all_po?filter_status=' + filter_status).load();
})