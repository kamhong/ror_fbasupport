//= require jquery.inputmask/dist/jquery.inputmask.bundle



var asin = "";
var selected_current_box_qty = 0;
var selected_stage_id_add = 0;
var selected_bagger_id = 0;
$('#datatable_outbound_start_table tbody').on( 'click', '.add_shipment', function () {
    var data = po_list.row( $(this).parents('tr') ).data();
    var item_type = data['datatype'];
    if (item_type != "stage_item"){
        alert("Action is not allowed.");
        return;
    }
    var id = data['id'];
    var box_qty = data['box_qty'];

    selected_stage_id_add = id;
      if ( $.fn.dataTable.isDataTable( '#table_updatable_shipment' ) ) {
          table_updatable_shipment = $('#table_updatable_shipment').dataTable();
          table_updatable_shipment.fnDestroy();
      }
      table_updatable_shipment = $('#table_updatable_shipment').DataTable( {
            "searching":  true,
            "processing": true,
            "serverSide": true,
            // "scrollY":        '50vh',
            // "scrollCollapse": true,
            // "paging":         true,
            sAjaxSource: '/inbound_shipments/getshipmentofstage?stage_id='+id,
            aoColumns: [
              { mData: 'user_name'},
              { mData: 'shipment_id' },
              { mData: 'destination_fulfillment_center_id' },
              { mData: 'sku_count' },
              { mData: null,
                "bSortable": false,
                "mRender": function(data, type, full) {
                  var shipment_id = data['shipment_id'];
                  if(data['operation'] == 'add'){
                    return '<a class="btn btn-info btn-sm" href="javascript:additemtoShipment(\''+shipment_id+ '\')">' + 'Add' + '</a>';
                  }
                  if(data['operation'] == 'remove'){
                    return '<a class="btn btn-info btn-sm" href="javascript:removeitemtoShipment(\''+shipment_id+ '\')">' + 'Remove' + '</a>';
                  }
                  return '';

                }
              }
            ],
            select: true,
            buttons: [
              {text: "New Shipment",
                action: function ( e, dt, node, config ) {

                  updateShipment(id, box_qty);
                }
              },
            ],

            dom: "Bfrt",
        } );
      $("#dlg_list_updatable_shipment").modal();
} );

function additemtoShipment(shipment_id){

  $.ajax({
    method: 'GET',
    url: '/inbound_shipments/additemfromstage?stage_id='+selected_stage_id_add+'&shipment_id='+shipment_id,
    datatype: "json",
    beforeSend: function(){
      $("body").addClass("loading");
      $("#loading").dialog('open').html();
    },
    success: function(data) {
      $('#loading').dialog('close');
      $("body").removeClass("loading");
      if(data.status){
        alert("Operation was successfully done.");
        table_updatable_shipment.ajax.reload();
        po_list.ajax.reload();
      }else{
        alert(data.msg);
      }
    },
    error: function(jqXHR){
      $('#loading').dialog('close');
      $("body").removeClass("loading");
      alert("Operation is failed.");
    }
  });
}

function removeitemtoShipment(shipment_id){
  $.ajax({
    method: 'GET',
    url: '/inbound_shipments/removeitem?stage_id='+selected_stage_id_add+'&shipment_id='+shipment_id,
    datatype: "json",
    beforeSend: function(){
      $("body").addClass("loading");
      $("#loading").dialog('open').html();
    },
    success: function(data) {
      $('#loading').dialog('close');
      $("body").removeClass("loading");
      if(data.status){
        alert("Operation was successfully done");
        table_updatable_shipment.ajax.reload();
        po_list.ajax.reload();
      }else{
        alert("Operation is failed.");
      }
    },
    error: function(jqXHR){
      $('#loading').dialog('close');
      $("body").removeClass("loading");
      alert("Operation is failed.");
    }
  });
}

function updateShipment(id,current_box_qty,tryable = true){
  selected_current_box_qty = current_box_qty;
  selected_stage_id = id;
  selected_split_id = -1;
  $("#txtShipmentname").html("");
  $("#txtDestinationId").html("");
  $("#txtPlanDestinationId").html("");
  $.ajax({
    method: 'GET',
    url: '/outbound_shipments/getshipplanfromstage?stage_id='+id,
    beforeSend: function(){
      $("body").addClass("loading");
      $("#loading").dialog('open').html();
    },
    success: function(data) {
      $('#loading').dialog('close');
      $("body").removeClass("loading");

      if(data.status == true){

        $("#sel_split_shipplan option").remove();
        $('#sel_split_shipplan').append($('<option>', {
              value: '-1',
              text: ''
          }));
        for (var i = 0; i< data.data.length; i ++) {
          $('#sel_split_shipplan').append($('<option>', {
              value: data.data[i].plan_id,
              text: data.data[i].plan_shipment_id
          }));
        }
        if(tryable){
          $('#dlg_list_updatable_shipment').modal('toggle');
        }

        $("#dlg_updatableShipment").modal();
      }else{
        $('#dlg_list_updatable_shipment').modal('toggle');
        if(data.msg != 'Shipplan is not created.'){
          alert(data.msg);
        }else{
          if(tryable){
            show_user_warehouse();
            return;
            $.ajax({
              method: 'GET',
              beforeSend: function(){
                $("body").addClass("loading");
                $("#loading").dialog('open').html();
              },
              url: '/outbound_shipments/createshipplan?stage_id='+id+'&qty='+current_box_qty,
              success: function(data) {
                $('#loading').dialog('close');
                $("body").removeClass("loading");
                if(data.status == false){
                   alert(data.msg);
                }else{
                  selected_stage_id_plan = 0;
                  po_list.ajax.reload();
                  updateShipment(id,current_box_qty,false);
                }

              },
              error: function(jqXHR) {
                $('#loading').dialog('close');
                $("body").removeClass("loading");
              }
            });
          }else{
            alert("Failed to create Shipplan");
          }
        }

      }

    },
    error: function(jqXHR) {
      $('#loading').dialog('close');
      console.log(jqXHR);
    }
  });
}

$("#btn_create_shipment").click(function(){
  selected_split_id = $('#sel_split_shipplan').val();
  if(selected_split_id < 0){
    alert('Please select the shipplan.');
    return;
  }
  selected_contents = "SHIPMENT";
  $("#txt_new_qty").val(selected_current_box_qty);
  $("#dlg_adjust_qty").modal();
});

$("#btn_recreate_shipplan").click(function(){
    show_user_warehouse();
});

function show_user_warehouse(){
    selected_contents = "SHIPPLAN";
    $("#txt_new_qty").val(selected_current_box_qty);
    $.ajax({
        method: 'GET',
        url: '/outbound_shipments/get_previous_address?id='+selected_stage_id_add,
        datatype: 'json',
        success: function(data){
            if(data.status){
                var val = data.data;
                $("#warehouse_value").val(val.id);
                $("#warehouse_name").val(val.warehouse_name);
                $("#warehouse_address_line1").val(val.warehouse_address_line1);
                $("#warehouse_address_line2").val(val.warehouse_address_line2);
                $("#warehouse_city").val(val.warehouse_city);
                $("#warehouse_state_or_province_code").val(val.warehouse_state_or_province_code);
                $("#warehouse_postal_code").val(val.warehouse_postal_code);
                $("#warehouse_country_code").val(val.warehouse_country_code);
                $("#dlg_shipment_address").modal();
            }else{
                alert("Customer information is not correct.");
            }            
        },
        error: function(jqXHR){
            alert("Customer information is not correct.");
        }
    });
}
$("#frm_warehouse_address").submit(function (e){
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
        url: 'users/update_warehouse',
        type: 'POST',
        data: formData,
        dataType: 'json',
        success: function (data) {
            $("#dlg_shipment_address").modal('toggle');
            $("#dlg_adjust_qty").modal();
        },
        error: function(jqXHR) {
        },
        cache: false,
        contentType: false,
        processData: false  
    });
});
$("#btn_inbound_create").click(function(){
  var new_box = $("#txt_new_qty").val();
  if(selected_contents == "SHIPMENT"){
    $.ajax({
      method: 'GET',
      url: '/inbound_shipments/create_from_plan?shipplan_id='+selected_split_id+'&qty='+new_box+'&stage_id='+selected_stage_id_add,
      datatype: "json",
      beforeSend: function(){
        $("body").addClass("loading");
        $("#loading").dialog('open').html();
      },

      success: function(data) {
        $('#loading').dialog('close');
        $("body").removeClass("loading");
        if(data.result == true){
            alert("Shipment successfully created.");
            po_list.ajax.reload();
        }else{
            alert(data.msg)
        }
      },
      error: function(jqXHR) {
        //alert('Oops connection error!');
        $('#loading').dialog('close');
        $("body").removeClass("loading");
      }
    });
  }else if(selected_contents == "SHIPPLAN"){
    $.ajax({
      method: 'GET',
      beforeSend: function(){
        $("body").addClass("loading");
        $("#loading").dialog('open').html();
      },
      url: '/outbound_shipments/createshipplan?stage_id='+selected_stage_id_add+'&qty='+new_box,
      success: function(data) {
        $('#loading').dialog('close');
        $("body").removeClass("loading");
        if(data.status == false){
          alert(data.msg);
        }else{
          po_list.ajax.reload();
          selected_stage_id_plan = 0;
          updateShipment(selected_stage_id_add,new_box,false);
        }

      },
      error: function(jqXHR) {
        $('#loading').dialog('close');
        $("body").removeClass("loading");
      }
    });
  }

});

$("#btn_outbound_show_all_plan").click(function(){
  var row = po_list.rows( 0 ).data();
  if (row[0] == undefined){
    alert("Please search the FNSKU first to check Shipment.");
    return;
  }
  var stage_id =  row[0]['id'] ;
  if (stage_id == undefined){
    alert("Please search the FNSKU first to check Shipment.");
    return;
  }

  if ( $.fn.dataTable.isDataTable( '#table_shipplan' ) ) {
    table = $('#table_shipplan').dataTable();
    table.fnDestroy();
  }
  table = $('#table_shipplan').DataTable( {
      'paging':   true,  // Table pagination
      "searching":  true,
      "processing": true,
      "serverSide": true,
      "pageLength": 7,
      // "scrollY":        '50vh',
      // "scrollCollapse": true,
      // "paging":         false,
      sAjaxSource: '/inbound_shipplans/loadshipplan?stage_id='+stage_id,
      aoColumns: [
      { mData: 'user_name'},
      { mData: 'shipplan_id' },
      { mData: 'destination_fulfillment_center_id' },
      { mData: 'sku_count' },
      { mData: 'ship_qty' },
    ],
      select: true,
      buttons: [
        // {extend: "remove", editor: deletor},
      ],

      dom: "Bfrtip",
  } );
  $("#dlg_shipplan_list").modal();
});

$("#btn_outbound_show_all_shipment").click(function(){
  var row = po_list.rows( 0 ).data();
  if (row[0] == undefined){
    alert("Please search the FNSKU first to check Shipment.");
    return;
  }
  var stage_id =  row[0]['id'] ;
  if (stage_id == undefined){
    alert("Please search the FNSKU first to check Shipment.");
    return;
  }
  if ( $.fn.dataTable.isDataTable( '#table_shipment' ) ) {
      table = $('#table_shipment').dataTable();
      table.fnDestroy();
  }
  table = $('#table_shipment').DataTable( {
        "searching":  true,
        "processing": true,
        "serverSide": true,
        // "scrollY":        '50vh',
        // "scrollCollapse": true,
        "paging":         true,
        "pageLength": 7,
        sAjaxSource: '/inbound_shipments/getshipmentofstage?stage_id='+stage_id,
        aoColumns: [
        { mData: 'user_name'},
        { mData: 'shipment_id' },
        { mData: 'destination_fulfillment_center_id' },
        { mData: 'sku_count' },
        { mData: 'ship_qty' },
        { mData: null, render: function (data, type, full, meta) {
          var shipment_id = data['id'];
          return '<a href="inbound_shipments/' + shipment_id + '" class="btn btn-info btn-sm" target="_blank">View Transport Plan</a>'
        } },
      ],
        select: true,
        buttons: [
          //  {extend: "remove", editor: deletor_shipment},
        ],

        dom: "Bfrtip",
    } );
  $("#dlg_shipment_list").modal();
});



/**
 * Auto Bagger
 */
function load_bagger_list(){
  if ( $.fn.dataTable.isDataTable( '#datatable_autobagger_list' ) ) {
      datatable_bagger_list = $('#datatable_autobagger_list').dataTable();
      datatable_bagger_list.fnDestroy();
    }
    po_all = $('#datatable_autobagger_list').DataTable({
      "iDisplayLength" : 12,
      "searching":  true,
      "processing": true,
      "serverSide": true,
      sAjaxSource: '/outbound_shipments/list_bagger',
      aoColumns: [
        { mData: 'image_url'},
        { mData: 'bagger_number'},
        { mData: 'name' },
      ],
      select: true,
      rowId: 'id',
      aoColumnDefs: [
        {
            "targets": [ 0 ],
            render: function(data) {
                return '<img style="width:70px;" src="'+data+'">'
              }
        }
      ],
  });
}

$('#datatable_autobagger_list tbody').on('click', 'tr', function () {
  var table = $('#datatable_autobagger_list').DataTable();

  var data = table.row( this ).data();
  $('#datatable_autobagger_list tbody td').css('background-color', 'inherit');
  $('td', this).css('background-color', 'red');
  // this.css('background-color', 'red');
  selected_bagger_id = data['id'];

  // $('#dataview input:radio').attr("checked",false);

  $.ajax({
      url: '/outbound_shipments/getbagger?id='+selected_bagger_id,
      dataType: 'json',
      success: function(data) {
          $("#btn_send_bagger_print").show();
          $("#img_product").attr('src',data.vendoritem.vendorasin.large_image);
          $("#div_single_unit").text(data.received_quantity);
          $("#div_bag_no").text("Bag "+data.vendoritem.seller_sku.bagger_number);
          $("#div_pack").text("Pack of "+data.vendoritem.packcount);
          $("#div_box_qty").text(data.shipped_packs);
          $("#div_itemname").text(data.name);

          $("#div_prep").text(data.vendoritem.seller_sku.prepinstruction);

          if(data.vendoritem.seller_sku.snl == 1){
            $("#div_snl").text("SNL");
          }else{
            $("#div_snl").text("");
          }

      },
      error: function(xhr){
        $("#btn_send_bagger_print").hide();
      }
  });
});
$("#btn_send_bagger_print").click(function(){
  var url = "/outbound_shipments/printbagger?id="+selected_bagger_id;
  window.open(url);
});

$('#datatable_outbound_start_table tbody').on('click', '.click_cell', function () {
  var data = po_list.row( $(this).parents('tr') ).data();
  var item_id;
  // if(data['vendoritem'] != null){
  //   item_id = data['vendoritem']['id'];
  // }else{
    item_id = data['pending_po']['vendoritem']['id'];
  // }
  
  
  $.ajax({
    url: '/vendoritems/getvendoritem?id='+item_id,
    method: 'get', // This also could be PUT or PATCH
    dataType: 'JSON',
    success: function(data){
      console.log(data['vendorasin']['name'])
      $("#img_product").attr('src',data['vendorasin']['large_image']);
      $("#product_name").text(data['vendorasin']['name'])
      $("#product_prep").text(data['seller_sku']['prepinstruction'] + " ,Pack Of " + data['packcount'] + " ," + data['vendorasin']['packagewidth'] + "*" + data['vendorasin']['packagelength'] + "*" + data['vendorasin']['packageheight'] + " , "+data['vendorasin']['packageweight'] + " lb");
      asin = data['asin'];
      $("#dlg_image").modal();
    },
  });
});

$(".shipment-selects select").on('change', function() {
  amazon_carrier = $("#transport_content_delivery_by_amazon").val()
  shipment_type = $("#transport_content_shipment_type").val()
  $(".transport-info-section").addClass("hidden")
  $(".submit-btn").addClass("hidden")
  if (amazon_carrier == "true" && shipment_type == "SP") {
    $(".partnered-small-parcel").removeClass("hidden")
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "false" && shipment_type == "SP") {
    $(".non-partnered-small-parcel").removeClass("hidden")
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "true" && shipment_type == "LTL") {
    $(".partnered-ltl-parcel").removeClass("hidden")
    $(".submit-btn").removeClass("hidden")
  } else if (amazon_carrier == "false" && shipment_type == "LTL") {
    $(".non-partnered-ltl-parcel").removeClass("hidden")
    $(".submit-btn").removeClass("hidden")
  }
});

function openasin(){
  window.open("https://www.amazon.com/dp/"+asin);
}
