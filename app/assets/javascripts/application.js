// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//

//= require jquery_ujs

//--- Wraith
//= require_tree ../../../vendor/assets/javascripts/wraith/

//--- Datatables
//= require datatables/media/js/jquery.dataTables.min
//= require datatables-colvis/js/dataTables.colVis
//= require datatables/media/js/dataTables.bootstrap

//= require datatables/media/js/dataTables.select.min
//= require datatables/media/js/moment.min.js

//--- Datatables Buttons
//= require datatables-buttons/js/dataTables.buttons
//= datatables-buttons/css/buttons.bootstrap.css
//= require datatables-buttons/js/buttons.bootstrap
//= require datatables-buttons/js/buttons.colVis
//= require datatables-buttons/js/buttons.flash
//= require datatables-buttons/js/buttons.html5
//= require datatables-buttons/js/buttons.print
//= require datatables-responsive/js/dataTables.responsive
//= require datatables-responsive/js/responsive.bootstrap

//= require datatable-editor/js/dataTables.editor.min
//= require datatable-editor/js/datatable_page_input

//= require jquery_nested_form

// --- Sweet Alert
//= require sweetalert/dist/sweetalert.min.js
$.fn.dataTable.ext.errMode = 'none';
$.fn.DataTable.ext.pager.numbers_length = 5;
var input_timer, input_delay = 500;
$(document).ready(function () {
 	$("#loading").dialog({
        hide: 'slide',
        show: 'slide',
        autoOpen: false
    });
});

function delay(callback, ms) {
    var timer = 0;
    return function() {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        callback.apply(context, args);
      }, ms || 0);
    };
}
function stopPropagation(evt) {
    if (evt.stopPropagation !== undefined) {
        evt.stopPropagation();
    } else {
        evt.cancelBubble = true;
    }
}
function asin_link(asin){
    return '<a href="'+"https://sellercentral.amazon.com/productsearch?q="+asin+ '" target="_blank">' + asin + '</a>';
}