module CustomerInvoicesHelper
    def valid_customer_invoice_items(pending_po_item_ids)
        already_added = PendingPo.where(id: pending_po_item_ids).where.not(customer_invoice_id: nil).count
        if already_added > 0
            return {:status => false, :error_message => 'Some items have already Invoices.'}
        end
        pending_po_indicies = PendingPo.where(id: pending_po_item_ids).pluck(:pending_po_index_id).uniq.count
        if pending_po_indicies > 1
            return {:status => false, :error_message => 'Invoice can includes items of same PendingPo.'}
        end
        return {:status => true}
    end

end
