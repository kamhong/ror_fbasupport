module VendoritemsHelper
  
  def get_vendoritem(id_value, id_type, cost, vendor_sku, vendor_title, vendor_id, packcount, seller_sku, min_price, max_price)
    vendor = Vendor.find(vendor_id)
    type = id_type.downcase
    if id_value.blank? || vendor.nil?
        return nil
    end
    if type == 'asin' || type == 'upc'
      vendoritem = vendor.vendoritems.where(type => id_value).first
      unless vendoritem.nil?
        if vendoritem.vendorasin.nil?
          asin = Vendorasin.where(type => id_value).first
          if asin.nil?
            asin = Vendorasin.where(type => id_value).create
            asin.fetched = 0
            asin.save
          end
          vendoritem.vendorasin_id = asin.id
        end
      else
        asin = Vendorasin.where(type => id_value).first
        if asin.nil?
          asin = Vendorasin.where(type => id_value).create
          asin.fetched = 0
          asin.save
        end
        asin.reload
        vendoritem = vendor.vendoritems.where(:asin => asin.asin).first_or_create
        vendoritem.vendorasin_id = asin.id
      end
      vendoritem.cost = cost unless cost.nil? 
      vendoritem.vendorsku = vendor_sku unless vendor_sku.nil? 
      vendoritem.packcount = packcount unless packcount.nil?
      vendoritem.vendortitle = vendor_title unless vendor_title.nil?
      vendoritem.min_price = min_price unless min_price.nil?
      vendoritem.max_price = max_price unless max_price.nil?
      vendoritem.save
      vendoritem.id
    else
        nil
    end
  end

  def set_defined_sku (ids, sku_data)
    vendoritems = Vendoritem.where(id: ids).left_outer_joins(:seller_sku).where("seller_skus.id is null")
    if vendoritems.count == 0
      return
    end
    user = vendoritems.first.vendor.user
    sku_titles = []
    vendoritems.each do |vendoritem|
      if vendoritem.vendorasin.nil?
        next
      end
      sku = user.seller_skus.where(:vendorasin_id => vendoritem.vendorasin.id).first
      unless sku.nil?
        vendoritem.seller_sku_id = sku.id
        vendoritem.save
        next
      end

      if sku_data[vendoritem.id].blank?
        next
      end
      sku_title = sku_data[vendoritem.id]
      vendoritem.pre_defined_sku = sku_title
      vendoritem.save
    end
  end
end
