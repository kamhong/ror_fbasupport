require 'net/http'
module SkynetsHelper
  def get_api_account user_id
    status= "NO";
    result = HTTParty.get(
      "http://skynet2.azurewebsites.net/api/Admin/GetApiAccountsByUserId/#{user_id}/false",
      :headers => {
        'Content-Type' => 'application/json',
        'apikey' => '9A38277F-721A-4473-90B9-9780BC02996A',
      }
    )
    if result.response.class == "Net::HTTPOK"
      result.parsed_response.each do |response|
        status = response["Enabled"]? "Enabled" : "Disabled"
        break
      end
    end
    status
  end

  def enable_api_account (user, enable = true)
    result = HTTParty.post(
      "http://skynet2.azurewebsites.net/api/Admin/EnableApiAccount",
      :headers => {
        'Content-Type' => 'application/json',
        'apikey' => '9A38277F-721A-4473-90B9-9780BC02996A',
      },
      :body => {
        "Enabled": enable,
        "UserId": user.skynet_api_user_id,
        "ApiKey": user.skynet_key
      }
    )
    if result.response.class == "Net::HTTPOK"
      true
    else
      false
    end
  end

  def create_api_account user
    params = {
      "SellerId": user.seller_id,
      "MarketplaceId": "ATVPDKIKX0DER",
      "AccountName": user.full_name,
      "AuthenticationToken": user.mws_access_token,
      "UserId": user.skynet_api_user_id,
      "MWSKey": "",
      "MWSSecret": "",
      "AWSKey": "",
      "AWSSecret": ""
    }
    created_api = HTTParty.post(
      'http://skynet2.azurewebsites.net/api/Admin/CreateApiAccount',
      :body => params.to_json,
      :headers => {
        'Content-Type' => 'application/json',
        'apikey' => '9A38277F-721A-4473-90B9-9780BC02996A',
      }
    )
    if created_api["Message"] == "An error has occurred."
      if created_api["ExceptionMessage"] == "There is an account already existing with this amazon account details."
      end
    end
  end
end
