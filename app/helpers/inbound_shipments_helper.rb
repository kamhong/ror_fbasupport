module InboundShipmentsHelper
  def class_by_partner partnered
    partnered ? "check" : "times"
  end
  
  def set_text_for_shipment_type shipment_type
    shipment_type == "SP" ? "Small Parcel Delivery (I am shipping individual boxes)" : "Less than truckload (I am shipping pallets)"
  end

  def set_text_for_partner_options amazon_partnered
    amazon_partnered ? "Delivery by Amazon" : "I will deliver on my own"
  end
end
