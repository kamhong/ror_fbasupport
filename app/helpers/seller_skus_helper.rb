require 'peddler'
module SellerSkusHelper
  $mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
  $mws_developer_secret = "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"
  
  def remove_sku_from_vendor(ids)
    success = true
    result = {
      :status => true,
      :error_message => "",
      :msg => "OK"
    }
    vendoritems = Vendoritem.where(id: ids).joins(:seller_sku)
    if vendoritems.count  == 0
      result[:status] = true
      return result
    end
    feed = ""
      
    user = vendoritems.first.vendor.user

    unless user.mws_key_valid
      result[:status] = false
      result[:error_message] = "Please connect your Amazon Account."
      return result
    end

    header = ['sku','product-id',  'product-id-type', 'price',   'minimum-seller-allowed-price',    'maximum-seller-allowed-price',    'item-condition',   'add-delete',  'fulfillment-center-id', 'batteries_required','supplier_declared_dg_hz_regulation','item_weight','item_weight_unit_of_measure']
    header.each do |head|
        feed = "#{feed}#{head}\t"
    end
    feed = "#{feed}\n"

    id_type = 1
    delete_list = []
    sku_titles = []

    vendoritems.each do |vendoritem|
        sku_title = vendoritem.seller_sku.sku

        delete_list << {sku_title: sku_title, asin_id: vendoritem.vendorasin.id}
        
        bbp = vendoritem.vendorasin.buyboxprice
        if bbp == 0
            bbp = 9999.99
        end
        weight = vendoritem.vendorasin.packageweight.nil? ? vendoritem.vendorasin.packageweight : 0
        feed = "#{feed}#{sku_title}\t#{vendoritem.asin}\t#{id_type}\t#{bbp}\t\t\11\tx\tAMAZON_NA\tFALSE\tunknown\t#{weight}\tKG\t\n"
    end

    if Rails.env.production? || true
      client = MWS.feeds(marketplace: user.mws_market_place_id,
      merchant_id: user.seller_id,
      aws_access_key_id: "AKIAIB5ZWGDEZQZFMISA",
      auth_token: user.mws_access_token,
      aws_secret_access_key: "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")
      begin
        submit_result = client.submit_feed(feed, '_POST_FLAT_FILE_INVLOADER_DATA_').parse
      rescue Peddler::Errors::AccessDenied => error
        result[:status] = false;
        result[:error_message] = "Access to Feeds.SubmitFeed is denied."
        return result 
      rescue Exception => error
        result[:status] = false;
        result[:error_message] = error.to_s
        return result 
      end

      feed_submission_id = submit_result["FeedSubmissionInfo"]["FeedSubmissionId"]
      puts "feed_submission_id"
      puts feed_submission_id
      # feed_submission_id = "953424017911"
      tried = 1
      retries = 5
      error_list = []
      error_message = ""
      success = false
      loop do
        begin
          break if tried > 5 || success
          feed_result = client.get_feed_submission_result(feed_submission_id)
          if feed_result.valid?
            success = true
            summary = feed_result.summary
            lines = summary.split("\n")
            
            records = lines[1].split("\t")
            total = records.last.to_i

            records = lines[2].split("\t")
            successful = records.last.to_i
            
            if total > successful
              contents = feed_result.content
              lines = contents.split("\n")
              lines[1..-1].each do |line|
                records = line.split("\t")
                sku = records[1].to_s
                error_type = records[3].to_s
                error_msg = records[4].to_s
                if error_type == "Error"
                  error_message = "#{error_message}#{sku}: #{error_msg}\n"
                  puts "sku_feed#{error_message}"
                end
                error_list << sku
              end
            end
            break
          else
            sleep tried*30
            tried = tried + 1
          end
        rescue Peddler::Errors::FeedProcessingResultNotReady => error
          tried += 1
          if tried < retries
              puts tried
              sleep tried*30
              retry
          end
        rescue Exception => error
          result[:status] = false;
          error_message = error.to_s
          return result
        end
      end
    else
        error_list = []
    end
    
    puts "DELETED_LIST"
    puts delete_list
    puts "ERROR_LIST"
    puts error_list
    if success
        delete_list.each do |item|
          unless error_list.include? item[:sku_title]
              user.seller_skus.where(:sku => item[:sku_title], :vendorasin_id => item[:asin_id]).destroy_all
              user.vendoritems.where(:vendorasin_id => item[:asin_id]).each do |vi|
                vi.seller_sku_id = nil
                vi.save
              end
          end
        end
    end
    result[:error_message] = error_message
    return result
  end
  
  def set_offer_of_sku(sku_id_list)
    puts "set_offer_of_sku__start"
    if sku_id_list.kind_of?(Array)
      sku = SellerSku.find_by id: sku_id_list[0]
    else
      sku = SellerSku.find_by id: sku_id_list
    end
    if sku.nil?
      return
    end

    user = sku.user
    unless user.mws_key_valid
      puts "user not mws_key_valid"
      return
    end
    client = MWS.feeds(marketplace: user.mws_market_place_id,
      merchant_id: user.seller_id,
      aws_access_key_id: $mws_developer_id,
      auth_token: user.mws_access_token,
      aws_secret_access_key: $mws_developer_secret)
    begin
      feed = '<?xml version="1.0" encoding="utf-8"?>
        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
        <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>'+user.seller_id+'</MerchantIdentifier>
        </Header>
        <MessageType>Price</MessageType>'
      SellerSku.where(:id => sku_id_list).distinct(:sku).each_with_index do |sku, index|
        bbp = sku&.vendorasin.buyboxprice || 9999
        message = "<Message>
          <MessageID>#{index+1}</MessageID>
          <Price>
            <SKU>#{sku.sku}</SKU>
            <StandardPrice currency=\"USD\">#{bbp}</StandardPrice>
          </Price>
          </Message>
          "
        feed = feed + message
      end
      feed = "#{feed}</AmazonEnvelope>"
      submit_result = client.submit_feed(feed, '_POST_PRODUCT_PRICING_DATA_').parse
      feed_submission_id = submit_result["FeedSubmissionInfo"]["FeedSubmissionId"]
      puts feed_submission_id
      result = get_feed_submission_result(client, feed_submission_id, 'price')
      puts "get_feed_submission_result"
    rescue Peddler::Errors::AccessDenied => error
      result[:status] = false
      result[:error_message] = "Access to Feeds.SubmitFeed is denied."
      return result 
    rescue Exception => error
      result[:status] = false
      result[:error_message] = error.to_s
      return result 
    end
    return result
  end


  def get_feed_submission_result(client, feed_submission_id, feed_type='sku')
    result = {
      :status => true,
    }
    retries = 5
    tried = 0
    success = false
    error_list = []
    loop do
      begin
        break if tried > 5 || success
        feed_result = client.get_feed_submission_result(feed_submission_id)
        if feed_result.valid?
          success = true
          if feed_type == "sku"
            summary = feed_result.summary
            lines = summary.split("\n")
            
            records = lines[1].split("\t")
            total = records.last.to_i

            records = lines[2].split("\t")
            successful = records.last.to_i
            
            if total > successful
              contents = feed_result.content
              lines = contents.split("\n")
              lines[1..-1].each do |line|
                records = line.split("\t")
                
                sku = records[1].to_s
                error_type = records[3].toa_s
                error_msg = records[4].to_s
                if error_type == "Error"
                  error_message = "#{error_message}#{sku}: #{error_msg}\n"
                  puts "sku_feed#{error_message}"
                end
                error_list << sku
              end
            end
          else
          end
          break
        else
            sleep tried*30
            tried = tried + 1
        end
      rescue Peddler::Errors::FeedProcessingResultNotReady => error
        tried += 1
        if tried < retries
          puts tried
          sleep tried*30
          retry
        end
      rescue Exception => error
        result[:status] = false
        result[:error_message] = error.to_s
        return result
      end
    end
    result[:status] = true
    result[:error_list] = error_list
    return result
  end

  def create_skus_from_vendor(ids, recreate=false)
    success = true
    result = {
      :status => true,
      :error_message => "",
      :msg => "OK"
    }
    vendoritems = Vendoritem.where(id: ids).left_outer_joins(:seller_sku).where("seller_skus.id is null")
    if vendoritems.count  == 0
      result[:status] = true
      result[:error_message] = "Please select the valid items."
      return result
    end
    feed = ""
    
    user = vendoritems.first.vendor.user

    unless user.mws_key_valid
      result[:status] = false
      result[:error_message] = "Please connect your Amazon Account."
      return result
    end

    header = ['sku','product-id',  'product-id-type', 'price',   'minimum-seller-allowed-price',    'maximum-seller-allowed-price',    'item-condition',   'add-delete',  'fulfillment-center-id', 'batteries_required','supplier_declared_dg_hz_regulation','item_weight','item_weight_unit_of_measure']
    header.each do |head|
        feed = "#{feed}#{head}\t"
    end
    feed = "#{feed}\n"

    id_type = 1
    creating_list = []
    sku_titles = []

    vendoritems.each do |vendoritem|
      sku_title = "#{user.first_name[0..2]}_#{vendoritem.vendorsku}"
      sku_title = vendoritem.pre_defined_sku unless vendoritem.pre_defined_sku.blank?
      
      sku_title = "#{sku_title}_#{[*('a'..'z'),*('0'..'9')].shuffle[0,3].join}" if recreate
      sku = user.seller_skus.where(:vendorasin_id => vendoritem.vendorasin.id).first
      unless sku.nil?
          vendoritem.seller_sku_id = sku.id
          vendoritem.save
          next
      end
      count = SellerSku.where('sku like ? or sku = ?', "#{sku_title}-%","#{sku_title}").count + sku_titles.count(sku_title)
      sku_titles << sku_title

      if count > 0
          sku_title = "#{sku_title}-#{count}"
          unless SellerSku.where(" sku = ?", sku_title).first.nil?
              sku_title = "#{sku_title}-#{[*('a'..'z'),*('0'..'9')].shuffle[0,4].join}"
          end
      end


      creating_list << {sku_title: sku_title, asin_id: vendoritem.vendorasin.id}
      
      bbp = vendoritem.vendorasin.buyboxprice
      if bbp == 0
          bbp = 9999.99
      end
      weight = vendoritem.vendorasin.packageweight.nil? ? 0 : vendoritem.vendorasin.packageweight
      feed = "#{feed}#{sku_title}\t#{vendoritem.asin}\t#{id_type}\t#{bbp}\t#{bbp-0.1}\t#{bbp+1000}\t11\ta\tAMAZON_NA\tFALSE\tunknown\t#{weight}\tKG\t\n"
    end

    if creating_list.count == 0
        result[:status] = true
        result[:msg] = "There is no  new SellerSku."
        return result
    end
    if Rails.env.production? || 1
      client = MWS.feeds(marketplace: user.mws_market_place_id,
      merchant_id: user.seller_id,
      aws_access_key_id: "AKIAIB5ZWGDEZQZFMISA",
      auth_token: user.mws_access_token,
      aws_secret_access_key: "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")
      begin
        submit_result = client.submit_feed(feed, '_POST_FLAT_FILE_INVLOADER_DATA_').parse
      rescue Peddler::Errors::AccessDenied => error
        result[:status] = false;
        result[:error_message] = "Access to Feeds.SubmitFeed is denied."
        return result 
      rescue Exception => error
        result[:status] = false;
        result[:error_message] = error.to_s
        return result 
      end

      feed_submission_id = submit_result["FeedSubmissionInfo"]["FeedSubmissionId"]
      puts "feed_submission_id"
      puts feed_submission_id
      # feed_submission_id = "953424017911"
      tried = 1
      retries = 5
      error_list = []
      error_message = ""
      success = false
      loop do
          begin
              break if tried > 5 || success
              feed_result = client.get_feed_submission_result(feed_submission_id)
              if feed_result.valid?
                  success = true
                  summary = feed_result.summary
                  lines = summary.split("\n")
                  
                  records = lines[1].split("\t")
                  total = records.last.to_i

                  records = lines[2].split("\t")
                  successful = records.last.to_i
                  
                  if total > successful
                      contents = feed_result.content
                      lines = contents.split("\n")
                      lines[1..-1].each do |line|
                          records = line.split("\t")
                          sku = records[1].to_s
                          error_type = records[3].to_s
                          error_msg = records[4].to_s
                          if error_type == "Error"
                              error_message = "#{error_message}#{sku}: #{error_msg}\n"
                              puts "sku_feed#{error_message}"
                          end
                          error_list << sku
                      end
                  end
                  
                  break
              else
                  sleep tried*30
                  tried = tried + 1
              end
          rescue Peddler::Errors::FeedProcessingResultNotReady => error
              tried += 1
              if tried < retries
                  puts tried
                  sleep tried*30
                  retry
              end
          rescue Exception => error
              result[:status] = false;
              error_message = error.to_s
              return result
          end
      end
    else
        error_list = []
    end
    
    if success
      creating_list.each do |item|
        unless error_list.include? item[:sku_title]
          new_sku = user.seller_skus.where(:sku => item[:sku_title], :vendorasin_id => item[:asin_id]).first_or_create
          user.vendoritems.where(:vendorasin_id => item[:asin_id]).each do |vi|
            vi.seller_sku_id = new_sku.id
            vi.save
          end
        end
      end
    end
    result[:error_message] = error_message
    return result
  end
  
    # seller_sku id list Array<Integer>
  def update_prepinstructions(sku_id_list, first_time_try=true)
    puts "update_prepinstructions__start"
    if sku_id_list.kind_of?(Array)
      sku = SellerSku.find_by id: sku_id_list[0]
    else
      sku = SellerSku.find_by id: sku_id_list
    end
    if sku.nil?
      return
    end

    seller_sku_list = SellerSku.where(:id => sku_id_list).distinct.pluck(:sku)
    puts seller_sku_list

    user = sku.user
    unless user.mws_key_valid
      puts "user not mws_key_valid"
      return
    end
    client = MWS.fulfillment_inbound_shipment(marketplace: user.mws_market_place_id,
      merchant_id: user.seller_id,
      aws_access_key_id: $mws_developer_id,
      auth_token: user.mws_access_token,
      aws_secret_access_key: $mws_developer_secret)

    begin
      result = client.get_prep_instructions_for_sku('US', seller_sku_list).parse
      puts result
      items = []
      unless result["SKUPrepInstructionsList"].nil?
        unless result["SKUPrepInstructionsList"]["SKUPrepInstructions"].nil?
          if result["SKUPrepInstructionsList"]["SKUPrepInstructions"].kind_of?(Array)
            items = result["SKUPrepInstructionsList"]["SKUPrepInstructions"]
          else
            items << result["SKUPrepInstructionsList"]["SKUPrepInstructions"]
          end
          items.each do |item|
            seller_sku = item["SellerSKU"]
            prep_instructions = ""
            unless item["PrepInstructionList"].nil?
              unless item["PrepInstructionList"]["PrepInstruction"].nil?
                item["PrepInstructionList"].each do |prep|
                  puts "prep_put"
                  puts prep
                  if prep[1].kind_of?(Array)
                    prep[1].each do |p|
                      prep_instructions = prep_instructions + p + "," 
                    end
                  else
                    prep_instructions = prep_instructions + prep[1] + ","
                  end
                end
              end
            end
            user.seller_skus.where(sku: seller_sku).update_all(prepinstruction: prep_instructions.chomp(","));
          end
        end
      end

      unless result["InvalidSKUList"].nil?
        unless result["InvalidSKUList"]["InvalidSKU"].nil?
          list = []
          unless result["InvalidSKUList"]["InvalidSKU"].kind_of?(Array)
            list << result["InvalidSKUList"]["InvalidSKU"]
          else
            list = result["InvalidSKUList"]["InvalidSKU"]
          end
          
          sku_offer_list = []
          list.each do |item|
            seller_sku = item["SellerSKU"]
            if item["ErrorReason"] == "DoesNotExist"
              issue_sku = user.seller_skus.where(sku: seller_sku).first
              sku_offer_list << issue_sku.id unless issue_sku.nil?
            else
              user.seller_skus.where(sku: seller_sku).update_all(is_invalid: true)
            end
          end

          if sku_offer_list.count > 0 && first_time_try
            result = set_offer_of_sku(sku_offer_list)
            puts "request_offer_#{sku_offer_list}" 
            if result[:status]
              result = update_prepinstructions(sku_offer_list, false)
            end
          end
        end
      end
    rescue Exception => error
      puts error
      return
    end
  end

  # seller_sku Id list Array<Integer>
  def update_shipplaninformation(sku_id_list)
    if sku_id_list.kind_of?(Array)
      sku = SellerSku.find_by id: sku_id_list[0]
    else
      sku = SellerSku.find_by id: sku_id_list
    end
    if sku.nil?
      return
    end
    seller_sku_list = SellerSku.where(:id => sku_id_list).distinct.pluck(:sku)

    
    user = sku.user
    unless user.mws_key_valid
      return
    end
    inbound_shipment_plan_request_items = []
    user.seller_skus.where(id: sku_id_list).where.not(prepinstruction: nil).each do |item|
      prep_details = []
      item.prepinstruction. split(',').each do |prep|
        prep_detail = {
          prep_instruction: prep,
          prep_owner: "SELLER"
        }
        prep_details << prep_detail
      end
      inbound_shipment_plan_request_item = {
        seller_sku: item.sku,
        # seller_sku: "WCF-KR-812974-2PK",
        quantity: 100, #only testing
        prep_details: prep_details
        # prep_details: [{prep_instruction: "Labeling",prep_owner: "SELLER"}, {prep_instruction: "Polybagging",prep_owner: "SELLER"}]
      }
      inbound_shipment_plan_request_items << inbound_shipment_plan_request_item
    end

    if inbound_shipment_plan_request_items.count == 0
      puts "No valid SKUs"
      return
    end
    opts = {
      ship_to_country_code: "US",
      label_prep_preference: "SELLER_LABEL"
    }
    client = MWS.fulfillment_inbound_shipment(marketplace: user.mws_market_place_id,
      merchant_id: user.seller_id,
      aws_access_key_id: $mws_developer_id,
      auth_token: user.mws_access_token,
      aws_secret_access_key: $mws_developer_secret)
    
    begin
      result = client.create_inbound_shipment_plan(user.warehouse_address, inbound_shipment_plan_request_items,opts).parse
    rescue Peddler::Errors::InvalidRequestException => error
      puts error.message
      error_message = error.message
      message_words = error_message.split(" ")
      error_sku = ""
      message_words.each_with_index do |m, index|
        if m == "MSKU" && message_words[index + 2] == "with"
          error_sku = message_words[index + 1]
          error_sku = user.seller_skus.where(:sku => error_sku).first
          break
        end
      end
      unless error_sku.nil?
        if error_message.downcase.include? "reason HAZMAT".downcase
          error_sku.sku_status = "HAZMAT"
          error_sku.save
        elsif error_message.downcase.include? "reason ITEM_INELIGIBLE".downcase
          error_sku.sku_status = "ITEM_INELIGIBLE"
          error_sku.save
        else
          error_sku.sku_status = error_message
          error_sku.save
        end
        return
      end
    rescue Exception => error
      return
    end

    unless result["InboundShipmentPlans"]["member"].nil?
      members = []
      if result["InboundShipmentPlans"]["member"].kind_of?(Array)
        members = result["InboundShipmentPlans"]["member"]
      else
        members << result["InboundShipmentPlans"]["member"]
      end
      puts members
      members.each do |shipment|
        destination_fulfillment_id = shipment["DestinationFulfillmentCenterId"]
        items = shipment["Items"]
        if items.kind_of?(Array)
          items["member"].each do |item|
            fnsku = item["FulfillmentNetworkSKU"]
            seller_sku = item["SellerSKU"]
            sku = user.seller_skus.find_by sku: seller_sku
            unless sku.nil?
              sku.fnsku = fnsku
              sku.inbound_destination = destination_fulfillment_id
              sku.sku_status = "OK"
              sku.save
            end
          end
        else
          item = items["member"]
          fnsku = item["FulfillmentNetworkSKU"]
          seller_sku = item["SellerSKU"]
          sku = user.seller_skus.find_by sku: seller_sku
          unless sku.nil?
            sku.fnsku = fnsku
            sku.inbound_destination = destination_fulfillment_id
            sku.sku_status = "OK"
            sku.save
          end
        end
        
      end
    end
  end
    
end
