class UserMailer < ApplicationMailer
	default from: 'info@fba.support'
	# @@sender = "steven@fba.support"
	@@sender = "kam@fba.support"
	def welcome_email(email, message)
		@email = email
		@message = message
		# @url = 'www.fba.support/skynets/new'
		@url = 'fba.support/skynets/new'
		mail(to: @email, subject: @message)
	end

	def receive(email)
	    page = Page.find_by(address: email.to.first)
	    page.emails.create(
	      subject: email.subject,
	      body: email.body
	    )

	    if email.has_attachments?
	      email.attachments.each do |attachment|
	        page.attachments.create({
	          file: attachment,
	          description: email.subject
	        })
	      end
	    end
	end

	def registration_confirmation(user)
	  recipients  user.email
	  from        "webmaster@example.com"
	  subject     "Thank you for Registering"
	  body        :user => user
	end

	def contact_me(message)
		@body = message.body
		mail to: @@sender, from: message.email, 'Reply-to': message.email, subject: "FBA.Support question from " + message.name
	end



	def send_skynetnotification(user, skynet)
		@url = skynet.outputfileurl
		@status = skynet.skynet_status.name
		@message = "FBA.Support file processing is done."
		# mail to: user.email, from:  "kam@fba.support", 'Reply-to': @@sender, subject: "FBA.Support file processing is done."
		mail(to: user.email, subject: @message)
	end

	def send_signin_notification(email)
		attachments.inline['logo.png'] = File.read(Rails.root.join('app/assets/images/back/logo.png'))
		@url = "https://calendly.com/fba-steve"
		@message = "FBA.Support"
		# mail to: email, from:  "Steven Rozen<#{@@sender}>", 'Reply-to': @@sender, subject: "FBA.Support"
		mail(to: email, subject: @message)
	end

	def send_welcome_email(email)
		attachments.inline['logo.png'] = File.read(Rails.root.join('app/assets/images/back/logo.png'))
		# mail to: email, from:  "Steven Rozen<#{@@sender}>", 'Reply-to': @@sender, subject: "Your Account is ready for USE! Start selling Wholesale Today!"
		@message = "Your Account is ready for USE! Start selling Wholesale Today!"
		mail(to: email, subject: @message)
	end

	def send_invoice(email, pdf)
		# @shipment = shipment
		# @logo = Rails.root.join('app/assets/images/dd_logo.png')
		# attachments.inline['invoice.pdf'] = WickedPdf.new.pdf_from_string(
		# 	render_to_string(
		# 		disposition: 'attachment',
		# 			template: "transport_plans/_invoice_pdf.html.erb",
		# 			layout: 'pdf_email.html.erb',
		# 			dpi: 300,
		# 			margin:  {
		# 									top: 15,
		# 									bottom: 10
		# 							},
		# 			footer:
		# 			{
		# 				html: {
		# 					template:'transport_plans/invoice_pdf_footer',
		# 					layout:  'pdf_plain',
		# 				},
		# 			}
		# 	)
		# )
		attachments.inline['Invoice.pdf'] = pdf
		@message = "Invoice is sent from FBA.Support"
		mail(to: email, subject: @message)
	end

end
