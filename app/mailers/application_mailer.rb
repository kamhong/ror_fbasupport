class ApplicationMailer < ActionMailer::Base
  default from: 'info@fba.support'
  layout 'mailer'
end
