class PoController < ApplicationController
	include SellerSkusHelper
	include VendoritemsHelper
	before_action :authenticate_user!

	def upload
		result = {'result'=>true, 'msg'=>'Successfully uploaded'}

		temp_path = params[:file_path]
		file_name = params[:file_name]
		restored_filename = params[:restored_filename]

		id_type = params[:id_type_col]
		vendor_id = params[:vendor_id_col]
		vendor_sku_col = params[:vendor_sku_col].to_i
		vendor_product_title_col = params[:vendor_product_title_col].to_i
		buy_qty_col = params[:buy_qty_col].to_i
		id_col = params[:id_col].to_i
		cost_col = params[:cost_col].to_i
		seller_sku_col = params[:seller_sku_col].to_i
		packcount_col = params[:packcount_col].to_i
		min_price_col = params[:min_price_col].to_i
		max_price_col = params[:max_price_col].to_i
		if id_col == -1
			attribute = I18n.t 'po.file_upload.attributes.id_column'
			flash[:error] = I18n.t 'po.file_upload.errors.messages.not_selected', :attribute => attribute
			redirect_to :back
			return
		end

		if vendor_id.nil?
			attribute = I18n.t 'po.file_upload.attributes.vendor_id'
			flash[:error] = I18n.t 'po.file_upload.errors.messages.not_selected', :attribute => attribute
			redirect_to :back
			return
		end

    extension = File.extname(file_name)
		vendoritem_ids = []
		sku_data = {}
		buy_qty_data = {}
    unless restored_filename.empty?
			error_msg = 'There was an error to read the file.'
      case extension
        when ".csv" then
					begin
						start_index = 0
						CSV.new(open('https://fba-skynets-csvs.s3.amazonaws.com/uploads/'+restored_filename)).each do |row|
							if start_index == 0
								start_index = start_index + 1
								next
							end
							if get_col_data_row(row,id_col).blank? || get_col_data_row(row,buy_qty_col).blank?
								next
							end
							vendoritem_id = get_vendoritem(get_col_data_row(row,id_col), id_type, get_col_data_row(row,cost_col,true), get_col_data_row(row,vendor_sku_col), get_col_data_row(row,vendor_product_title_col), vendor_id,get_col_data_row(row,packcount_col), get_col_data_row(row,seller_sku_col), get_col_data_row(row,min_price_col, true), get_col_data_row(row, max_price_col, true))
							unless vendoritem_id.nil?
								sku_data[vendoritem_id] = get_col_data_row(row,seller_sku_col)
								buy_qty_data[vendoritem_id] = get_col_data_row(row,buy_qty_col)
								vendoritem_ids << vendoritem_id
							end
						end
						result = send_vendoritem_to_po(vendoritem_ids, sku_data, buy_qty_data)
          rescue Exception => e
            if e.message == 'Missing or stray quote in line 1'
							data = CSV.parse(File.read(temp_path).gsub /\n/, '')[1 .. -1]
							data.each do |row|
								if get_col_data_row(row,id_col).blank? || get_col_data_row(row,buy_qty_col).blank?
									next
								end
								vendoritem_id = get_vendoritem(get_col_data_row(row,id_col), id_type, get_col_data_row(row,cost_col,true), get_col_data_row(row,vendor_sku_col), get_col_data_row(row,vendor_product_title_col),  vendor_id,get_col_data_row(row,packcount_col), get_col_data_row(row,seller_sku_col), get_col_data_row(row,min_price_col, true), get_col_data_row(row, max_price_col, true))
								unless vendoritem_id.nil?
									sku_data[vendoritem_id] = get_col_data_row(row,seller_sku_col)
									buy_qty_data[vendoritem_id] = get_col_data_row(row,buy_qty_col)
									vendoritem_ids << vendoritem_id
								end
							end
							result = send_vendoritem_to_po(vendoritem_ids, sku_data, buy_qty_data)
            else
              result = {'result'=>false, 'msg'=>e.message.force_encoding("ISO-8859-1").encode("UTF-8")}
            end
          end

				when ".xls" then
					begin
						data = Roo::Excel.new(temp_path)
						data.each_row_streaming(offset: 1) do |row|
							if get_excel_col_data_row(row,id_col).blank? || get_excel_col_data_row(row,buy_qty_col).blank?
								next
							end
							vendoritem_id = get_vendoritem(get_excel_col_data_row(row,id_col), id_type, get_excel_col_data_row(row,cost_col,true), get_excel_col_data_row(row,vendor_sku_col), get_excel_col_data_row(row,vendor_product_title_col), vendor_id,get_excel_col_data_row(row,packcount_col), get_excel_col_data_row(row,seller_sku_col), get_excel_col_data_row(row,min_price_col, true), get_excel_col_data_row(row, max_price_col, true))
							unless vendoritem_id.nil?
								vendoritem_ids << vendoritem_id
								sku_data[vendoritem_id] = get_excel_col_data_row(row,seller_sku_col)
								buy_qty_data[vendoritem_id] = get_excel_col_data_row(row,buy_qty_col)
							end
						end
						result = send_vendoritem_to_po(vendoritem_ids, sku_data,buy_qty_data)
					rescue Exception => e
						result = {'result'=>false, 'msg'=>e.message.force_encoding("ISO-8859-1").encode("UTF-8")}
					end
				when ".xlsx" then
					begin
						data = Roo::Excelx.new(temp_path)
						data.each_row_streaming(offset: 1) do |row|
							if get_excel_col_data_row(row,id_col).blank? || get_excel_col_data_row(row,buy_qty_col).blank?
								next
							end
							vendoritem_id = get_vendoritem( get_excel_col_data_row(row,id_col), id_type, get_excel_col_data_row(row,cost_col,true), get_excel_col_data_row(row,vendor_sku_col), get_excel_col_data_row(row,vendor_product_title_col), vendor_id,get_excel_col_data_row(row,packcount_col), get_excel_col_data_row(row,seller_sku_col), get_excel_col_data_row(row,min_price_col, true), get_excel_col_data_row(row, max_price_col, true))
							unless vendoritem_id.nil?
								vendoritem_ids << vendoritem_id
								sku_data[vendoritem_id] = get_excel_col_data_row(row,seller_sku_col)
								buy_qty_data[vendoritem_id] = get_excel_col_data_row(row,buy_qty_col)
							end
						end
						result = send_vendoritem_to_po(vendoritem_ids, sku_data,buy_qty_data)
					rescue => e
						result = {'result'=>false, 'msg'=> e.message.force_encoding("ISO-8859-1").encode("UTF-8")}
					end
        else result = {'result'=>false, 'msg'=>'Invalid file format.'}
      end

    end

		unless result.nil?
			if result["result"]
				flash[:notice] = "Purchase Orders are successfully uploaded"
			else
				flash[:error] = result["msg"]
			end
		end

		redirect_back(fallback_location: root_path)
	end

	def uploadfile
		@tempfile = params[:file]
		if @tempfile == "undefined"
			render json: {'result'=>false, 'msg'=>'Please upload valid file to continue!'}
			return
		end
    original_filename = @tempfile.original_filename
		extension = File.extname(original_filename)
		restored_filename = "#{current_user.id}_po_#{Digest::SHA1.hexdigest Time.now.to_s}"
    extension = File.extname(original_filename)
    restored_filename = restored_filename + extension
    if extension == '.xls' || extension == '.xlsx'
      @file_type = "File Type : Excel file"
    elsif extension == '.csv'
      @file_type = "File Type : text/csv file"
    else
     render json: {'result'=>false, 'msg'=>'Not supported file format.'}
     return
    end
		unless !@tempfile || @tempfile.blank?
			obj = S3_BUCKET.put_object({
				key: "uploads/#{restored_filename}",
				body: @tempfile.to_io,
				acl: 'public-read'
			})

      result = true
      error_msg = 'There was an error to read the file.'
      case extension
        when ".csv" then

          begin
            # contents = File.open(@tempfile.path).read

            # contents.sub!("\xEF\xBB\xBF", '')

            # @row = CSV.parse(contents).first
            CSV.foreach(@tempfile.path, encoding: "iso-8859-1:UTF-8") do |row|
              @row = row
              break
            end

          rescue Exception => e
            if e.message == 'Missing or stray quote in line 1'
              @tempfile.tempfile.rewind
              @row = CSV.parse(@tempfile.tempfile.read.gsub /\n/, '').first
            else
              result = false
            end
          end

        when ".xls" then
          data = Roo::Excel.new(@tempfile.path)
          @row = data.row(1)
          if @row == false
            result = false
          end
        when ".xlsx" then
          data = Roo::Excelx.new(@tempfile.path)
          @row = data.row(1)
          if @row == false
            result = false
          end
        else result = false
      end

    end
    if result
      @row.unshift("None")
    end

    render json: {'header'=>@row, 'result'=>result, 'temp_path'=>@tempfile.path, 'file_name'=>original_filename,'msg'=>error_msg, 'restored_filename'=>restored_filename}

	end

	def sendpo

		ids = params[:ids]
		result = send_vendoritem_to_po(ids)
		render json: result
		# render :js => "window.location = '/po/index?vendor_id=#{vendor_id}'"
	end

	def send_pending
		ids = params[:ids]
		po_items = Po.where(id: ids)
		if po_items.count == 0
			render json: {'result'=>false, 'msg'=>'Selected items are invalid'}
			return
		end
		unless current_user.mws_key_valid
			render json: {'result'=>false, 'msg'=>'Please input MWS keys to send Po.'}
			return
		end
		vendoritem_ids = po_items.pluck(:vendoritem_id)
		result = create_skus_from_vendor(vendoritem_ids)
		error_sku_create = "Seller SKU is not created."
		error_sku_create = result[:error_message] unless result[:error_message].blank?

		po_items = po_items.joins(:vendoritem).where("vendoritems.seller_sku_id is not null")
		if po_items.count == 0
			render json: {'result'=>false, 'msg'=>error_sku_create}
			return
		end

		vendor_id = po_items[0].vendoritem.vendor_id
		pending_po_index = Vendor.find(vendor_id).pending_po_indices.create
		po_items.each do |x|
			next if x.vendoritem.nil?
			pending_po_index.pending_pos.create([{vendoritem_id: x.vendoritem_id, order_quantity: x.order_quantity, cost: x.vendoritem.cost}])
		end
		po_items.destroy_all
		render json: {'result'=>true, 'vendor_id'=>vendor_id,'msg'=>'Selected items are added to Pending'}
	end

	def index
		@pendingpos = PendingPoIndex.where(:vendor_id => current_user.vendors.first.id).all
	end

	def list

		# format.json  { render :json => current_user.vendors, :methods => [:po_order_costs, :po_order_skus, :po_order_qty]}
		# respond_to do
	end

	def listpo
		vendors = current_user.vendors.where(:id => current_user.pos.joins(:vendoritem).pluck("vendoritems.vendor_id"))
		vendors = vendors.includes({pos: {vendoritem: [:vendorasin, :seller_sku, {vendor: :warehouse_vendor}]}})
		# pos = pos.includes(:warehouse_vendor, vendoritems: [:po, vendor: :user])
		# render json: {'aaData'=>current_user.vendors.as_json(:methods => [:po_order_costs, :po_order_skus, :po_order_qty, :po_order_profit])}
		render json: 
		{
			aaData: vendors.as_json(
				methods: [:po_order_costs, :po_order_skus, :po_order_qty, :po_order_profit],
				include: [{pos: {include: {vendoritem: {include: [:vendorasin, :seller_sku, vendor: {include: :warehouse_vendor}]}}}}]
			)
		}
	end

	def delete
		data = params[:data]
		data.each do |key, item|
			po = Po.find(key)

			if po.present?
				po.destroy
			end
		end
	end

	def deletebyvendor

		vendor_id = params[:vendor_id]
		po = Po.joins(:vendoritem).where("vendoritems.vendor_id =?", vendor_id)
		if po.present?
			po.destroy_all
		end

	end

	def updateitem

		@po = nil
    data = params[:data]
    data.each do |key, item|
      @po = Po.find(key)

			item.each do |title, value|
        if title=='buyboxprice' || title == 'salespermonth' || title == 'fbafee'
          @po.vendoritem.vendorasin[title.to_sym] = value
				elsif title == 'order_quantity' || title == 'received_qty'
					@po[title.to_sym] = value
        elsif title == 'packcount'
          if value.to_i > 0
            @po.vendoritem.packcount = value.to_i
          end
				else
					@po.vendoritem[title.to_sym] = value
        end

			end
			@po.vendoritem.vendorasin.save
			@po.vendoritem.save
      @po.save
    end

    render json: {'data'=>@po}
	end

	def load
		vendoritem_columns = ['id','cost','upc','created_at','vendorsku','packcount','vendortitle']
		express_columns = ['profit','margin','salespermonth','salesrank'];
		sku_columns = ['sku']
		po_columns = ['order_quantity', 'received_qty']
		notsearch_columns = ['packcost']

		start = params[:iDisplayStart].to_i
		limit = params[:iDisplayLength].to_i
		vendor_id = params[:vendor]

		vendor = Vendor.find(vendor_id)


		inventories = Po.joins(vendoritem: :vendorasin).where("vendoritems.vendor_id =?", vendor_id).joins("LEFT JOIN seller_skus ON vendoritems.seller_sku_id = seller_skus.id")
		total = inventories.load.size



		sort_columnIndex = params[:iSortCol_0].to_i;
		sort_columnName = params["mDataProp_#{sort_columnIndex}"]

		sort_order = params[:sSortDir_0]



		iColumns = params[:iColumns].to_i

		i=0
		true_search = ""
		search = params["sSearch"]
		unless params["sSearch"].nil? || params["sSearch"] == ''
		  # inventories = inventories.joins(:vendorasin).where(:vendorasins => {:upc like "%#{search}%"}).or(inventories.where(:vendorasins => {:asin like "%#{search}%"}))
		  inventories = inventories.where("vendorasins.asin like :l_search or vendoritems.upc like :l_search or vendorasins.name like :l_search", {:l_search =>"%#{search}%"})
		end

		while i < iColumns do
		  search = params["sSearch_#{i}"]
		  unless search.nil? || search == ''
			true_search = search
			search_column = params["mDataProp_#{i}"]

			unless notsearch_columns.include? search_column
			  if vendoritem_columns.include? search_column
				inventories = inventories.where("vendoritems.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
			  elsif express_columns.include? search_column
					search.split("##").each do |x|
						search_value = x.split("$$");
						exp = search_value[0].to_s
						val = search_value[1].to_f
						if exp == 'eq'
							exp = '='
						elsif exp == 'lg'
							exp = '>'
						elsif exp == 'el'
							exp = '>='
						elsif exp == 'ls'
							exp = '<'
						elsif exp == 'es'
							exp = '<='
						else
							exp = '='
						end
						inventories = inventories.number_compare(exp,val, search_column)
					end
				elsif	sku_columns.include? search_column
					inventories = inventories.where("seller_skus.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
				elsif po_columns.include? search_column
					inventories = inventories.where("pos.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
				else
					inventories = inventories.where("vendorasins.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
			  end
			end

		  end
		  i = i + 1
		end


		if sort_columnName == 'margin'
		  inventories = inventories.order("(vendorasins.buyboxprice-cost-vendorasins.totalfbafee) / vendoritems.cost #{sort_order}")
		elsif sort_columnName == 'profit'
		  inventories = inventories.order("vendorasins.buyboxprice-cost-vendorasins.totalfbafee #{sort_order}")
		elsif sort_columnName == 'packcost'
		  inventories = inventories.order("vendoritems.cost*vendoritems.packcount #{sort_order}")
		else
		  if vendoritem_columns.include? sort_columnName
				inventories = inventories.order("vendoritems.#{sort_columnName} #{sort_order}")
			elsif sku_columns.include? sort_columnName
				inventories = inventories.order("seller_skus.#{sort_columnName} #{sort_order}")
			elsif po_columns.include? sort_columnName
				inventories = inventories.order("pos.#{sort_columnName} #{sort_order}")
		  else
				inventories = inventories.order("vendorasins.#{sort_columnName} #{sort_order}")
		  end
		end

		filtered = inventories.load.size
		inventories =inventories.limit(limit).offset(start).includes(vendoritem: [:seller_sku,:vendor, :vendorasin])
		render json: {'aaData'=>inventories,"iTotalRecords": total,"iTotalDisplayRecords": filtered}
	end

	private

	def get_col_data_row(row, col_index, cost_col=false)
		value = nil
		begin
			unless col_index.nil? || col_index == -1
				value =  row[col_index].force_encoding("ISO-8859-1").encode("UTF-8")
			end

			unless value.nil? || cost_col == false
				value = value.to_s.gsub(/[$,]/,'').to_f
			end
		rescue Exception => exception

		end
		value
	end

	def get_excel_col_data_row(row, col_index, cost_col=false)
		value = nil
		begin
			unless col_index.nil? || col_index == -1
				value =  row[col_index].cell_value.force_encoding("ISO-8859-1").encode("UTF-8")
			end

			unless value.nil? || cost_col == false
				value = value.to_s.gsub(/[$,]/,'').to_f
			end
		rescue Exception => exception
		end

		value
	end


	def send_vendoritem_to_po(ids, sku_data={}, buy_qty_data={})
		set_defined_sku(ids, sku_data)
		vendoritems = Vendoritem.where(id: ids)
		vendor_id = 0;
		if vendoritems.length > 0
			vendor_id = vendoritems[0].vendor_id
		else
			return {'result'=>false, 'msg'=>'There is no valid items.'}
		end
		new_ids = vendoritems.pluck(:id)
		existed_ids = Po.where(vendoritem_id: ids).pluck(:vendoritem_id)
		# new_ids = new_ids - existed_ids
		if new_ids.length ==  0
			return {'result'=>false, 'msg'=>'Selected items are already existed on PO'}

		else
			new_ids.each do |id|
				# po = Po.create
				po = Po.where(:vendoritem_id => id).first_or_create
				# po.vendoritem_id = id
				unless buy_qty_data[id].blank?
					po.order_quantity = buy_qty_data[id].to_i
				end
				po.save
			end
		end
		return {'result'=>true, 'vendor_id'=>vendor_id,'msg'=>'Selected items are added to  PO'}
	end
end
