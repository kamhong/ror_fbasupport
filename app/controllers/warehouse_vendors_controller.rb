class WarehouseVendorsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_warehouse_vendor, only: [:show, :edit, :update, :destroy]

  # GET /warehouse_vendors
  # GET /warehouse_vendors.json
  def index
    @warehouse_vendors = WarehouseVendor.all
    @user_option = User.where(:role => 3).all
  end

  # GET /warehouse_vendors/1
  # GET /warehouse_vendors/1.json
  def show
  end

  # GET /warehouse_vendors/new
  def new
    @warehouse_vendor = WarehouseVendor.new
  end

  # GET /warehouse_vendors/1/edit
  def edit
  end

  # POST /warehouse_vendors
  # POST /warehouse_vendors.json
  def create
    @warehouse_vendor = WarehouseVendor.new(warehouse_vendor_params)

    respond_to do |format|
      if @warehouse_vendor.save
        format.html { redirect_to @warehouse_vendor, notice: 'Warehouse vendor was successfully created.' }
        format.json { render :show, status: :created, location: @warehouse_vendor }
      else
        format.html { render :new }
        format.json { render json: @warehouse_vendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /warehouse_vendors/1
  # PATCH/PUT /warehouse_vendors/1.json
  def update
    respond_to do |format|
      if @warehouse_vendor.update(warehouse_vendor_params)
        format.html { redirect_to @warehouse_vendor, notice: 'Warehouse vendor was successfully updated.' }
        format.json { render :show, status: :ok, location: @warehouse_vendor }
      else
        format.html { render :edit }
        format.json { render json: @warehouse_vendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /warehouse_vendors/1
  # DELETE /warehouse_vendors/1.json
  def destroy
    @warehouse_vendor.destroy
    respond_to do |format|
      format.html { redirect_to warehouse_vendors_url, notice: 'Warehouse vendor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def load_warehouse_vendor
    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i

    sort_columnIndex = params[:iSortCol_0].to_i;
    sort_columnName = params["mDataProp_#{sort_columnIndex}"]
    sort_columnName = sort_columnName.split(".")[-1]
    sort_order = params[:sSortDir_0]
    iColumns = params[:iColumns].to_i

    warehouse_vendors = WarehouseVendor.all
    total = warehouse_vendors.load.size

    (0...iColumns).step(1).each do |i|
      ori_search_column = params["mDataProp_#{i}"]
      search_column = ori_search_column.split(".")[-1]
      search = params["sSearch_#{i}"]
      unless search.nil? || search == ''
        if search_column == "buyer_name"
          warehouse_vendors = warehouse_vendors.joins(:user).where("concat_ws(' ',users.first_name, users.last_name) = ?", search)
        else
          warehouse_vendors = warehouse_vendors.where("#{search_column} = ?", search)
        end
      end
    end
    filtered = warehouse_vendors.load.size
    warehouse_vendors = warehouse_vendors.limit(limit).offset(start).includes(:user)
    render json: {'aaData'=>warehouse_vendors.limit(limit).offset(start),"iTotalRecords": total,"iTotalDisplayRecords": filtered}
  end

  def update_fields
    warehouse_vendor = nil
    data = params[:data]
    data.each do |key, item|
      warehouse_vendor = WarehouseVendor.find(key)
      item.each do |title, value|
        if title == "buyer_name"
          warehouse_vendor.user_id = value
				else
					warehouse_vendor[title.to_sym] = value 
        end
			end
      warehouse_vendor.save
    end
    render json: {'data'=>warehouse_vendor}
  end

  def list
    type = params[:type]
    users = User.where(:role_id => type).all
    render json: {'user'=>users,'payment'=>WarehouseVendor.payment_term_list}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_warehouse_vendor
      @warehouse_vendor = WarehouseVendor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def warehouse_vendor_params
      params.fetch(:warehouse_vendor, {}).permit(:name, :supplier_code, :user_id)
      # params.require(:vendor).permit(:name, :supplier_code)
    end
end
