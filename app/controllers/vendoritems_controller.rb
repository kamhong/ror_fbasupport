include ActionView::Helpers::NumberHelper

class VendoritemsController < ApplicationController
  include SellerSkusHelper
  before_action :authenticate_user!
  before_action :set_vendoritem, only: [:show, :edit, :update, :destroy]

  
  def index
    # @clean_vendor = params[:delete_vendor]
    # if (@clean_vendor)
    #   vendoritems = current_user.vendoritems.where(:vendor_id => @clean_vendor)
    #   if vendoritems.empty? || vendoritems.blank?
    #     flash[:error] = "There is no items to delete."
    #     redirect_to vendoritems_path
    #   else
    #     if vendoritems.count < 100
    #       vendoritems.destroy_all
    #     else
    #       ProcessDeletevendoritemWorker.perform_async(@clean_vendor);
    #     end
    #     flash[:success] = "Deleted all items successfully."
    #     redirect_to vendors_path(:user => current_user.id)
    #   end
    # end
    
   
  end


  def getvendoritem
    id = params[:id]
    vendoritem = Vendoritem.find(id)
    render json: vendoritem.to_json(include: :vendorasin)
  end

  def updatevendoritem
    @vendoritem = nil
    data = params[:data]
    data.each do |key, item|
      @vendoritem = Vendoritem.find(key)

      item.each do |title, value|
        if title=='buyboxprice' || title == 'salespermonth' || title == 'salesrank'
          @vendoritem.vendorasin[title.to_sym] = value
        elsif title =='fbafee'
          @vendoritem.vendorasin[title.to_sym] = value
          # @vendoritem.vendorasin.updatetotalfee
          @vendoritem.vendorasin['totalfbafee'] = @vendoritem.vendorasin['fbafee'] + @vendoritem.vendorasin['commissiionfee'] + @vendoritem.vendorasin['storagefee'] + @vendoritem.vendorasin['variableclosingfee']
        else
          @vendoritem[title.to_sym] = value  
        end
        
      end
      @vendoritem.save
    end
    
    render json: {'data'=>@vendoritem}
  end

  def deletevendoritem
    data = params[:data]
    data.each do |key, item|
      vendoritem = Vendoritem.find(key)

      if vendoritem.present?
        vendoritem.destroy
      end
    end
  end

  def addvendoritem
    vendor = Vendor.find(3)
    data = params[:data]
    data.each do |key, item|
      asin_text = item[:asin]
      cost = item[:cost]
      vendorsku = item[:vendorsku]
       # binding.pry
      vendoritem = vendor.vendoritems.where(:asin => asin_text).first_or_create
      vendoritem.vendorsku = vendorsku
      if !asin_text.empty?
        asin = Vendorasin.where(:asin => asin_text).first_or_create
      end
      
    end
  end

  def getprofitable
    vendor_id = params[:vendor]
    @vendoritems = Vendoritem.where(vendor_id: vendor_id).profitable
    count = @vendoritems.count
    avg_profit = 0
    avg_margin = 0;
    if count > 0
      avg_profit = @vendoritems.includes(:vendorasin, :seller_sku).sum(&:profit) / count
      avg_margin = @vendoritems.includes(:vendorasin, :seller_sku).sum(&:margin) / count
    end
    avg_profit = number_to_currency(avg_profit)
    avg_margin = number_to_percentage(avg_margin)
    render json: {'count'=>count, 'avg_profit'=>avg_profit, 'avg_margin'=>avg_margin}
  end

  def check_sku
    ids = params[:ids]
    result = create_skus_from_vendor(ids)
    sku_list = Vendoritem.where(id: ids).joins(:seller_sku).distinct.pluck("seller_skus.id")
    if sku_list.count < ids.count
      result[:status] = false
    end
    update_prepinstructions(sku_list)
    sku_list.each do |sku|
      update_shipplaninformation(sku)
    end
    render json: result
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vendoritem
      @vendoritem = Vendoritem.find(params[:id])
    end
end
