class MwsReportRequestsController < ApplicationController
  before_action :set_mws_report_request, only: [:show, :edit, :update, :destroy]

  # GET /mws_report_requests
  # GET /mws_report_requests.json
  def index
    @mws_report_requests = MwsReportRequest.all
  end

  # GET /mws_report_requests/1
  # GET /mws_report_requests/1.json
  def show
  end

  # GET /mws_report_requests/new
  def new
    @mws_report_request = MwsReportRequest.new
  end

  # GET /mws_report_requests/1/edit
  def edit
  end

  # POST /mws_report_requests
  # POST /mws_report_requests.json
  def create
    @mws_report_request = MwsReportRequest.new(mws_report_request_params)

    respond_to do |format|
      if @mws_report_request.save
        format.html { redirect_to @mws_report_request, notice: 'Mws report request was successfully created.' }
        format.json { render :show, status: :created, location: @mws_report_request }
      else
        format.html { render :new }
        format.json { render json: @mws_report_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mws_report_requests/1
  # PATCH/PUT /mws_report_requests/1.json
  def update
    respond_to do |format|
      if @mws_report_request.update(mws_report_request_params)
        format.html { redirect_to @mws_report_request, notice: 'Mws report request was successfully updated.' }
        format.json { render :show, status: :ok, location: @mws_report_request }
      else
        format.html { render :edit }
        format.json { render json: @mws_report_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mws_report_requests/1
  # DELETE /mws_report_requests/1.json
  def destroy
    @mws_report_request.destroy
    respond_to do |format|
      format.html { redirect_to mws_report_requests_url, notice: 'Mws report request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mws_report_request
      @mws_report_request = MwsReportRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mws_report_request_params
      params.fetch(:mws_report_request, {})
    end
end
