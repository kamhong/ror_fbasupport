class InboundShipplansController < ApplicationController
  before_action :authenticate_user!
  def loadshipplan
    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i
    search = params[:sSearch]

    stage_id = params[:stage_id]
    out_stage_item = OutboundShipment.find(stage_id)
    user = out_stage_item.try(:pending_po).try(:vendoritem).try(:vendor).try(:user)
    if user.nil?
      render json: {'aaData'=>[],"iTotalRecords": 0,"iTotalDisplayRecords": 0}
      return
    end

    # if current_user.is_warehouse
    #     @ship_plans = InboundShipplan.all
    # else
    #     @ship_plans = current_user.inbound_shipplans
    # end
    @ship_plans = user.inbound_shipplans
    total = @ship_plans.count
    @ship_plans = @ship_plans.search_on(search)
    filtered = @ship_plans.count
    @ship_plans = @ship_plans.limit(limit).offset(start)
    filtered_data = []
    @ship_plans.each do |shipplan|
      data = {
        shipplan_id: shipplan.shipplan_id,
        destination_fulfillment_center_id: shipplan.destination_fulfillment_center_id,
        sku_count: shipplan.sku_count,
        ship_qty: shipplan.ship_qty,
        user_name: user.full_name
      }

      filtered_data << data
    end

    render json: {'aaData'=>filtered_data,"iTotalRecords": total,"iTotalDisplayRecords": filtered}
  end
end
