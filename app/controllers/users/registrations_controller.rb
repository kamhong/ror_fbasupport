class Users::RegistrationsController < Devise::RegistrationsController
  # layout "users", only: [:new, :create]
  layout :resolve_layout
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    # self.resource = resource_class.new(sign_up_params)
    # store_location_for(resource, params[:redirect_to])
    super
  end

  # POST /resource
  def create
    super
    
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    
   
    # super
    
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        # set_flash_message :notice, flash_key
      end
      bypass_sign_in resource, scope: resource_name
      # respond_with resource, location: after_update_path_for(resource)
    else
      clean_up_passwords resource
      set_minimum_password_length
      # respond_with redirect_to: new_skynet_path,
                  #  notice: resource.errors['current_password'][0]
    end
    redirect_to edit_user_registration_path
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  # def after_sign_up_path_for(resource)
  #   super
  #   # sign_in(resource)
  #   signed_in_root_path(resource)
  # end

  def after_update_path_for(resource)
    # flash[:notice] = "Account succesfully updated"
    edit_user_registration_path
      
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:eamil, :password, :password_confirmation])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:email, :password, :mws_access_key_id])
  # end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    
    # super(resource)
    company_name = params[:user][:company][:name]

    company = Company.where(:name => company_name).first_or_create
    
    role_id = 2
    if company.users.count == 0
      role_id = 2
    else
      role_id = 2
    end
    resource.role_id = role_id
    resource.company_id = company.id

    if resource.save
      # UserMailer.send_welcome_email(resource.email).deliver_now
      if resource.role_id
        case current_user.role.name
          when 'Buyer'
            new_skynet_path
          when 'Manager'
            new_skynet_path
          when 'Admin'
            dashboard_dashboard_admin_path
        end
      end
    else
      "/"
    end
    
  end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    super(resource)
  end

  protected

 
  def update_resource(resource, params)
    if params[:password].blank? && params[:password_confirmation].blank?
      params.delete(:password)
      params.delete(:password_confirmation)

      resource.update_without_password(params)
    else
      resource.update_with_password(params)
    end 

    if resource.key_changed(params)
      CheckUserMwsValidWorker.perform_async(resource.id)
    end    
    
  end

 

  private
    def sign_up_params
      # params.require(:user).permit(:email, :password, :password_confirmation, :company_id, :role_id)
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, company_attributes: [:name])
    end

    
    

  def resolve_layout
    case action_name
    when "new", "create"
      "tempusers"
    when "edit"
      "application"
    else
      "application"
    end
  end
end
