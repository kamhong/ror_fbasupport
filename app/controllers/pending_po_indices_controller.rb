class PendingPoIndicesController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_manager, only: [:updateitem]
  before_action :set_pending_po_index, only: [:show, :edit, :update, :destroy]

  # GET /pending_po_indices
  # GET /pending_po_indices.json
  def index
    @pending_po_index_status = PendingPoIndex.status_list
    if current_user.is_warehouse
      @user_option = User.where(id: PendingPoIndex.joins(vendor: :user).pluck("users.id"))
      @po_option = PendingPoIndex.all
      @vendor_option = Vendor.where(id: PendingPoIndex.all.pluck("vendor_id"))
    else
      @user_option = [current_user]
      @po_option = current_user.pending_po_indices
		  @vendor_option = current_user.vendors.where(id: PendingPoIndex.all.pluck("vendor_id"))
    end  
    
  end

  # GET /pending_po_indices/1
  # GET /pending_po_indices/1.json
  def show
    @status_list = PendingPo.status_list
  end

  # GET /pending_po_indices/new
  def new
    @pending_po_index = PendingPoIndex.new
  end

  # GET /pending_po_indices/1/edit
  def edit
  end

  # POST /pending_po_indices
  # POST /pending_po_indices.json
  def create
    @pending_po_index = PendingPoIndex.new(pending_po_index_params)

    respond_to do |format|
      if @pending_po_index.save
        format.html { redirect_to @pending_po_index, notice: 'Pending po index was successfully created.' }
        format.json { render :show, status: :created, location: @pending_po_index }
      else
        format.html { render :new }
        format.json { render json: @pending_po_index.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pending_po_indices/1
  # PATCH/PUT /pending_po_indices/1.json
  def update
    respond_to do |format|
      if @pending_po_index.update(pending_po_index_params)
        format.html { redirect_to @pending_po_index, notice: 'Pending po index was successfully updated.' }
        format.json { render :show, status: :ok, location: @pending_po_index }
      else
        format.html { render :edit }
        format.json { render json: @pending_po_index.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pending_po_indices/1
  # DELETE /pending_po_indices/1.json
  def destroy
    @pending_po_index.destroy
    respond_to do |format|
      format.html { redirect_to pending_po_indices_url, notice: 'Pending po index was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def loadsummary
    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i
    filter_status = params[:filter_status]

    if current_user.is_warehouse
      pending_po_indices = PendingPoIndex.all
    else
      pending_po_indices = current_user.pending_po_indices.all
    end
    
    pending_po_indices = pending_po_indices.joins(:vendor)
    # pending_filter = false
    pending_po_indices = pending_po_indices.per_status(filter_status)
    total = pending_po_indices.load.size
    iColumns = params[:iColumns].to_i
    (0...iColumns).step(1).each do |i|
      ori_search_column = params["mDataProp_#{i}"]
      search_column = ori_search_column.split(".")[-1]
      search = params["sSearch_#{i}"]
      unless search.nil? || search == ''
        if search_column == "po_name"
          pending_po_indices = pending_po_indices.where(:po_name => search)
        elsif ori_search_column == "vendor.warehouse_vendor.name"
          pending_po_indices = pending_po_indices.joins(vendor: :warehouse_vendor).where("warehouse_vendors.name = ?", search)
        elsif search_column == "full_name"
          pending_po_indices = pending_po_indices.joins(vendor: :user).where("concat_ws(' ',users.first_name, users.last_name) = ?", search)
        elsif search_column == "pending_status"
          # pending_filter = true
          # pending_po_indices = pending_po_indices.where("pending_po_indices.pending_status = ?", PendingPoIndex.pending_statuses[search.to_sym])
        elsif search_column == "name" || search_column == "supplier_prefix"
          pending_po_indices = pending_po_indices.joins(:vendor).where("vendors.#{search_column} = ?", search)
        end
      end
    end
    # unless pending_filter
    #   pending_po_indices = pending_po_indices.per_status(filter_status)
    # end
    
    filtered = pending_po_indices.load.size
    pending_po_indices = pending_po_indices.limit(limit).offset(start).joins({pending_pos: :vendoritem}, {vendor: :user}).includes({pending_pos: :vendoritem}, {vendor: [:warehouse_vendor, :user]})
    render json: {
      # 'aaData'=>pending_po_indices.includes(:pending_pos, vendor: :warehouse_vendor),
      'aaData'=>pending_po_indices.as_json(
        methods: [:order_qty, :order_e_cost, :confirmed_qty, :confiremd_e_cost, :received_quantity, :total_shipped_pack, :sku_count, :missing_quantity],
        include: [{pending_pos: {include: :vendoritem}}, {vendor: {include: [:warehouse_vendor, user: {methods: :full_name}]}}]
      ),
      "iTotalRecords": total,
      "iTotalDisplayRecords": filtered}
  end

  def list
    render json: {'aaData'=>current_user.pending_po_indices} 
  end

  def list_options
    render json: {'status_list'=>PendingPoIndex.status_list}
  end

  def updateitem
    pending_po_index = nil
    data = params[:data]
    data.each do |key, item|
      pending_po_index = PendingPoIndex.find(key)
      item.each do |title, value|
        pending_po_index[title.to_sym] = value
      end
      pending_po_index.save
    end
    render json: {'data'=>pending_po_index}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pending_po_index
      begin
        @pending_po_index = PendingPoIndex.find(params[:id])
        unless @pending_po_index.vendor.user == current_user || current_user.is_warehouse
          redirect_to po_list_path
        end
      rescue ActiveRecord::RecordNotFound => e
        redirect_to po_list_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pending_po_index_params
      params.fetch(:pending_po_index, {})
    end
end
