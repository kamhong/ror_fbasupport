class CustomerInvoicesController < ApplicationController
  layout 'pdf'
  include CustomerInvoicesHelper
  before_action :set_customer_invoice, only: [:show, :edit, :update, :destroy]

  # GET /customer_invoices
  # GET /customer_invoices.json
  def index
    @customer_invoices = CustomerInvoice.all
  end

  # GET /customer_invoices/1
  # GET /customer_invoices/1.json
  def show
    @logo = "#{request.protocol}#{request.host_with_port}/images/dd_logo.png"
    @pending_pos = @customer_invoice.pending_pos
    respond_to do |format|
      format.html do
        
        render "customer_invoices/_report_pdf.html.erb"
      end
      format.pdf do
        render pdf: "Invoice_#{@customer_invoice.invoice_name}",
        disposition: 'attachment',
        template: "customer_invoices/_report_pdf.html.erb",
        layout: 'pdf.html.erb',
        dpi: 300,
        margin:  {
                    top: 15,
                    bottom: 10
                },
        footer:
        {
          html: {
            template:'customer_invoices/pdf_footer',
            layout:  'pdf_plain',
          },
        }
      end
    end
  end

  # GET /customer_invoices/new
  def new
    @customer_invoice = CustomerInvoice.new
  end

  # GET /customer_invoices/1/edit
  def edit
  end

  # POST /customer_invoices
  # POST /customer_invoices.json
  def create
    @customer_invoice = CustomerInvoice.new(customer_invoice_params)

    respond_to do |format|
      if @customer_invoice.save
        format.html { redirect_to @customer_invoice, notice: 'Customer invoice was successfully created.' }
        format.json { render :show, status: :created, location: @customer_invoice }
      else
        format.html { render :new }
        format.json { render json: @customer_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customer_invoices/1
  # PATCH/PUT /customer_invoices/1.json
  def update
    respond_to do |format|
      if @customer_invoice.update(customer_invoice_params)
        format.html { redirect_to @customer_invoice, notice: 'Customer invoice was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer_invoice }
      else
        format.html { render :edit }
        format.json { render json: @customer_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_invoices/1
  # DELETE /customer_invoices/1.json
  def destroy
    @customer_invoice.destroy
    @customer_invoice.pending_pos.each do |po|
      po.customer_invoice_id = nil
      po.save
    end
    respond_to do |format|
      format.html { redirect_to customer_invoices_url, notice: 'Customer invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def generate_invoice
    ids = params[:ids]
    check_result = valid_customer_invoice_items(ids)
    unless check_result[:status]
      render json: check_result
      return
    else
      pending_po = PendingPo.find(ids[0])
      vendor = pending_po&.vendoritem&.vendor
      unless vendor.blank?
        c_invoice = vendor.customer_invoices.create({user_id: vendor&.user_id, pending_po_index_id: pending_po.pending_po_index_id})
        unless c_invoice.blank?
          ids.each do |id|
            pending_po = PendingPo.find(id)
            unless pending_po.blank?
              pending_po.customer_invoice_id = c_invoice.id
              pending_po.save
            end
          end
        end
        render json: {:status => true, :id => c_invoice.id}
        return
      end
    end
    render json: {:status => false}
  end

  def print
    stage = OutboundShipment.find(params[:id])
    sku = stage.pending_po.vendoritem.seller_sku
    barcode = Barby::Code128B.new(sku.fnsku || '')

    dir = Rails.root.join('public','images','barcode')
    File.open(dir.join("#{sku.fnsku}.png"), 'wb'){|f| f.write barcode.to_png }

    pdf = WickedPdf.new.pdf_from_string(
            render_to_string(:template => 'outbound_shipments/bagger_pdf.html.erb', :locals => {:stage=>stage,:barcode_url=>"#{request.protocol}#{request.host_with_port}/images/barcode/#{sku.fnsku}.png"}, :layout => 'layouts/pdf.html.erb'),
            page_size: 'Letter',
            :page_height => 30,
            :page_width => 60,
            :margin => {
              top: 1,
                bottom:0,
                left:0,
                right:0
              },
          )
    send_data pdf, type: 'application/pdf', filename: "Invoice.pdf"
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_invoice
      @customer_invoice = CustomerInvoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_invoice_params
      params.fetch(:customer_invoice, {})
    end
end
