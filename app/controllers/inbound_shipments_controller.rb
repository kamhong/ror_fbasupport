require 'peddler'

class InboundShipmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_inbound_shipment, only: [:show, :edit, :update, :destroy, :send_invoice, :add_invoice]

  # GET /inbound_shipments
  # GET /inbound_shipments.json
  def index
    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i
    search = params[:sSearch]
    if current_user.is_warehouse
      @inbound_shipments = InboundShipment.all
    else
      @inbound_shipments = current_user.inbound_shipments.all
    end
    total = @inbound_shipments.count
    @inbound_shipments = @inbound_shipments.search_on(search)
    filtered = @inbound_shipments.count

    respond_to do |format|
      format.html #{ redirect_to @inbound_shipment, notice: 'Inbound shipment was successfully created.' }
      format.json { render json: {'aaData'=>@inbound_shipments,"iTotalRecords": total,"iTotalDisplayRecords": filtered}, status: 200 }
    end

  end

  def getshipmentofstage
    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i
    search = params[:sSearch]
    stage_id = params[:stage_id]
    out_stage_item = OutboundShipment.find(stage_id)
    user = out_stage_item.try(:pending_po).try(:vendoritem).try(:vendor).try(:user)
    if user.nil?
      render json: {'aaData'=>[],"iTotalRecords": 0,"iTotalDisplayRecords": 0}
      return
    end
    selected_shipment = ""
    selected_status = ""
    unless out_stage_item.nil?
      selected_shipment = out_stage_item.shipment_id
      selected_status = out_stage_item.stage_status
    end
    # if current_user.is_warehouse
    #   inbound_shipments = InboundShipment.all
    # else
    #   inbound_shipments = current_user.inbound_shipments.all
    # end
    inbound_shipments = user.inbound_shipments.where("shipment_status = 'WORKING'")
    total = inbound_shipments.count
    inbound_shipments = inbound_shipments.search_on(search)
    filtered = inbound_shipments.count
    inbound_shipments = inbound_shipments.limit(limit).offset(start)
    filtered_data = []
    inbound_shipments.each do |shipment|
      data = {
        id: shipment.id,
        shipment_id: shipment.shipment_id,
        destination_fulfillment_center_id: shipment.destination_fulfillment_center_id,
        sku_count: shipment.sku_count,
        ship_qty: shipment.ship_qty,
        user_name: user.full_name
      }
      data[:operation] = 'add'
      if selected_status == "SHIPMENT"
        if shipment.shipment_id == selected_shipment
          data[:operation] = 'remove'
        else
          data[:operation] = 'none'
        end
      end
      filtered_data << data
    end

    render json: {'aaData'=>filtered_data,"iTotalRecords": total,"iTotalDisplayRecords": filtered}
  end

  # GET /inbound_shipments/1
  # GET /inbound_shipments/1.json
  def show
    respond_to do |format|
      format.html do
        begin
          @inbound_shipment.build_ship_cus_invoice if @inbound_shipment.ship_cus_invoice.nil?
          # response = TransportContentService.new(nil, @inbound_shipment.shipment_id).sync_transport_with_amazon @inbound_shipment.user
        rescue => e
        end
        @transport_plan = @inbound_shipment.latest_transport_plan
        @transport_content = @inbound_shipment.transport_contents.new
      end
      format.pdf do
        @logo = "#{request.protocol}#{request.host_with_port}/images/dd_logo.png"
        @shipment = @inbound_shipment
        if @shipment.invoice_name.blank?
          @shipment.invoice_name = "#{@shipment.id}#{('A'..'Z').to_a.shuffle[0,6-@shipment.id.to_s.length].join}"
          @shipment.save
        end
        render pdf: "Invoice_#{@inbound_shipment.shipment_id}",
            disposition: 'attachment',
            template: "transport_plans/_invoice_pdf.html.erb",
            layout: 'pdf.html.erb',
            dpi: 300,
            margin:  {
                        top: 15,
                        bottom: 10
                    },
            footer:
            {
              html: {
                template:'transport_plans/invoice_pdf_footer',
                layout:  'pdf_plain',
              },
            }
      end
    end

  end

  # GET /inbound_shipments/new
  def new
    @inbound_shipment = InboundShipment.new
  end

  # GET /inbound_shipments/1/edit
  def edit
  end

  # POST /inbound_shipments
  # POST /inbound_shipments.json
  def create
    @inbound_shipment = InboundShipment.new(inbound_shipment_params)

    respond_to do |format|
      if @inbound_shipment.save
        format.html { redirect_to @inbound_shipment, notice: 'Inbound shipment was successfully created.' }
        format.json { render :show, status: :created, location: @inbound_shipment }
      else
        format.html { render :new }
        format.json { render json: @inbound_shipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inbound_shipments/1
  # PATCH/PUT /inbound_shipments/1.json
  def update
    respond_to do |format|
      if @inbound_shipment.update(inbound_shipment_params)
        format.html { redirect_to @inbound_shipment, notice: 'Inbound shipment was successfully updated.' }
        format.json { render :show, status: :ok, location: @inbound_shipment }
      else
        format.html { render :edit }
        format.json { render json: @inbound_shipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inbound_shipments/1
  # DELETE /inbound_shipments/1.json
  def destroy
    @inbound_shipment.destroy
    respond_to do |format|
      format.html { redirect_to inbound_shipments_url, notice: 'Inbound shipment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end



  def create_from_plan
    plan_id = params[:shipplan_id]
    qty = params[:qty].to_i
    stage_id = params[:stage_id]

    ship_plan = InboundShipplan.find(plan_id)


    if ship_plan.nil?
        render json: {'status'=>false, 'msg' => 'Shipplan is not valid.'}
    end

    ship_plan_item = ship_plan.inbound_shipplan_items.where(:outbound_shipment_id => stage_id).first
    shipment_id = ship_plan.shipplan_id

    stage_item = OutboundShipment.find(stage_id)
    if stage_item.nil? || ship_plan_item.nil?
      render json: {'status'=>false, 'msg' => 'Stage item is not valid.'}
    end

    if qty < 1
      render json: {'status'=>false, 'msg' => 'Please input positive quantity.'}
    end
    result = createshipment(plan_id)
    if result[:status]
      if stage_item.box_qty == qty && ship_plan_item.box_qty == qty
        render json: {'status'=>true, 'msg'=>'Shippment successfully created.'}
        return
      end
      sku = stage_item.pending_po.vendoritem.seller_sku.sku
      shipped_qty = (qty * stage_item.case_qty / stage_item.pending_po.vendoritem.packcount).to_i
      shipment = InboundShipment.where(:shipment_id => shipment_id).first
      result = updateshipment(shipment.id, stage_id,0)
      if result[:status]
        result = updateshipment(shipment.id, stage_id,shipped_qty)
        if result[:status]
          inbound_shipment_item = shipment.inbound_shipment_items.where(:seller_sku => sku).first_or_create
          inbound_shipment_item.quantity_shipped = shipped_qty
          inbound_shipment_item.pending_po_id = stage_item.pending_po_id
          inbound_shipment_item.save

          stage_item.update_pending_po_receive(qty - stage_item.box_qty)
          stage_item.box_qty = qty
          stage_item.save
          render json: {'status'=>true, 'msg'=>'Shippment successfully created.'}
          return
        else
          inbound_shipment_item = shipment.inbound_shipment_items.where(:seller_sku => sku).first
          inbound_shipment_item.destroy unless inbound_shipment_item.nil?

          OutboundShipment.joins(pending_po: :vendoritem).joins("INNER JOIN seller_skus on seller_skus.id = vendoritems.seller_sku_id").where(:shipment_id => inbound_shipment.shipment_id).where("seller_skus.sku = ?", sku).update_all(:shipment_id => "", :stage_status => "RECEIVED")

          render json: {'status'=>false, 'msg'=>'Failed to update the quantity of shipment item.'}
          return
        end
      end
    end
    render json: {'status'=>false, 'msg'=>result[:msg]}
    return
  end

  def additemfromstage
    stage_id = params[:stage_id]
    shipment_id = params[:shipment_id]
    out_stage = OutboundShipment.find(stage_id)
    if out_stage.nil?
      render json: {'status'=>false, 'msg'=>'Stage Item is not valid.'}
      return
    end
    inbound_shipment = InboundShipment.where(:shipment_id => shipment_id).first
    if inbound_shipment.nil?
      render json: {'status'=>false, 'msg'=>'Shipment is not valid'}
      return
    end

    sku = out_stage.pending_po.vendoritem.seller_sku.sku
    same_product = inbound_shipment.inbound_shipment_items.where(:seller_sku => sku).first
    unless same_product.blank?
      new_qty = same_product.quantity_shipped +  out_stage.received_quantity_pack
      result = updateshipment(inbound_shipment.id, stage_id,0)
    else
      result = {:status => true}
      new_qty = out_stage.received_quantity_pack
    end
    if result[:status]
      result = updateshipment(inbound_shipment.id, stage_id,new_qty)
      if result[:status]
        inbound_shipment_item = inbound_shipment.inbound_shipment_items.where(:seller_sku => sku).first_or_create
        inbound_shipment_item.quantity_shipped = new_qty
        inbound_shipment_item.pending_po_id = out_stage.pending_po_id
        inbound_shipment_item.save

        out_stage.shipment_id = inbound_shipment.shipment_id
        out_stage.stage_status = "SHIPMENT"
        out_stage.save
        render json: {'status'=>true, 'msg'=>'Operations successfully done.'}
        return
      else
        inbound_shipment_item = inbound_shipment.inbound_shipment_items.where(:seller_sku => sku).first
        inbound_shipment_item.destroy unless inbound_shipment_item.nil?

        OutboundShipment.joins(pending_po: :vendoritem).joins("INNER JOIN seller_skus on seller_skus.id = vendoritems.seller_sku_id").where(:shipment_id => inbound_shipment.shipment_id).where("seller_skus.sku = ?", sku).update_all(:shipment_id => "", :stage_status => "RECEIVED")

        render json: {'status'=>false, 'msg'=>'Failed to add the quantity of shipment item.'}
        return
      end
    end

    render json: {'status'=>false, 'msg'=>result[:msg]}
  end

  def removeitem
    stage_id = params[:stage_id]
    shipment_id = params[:shipment_id]

    out_stage = OutboundShipment.find(stage_id)
    if out_stage.nil?
      render json: {'status'=>false, 'msg'=>'Stage Item is not valid.'}
      return
    end
    inbound_shipment = InboundShipment.where(:shipment_id => shipment_id).first
    if inbound_shipment.nil?
      render json: {'status'=>false, 'msg'=>'Shipment is not valid'}
      return
    end


    if inbound_shipment.shipment_id != out_stage.shipment_id && qty == 0
      render json: {'status'=>false, 'msg'=>'Shipment is not valid'}
      return
    end

    sku = out_stage.pending_po.vendoritem.seller_sku.sku
    existed_shipped = inbound_shipment.inbound_shipment_items.where(:seller_sku => sku).sum(:quantity_shipped)
    shipped_qty = existed_shipped - out_stage.received_quantity_pack
    shipped_qty = 0 if shipped_qty < 0

    result = updateshipment(inbound_shipment.id, stage_id,0)
    if result[:status]
      unless shipped_qty == 0
        result = updateshipment(inbound_shipment.id, stage_id,shipped_qty)
        if result[:status]
          inbound_shipment_item = inbound_shipment.inbound_shipment_items.where(:seller_sku => sku).first_or_create
          inbound_shipment_item.quantity_shipped = shipped_qty
          inbound_shipment_item.save

          out_stage.shipment_id = ""
          out_stage.stage_status = "RECEIVED"
          out_stage.save
          render json: {'status'=>true, 'msg'=>'Operations successfully done.'}
          return
        else
          inbound_shipment_item = inbound_shipment.inbound_shipment_items.where(:seller_sku => sku).first
          inbound_shipment_item.destroy unless inbound_shipment_item.nil?

          OutboundShipment.joins(pending_po: :vendoritem).joins("INNER JOIN seller_skus on seller_skus.id = vendoritems.seller_sku_id").where(:shipment_id => inbound_shipment.shipment_id).where("seller_skus.sku = ?", sku).update_all(:shipment_id => "", :stage_status => "RECEIVED")
          render json: {'status'=>false, 'msg'=>'Failed to Update the quantity of shipment item'}
          return
        end
      else
        inbound_shipment_item = inbound_shipment.inbound_shipment_items.where(:seller_sku => sku).first
        inbound_shipment_item.destroy unless inbound_shipment_item.nil?

        OutboundShipment.joins(pending_po: :vendoritem).joins("INNER JOIN seller_skus on seller_skus.id = vendoritems.seller_sku_id").where(:shipment_id => inbound_shipment.shipment_id).where("seller_skus.sku = ?", sku).update_all(:shipment_id => "", :stage_status => "RECEIVED")
        render json: {'status'=>true, 'msg'=>'Operations successfully done.'}
        return
      end
    end

    render json: {'status'=>false, 'msg'=>result[:msg]}
    return
  end

  def reset_transport_plan
    inbound_shipment = InboundShipment.find_by(id: params[:id])
    if (inbound_shipment.latest_transport_plan.status == "CONFIRMED" or inbound_shipment.shipment_status == "CLOSED")
      redirect_to inbound_shipment_path(inbound_shipment), notice: "Cannot reset transport plan. "
    else
      inbound_shipment.transport_contents.destroy_all
      redirect_to inbound_shipment_path(inbound_shipment), notice: "Transport Plan has been reset. Please add details again!"
    end
  end

  def sync_transport_plan
    inbound_shipment = InboundShipment.find_by(id: params[:id])
    response = TransportContentService.new(nil, inbound_shipment.shipment_id).sync_transport_with_amazon
    notice = response[:success] ? "Plan has been synced with amazon!" : "There was a problem while syncing transport plan, please try again! #{response[:error]}"
    redirect_to inbound_shipment_path(inbound_shipment), notice: notice
  end

  def void_transport_plan
    begin
      @transport_content = TransportContent.find_by(id: params[:id])
      response = TransportContentService.new(@transport_content.id).void_transport_plan
      if response[:success]
        redirect_to inbound_shipment_path(@transport_content.inbound_shipment), notice: "Successfully updated transport plan!"
      else
        redirect_to inbound_shipment_path(@transport_content.inbound_shipment), notice: response[:error].to_s
      end
    rescue => e
      redirect_to inbound_shipment_path(@transport_content.inbound_shipment), notice: e.to_s
    end
  end

  def mark_as_shipped
    inbound_shipment = InboundShipment.find_by(id: params[:id])
    response = TransportContentService.new(nil, inbound_shipment.shipment_id).mark_as_shipped
    if response[:success]
      redirect_to inbound_shipment_path(inbound_shipment), notice: "Successfully marked as shipped!"
    else
      redirect_to inbound_shipment_path(inbound_shipment), notice: response[:error].to_s
    end
  end

  def add_invoice
    p = inbound_shipment_invoice_params
    invoice = @inbound_shipment.ship_cus_invoice
    if invoice.nil?
      invoice = ShipCusInvoice.create({inbound_shipment_id: @inbound_shipment.id, invoice_name: p[:ship_cus_invoice_attributes][:invoice_name], new_box_count: p[:ship_cus_invoice_attributes][:new_box_count]})
    else
      invoice.invoice_name = p[:ship_cus_invoice_attributes][:invoice_name] if p[:ship_cus_invoice_attributes][:invoice_name]
      invoice.new_box_count = p[:ship_cus_invoice_attributes][:new_box_count] if p[:ship_cus_invoice_attributes][:new_box_count]
      invoice.save
    end
    respond_to do |format|
      format.html { redirect_to @inbound_shipment, notice: 'Invoice was successfully updated.' }
      format.json { render json: @inbound_shipment, status: :ok }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inbound_shipment
      @inbound_shipment = InboundShipment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inbound_shipment_params
      # .permit(ship_cus_invoice_attributes: [:invoice_name])
      params.fetch(:inbound_shipment, {})
    end

    def inbound_shipment_invoice_params
      params.fetch(:inbound_shipment, {}).permit(ship_cus_invoice_attributes: [:invoice_name, :new_box_count])
    end

    def createshipment(plan_id)
      ship_plan = InboundShipplan.find(plan_id)
      adderss = {
        name: ship_plan.name,
        address_line1: ship_plan.address_line1,
        address_line2: ship_plan.address_line2,
        city: ship_plan.city,
        state_or_province_code: ship_plan.state_or_province_code,
        postal_code: ship_plan.postal_code,
        country_code: ship_plan.country_code
      }
      inbound_shipment_header = {
        shipment_name: ship_plan.shipment_name,
        ship_from_address: adderss,
        destination_fulfillment_center_id: ship_plan.destination_fulfillment_center_id,
        label_prep_preference: "SELLER_LABEL",
        are_cases_required: ship_plan.are_cases_required,
        shipment_status: "WORKING",
        intended_box_contents_source: "2D_BARCODE",
      }
      user = ship_plan.user
      client = MWS.fulfillment_inbound_shipment(marketplace: user.mws_market_place_id,
        merchant_id: user.seller_id,
        aws_access_key_id: $mws_developer_id,
        auth_token: user.mws_access_token,
        aws_secret_access_key: $mws_developer_secret)

      inbound_shipment_items = []
      ship_plan.inbound_shipplan_items.each do |item|
        prep_details = []
        item.outbound_shipment.pending_po.vendoritem.seller_sku.prepinstruction. split(',').each do |prep|
          if prep.blank?
            next
          end
          prep_detail = {
            prep_instruction: prep,
            prep_owner: "SELLER"
          }
          prep_details << prep_detail
        end
        inbound_shipment_item = {
          :seller_sku => item.outbound_shipment.pending_po.vendoritem.sellersku,
          :quantity_shipped => item.ship_qty,
          :prep_details_list => prep_details
        }
        inbound_shipment_items << inbound_shipment_item
      end
      begin
        result = client.create_inbound_shipment(ship_plan.shipplan_id, inbound_shipment_header, inbound_shipment_items).parse
        shipment_id = result["ShipmentId"]
        unless shipment_id.blank?
          shipment = user.inbound_shipments.create(shipment_id: shipment_id)
          shipment.copy_from_plan(ship_plan)
          ship_plan.inbound_shipplan_items.each do |item|
            item_sku = item.try(:outbound_shipment).try(:pending_po).try(:vendoritem).try(:seller_sku).try(:sku)
            if item_sku.blank?
              next
            end
            shipment_item = shipment.inbound_shipment_items.where(:seller_sku => item_sku).first_or_create
            unless shipment_item.nil?
              shipment_item.seller_sku = item_sku
              shipment_item.quantity_shipped = item.ship_qty
              shipment_item.quantity_in_case = item.case_qty
              shipment_item.quantity_received = 0
              shipment_item.expired_date = item.try(:outbound_shipment).try(:expired_date)
              shipment_item.pending_po_id = item&.outbound_shipment&.pending_po_id
              shipment_item.save
            end
            out_stage_item = item.outbound_shipment
            unless out_stage_item.nil?
              out_stage_item.stage_status = "SHIPMENT"
              out_stage_item.shipment_id = shipment_id

              out_stage_item.save
            end
          end
          ship_plan.destroy
        end
      rescue Exception => error
        return {:status => false, :msg => error.message}
      end
      return {:status => true, :msg => ''}
    end

    #shipment_id => inbound_shipment, stage_id => outbound_shipment, qty => shipped_qty
    def updateshipment(shipment_id, stage_id, qty)
      out_stage = OutboundShipment.find(stage_id)
      shipment = InboundShipment.find(shipment_id)

      # if shipment.shipment_id != out_stage.shipment_id && qty == 0
      #   return false
      # end

      adderss = {
        name: shipment.name,
        address_line1: shipment.address_line1,
        address_line2: shipment.address_line2,
        city: shipment.city,
        state_or_province_code: shipment.state_or_province_code,
        postal_code: shipment.postal_code,
        country_code: shipment.country_code
      }
      inbound_shipment_header = {
        shipment_name: shipment.shipment_name,
        ship_from_address: adderss,
        destination_fulfillment_center_id: shipment.destination_fulfillment_center_id,
        label_prep_preference: "SELLER_LABEL",
        are_cases_required: shipment.are_cases_required,
        shipment_status: "WORKING",
        intended_box_contents_source: "2D_BARCODE",
      }

      user = shipment.user

      client = MWS.fulfillment_inbound_shipment(marketplace: user.mws_market_place_id,
        merchant_id: user.seller_id,
        aws_access_key_id: $mws_developer_id,
        auth_token: user.mws_access_token,
        aws_secret_access_key: $mws_developer_secret)

      inbound_shipment_items = []
      prep_details = []
      prep_instructions = out_stage.pending_po.vendoritem.seller_sku.prepinstruction
      if prep_instructions.present?
        prep_instructions.split(',').each do |prep|
          prep_detail = {
            prep_instruction: prep,
            prep_owner: "SELLER"
          }
          prep_details << prep_detail
        end
      end
      inbound_shipment_item = {
        :seller_sku => out_stage.pending_po.vendoritem.sellersku,
        :quantity_shipped => qty,
        :prep_details_list => prep_details
      }
      inbound_shipment_items << inbound_shipment_item
      begin
        result = client.update_inbound_shipment(shipment.shipment_id, inbound_shipment_header, inbound_shipment_items).parse
        puts "updateshipment_result #{result}"
        # sku = out_stage.pending_po.vendoritem.sellersku


        # if qty == 0
        #   inbound_shipment_item = shipment.inbound_shipment_items.where(:seller_sku => sku).first
        #   inbound_shipment_item.destroy unless inbound_shipment_item.nil?


        #   # out_stage.box_qty = qty
        #   out_stage.shipment_id = ""
        #   out_stage.stage_status = "RECEIVED"
        #   out_stage.save


        # else
        #   inbound_shipment_item = shipment.inbound_shipment_items.where(:seller_sku => sku).first_or_create
        #   inbound_shipment_item.quantity_shipped = qty

        #   inbound_shipment_item.save

        #   #

        #   # out_stage.box_qty = qty

        #   out_stage.shipment_id = shipment.shipment_id
        #   out_stage.stage_status = "SHIPMENT"
        #   out_stage.save
        # end

      rescue Exception => error
        puts "updateshipment_error #{error.message}"
        puts inbound_shipment_items
        return {:status => false, :msg => error.message}
      end
      return {:status => true, :msg => ''}
    end
end
