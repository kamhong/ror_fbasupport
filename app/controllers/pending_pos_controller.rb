class PendingPosController < ApplicationController
  include SellerSkusHelper
  before_action :authenticate_user!
  before_action :authenticate_manager, only: [:index]
  before_action :set_pending_po, only: [:show, :edit, :update, :destroy]

  # GET /pending_pos
  # GET /pending_pos.json
  def index

    @pending_po_status = PendingPo.status_list

    if current_user.is_warehouse
      @total_quantitiy_without_invoice = PendingPo.where(:customer_invoice_id => nil).sum(:received_quantity)
      @total_cost_without_invoice = PendingPo.where(:customer_invoice_id => nil).sum("received_quantity * cost")
      @user_option = User.where(id: PendingPo.joins(vendoritem: :vendor).joins("INNER JOIN users on vendors.user_id = users.id").pluck("users.id"))
      @vendor_option = Vendor.where(id: PendingPo.joins(vendoritem: :vendor).pluck("vendors.id"))
      @po_option = PendingPoIndex.all
      @pending_pos = PendingPo.all
    else
      @total_quantitiy_without_invoice = current_user.pending_pos.where(:customer_invoice_id => nil).sum(:received_quantity)
      @total_cost_without_invoice = current_user.pending_pos.where(:customer_invoice_id => nil).sum("received_quantity * cost")
      @user_option = [current_user]
      @vendor_option = current_user.vendors.where(id: PendingPo.joins(vendoritem: :vendor).pluck("vendors.id"))
      @po_option = current_user.pending_po_indices.all
      @pending_pos = current_user.pending_pos.all
    end
		@invoice = params[:invoice] || ""
		@warehouse_vendor = params[:warehouse_vendor] || ""
  end

  # GET /pending_pos/1
  # GET /pending_pos/1.json
  def show
  end

  # GET /pending_pos/new
  def new
    @pending_po = PendingPo.new
  end

  # GET /pending_pos/1/edit
  def edit
  end

  # POST /pending_pos
  # POST /pending_pos.json
  def create
    @pending_po = PendingPo.new(pending_po_params)

    respond_to do |format|
      if @pending_po.save
        format.html { redirect_to @pending_po, notice: 'Pending po was successfully created.' }
        format.json { render :show, status: :created, location: @pending_po }
      else
        format.html { render :new }
        format.json { render json: @pending_po.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pending_pos/1
  # PATCH/PUT /pending_pos/1.json
  def update
    respond_to do |format|
      if @pending_po.update(pending_po_params)
        format.html { redirect_to @pending_po, notice: 'Pending po was successfully updated.' }
        format.json { render :show, status: :ok, location: @pending_po }
      else
        format.html { render :edit }
        format.json { render json: @pending_po.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pending_pos/1
  # DELETE /pending_pos/1.json
  def destroy
    @pending_po.destroy
    respond_to do |format|
      format.html { redirect_to pending_pos_url, notice: 'Pending po was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

	def complete
		id = params[:id]
		pending_po = PendingPo.find(id)
		pending_po.status = "Completed"
		if pending_po.save
			render json: {'status'=>true}
		else
			render json: {'status'=>false, 'msg'=>'Failed to update the status'}
		end
	end


  def check_sku
    render json: {'status' => false}
    return
		pending_po_id = params[:pending_po_id]
		pending_po = PendingPo.find(pending_po_id)
		seller_sku = pending_po.try(:vendoritem).try(:seller_sku)
		if seller_sku.nil?
			# result = create_skus_from_vendor(pending_po.vendoritem_id)
			# if result[:status] == false
			# 	render json: result
			# 	return
			# else
			# 	seller_sku = pending_po.try(:vendoritem).try(:seller_sku)
			# 	if seller_sku.fnsku.blank?
			# 		r = update_shipplaninformation(seller_sku.id)
			# 		puts r
			# 		# seller_sku.reload
			# 	end
      # end
    else
      # user = seller_sku.user
      # client = MWS.feeds(marketplace: user.mws_market_place_id,
      #   merchant_id: user.seller_id,
      #   aws_access_key_id: "AKIAIB5ZWGDEZQZFMISA",
      #   auth_token: user.mws_access_token,
      #   aws_secret_access_key: "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")
      # result = set_offer_of_sku(seller_sku.id)
		end
		render json: {'status' => true}
	end

	def remove_sku
		pending_po_id = params[:pending_po_id]
		pending_po = PendingPo.find(pending_po_id)
		seller_sku = pending_po.try(:vendoritem).try(:seller_sku)
		if seller_sku.nil?
			render json: {'status' => true}
			return
		end
		# unless seller_sku.is_valid_fnsku
			puts "START _DELETE__"
			result = remove_sku_from_vendor(pending_po.vendoritem_id)
			if result[:status] == false
				render json: result
				return
			end
		# end
		render json: {'status' => true}
	end

	def recreate_sku
		pending_po_id = params[:pending_po_id]
		pending_po = PendingPo.find(pending_po_id)
		seller_sku = pending_po.try(:vendoritem).try(:seller_sku)
		unless seller_sku.nil?
			render json: {'status' => false, 'error_message'=>'Item was not removed from Amazon.'}
			return
		end
		puts "START CREATE__"
		result = create_skus_from_vendor(pending_po.vendoritem_id, true)
		puts result
		if result[:status]
			pending_po.reload
			seller_sku = pending_po.try(:vendoritem).try(:seller_sku)
			if seller_sku.needed_refresh_prep
				update_prepinstructions(seller_sku.id)
				seller_sku.reload
			end
			puts "START FNSKU__"
			if seller_sku.fnsku.blank?
				r = update_shipplaninformation(seller_sku.id)
				puts r
			end
			puts "END FNSKU__"
		end

		render json: result
	end

  def receive
		pending_po_id = params[:pending_po_id]
		box_qty = params[:box_qty].to_i
		case_qty = params[:case_qty].to_i
		exp_date = params[:exp_date]
		pending_po = PendingPo.find(pending_po_id)
		if pending_po
			seller_sku = pending_po.try(:vendoritem).try(:seller_sku)
			if seller_sku.nil?
				render json: {'status'=>false,'msg'=>'SellerSku is not created yet.'}
				return
      end

      unless seller_sku.user.mws_key_valid
        render json: {'status'=>false,'msg'=>'Please check the Amazon Key on your profile.'}
        return
      end
			if seller_sku.needed_refresh_prep
				update_prepinstructions(seller_sku.id)
				seller_sku.reload
			end

			if seller_sku.fnsku.blank? || !seller_sku.is_valid_fnsku
				update_shipplaninformation(seller_sku.id)
				seller_sku.reload
			end

			unless seller_sku.is_valid_fnsku
				render json: {'status'=>false, 'msg'=>'FNSKU is not created successfully. Please recreate the SKU after enable Amazon Barcode.'}
				return
			end
			pending_po.received_quantity = pending_po.received_quantity + box_qty * case_qty
			if pending_po.save
				if box_qty > 0 && case_qty > 0
					begin
						expired_date =  Date.strptime(exp_date, "%m/%d/%Y").to_s
						pending_po.outbound_shipments.create([{box_qty: box_qty, case_qty: case_qty, show_case_qty: case_qty, expired_date: expired_date}])
					rescue ArgumentError
						pending_po.outbound_shipments.create([{box_qty: box_qty, case_qty: case_qty, show_case_qty: case_qty}])
          end
        elsif box_qty * case_qty < 0
          last_out_bound = pending_po.outbound_shipments.last
          unless last_out_bound.nil?
            last_out_bound.box_qty = (last_out_bound.box_qty + box_qty * case_qty / last_out_bound.case_qty).to_i
            last_out_bound.save
          end
				end
				render json: {'status'=>true}
				return
			end
		end
		render json: {'status'=>false, 'msg'=>'Error occured when receive the item.'}
	end

	def loadpoindex
			@pendingpos = PendingPoIndex.where(:vendor_id => params[:vendor_id]).all
			render :partial => "layouts/partials/pendingpos", :object => @pendingpos
	end


  def list_all_po
    invoice = params[:invoice] || ""
    warehouse_vendor = params[:ware] || ""
    filter_status = params[:filter_status] || ""

    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i

    sort_columnIndex = params[:iSortCol_0].to_i;
    sort_columnName = params["mDataProp_#{sort_columnIndex}"]

    sort_order = params[:sSortDir_0]
    iColumns = params[:iColumns].to_i

    vendoritem_columns = ['id','cost','upc','vendorsku','packcount','vendortitle','order_cost']
    express_columns = ['profit','margin','salespermonth','salesrank'];
    sku_columns = ['sku']
    sku_label_columns = ['labeling_fee','bubble_fee','polybag_fee','bundle_fee','box_fee']
    po_columns = ['order_quantity', 'received_quantity','status','created_at']
    notsearch_columns = ['packcost']
    if current_user.is_warehouse
      pending_pos = PendingPo.all
    else
      return
    end

    pending_pos = pending_pos.joins(vendoritem: [:vendorasin, :seller_sku, {vendor: :user}]).joins("LEFT JOIN warehouse_vendors ON warehouse_vendors.supplier_code = vendors.supplier_prefix")
    if invoice == "no"
      pending_pos = pending_pos.where(:customer_invoice_id => nil)
    end

    unless warehouse_vendor.blank?
      pending_pos = pending_pos.where("warehouse_vendors.id = ?", warehouse_vendor)
    end
    total = pending_pos.load.size

    i=0
    while i < iColumns do
      search = params["sSearch_#{i}"]
      unless search.nil? || search == ''
        true_search = search
        ori_search_column = params["mDataProp_#{i}"]
        search_column = ori_search_column.split(".")[-1]
        unless notsearch_columns.include? search_column
          if ori_search_column == "vendoritem.vendor.name"
            pending_pos = pending_pos.where("vendors.name = ?", search)
          elsif search_column == 'full_name'
            pending_pos = pending_pos.where("concat_ws(' ',users.first_name, users.last_name) = ?", search)
          elsif vendoritem_columns.include? search_column
            pending_pos = pending_pos.where("vendoritems.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"})
          elsif sku_label_columns.include? search_column
            pending_pos = pending_pos.where("seller_skus.prepinstruction LIKE ?", "%#{search}%")
          elsif	sku_columns.include? search_column
            pending_pos = pending_pos.where("seller_skus.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          elsif search_column == "status"
          elsif ori_search_column == "customer_invoice.invoice_name"

            if search == "Yes"
              pending_pos = pending_pos.where.not(:customer_invoice_id => nil)
            elsif search == "No"
              pending_pos = pending_pos.where(:customer_invoice_id => nil)
            end
          elsif ori_search_column == "invoice_names"
            pending_po_ids_with_invoice = InvoiceItem.pluck(:pending_po_id).uniq
            if search == "Yes"
              pending_pos = pending_pos.where("pending_pos.id in (?)",pending_po_ids_with_invoice)
            elsif search == "No"
              pending_pos = pending_pos.where("pending_pos.id not in (?)",pending_po_ids_with_invoice)
            end
          elsif search_column == "po_name"
            pending_pos = pending_pos.joins(:pending_po_index).where(pending_po_indices: {po_name: search})
          elsif search_column == "created_at"
            selected_date = Date.strptime(search, "%m/%d/%Y")
            pending_pos = pending_pos.joins(:pending_po_index).where(pending_po_indices: {created_at:  selected_date.beginning_of_day..selected_date.end_of_day})
          else
            pending_pos = pending_pos.where("vendorasins.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          end
        end

      end
      i = i + 1
    end
    
    pending_pos = pending_pos.per_status(filter_status)

    if po_columns.include? sort_columnName
      pending_pos = pending_pos.order("pending_pos.status #{sort_order}")
    end

    filtered = pending_pos.load.size
    pending_pos = pending_pos.limit(limit).offset(start).joins(vendoritem: [:vendorasin, :vendor, {seller_sku: :user}]).includes({vendoritem: [:vendorasin, :vendor, {seller_sku: :user}]}, :customer_invoice, :pending_po_index, {invoice_items: :invoice}, :invoices)
    # render json: {'aaData'=>pending_pos,"iTotalRecords": total,"iTotalDisplayRecords": filtered}
    render json: 
    {
      aaData: pending_pos.as_json(
          methods: [:status_date_string, :order_cost, :remained, :missing, :shipped_packs, :confirmed_cost, :confiremd_pack_qty, :labeling_fee, :bubble_fee, :polybag_fee, :bundle_fee, :box_fee, :invoice_names], 
          include: [
            {
              vendoritem: {
                include: [:vendor, :vendorasin, seller_sku: {include: {user: {methods: :full_name}}}]
              }
            }, 
            :pending_po_index, 
            { invoice_items: {include:  :invoice } },
            :invoices
          ]
        ),
      iTotalRecords: total,
      iTotalDisplayRecords: filtered
    }
  end

  def updateitem
    pending_po = nil
    data = params[:data]
    data.each do |key, item|
      pending_po = PendingPo.find(key)
      item.each do |title, value|
        if title ==  "vendoritem"
          value.each do |t, v|
            pending_po.vendoritem[t.to_sym] = v
            pending_po.vendoritem.save
          end
        else
          if title == "confirmed_qty" && value.to_i > 0
            pending_po.status = "Confirmed"
            pending_po.status_date = Date.today
          elsif title == "cost" && value.to_f > 0
            pending_po.vendoritem.cost = value.to_f
            pending_po.vendoritem.save
          elsif title == "status"
            pending_po.status_date = Date.today
          end
          pending_po[title.to_sym] = value
        end
      end
      pending_po.save
    end
    render json: {'data'=>pending_po}
  end

  def load
    vendoritem_columns = ['id','cost','upc','created_at','vendorsku','packcount','vendortitle','order_cost']
    express_columns = ['profit','margin','salespermonth','salesrank'];
    sku_columns = ['sku']
    po_columns = ['order_quantity', 'received_quantity']
    notsearch_columns = ['packcost']

    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i
    pending_po = params[:pending_po]


    inventories = PendingPoIndex.find(pending_po).pending_pos.joins(vendoritem: :vendorasin).joins("INNER JOIN seller_skus ON vendoritems.seller_sku_id = seller_skus.id")
    # inventories = Vendoritem.where('vendor_id = ?',vendor_id).joins(:vendorasin).joins(:po)
    total = inventories.load.size



    sort_columnIndex = params[:iSortCol_0].to_i;
    sort_columnName = params["mDataProp_#{sort_columnIndex}"]
    sort_columnName = sort_columnName.split(".")[-1]
    sort_order = params[:sSortDir_0]



    iColumns = params[:iColumns].to_i

    i=0
    true_search = ""
    search = params["sSearch"]
    unless params["sSearch"].nil? || params["sSearch"] == ''
      # inventories = inventories.joins(:vendorasin).where(:vendorasins => {:upc like "%#{search}%"}).or(inventories.where(:vendorasins => {:asin like "%#{search}%"}))
      inventories = inventories.where("vendorasins.asin like :l_search or vendoritems.upc like :l_search or vendorasins.name like :l_search", {:l_search =>"%#{search}%"})
    end

    while i < iColumns do
      search = params["sSearch_#{i}"]
      unless search.nil? || search == ''
        true_search = search
        search_column = params["mDataProp_#{i}"]
        search_column = search_column.split(".")[-1]
        unless notsearch_columns.include? search_column
          if vendoritem_columns.include? search_column
          inventories = inventories.where("vendoritems.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          elsif express_columns.include? search_column
            search.split("##").each do |x|
              search_value = x.split("$$");
              exp = search_value[0].to_s
              val = search_value[1].to_f
              if exp == 'eq'
                exp = '='
              elsif exp == 'lg'
                exp = '>'
              elsif exp == 'el'
                exp = '>='
              elsif exp == 'ls'
                exp = '<'
              elsif exp == 'es'
                exp = '<='
              else
                exp = '='
              end
              inventories = inventories.number_compare(exp,val, search_column)
            end
          elsif	sku_columns.include? search_column
            inventories = inventories.where("seller_skus.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          elsif po_columns.include? search_column
            inventories = inventories.where("pos.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          else
            inventories = inventories.where("vendorasins.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          end
        end

      end
      i = i + 1
    end


    if sort_columnName == 'margin'
      inventories = inventories.order("(vendorasins.buyboxprice-pending_pos.cost*vendoritems.packcount-vendorasins.totalfbafee) / vendoritems.cost #{sort_order}")
    elsif sort_columnName == 'profit'
      inventories = inventories.order("vendorasins.buyboxprice-pending_pos.cost*vendoritems.packcount-vendorasins.totalfbafee #{sort_order}")
    elsif sort_columnName == 'packcost'
      inventories = inventories.order("vendoritems.cost*vendoritems.packcount #{sort_order}")
    elsif sort_columnName == 'order_cost'
      inventories = inventories.order("pending_pos.cost*pending_pos.order_quantity #{sort_order}")
    else
      if vendoritem_columns.include? sort_columnName
        inventories = inventories.order("vendoritems.#{sort_columnName} #{sort_order}")
      elsif sku_columns.include? sort_columnName
        inventories = inventories.order("seller_skus.#{sort_columnName} #{sort_order}")
      elsif po_columns.include? sort_columnName
        inventories = inventories.order("pending_pos.#{sort_columnName} #{sort_order}")
      else
        inventories = inventories.order("vendorasins.#{sort_columnName} #{sort_order}")
      end
    end

    filtered = inventories.load.size
    # inventories =inventories.limit(limit).offset(start)
    # render json: {'aaData'=>inventories,"iTotalRecords": total,"iTotalDisplayRecords": filtered}

    inventories = inventories.includes({vendoritem: [:vendorasin, :seller_sku]}, :pending_po_index).limit(limit).offset(start)
    render json: 
    {
      aaData: inventories.as_json(
          methods: [:status_date_string, :order_cost, :remained, :missing, :shipped_packs, :confirmed_cost, :confiremd_pack_qty], 
          include: [{vendoritem: {include: [:vendorasin, :seller_sku], methods: [:profit, :margin, :packcost]}}, :pending_po_index]
        ),
      iTotalRecords: total,
      iTotalDisplayRecords: filtered
    }
  end

  def list_options
    render json: {'status_list'=>PendingPo.status_list}
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pending_po
      @pending_po = PendingPo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pending_po_params
      params.fetch(:pending_po, {})
    end
end
