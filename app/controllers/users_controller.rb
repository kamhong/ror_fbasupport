class UsersController < ApplicationController
  layout 'application'
  before_action :authenticate_user!

  def edit
    @user = User.find(params[:id])
  end

  def profile
    @user = current_user
  end

  def plans
  end
  
  def new
    @user = User.new
    @company = Company.find(params[:company])
  end

  def update
    @user = User.find(params[:id])
    updated = @user.key_changed(params[:user])
    if params[:user][:password].blank?
      if @user.update(user_params_without_password)
        
        if updated
          CheckUserMwsValidWorker.perform_async(@user.id)
        end

        flash[:notice] = "Edit Successful."
        # redirect_to company_path(@user.company_id)
      else
        flash[:notice] = 'User data is not updated.'
        # redirect_to edit_user_path(@user)
      end
    else
      if @user.update(user_params)
        if updated
          CheckUserMwsValidWorker.perform_async(@user.id)
        end
        flash[:notice] = 'Edit Successful.'
      else
        flash[:notice] = 'User data is not updated.'
      end
    end

    redirect_to :back
    # @user = User.find(params[:id])
    # if @user.update(user_params)
    #   redirect_to company_path(@user.company_id)
    # else
    #   flash[:notice] = 'User data is not updated'     
    # end
  end

  def create
    @user = User.new(user_updateparams)
    if @user.save
      redirect_to company_path(@user.company_id)
    else
      flash[:notice] = 'User was not created.'
      @company = @user.company
      render 'new'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destory
    redirect_to company_path(@user.company_id)
  end

  def sendnotification
    User.all.each do |user|
      weeks = user.get_weeks_last_sign()
      if weeks > 0
        @email = user.email
        UserMailer.send_signin_notification(@email).deliver_later
      end
    end
    UserMailer.send_signin_notification('daniel.parker989@gmail.com').deliver_later
    render :nothing => true
  end

  def update_warehouse
    user = User.find(params[:warehouse_value])
    user.warehouse_name = params[:warehouse_name]
    user.warehouse_address_line1 = params[:warehouse_address_line1]
    user.warehouse_address_line2 = params[:warehouse_address_line2]
    user.warehouse_city = params[:warehouse_city]
    user.warehouse_state_or_province_code = params[:warehouse_state_or_province_code]
    user.warehouse_postal_code = params[:warehouse_postal_code]
    user.warehouse_country_code = params[:warehouse_country_code]
    user.save
    render json: {'status'=>true}
  end


  private
  def user_params
    page = params[:page]
    if page.nil? && page != 'profile'
      params.require(:user).permit(:email, :password, :password_confirmation, :role_id)
    else
      params.require(:user).permit(:email, :password, :password_confirmation, :seller_id, :mws_access_token,  :first_name, :last_name)
    end
    
  end

  def user_params_without_password
    page = params[:page]
    if page.nil? && page != 'profile'
      params.require(:user).permit(:email, :role_id)
    else
      params.require(:user).permit(:email, :seller_id, :mws_access_token, :first_name, :last_name)
    end
    
  end


  def user_updateparams
    params.require(:user).permit(:email, :password, :password_confirmation, :role_id, :company_id)
  end
end
