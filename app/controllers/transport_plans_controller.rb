require 'peddler'
require "pdfkit"

class TransportPlansController < ApplicationController
  before_action :set_shipments, only: :index

  def index
    @inbound_shipments = InboundShipment.all
    if current_user.admin?
      @selected_user = User.find_by(id: params[:user_id])
      @inbound_shipments = @selected_user.present? ? @selected_user.inbound_shipments.all :  @inbound_shipments
      @users = User.all
    else
      @inbound_shipments = current_user.inbound_shipments.all
    end
    @inbound_shipments = params[:shipment_status].nil? ? @inbound_shipments.working : (@inbound_shipments.by_status params[:shipment_status])
  end

  def working
    @transport_plans = TransportContent.not_confirmed
  end

  def new
    @transport_content = TransportContent.new
    @package = @transport_content.packages.build
  end

  def show
    @transport_content = TransportContent.find_by(id: params[:id])
  end

  def create
    @transport_content = TransportContent.create(transport_content_params)
    error_message  = @transport_content.check_plan_structure
    if @transport_content.save and error_message.nil?
      response = TransportContentService.new(@transport_content.id).upload_to_amazon
      if response[:success]
        redirect_to request.referer, notice: 'Shipment was successfully uploaded to Amazon.'
      else
        redirect_to request.referer, notice: response[:error].to_s
      end
    else
      @transport_content.destroy
      redirect_to request.referer, notice: error_message.try(:first).presence || @transport_content.errors.full_messages
    end
  end

  def upload_to_amazon
    @transport_content = TransportContent.find_by(id: params[:id])
    response = TransportContentService.new(@transport_content.id).upload_to_amazon
    if response[:success]
      @transport_content.update(uploaded_to_amazon: true)
      redirect_to request.referer, notice: 'Shipment was successfully uploaded to Amazon.'
    else
      redirect_to request.referer, notice: response[:error].to_s
    end

  end

  def initiate_estimate
    @transport_content = TransportContent.find_by(id: params[:id])
    response = TransportContentService.new(@transport_content.id).get_estimate
    if response[:success]
      response = TransportContentService.new(@transport_content.id).update_transport_status
      redirect_to request.referer, notice: 'Shipment was successfully uploaded to Amazon.'
    else
      redirect_to request.referer, notice: "Sorry, either this plan already estimated or cannot be estimated by amazon!, Error: " + response[:error].to_s
    end
  end

  def estimate
    @transport_content = TransportContent.find_by(id: params[:id])
    TransportContentService.new(@transport_content.id).sync_transport_with_amazon
    response = TransportContentService.new(@transport_content.id).update_transport_status
    if response[:success]
      redirect_to request.referer
    else
      redirect_to request.referer, notice: response[:error].to_s
    end
  end

  def initiate_confirmation
    @transport_content = TransportContent.find_by(id: params[:id])
    TransportContentService.new(@transport_content.id).sync_transport_with_amazon
    response = TransportContentService.new(@transport_content.id).confirm_on_amazon
    if response[:success]
      response = TransportContentService.new(@transport_content.id).update_transport_status
    else
      response = TransportContentService.new(@transport_content.id).sync_transport_with_amazon
      if @transport_content.status == "CONFIRMED"
        redirect_to request.referer, notice: "Shipment has been confirmed!"
      else
        redirect_to request.referer, notice: "Sorry, either this plan already confirmed or cannot be confirmed by amazon!, Error: " + response[:error].to_s
      end
    end
  end

  def confirm
    @transport_content = TransportContent.find_by(id: params[:id])
    response = TransportContentService.new(@transport_content.id).sync_transport_with_amazon
    response = TransportContentService.new(@transport_content.id).update_transport_status
    response[:success] = true
    if response[:success]
      redirect_to request.referer, notice: "Transport Plan has been confirmed!"
    else
      redirect_to request.referer, notice: response[:error].to_s
    end
  end

  def print_box_labels
    begin
      @transport_content = TransportContent.find_by(id: params[:id])
      response = TransportContentService.new(@transport_content.id).get_package_labels
      if response[:success]
        decoded_string = Base64.decode64(response[:base64_pdf])
        zip_file = File.open("Output.zip", "wb") do |f|
          f.write(decoded_string)
        end
        Zip::File.open("Output.zip") do |zipfile|
          zipfile.each do |file|
            send_data(
              file.get_input_stream.read,
              file_name: "box_labels.pdf",
              type: "application/pdf"
            )
          end
        end
      else
        redirect_to request.referer, notice: response[:error].to_s
      end
    rescue => e
      redirect_to request.referer, notice: e.to_s
    end
  end

  def print_pallete_labels
    begin
      @transport_content = TransportContent.find_by(id: params[:id])
      pallete_count = @transport_content.palletes.count.presence || @transport_content.pallete_count
      pallete_count = params[:pallete_labels][:pallete_count] if params[:pallete_labels]
      response = TransportContentService.new(@transport_content.id).get_pallete_labels pallete_count
      if response[:success]
        decoded_string = Base64.decode64(response[:base64_pdf])
        zip_file = File.open("Output.zip", "wb") do |f|
          f.write(decoded_string)
        end
        Zip::File.open("Output.zip") do |zipfile|
          zipfile.each do |file|
            send_data(
              file.get_input_stream.read,
              file_name: "pallete_labels.pdf",
              type: "application/pdf"
            )
          end
        end
      else
        redirect_to request.referer, notice: response[:error].to_s
      end
    rescue => e
      redirect_to request.referer, notice: e.to_s
    end
  end

  def print_bill_of_lading
    begin
      @transport_content = TransportContent.find_by(id: params[:id])
      response = TransportContentService.new(@transport_content.id).get_bill_of_lading
      if response[:success]
        decoded_string = Base64.decode64(response[:base64_pdf])
        zip_file = File.open("Output.zip", "wb") do |f|
          f.write(decoded_string)
        end
        Zip::File.open("Output.zip") do |zipfile|
          zipfile.each do |file|
            send_data(
              file.get_input_stream.read,
              file_name: "pallete_labels.pdf",
              type: "application/pdf"
            )
          end
        end
      else
        redirect_to request.referer, notice: response[:error].to_s
      end
    rescue => e
      redirect_to request.referer, notice: e.to_s
    end
  end

  def remove_package
    package = Package.find_by(id: params[:id])
    transport_content = package.transport_content
    package.destroy
    response = TransportContentService.new(transport_content.id).upload_to_amazon if transport_content.packages.count != 0
    redirect_to request.referrer
  end

  def add_packages
    begin
      transport_content = TransportContent.find_by(id: params[:id])
      transport_content.update(transport_content_params)
      error_message  = transport_content.check_plan_structure
      if transport_content.save and error_message.nil?
        TransportContentService.new(transport_content.id).upload_to_amazon
        redirect_to inbound_shipment_path(transport_content.inbound_shipment), notice: "Successfully Updated!"
      else
        redirect_to inbound_shipment_path(transport_content.inbound_shipment), notice: error_message.try(:first).presence || transport_content.errors.full_messages
      end
    rescue => e
      redirect_to inbound_shipment_path(transport_content.inbound_shipment), notice: e.to_s
    end
  end

  def remove_pallete
    pallete = Pallete.find_by(id: params[:id])
    transport_content = pallete.transport_content
    pallete.destroy
    response = TransportContentService.new(transport_content.id).upload_to_amazon if transport_content.palletes.count != 0
    redirect_to request.referrer
  end

  def remove_tracking
    tracking = Tracking.find_by(id: params[:id])
    transport_content = tracking.transport_content
    tracking.destroy
    response = TransportContentService.new(transport_content.id).upload_to_amazon if transport_content.trackings.count != 0
    redirect_to request.referrer
  end

  def update_box_information
    @transport_content = TransportContent.find_by(id: params[:id])
    @transport_content.update(box_count: transport_content_params[:box_count], frieght_read_date: (parse_frieght_date transport_content_params[:frieght_read_date]))
    if @transport_content.save
      response = TransportContentService.new(@transport_content.id).upload_to_amazon
      if response[:success]
        redirect_to request.referer, notice: "Successfully updated transport plan!"
      else
        redirect_to request.referer, notice: response[:error].to_s
      end
    else
      redirect_to request.referer, notice: @transport_content.errors.full_messages
    end
  end

  def update_shipping_fee
    @transport_content = TransportContent.find_by(id: params[:id])
    @transport_content.update(shipping_fee: transport_content_params[:shipping_fee])
    if @transport_content.save
      redirect_to request.referer, notice: "Successfully updated shipping fee!"
    else
      redirect_to request.referer, notice: @transport_content.errors.full_messages
    end
  end
  private

  def set_shipments
    TransportContentService.generate_shipments_from_amazon current_user if InboundShipment.count == 0
  end

  def parse_frieght_date date_str
    Date.strptime(date_str, '%m/%d/%Y') rescue nil
  end

  def transport_content_params
    params.require(:transport_content).permit(:id,
                                              :delivery_by_amazon,
                                              :shipment_type,
                                              :carrier,
                                              :tracking_id,
                                              :name,
                                              :email,
                                              :phone,
                                              :fax,
                                              :box_count,
                                              :frieght_class,
                                              :frieght_read_date,
                                              :total_weight,
                                              :declared_value,
                                              :pro_number,
                                              :shipping_fee,
                                              :inbound_shipment_id,
                                              packages_attributes:
                                                                  [:id,
                                                                   :dimenstion_unit,
                                                                   :length,
                                                                   :width,
                                                                   :height,
                                                                   :weight_unit,
                                                                   :weight,
                                                                   :_destroy,
                                                                  ],
                                              boxes_attributes:
                                                                  [:id,
                                                                   :dimenstion_unit,
                                                                   :length,
                                                                   :width,
                                                                   :height,
                                                                   :weight_unit,
                                                                   :weight,
                                                                   :_destroy,
                                                                   package_informations_attributes: [
                                                                                                    :id,
                                                                                                     :asin,
                                                                                                     :quantity,
                                                                                                     :expiry_date,
                                                                                                     :_destroy]
                                                                  ],
                                              trackings_attributes:
                                                                  [:id,
                                                                   :tracking_number,
                                                                   :_destroy],
                                              palletes_attributes:
                                                                  [:id,
                                                                  :dimension_unit,
                                                                  :length,
                                                                  :width,
                                                                  :height,
                                                                  :weight_unit,
                                                                  :weight,
                                                                  :_destroy])
  end
end
