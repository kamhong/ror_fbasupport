class SellerSkusController < ApplicationController
  before_action :authenticate_user!
  before_action :set_seller_sku, only: [:show, :edit, :update, :destroy]

  # GET /seller_skus
  # GET /seller_skus.json
  def index
    
  end

  def load
    vendoritem_columns = ['id','cost','upc','created_at','vendorsku','packcount','vendortitle']
		express_columns = ['profit','margin','salespermonth','salesrank'];
    notsearch_columns = ['packcost']
    vendorasin_columns = ['asin','amazon_selling']
    sku_columns = ['sku','item_name']
		start = params[:iDisplayStart].to_i
		limit = params[:iDisplayLength].to_i
		
		inventories = current_user.seller_skus.joins(:vendorasin)
		
		total = inventories.count
    
		sort_columnIndex = params[:iSortCol_0].to_i;
		sort_columnName = params["mDataProp_#{sort_columnIndex}"]
	
		sort_order = params[:sSortDir_0]
		
		iColumns = params[:iColumns].to_i
	
		i=0
		
		
		while i < iColumns do
		  search = params["sSearch_#{i}"]
		  unless search.nil? || search == ''
			search_column = params["mDataProp_#{i}"]
	
			unless notsearch_columns.include? search_column
			  if vendorasin_columns.include? search_column
          inventories = inventories.where("vendorasins.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
        elsif sku_columns.include? search_column
          inventories = inventories.where("seller_skus.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
			  elsif express_columns.include? search_column
					search.split("##").each do |x|
            search_value = x.split("$$");
            exp = search_value[0].to_s
            val = search_value[1].to_f
            if exp == 'eq'
              exp = '='
            elsif exp == 'lg'
              exp = '>'
            elsif exp == 'el'
              exp = '>='
            elsif exp == 'ls'
              exp = '<'
            elsif exp == 'es'
              exp = '<='
            else
              exp = '='
            end

            inventories = inventories.number_compare(exp,val, search_column)
          end
        elsif search_column == "bbs"
          inventories = inventories.bbs_search(search)
			  else
				  inventories = inventories.where("seller_skus.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
			  end
			end
		   
		  end
		  i = i + 1
    end
    
    

    if vendorasin_columns.include? sort_columnName
      inventories = inventories.order("vendorasins.#{sort_columnName} #{sort_order}")
      filtered = inventories.count
      inventories =inventories.limit(limit).offset(start)
    else
      inventories = inventories.order("seller_skus.#{sort_columnName} #{sort_order}")
      filtered = inventories.count
      inventories =inventories.limit(limit).offset(start)
    end
    
    filtered = inventories.count
    inventories =inventories.limit(limit).offset(start).includes(:vendorasin)
    # inventories = inventories
		render json: {'aaData'=>inventories,"iTotalRecords": total,"iTotalDisplayRecords": filtered}
  end

  # GET /seller_skus/1
  # GET /seller_skus/1.json
  def show
  end

  # GET /seller_skus/new
  def new
    @seller_sku = SellerSku.new
  end

  # GET /seller_skus/1/edit
  def edit
  end

  # POST /seller_skus
  # POST /seller_skus.json
  def create
    @seller_sku = SellerSku.new(seller_sku_params)

    respond_to do |format|
      if @seller_sku.save
        format.html { redirect_to @seller_sku, notice: 'Seller sku was successfully created.' }
        format.json { render :show, status: :created, location: @seller_sku }
      else
        format.html { render :new }
        format.json { render json: @seller_sku.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seller_skus/1
  # PATCH/PUT /seller_skus/1.json
  def update
    respond_to do |format|
      if @seller_sku.update(seller_sku_params)
        format.html { redirect_to @seller_sku, notice: 'Seller sku was successfully updated.' }
        format.json { render :show, status: :ok, location: @seller_sku }
      else
        format.html { render :edit }
        format.json { render json: @seller_sku.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seller_skus/1
  # DELETE /seller_skus/1.json
  def destroy
    @seller_sku.destroy
    respond_to do |format|
      format.html { redirect_to seller_skus_url, notice: 'Seller sku was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_seller_sku
      @seller_sku = SellerSku.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seller_sku_params
      params.fetch(:seller_sku, {})
    end
end
