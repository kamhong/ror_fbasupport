class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found
  rescue_from ActionController::RoutingError, :with => :render_not_found
  before_action :configure_permitted_parameters, if: :devise_controller?

  def dashboard
  end

  def after_sign_in_path_for(resource)
    super
      
    sign_in(resource)
    if current_user.first_name.empty? || current_user.last_name.empty?
      flash[:alert] = "Please input name!"
      # edit_user_registration_path
      profile_path
    else
      flash[:alert] = "Signed in successfully!"
      if current_user.role_id
        case current_user.role.name
          when 'Buyer'
            new_skynet_path
            # if current_user.company_id != nil and current_user.company_id != ''
            #   if current_user.active
            #     new_skynet_path
            #   else
            #     stripe_notification_path
            #   end
            # else
            #  # plans_path
            #  welcome_path
            # end
          when 'Manager'
            new_skynet_path
            # if current_user.company_id != nil and current_user.company_id != ''
            #   if current_user.active
            #     new_skynet_path
            #   else
            #     stripe_notification_path
            #   end
            # else
            #  welcome_path
            # end
          when 'Admin'
            dashboard_dashboard_admin_path
          else
            new_skynet_path
        end
      else
      end
    end
    
    
  end

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  def after_sign_up_path_for_plan(resource)
     root_path
  end

  def after_sign_up_path_for(resource)
    super
    flash[:alert] = "Signed up successfully!"
    case current_user.role.name
      when 'Buyer'
        new_skynet_path
      when 'Manager'
        new_skynet_path
      when 'Admin'
        dashboard_dashboard_admin_path
    end
  end

  

  def self.skip_params_parsing(*paths)
    ActionDispatch::ParamsParser.skipped_paths += Array.wrap(paths).flatten
  end

  def back_to_plan_without_subscribe
    if !current_user.active
      redirect_to '/stripe_notification'
    end
  end

  def render_not_found
    render :file => "/public/404.html",  :status => 404
  end

  def configure_permitted_parameters
    added_attrs = %i[first_name last_name email password password_confirmation  seller_id mws_access_token mws_market_place_id ]
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  def mws_valid_check(user_id)
    user = User.find(user_id)
    unless user.nil? || user.mws_key_valid == false
      redirect_to :back
    end
  end

  def authenticate_manager
    unless current_user&.is_warehouse
      redirect_to destroy_user_session_path, :method => :delete
    end
  end
end
