class ShipCusInvoicesController < ApplicationController
  before_action :set_ship_cus_invoice, only: [:show, :edit, :update, :destroy, :send_invoice]

  # GET /ship_cus_invoices
  # GET /ship_cus_invoices.json
  def index
    @user_option = User.where(id: InboundShipment.pluck("user_id"))
  end

  # GET /ship_cus_invoices/1
  # GET /ship_cus_invoices/1.json
  def show
    respond_to do |format|
      format.html do
      end
      format.pdf do
        @logo = "#{request.protocol}#{request.host_with_port}/images/dd_logo.png"
        @shipment = @ship_cus_invoice.inbound_shipment
        render pdf: "Invoice_#{@ship_cus_invoice.invoice_name}",
            disposition: 'attachment',
            template: "transport_plans/_invoice_pdf.html.erb",
            layout: 'pdf.html.erb',
            dpi: 300,
            margin:  {
                        top: 15,
                        bottom: 10
                    },
            footer:
            {
              html: {
                template:'transport_plans/invoice_pdf_footer',
                layout:  'pdf_plain',
              },
            }
      end
    end
  end

  def send_invoice
    @shipment = @ship_cus_invoice.inbound_shipment
    @logo = "#{request.protocol}#{request.host_with_port}/images/dd_logo.png"

    pdf = WickedPdf.new.pdf_from_string(
			render_to_string(
				disposition: 'attachment',
					template: "transport_plans/_invoice_pdf.html.erb",
					layout: 'pdf_email.html.erb',
					dpi: 300,
					margin:  {
											top: 15,
											bottom: 10
									},
					footer:
					{
						html: {
							template:'transport_plans/invoice_pdf_footer',
							layout:  'pdf_plain',
						},
					}
			)
		)
    UserMailer.send_invoice(@shipment.user.email, pdf).deliver_now
    @shipment.update(invoice_sent: true)
    redirect_to inbound_shipment_path(@shipment), flash: {notice: "Invoice sent successfully!"}
  end

  # GET /ship_cus_invoices/new
  def new
    @ship_cus_invoice = ShipCusInvoice.new
  end

  # GET /ship_cus_invoices/1/edit
  def edit
  end

  # POST /ship_cus_invoices
  # POST /ship_cus_invoices.json
  def create
    @ship_cus_invoice = ShipCusInvoice.new(ship_cus_invoice_params)

    respond_to do |format|
      if @ship_cus_invoice.save
        format.html { redirect_to @ship_cus_invoice, notice: 'Ship cus invoice was successfully created.' }
        format.json { render :show, status: :created, location: @ship_cus_invoice }
      else
        format.html { render :new }
        format.json { render json: @ship_cus_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ship_cus_invoices/1
  # PATCH/PUT /ship_cus_invoices/1.json
  def update
    respond_to do |format|
      if @ship_cus_invoice.update(ship_cus_invoice_params)
        format.html { redirect_to @ship_cus_invoice, notice: 'Ship cus invoice was successfully updated.' }
        format.json { render :show, status: :ok, location: @ship_cus_invoice }
      else
        format.html { render :edit }
        format.json { render json: @ship_cus_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ship_cus_invoices/1
  # DELETE /ship_cus_invoices/1.json
  def destroy
    @ship_cus_invoice.destroy
    respond_to do |format|
      format.html { redirect_to ship_cus_invoices_url, notice: 'Ship cus invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def load_invoices
    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i

    sort_columnIndex = params[:iSortCol_0].to_i;
    sort_columnName = params["mDataProp_#{sort_columnIndex}"]
    sort_columnName = sort_columnName.split(".")[-1]
    sort_order = params[:sSortDir_0]
    iColumns = params[:iColumns].to_i

    ship_invoices = ShipCusInvoice.all
    total = ship_invoices.count

    (0...iColumns).step(1).each do |i|
      ori_search_column = params["mDataProp_#{i}"]
      search_column = ori_search_column.split(".")[-1]
      search = params["sSearch_#{i}"]
      unless search.nil? || search == ''
        if ori_search_column.include? "user.full_name"
          ship_invoices = ship_invoices.joins(:inbound_shipment).where(inbound_shipments: {user_id: search})
        elsif ori_search_column.include? "inbound_shipment."
          ship_invoices = ship_invoices.joins(:inbound_shipment).where("inbound_shipments.#{search_column} like ?", "%#{search}%")
        else
          ship_invoices = ship_invoices.where("#{search_column} = ?", search)
        end
      end
    end
    filtered = ship_invoices.count
    ship_invoices = ship_invoices.limit(limit).offset(start).joins(inbound_shipment: [{inbound_shipment_items: :pending_po}, :user]).includes(inbound_shipment: [{inbound_shipment_items: :pending_po}, :user])
    render json: {
      "aaData"=> ship_invoices.as_json(
        include: [inbound_shipment: {
          methods: [:sku_count, :ship_qty, :ship_cost],
          include: [:inbound_shipment_items, user: {methods: :full_name}],
        }]
      ),
      "iTotalRecords": total,
      "iTotalDisplayRecords": filtered
    }
  end

  def updateitem
    invoice = nil
    data = params[:data]
    data.each do |key, item|
      invoice = ShipCusInvoice.find(key)
      item.each do |title, value|
        invoice[title.to_sym] = value
      end
      invoice.save
    end
    render json: {'data'=>invoice}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ship_cus_invoice
      @ship_cus_invoice = ShipCusInvoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ship_cus_invoice_params
      params.fetch(:ship_cus_invoice, {})
    end
end
