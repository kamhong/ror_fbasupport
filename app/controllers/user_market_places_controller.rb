class UserMarketPlacesController < ApplicationController
  before_action :set_user_market_place, only: [:show, :edit, :update, :destroy]

  # GET /user_market_places
  # GET /user_market_places.json
  def index
    @user_market_places = UserMarketPlace.all
  end

  # GET /user_market_places/1
  # GET /user_market_places/1.json
  def show
  end

  # GET /user_market_places/new
  def new
    @user_market_place = UserMarketPlace.new
  end

  # GET /user_market_places/1/edit
  def edit
  end

  # POST /user_market_places
  # POST /user_market_places.json
  def create
    @user_market_place = UserMarketPlace.new(user_market_place_params)

    respond_to do |format|
      if @user_market_place.save
        format.html { redirect_to @user_market_place, notice: 'User market place was successfully created.' }
        format.json { render :show, status: :created, location: @user_market_place }
      else
        format.html { render :new }
        format.json { render json: @user_market_place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_market_places/1
  # PATCH/PUT /user_market_places/1.json
  def update
    respond_to do |format|
      if @user_market_place.update(user_market_place_params)
        format.html { redirect_to @user_market_place, notice: 'User market place was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_market_place }
      else
        format.html { render :edit }
        format.json { render json: @user_market_place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_market_places/1
  # DELETE /user_market_places/1.json
  def destroy
    @user_market_place.destroy
    respond_to do |format|
      format.html { redirect_to user_market_places_url, notice: 'User market place was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_market_place
      @user_market_place = UserMarketPlace.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_market_place_params
      params.fetch(:user_market_place, {})
    end
end
