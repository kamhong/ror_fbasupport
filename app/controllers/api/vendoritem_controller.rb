require 'net/http'
require 'time'
require 'uri'
require 'openssl'
require 'base64'
require 'nokogiri'
require 'peddler'

class Api::VendoritemController < ActionController::Base
  include SellerSkusHelper
	include VendoritemsHelper
  before_action :authenticate_user!

  # Your Access Key ID, as taken from the Your Account page
    ACCESS_KEY_ID = "AKIAJZCOY2OOAUS72CGA"

    # Your Secret Key corresponding to the above ID, as taken from the Your Account page
    SECRET_KEY = "hLITVfSaMfagHI/dBDxgkh/avrY+qgxFVzYmH1mz"

    # The region you are interested in
    ENDPOINT = "webservices.amazon.com"

    REQUEST_URI = "/onca/xml"

    AWS_ACCESS_KEY_ID = "AKIAIB5ZWGDEZQZFMISA"

    AWS_SECRET_ACCESS_KEY = "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"

    MWSAuthToken = "amzn.mws.eea7a818-be78-8ebe-1adf-7800e3c7b942"

    MERCHANT_ID = "A2DVKAFYE38NFL"

    MARKETPLACE_ID = "ATVPDKIKX0DER"

  def getitemlist
    vendoritem_columns = ['id','cost','upc','created_at','vendorsku','packcount','vendortitle']
    express_columns = ['profit','margin','salespermonth','salesrank'];
    sku_columns = ['sku']
    notsearch_columns = ['packcost']

    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i
    vendor_id = params[:vendor]
    # inventories = Vendoritem.include(where('vendor_id = ?',vendor_id)
    # inventories = Vendoritem.joins(:vendorasin).left_outer_joins(:seller_sku).where('vendor_id = ?',vendor_id)
    inventories = Vendor.find(vendor_id).vendoritems.joins(:vendorasin)
    # inventories = Vendoritem.all
    total = inventories.count
    
    

    sort_columnIndex = params[:iSortCol_0].to_i;
    sort_columnName = params["mDataProp_#{sort_columnIndex}"]

    sort_order = params[:sSortDir_0]



    iColumns = params[:iColumns].to_i

    i=0
    true_search = ""
    search = params["sSearch"]

    unless params["sSearch"].nil? || params["sSearch"] == ''
      # inventories = inventories.joins(:vendorasin).where(:vendorasins => {:upc like "%#{search}%"}).or(inventories.where(:vendorasins => {:asin like "%#{search}%"}))
      inventories = inventories.where("vendorasins.asin like :l_search or vendoritems.vendortitle like :l_search or vendoritems.upc like :l_search or vendorasins.name like :l_search", {:l_search =>"%#{search}%"})
    end
    while i < iColumns do
      search = params["sSearch_#{i}"]
      unless search.nil? || search == ''
        true_search = search
        search_column = params["mDataProp_#{i}"]

        unless notsearch_columns.include? search_column
          if vendoritem_columns.include? search_column
            # inventories = inventories.where("cast(vendoritems.#{search_column} AS text) LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
            inventories = inventories.where("vendoritems.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          elsif express_columns.include? search_column
            search.split("##").each do |x|
              search_value = x.split("$$");
              exp = search_value[0].to_s
              val = search_value[1].to_f
              if exp == 'eq'
                exp = '='
              elsif exp == 'lg'
                exp = '>'
              elsif exp == 'el'
                exp = '>='
              elsif exp == 'ls'
                exp = '<'
              elsif exp == 'es'
                exp = '<='
              else
                exp = '='
              end

              inventories = inventories.number_compare(exp,val, search_column)
            end
          elsif sku_columns.include? search_column
            inventories = inventories.where("seller_skus.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          elsif search_column == "bbs"
            inventories = inventories.bbs_search(search)
          else
            inventories = inventories.where("vendorasins.#{search_column} LIKE :l_name",{:l_name => "%#{search}%"}) unless search.blank?
          end
        end

      end
      i = i + 1
    end

    if sort_columnName == 'margin'
      inventories = inventories.order("(vendorasins.buyboxprice-cost-vendorasins.totalfbafee) / vendoritems.cost #{sort_order}")
    elsif sort_columnName == 'profit'
      inventories = inventories.order("vendorasins.buyboxprice-cost-vendorasins.totalfbafee #{sort_order}")
    elsif sort_columnName == 'packcost'
      inventories = inventories.order("vendoritems.cost*vendoritems.packcount #{sort_order}")
    else
      if vendoritem_columns.include? sort_columnName
        inventories = inventories.order("vendoritems.#{sort_columnName} #{sort_order}")
      elsif sku_columns.include? sort_columnName
        inventories = inventories.order("seller_skus.#{sort_columnName} #{sort_order}")
      else
        inventories = inventories.order("vendorasins.#{sort_columnName} #{sort_order}")
      end
    end

    filtered = inventories.count
    inventories =inventories.includes(:vendorasin, {vendor: :user}, :seller_sku).limit(limit).offset(start)
    render json: {'aaData'=>inventories,"iTotalRecords": total,"iTotalDisplayRecords": filtered}
  end

  def refreshitems
    vendorasin_ids = params[:ids]
    begin
      refresh_bbp_by_asin(vendorasin_ids)
      refresh_fee_by_asin(vendorasin_ids)
      refresh_asin_product_adv(vendorasin_ids)
      refres_estimated_sales(vendorasin_ids)
    rescue Exception => error
      render json: {'result'=>false, 'msg'=>error}
      return
    end

    render json: {'result'=>true}
  end

  def send_to_po
    send_vendoritem_to_po(params[:sku_data])

  end

  private
    def refresh_fee_by_asin(ids)
      count = ids.length
      client = MWS.products(marketplace: MARKETPLACE_ID,
        merchant_id: MERCHANT_ID,
        aws_access_key_id: AWS_ACCESS_KEY_ID,
        auth_token: MWSAuthToken,
        aws_secret_access_key: AWS_SECRET_ACCESS_KEY)
      start = 0
      begin
        while start < count
          asins = Vendorasin.where(id: ids).distinct(:asin).offset(start).limit(20)
          fee_params = []
          asins.each do |asin|
            fee_param = {
              marketplace_id: MARKETPLACE_ID,
              id_type: 'ASIN',
              id_value: asin.asin,
              identifier: "request#{i}",
              price_to_estimate_fees: {
                listing_price: {
                  currency_code: 'USD',
                  amount: asin.buyboxprice.to_f
                },
                shipping: {
                  currency_code: 'USD',
                  amount: 0
                },
                points: {
                  points_number: 0
                }
              }
            }
            fee_params << fee_param
          end

          result = client.get_my_fees_estimate(fee_params).parse

          if result['FeesEstimateResultList']['FeesEstimateResult']
            fees_estimates = result['FeesEstimateResultList']['FeesEstimateResult']

            if fees_estimates.kind_of?(Array)
              fees_estimates.each do |fee_estimate|
                if fee_estimate['Status'] == 'Success'
                  total_fee = fee_estimate['FeesEstimate']['TotalFeesEstimate']['Amount']
                  if fee_estimate['FeesEstimateIdentifier']['IdValue']
                    asin = Vendorasin.where(:asin => fee_estimate['FeesEstimateIdentifier']['IdValue'].to_s).first_or_create
                    asin.fbafee= total_fee.to_f
                    asin.save
                  end
                end
              end
            else
              fee_estimate = fees_estimates
              if fee_estimate['Status'] == 'Success'
                total_fee = fee_estimate['FeesEstimate']['TotalFeesEstimate']['Amount']
                if fee_estimate['FeesEstimateIdentifier']['IdValue']
                  asin = Vendorasin.where(:asin => fee_estimate['FeesEstimateIdentifier']['IdValue'].to_s).first_or_create
                  asin.fbafee= total_fee.to_f
                  asin.save
                end
              end
            end

          end

          start += 20
        end
      rescue Exception=>error
      end
    end

    def refresh_bbp_by_asin(ids)
      count = ids.length
      if count == 0
        return
      end
      client = MWS.products(marketplace: MARKETPLACE_ID,
        merchant_id: MERCHANT_ID,
        aws_access_key_id: AWS_ACCESS_KEY_ID,
        auth_token: MWSAuthToken,
        aws_secret_access_key: AWS_SECRET_ACCESS_KEY)
      start = 0
      begin
        while start < count
          asin_list = []
          asins = Vendorasin.where(id: ids).distinct(:asin).offset(start).limit(20)
          asins.each do |asin|
            asin_list << asin.asin
          end
          competive_prices = client.get_competitive_pricing_for_asin(MARKETPLACE_ID, asin_list).parse

          competive_prices.each do |competive_price|

            unless competive_price["status"].to_s == "Success"
              next
            end
            asin_str = competive_price["ASIN"].to_s
            asin = Vendorasin.where(:asin => asin_str).first

            product = competive_price["Product"]
            competitive_pricing = product["CompetitivePricing"]
            competitive_prices = competitive_pricing["CompetitivePrices"]
            if competitive_prices.nil?
              next
            end
            competitive_price = competitive_prices["CompetitivePrice"]

            if competitive_price.kind_of?(Array)
              competitive_price.each do |item|
                if item["condition"].to_s == "New"
                  price = item["Price"]
                  asin.buyboxprice = price["LandedPrice"]["Amount"].to_f
                  asin.save
                end
              end
            else
              if competitive_price["condition"].to_s == "New"
                price = competitive_price["Price"]
                asin.buyboxprice = price["LandedPrice"]["Amount"].to_f
                asin.save
              end
            end
          end
          start += 20
        end
      rescue Exception => error
        puts error
      end
    end


    def refresh_asin_product_adv(ids)
      count = ids.count
      start = 0;
      while start < count
        asins = Vendorasin.where(id: ids).distinct(:asin).offset(start).limit(20)
        asin_string = ""
        asins.each do |asin|
          asin_string = "#{asin_string}#{asin.asin},"
        end
        asin_string.chomp(",")
        data = {
          "Service" => "AWSECommerceService",
          "Operation" => "ItemLookup",
          "AWSAccessKeyId" => "AKIAJZCOY2OOAUS72CGA",
          "AssociateTag" => "wraithco-20",
          "ItemId" => asin_string,
          "IdType" => "ASIN",
          "ResponseGroup" => "Images,ItemAttributes,Offers,Large"
        }

        data["Timestamp"] = Time.now.gmtime.iso8601 if !data.key?("Timestamp")
        canonical_query_string = data.sort.collect do |key, value|
          [URI.escape(key.to_s, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]")), URI.escape(value.to_s, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))].join('=')
        end.join('&')
        string_to_sign = "GET\n#{ENDPOINT}\n#{REQUEST_URI}\n#{canonical_query_string}"
        signature = Base64.encode64(OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), SECRET_KEY, string_to_sign)).strip()
        request_url = "https://#{ENDPOINT}#{REQUEST_URI}?#{canonical_query_string}&Signature=#{URI.escape(signature, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))}"
        xml_str = HTTParty.get(request_url)
        item_lookup_response = xml_str.parsed_response["ItemLookupResponse"]
        unless item_lookup_response.nil?
          items = item_lookup_response["Items"]
          request = items["Request"]
          if request["IsValid"].downcase != "TRUE".downcase
            return false
          end
          errors = items["Errors"]
          unless errors.nil?
            errors["Error"].each do |error|
              unless error["Message"].nil?
                unless error["Message"].to_s.index("not a valid value").nil?
                  arr = error["Message"].to_s.split(" ")
                  invalid_asin = arr[0]
                  asin = Vendorasin.where(:asin => invalid_asin).first
                  unless asin.nil?
                    asin.invalidid = true
                    asin.save
                  end
                end
              end
            end
          end

          unless items["Item"].nil?
            if items["Item"].kind_of?(Array)
              items["Item"].each do |item|
                update_asin(item)
              end
            else
              item = items["Item"]
              update_asin(item)
            end

          end
        end
        start += 20
      end
    end

    def refres_estimated_sales(ids)
      headers = {
        "Referer"  => "http://www.junglescout.com/estimator/",
      }
      Vendoritem.where(id: ids).joins(:vendorasin).where.not("vendorasins.outer_category is null or vendorasins.outer_category = ''").all.each do |item|
        url = "https://junglescoutpro.herokuapp.com/api/v1/sales_estimator?store=us&rank=#{item.vendorasin.salesrank}&category=#{item.vendorasin.outer_category}"
        result = HTTParty.get(url, :headers => headers).parsed_response
        if result["status"]
          if result["estSalesResult"] == "N.A."
            item.vendorasin.salesrank = 0
          else
            item.vendorasin.salesrank = result["estSalesResult"].to_i
          end
          item.vendorasin.save
        end
      end

    end

    def update_asin(item)
      asin_str = item["ASIN"].to_s
      asin = Vendorasin.where(:asin => asin_str).first_or_create
      url = "";
      url = item["SmallImage"]["URL"].to_s unless item["SmallImage"]["URL"].blank?
      url.sub! 'images-na.ssl-images-amazon.com', 'ecx.wraithco.com'
      url.sub! 'https://', 'http://'
      asin.small_image = url unless url.blank?

      url = "";
      url = item["MediumImage"]["URL"].to_s unless item["MediumImage"]["URL"].blank?
      url.sub! 'images-na.ssl-images-amazon.com', 'ecx.wraithco.com'
      url.sub! 'https://', 'http://'
      asin.medium_image = url unless url.blank?

      url = "";
      url = item["LargeImage"]["URL"].to_s unless item["LargeImage"]["URL"].blank?
      url.sub! 'images-na.ssl-images-amazon.com', 'ecx.wraithco.com'
      url.sub! 'https://', 'http://'
      asin.large_image = url unless url.blank?



      item_attributes = item["ItemAttributes"]

      brandtext = item_attributes["Brand"].to_s
      brand = Brand.where(:name => brandtext).first_or_create
      asin.brand_id = brand.id

      item_dimensions = item_attributes["ItemDimensions"]
      unless item_dimensions.nil?
        asin.height = item_dimensions["Height"]["__content__"].to_f / 100 if item_dimensions["Height"]
        asin.length = item_dimensions["Length"]["__content__"].to_f / 100 if item_dimensions["Length"]
        asin.weight = item_dimensions["Weight"]["__content__"].to_f / 100 if item_dimensions["Weight"]
        asin.width = item_dimensions["Width"]["__content__"].to_f / 100 if item_dimensions["Width"]
      end


      asin.manufacturer = item_attributes["Manufacturer"].to_s
      asin.manufacturer_minimum_age = item_attributes["ManufacturerMinimumAge"]["__content__"].to_i if item_attributes["ManufacturerMinimumAge"]

      package_dimensions = item_attributes["PackageDimensions"]
      unless package_dimensions.nil?
        asin.packageheight = package_dimensions["Height"]["__content__"].to_f / 100 if package_dimensions["Height"]
        asin.packagelength = package_dimensions["Length"]["__content__"].to_f / 100 if package_dimensions["Length"]
        asin.packageweight = package_dimensions["Weight"]["__content__"].to_f / 100 if package_dimensions["Weight"]
        asin.packagewidth = package_dimensions["Width"]["__content__"].to_f / 100 if package_dimensions["Width"]
      end


      productgroup = ProductGroup.where(:name => item_attributes["ProductGroup"].to_s).first_or_create
      asin.product_groups_id= productgroup.id

      offer_summary = item["OfferSummary"]
      bbp = 0
      unless offer_summary.nil?
        lowest_new_price = offer_summary["LowestNewPrice"]
        unless lowest_new_price.nil?
          bbp = lowest_new_price["Amount"].to_f / 100
        end
      end
      offers = item["Offers"]

      bbp =  parse_offer_bbp(offers)
      if bbp > 0
        asin.buyboxprice = bbp
      end

      browse_nodes = item["BrowseNodes"]
      unless browse_nodes.nil?
        category = get_category(browse_nodes)
        asin.outer_category = category[:outer_category]
        asin.outer_category_id = category[:outer_category_id]
        asin.inner_category = category[:inner_category]
        asin.inner_category_id = category[:inner_category_id]
      end
      asin.save
    end

    def get_category(nodes)
      category = {
        outer_category: "",
        outer_category_id: "",
        inner_category: "",
        inner_category_id: ""
      }
      browse_nodes = nodes["BrowseNode"]
      unless browse_nodes.nil?
        if browse_nodes.kind_of?(Array)
          browse_nodes.each do |browse_node|
            r = browse_node
            while r["Ancestors"]
              r = r["Ancestors"]["BrowseNode"]
              if r["Ancestors"].nil? && r["Name"] && r ["BrowseNodeId"]
                category[:outer_category] = r["Name"].to_s
                category[:outer_category_id] = r["BrowseNodeId"].to_s
              end
            end
          end

          browse_nodes.each do |browse_node|
            category[:inner_category] = browse_node["Name"].to_s
            category[:inner_category_id] = browse_node["BrowseNodeId"].to_s
            break
          end
        else
          r = browse_nodes
          while r["Ancestors"]
            r = r["Ancestors"]["BrowseNode"]
            if r["Ancestors"].nil? && r["Name"] && r ["BrowseNodeId"]
              category[:outer_category] = r["Name"].to_s
              category[:outer_category_id] = r["BrowseNodeId"].to_s
            end
          end

          category[:inner_category] = browse_nodes["Name"].to_s
          category[:inner_category_id] = browse_nodes["BrowseNodeId"].to_s
        end
      end
      return category
    end

    def parse_offer_bbp(offers)
      bbp = 0
      unless offers.nil?
        offer = offers["Offer"]
        unless offer.nil?
          if offer.kind_of?(Array)
            offer_listing = offer[0]["OfferListing"]
            unless offer_listing.nil?
              bbp = offer_listing["Price"]["Amount"].to_f / 100
            end
          else
            offer_listing = offer["OfferListing"]
            unless offer_listing.nil?
              bbp = offer_listing["Price"]["Amount"].to_f / 100
            end
          end
        end
      end
      return bbp
    end

    def send_vendoritem_to_po(sku_data)
      unavailable_skus, skus_wihout_po, already_created_po = [], [], []

      sku_data.keys.each do |key|
        seller_sku = SellerSku.find_by(sku: sku_data[key][1])
        if seller_sku.vendoritems.present?
          re_order_quantity = seller_sku.re_order_quantity_for(30)
          vendor_item = seller_sku.vendoritems.last
          po = Po.find_by(vendoritem_id: vendor_item.id)
          if po.present?
            skus_wihout_po << seller_sku.sku
          else
            po = Po.create(vendoritem_id: vendor_item.id, order_quantity: re_order_quantity)
          end
        else
          unavailable_skus << seller_sku.sku
        end
      end

      if unavailable_skus.present? or skus_wihout_po.present?
        message ||= unavailable_skus.present? ? "Following SKUS does not have a valid Vendor Item: #{unavailable_skus} " : ""
        message += skus_wihout_po.present? ? "Following SKUS are already added to PO: #{skus_wihout_po} " : ""
      else
        message = "Selected items have been added to PO!"
      end

      redirect_to seller_skus_path, flash: {notice: message}

    end
end
