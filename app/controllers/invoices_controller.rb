class InvoicesController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_manager, only: [:index]
  before_action :set_invoice_item, only: [:show]
  def index
    @user_option = User.where(id: PendingPo.joins(vendoritem: :vendor).joins("INNER JOIN users on vendors.user_id = users.id").pluck("users.id"))
    @vendor_option = Vendor.where(id: PendingPo.joins(vendoritem: :vendor).pluck("vendors.id"))
    @po_option = PendingPoIndex.all
  end
  
  def show
    respond_to do |format|
      format.html do
      end
      format.pdf do
        @logo = "#{request.protocol}#{request.host_with_port}/images/dd_logo.png"
        @users = User.where(:id => @invoice.pending_pos.joins(vendoritem: :vendor).distinct.pluck(:user_id))
        render pdf: "Invoice_#{@invoice.invoice_name}",
          disposition: 'attachment',
          template: "invoices/_invoice_pdf.html.erb",
          layout: 'pdf.html.erb',
          dpi: 300,
          margin:  {
                      top: 15,
                      bottom: 10
                  },
          footer:
          {
            html: {
              template:'invoices/_invoice_pdf_footer',
              layout:  'pdf_plain',
            },
          }
      end
    end
  end

  def list
    invoices = Invoice.all
    total = invoices.load.size
    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i
    iColumns = params[:iColumns].to_i
    (0...iColumns).step(1).each do |i|
      search = params["sSearch_#{i}"]
      ori_search_column = params["mDataProp_#{i}"]
      search_column = ori_search_column.split(".")[-1]
      unless search.nil? || search == ''
        if ori_search_column.include?("vendor_list")
          invoice_ids = InvoiceItem.joins(pending_po: :vendoritem).where(vendoritems: {vendor_id: search}).pluck("invoice_items.invoice_id").uniq
          invoices = invoices.where(:id => invoice_ids)
        elsif ori_search_column.include?("user_list")
          invoice_ids = InvoiceItem.joins(pending_po: :vendoritem).joins("INNER JOIN vendors on vendors.id = vendoritems.vendor_id").where(vendors: {user_id: search}).pluck("invoice_items.invoice_id").uniq
          invoices = invoices.where(:id => invoice_ids)
        elsif ori_search_column == "created_at"
          selected_date = Date.strptime(search, "%m/%d/%Y")
          invoices = invoices.where(created_at:  selected_date.beginning_of_day..selected_date.end_of_day)
        end
      end
    end
    filtered = invoices.load.size
    invoices = invoices.limit(limit).offset(start).joins(:invoice_items, {pending_pos: :pending_po_index}).includes(:invoice_items, {pending_pos: :pending_po_index})
    render json: {
      # 'aaData'=>invoices.limit(limit).offset(start),
      'aaData'=>invoices.as_json(
        methods: [:item_count, :confirmed_qty, :confirmed_cost, :invoiced_qty, :invoiced_cost, :pending_po_list, :user_list, :vendor_list],
        include: [:invoice_items, :pending_pos]
      ),
      "iTotalRecords": total,
      "iTotalDisplayRecords": filtered}
  end

  def updateitem
    invoice = nil
    data = params[:data]
    data.each do |key, item|
      invoice = Invoice.find(key)
      item.each do |title, value|
        invoice[title.to_sym] = value
      end
      invoice.save
    end
    render json: {'data'=>invoice}
  end

  def set_invoice
    po_id = params[:po_id]
    pending_po_item = PendingPo.find(po_id)
    txt_invoice_qty = params[:invoice_qty]&.to_i || 0
    txt_invoice_cost = params[:invoice_cost]&.to_f || 0

    invoice_name = params[:invoice_name]
    if invoice_name.blank?
      render json: {'status'=>false, 'error_message'=>'Please input valid Invoice name.'}
      return
    end

    if txt_invoice_qty <= 0 || txt_invoice_cost <= 0
      render json: {'status'=>false, 'error_message'=>'Please input valid Invoice data.'}
      return
    end

    invoice = Invoice.where(:invoice_name => invoice_name).first_or_create
    
    invoice_item = invoice.invoice_items.where(:pending_po_id => po_id).first_or_create    

    invoice_item.quantity = txt_invoice_qty
    invoice_item.cost = txt_invoice_cost
    
    invoice_item.save 

    render json: {'status'=>true}

  end

  private
    def set_invoice_item
      @invoice = Invoice.find(params[:id])
    end
end
