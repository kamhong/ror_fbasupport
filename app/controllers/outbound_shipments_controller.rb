require 'barby'
require 'barby/barcode/code_128'
require 'barby/outputter/ascii_outputter'
require 'barby/outputter/png_outputter'
require 'barby/outputter/html_outputter'

# require 'barby/outputter/rmagick_outputter'
require 'peddler'


$mws_developer_id = "AKIAIB5ZWGDEZQZFMISA"
$mws_developer_secret = "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"

class OutboundShipmentsController < ApplicationController
  include SellerSkusHelper
  before_action :authenticate_user!
  before_action :set_outbound_shipment, only: [:show, :edit, :update, :destroy, :get_previous_address]

  # GET /outbound_shipments
  # GET /outbound_shipments.json
  def index
  end

  # GET /outbound_shipments/1
  # GET /outbound_shipments/1.json
  def show
  end

  # GET /outbound_shipments/new
  def new
    @outbound_shipment = OutboundShipment.new
  end

  # GET /outbound_shipments/1/edit
  def edit
  end

  # POST /outbound_shipments
  # POST /outbound_shipments.json
  def create
    @outbound_shipment = OutboundShipment.new(outbound_shipment_params)

    respond_to do |format|
      if @outbound_shipment.save
        format.html { redirect_to @outbound_shipment, notice: 'Outbound shipment was successfully created.' }
        format.json { render :show, status: :created, location: @outbound_shipment }
      else
        format.html { render :new }
        format.json { render json: @outbound_shipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /outbound_shipments/1
  # PATCH/PUT /outbound_shipments/1.json
  def update
    respond_to do |format|
      if @outbound_shipment.update(outbound_shipment_params)
        format.html { redirect_to @outbound_shipment, notice: 'Outbound shipment was successfully updated.' }
        format.json { render :show, status: :ok, location: @outbound_shipment }
      else
        format.html { render :edit }
        format.json { render json: @outbound_shipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /outbound_shipments/1
  # DELETE /outbound_shipments/1.json
  def destroy
    @outbound_shipment.destroy
    respond_to do |format|
      format.html { redirect_to outbound_shipments_url, notice: 'Outbound shipment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def list_pending
    search = params[:upc]
    if search == "none_xxx"
      render json: {'aaData'=>[]}
      return
    end
    target = ""
    if current_user.is_warehouse
      # sku = SellerSku.find_by fnsku: search
      sku = SellerSku.where("fnsku = ? or sku = ?", search,search).first
      unless sku.nil?
        unless sku.vendorasin.asin == search
          target = "fnsku"
          outbound_shipments = OutboundShipment.all
        end
      end
      unless target == "fnsku"
        upc = Vendoritem.where("asin like ? or upc like ? or vendorsku like ?", "%#{search}%","%#{search}%","%#{search}%").first
        unless upc.nil?
          target = "upc"
          pending_pos = PendingPo.all
        end
      end

    else
      sku = current_user.seller_skus.where("fnsku = ? or sku = ?", search,search).first
      unless sku.nil?
        unless sku.vendorasin.asin == search
          outbound_shipments = current_user.outbound_shipments
          target = "fnsku"
        end
      end

      unless target == "fnsku"
        # upc = current_user.vendoritems.find_by upc: search
        upc = current_user.vendoritems.where("asin like ? or upc like ? or vendorsku like ?", "%#{search}%","%#{search}%","%#{search}%").first
        unless upc.nil?
          target = "upc"
          pending_pos = current_user.pending_pos
        end
      end
    end
    case target
    when "fnsku"
      # render json: {'aaData'=>outbound_shipments.search_fnsku(search)}
      outbound_shipments = outbound_shipments.joins(pending_po: [{vendoritem: [:vendor, {seller_sku: :user}]}, :pending_po_index]).where('seller_skus.fnsku LIKE ? or seller_skus.sku LIKE ?', "%#{search}%", "%#{search}%")
      outbound_shipments = outbound_shipments.includes({pending_po: [{vendoritem: [:vendor, {seller_sku: :user}]}, :pending_po_index]})
      render json:
      {
        aaData: outbound_shipments.as_json(
          methods: [:received_quantity, :received_quantity_pack],
          include: [{pending_po: {
                methods: [:missing],
                include: [:pending_po_index, {vendoritem:
                  {
                    include: [:vendor, {seller_sku: {include: {user: {methods: :full_name}}}}],
                    methods: [:title],
                  }}]
              }
            }]
        )
      }
    when "upc"
      pending_pos = pending_pos.joins(:pending_po_index, {vendoritem: [{seller_sku: :user}, :vendorasin]}).where('vendoritems.asin LIKE ? OR vendoritems.upc LIKE ? OR vendoritems.vendorsku like ? OR  seller_skus.sku LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%",  "%#{search}%").includes({vendoritem: [:vendorasin, {seller_sku: :user}, :vendor]}, :pending_po_index)
      render json:
      {
        aaData: pending_pos.as_json(
            root: true,
            methods: [:status_date_string, :order_cost, :remained, :missing, :shipped_packs, :confirmed_cost, :confiremd_pack_qty, :labeling_fee, :bubble_fee, :polybag_fee, :bundle_fee, :box_fee],
            include: [{vendoritem: {methods: [:title], include: [:vendor, :vendorasin, seller_sku: {include: {user: {methods: :full_name}}}]}}, :pending_po_index]
          ),
      }
      # render json: {'aaData' => pending_pos.search_on(search).as_json(:include => { :pending_po_index => {:only => :po_name}})}
    else
      render json: {'aaData'=>[]}
    end
  end

  def list_pending_all
    vendor = params[:vendor]
    name = params[:name]
    render json: {'aaData'=>PendingPo.search_all(vendor, name).as_json(:include => { :pending_po_index => {:only => :po_name}})}
  end

  def check_shipplan
    pending_po_id = params[:pending_po_id]
    box_qty = 10
    case_qty = 20

    @pending_po = PendingPo.find(pending_po_id)
    sku = @pending_po.vendoritem.seller_sku

    user = sku.user
    if user.nil? || user.mws_key_valid == false
      render json: {'status'=>false, 'msg'=>'Customer\'s API KEY is not valid.'}
      return
    end
    if sku.needed_refresh_prep
      update_prepinstructions(sku.id)
    end

    update_shipplaninformation(sku.id)

    sku.reload
    if sku.is_invalid? || sku.sku_status != "OK"
      render json: {'status'=>false, 'msg'=> sku.sku_status.presence || "Selected SKU is not valid on Marketplaces"}
      return
    end


    render json: {'status'=>true}
  end

  def send_bagger
    id = params[:id]
    outbound_shipment = OutboundShipment.find(id)
    outbound_shipment.bagger = true
    if outbound_shipment.save
      render json: {'status'=>true}
    else
      render json: {'status'=>false, 'msg'=>'Failed to send to Bagger'}
    end
  end

  def bagger_list

  end

  def list_bagger
    start = params[:iDisplayStart].to_i
    limit = params[:iDisplayLength].to_i
    search = params[:sSearch]

    unless search.blank?
      outbound_shipments = OutboundShipment.where(:bagger => 1).search_bagger(search)
    else
      outbound_shipments = OutboundShipment.where(:bagger => 1)
    end
    total = outbound_shipments.count
    outbound_shipments = outbound_shipments.limit(limit).offset(start)
    nested_data = []
    outbound_shipments.each do |item|
      data = {
        :id => item.id,
        :bagger_number => item.pending_po.vendoritem.seller_sku.bagger_number,
        :image_url => item.pending_po.vendoritem.vendorasin.small_image,
        :name => item.pending_po.vendoritem.vendorasin.name
      }
      nested_data << data
    end
    render json: {'aaData'=>nested_data,"iTotalRecords": total,"iTotalDisplayRecords": outbound_shipments.count}
  end

  def getbagger
    id = params[:id]
    render json: OutboundShipment.find(id)
  end

  def printbagger
    stage = OutboundShipment.find(params[:id])
    sku = stage.pending_po.vendoritem.seller_sku
    barcode = Barby::Code128B.new(sku.fnsku || '')

    dir = Rails.root.join('public','images','barcode')
    File.open(dir.join("#{sku.fnsku}.png"), 'wb'){|f| f.write barcode.to_png }

    pdf = WickedPdf.new.pdf_from_string(
            render_to_string(:template => 'outbound_shipments/bagger_pdf.html.erb', :locals => {:stage=>stage,:barcode_url=>"#{request.protocol}#{request.host_with_port}/images/barcode/#{sku.fnsku}.png"}, :layout => 'layouts/pdf.html.erb'),
            page_size: 'Letter',
            :page_height => 30,
            :page_width => 60,
            :margin => {
              top: 1,
                bottom:0,
                left:0,
                right:0
              },
          )
    send_data pdf, type: 'application/pdf', filename: "Bagger Report.pdf"
    return
  end

  def ajax_download
    pending_po_id = params[:id]
    box_qty = params[:box_qty]
    case_qty = params[:case_qty]
    @exp_date = params[:exp_date]

    @pending_po = PendingPo.find(pending_po_id)
    sku = @pending_po.vendoritem.seller_sku
    if sku.nil?
      pdf = WickedPdf.new.pdf_from_string(
        render_to_string(:template => 'outbound_shipments/error.html.erb', :locals => {:pending_po => @pending_po, :sticker_page => @sticker_page, :exp_date => @exp_date, :error => "SKU is not valid."}, :layout => 'layouts/pdf.html.erb'),
        page_size: 'Letter',
        :page_height => 30,
        :page_width => 60,
        :margin => {
          top: 1,
            bottom:0,
            left:0,
            right:0
          },
      )
      send_data pdf, type: 'application/pdf', filename: "sticker.pdf"
      return
    end
    user = sku.user
    if user.nil? || user.mws_key_valid == false
      pdf = WickedPdf.new.pdf_from_string(
        render_to_string(:template => 'outbound_shipments/error.html.erb', :locals => {:pending_po => @pending_po, :sticker_page => @sticker_page, :exp_date => @exp_date, :error => "MWS Access is denied."}, :layout => 'layouts/pdf.html.erb'),
        page_size: 'Letter',
        :page_height => 30,
        :page_width => 60,
        :margin => {
          top: 1,
            bottom:0,
            left:0,
            right:0
          },
      )
      send_data pdf, type: 'application/pdf', filename: "sticker.pdf"
      return
    end

    if user.nil? || user.mws_key_valid == false || sku.nil?
      pdf = WickedPdf.new.pdf_from_string(
                render_to_string(:template => 'outbound_shipments/error.html.erb', :locals => {:pending_po => @pending_po, :sticker_page => @sticker_page, :exp_date => @exp_date, :error => "MWS Access is not valid."}, :layout => 'layouts/pdf.html.erb'),
                page_size: 'Letter',
                :page_height => 30,
                :page_width => 60,
                :margin => {
                  top: 1,
                    bottom:0,
                    left:0,
                    right:0
                  },
              )
      send_data pdf, type: 'application/pdf', filename: "sticker.pdf"
      return
    end

    if sku.prepinstruction.nil?
      update_prepinstructions(sku.id)
    end

    sku.reload
    if sku.is_invalid?
      pdf = WickedPdf.new.pdf_from_string(
                render_to_string(:template => 'outbound_shipments/error.html.erb', :locals => {:pending_po => @pending_po, :sticker_page => @sticker_page, :exp_date => @exp_date, :error => "Can't print the invalid SKU."}, :layout => 'layouts/pdf.html.erb'),
                page_size: 'Letter',
                :page_height => 30,
                :page_width => 60,
                :margin => {
                  top: 1,
                    bottom:0,
                    left:0,
                    right:0
                  },
              )
      send_data pdf, type: 'application/pdf', filename: "sticker.pdf"
      return
    end

    if sku.fnsku.nil? || sku.inbound_destination.nil?
      update_shipplaninformation(sku.id)
    end
    sku.reload

    if sku.fnsku.nil?
      pdf = WickedPdf.new.pdf_from_string(
                render_to_string(:template => 'outbound_shipments/error.html.erb', :locals => {:pending_po => @pending_po, :sticker_page => @sticker_page, :exp_date => @exp_date, :error => "FNSKU is not ready on Amazon."}, :layout => 'layouts/pdf.html.erb'),
                page_size: 'Letter',
                :page_height => 30,
                :page_width => 60,
                :margin => {
                  top: 1,
                    bottom:0,
                    left:0,
                    right:0
                  },
              )
      send_data pdf, type: 'application/pdf', filename: "sticker.pdf"
      return
    end
    @pending_po = PendingPo.find(pending_po_id)
    @sticker_page = box_qty.to_i * case_qty.to_i / (@pending_po.vendoritem.packcount || 1)
    # @barcode = Barby::Code128B.new(sku.fnsku || '').to_image.to_data_url
    # br = Barby::Code128B.new(sku.fnsku || '')
    barcode = Barby::Code128B.new(sku.fnsku || '')

    dir = Rails.root.join('public','images','barcode')
    File.open(dir.join("#{sku.fnsku}.png"), 'wb'){|f| f.write barcode.to_png }

    # respond_to do |format|
      # format.html
      # format.pdf do
        pdf = WickedPdf.new.pdf_from_string(
                render_to_string(:template => 'outbound_shipments/pdf.html.erb', :locals => {:pending_po => @pending_po, :sticker_page => @sticker_page, :exp_date => @exp_date, :barcode => @barcode, :barcode_url=>"#{request.protocol}#{request.host_with_port}/images/barcode/#{sku.fnsku}.png"}, :layout => 'layouts/pdf.html.erb'),
                page_size: 'Letter',
                :page_height => 30,
                :page_width => 60,
                :margin => {
                  top: 1,
                    bottom:0,
                    left:0,
                    right:0
                  },
              )
        send_data pdf, type: 'application/pdf', filename: "#{sku.fnsku || 'sticker'}.pdf"
      # end
    # end

  end

  def printsticker
    pending_po_id = params[:pending_po_id]
    box_qty = params[:box_qty]
    case_qty = params[:case_qty]
    exp_date = params[:exp_date]
    file_name = '1484234499.pdf'

    pdf = WickedPdf.new.pdf_from_string(
                render_to_string('outbound_shipments/pdf.html.erb', :layout => 'layouts/pdf.html.erb'),
              )
    send_data pdf, type: 'application/pdf', filename: "purchase_order.pdf"
    respond_to do |format|
      @java_url = "/outbound_shipments/ajax_download?file=#{file_name}"
      format.js {render :partial => "downloadFile"}
    end
  end

  def getshipplanfromstage
    stage_id = params[:stage_id]
    outbound_stage = OutboundShipment.find(stage_id)
    if outbound_stage.nil?
      render json: {'status'=>false, 'msg'=>'Stage Item is not valid.'}
      return
    end
    if outbound_stage.stage_status == "SHIPMENT"
      render json: {'status'=>false, 'msg'=>'Shipment is already created.'}
      return
    end
    shipplan_items = outbound_stage.inbound_shipplan_items
    if shipplan_items.count == 0
      render json: {'status' => false, 'msg' => 'Shipplan is not created.'}
      return
    end
    render json: {'status' => true, 'data' => shipplan_items}
  end

  def createshipplan
    stage_id = params[:stage_id].to_i
    qty = params[:qty].to_i
    outbound_stage = OutboundShipment.find(stage_id);
    if outbound_stage.nil?
      render json: {'status'=>false, 'msg'=>'Stage Item is not valid.'}
      return
    end
    unless outbound_stage.shipplan_creatable
      render json: {'status'=>false, 'msg'=>'It is unable to create shipplan.'}
      return
    end

    outbound_stage.inbound_shipplan_items.joins(:inbound_shipplan).pluck(:inbound_shipplan_id).each do |plan_id|
      ship_plan = InboundShipplan.find(plan_id)
      unless ship_plan.nil?
        ship_plan.destroy
      end
    end
    outbound_stage.stage_status = "RECEIVED"
    outbound_stage.save
    sku = outbound_stage.pending_po.vendoritem.seller_sku

    if sku.nil?
      render json: {'status'=>false, 'msg'=>'Seller SKU information is not existed.'}
      return
    end

    user = sku.user
    if user.nil? || user.mws_key_valid == false
      render json: {'status'=>false, 'msg'=>'Customer\'s API KEY is not valid.'}
      return
    end

    if sku.needed_refresh_prep
      update_prepinstructions(sku.id)
    end

    sku.reload
    if sku.is_invalid? || sku.prepinstruction.nil?
      render json: {'status'=>false, 'msg'=>"Selected SKU is not valid on Marketplaces"}
      return
    end

    if outbound_stage.box_qty > qty
      # new_outbound_shipment = outbound_stage.dup
      # new_outbound_shipment.box_qty = new_outbound_shipment.box_qty - qty
      # new_outbound_shipment.stage_status = "RECEIVED"
      # new_outbound_shipment.shipment_id = ""

      # new_outbound_shipment.save
      outbound_stage.update_pending_po_receive(qty - outbound_stage.box_qty)
      outbound_stage.box_qty = qty
      outbound_stage.save
    elsif outbound_stage.box_qty < qty
      outbound_stage.update_pending_po_receive(qty - outbound_stage.box_qty)
      outbound_stage.box_qty = qty
      outbound_stage.save
    end

    inbound_shipment_plan_request_items = []

      prep_details = []
      sku.prepinstruction. split(',').each do |prep|
        prep_detail = {
          prep_instruction: prep,
          prep_owner: "SELLER"
        }
        prep_details << prep_detail
      end
      inbound_shipment_plan_request_item = {
        seller_sku: sku.sku,
        quantity: outbound_stage.received_quantity_pack,
        prep_details_list: prep_details #need to check
      }
      inbound_shipment_plan_request_items << inbound_shipment_plan_request_item


    if inbound_shipment_plan_request_items.count == 0
      render json: {'status'=>false, 'msg'=>"Selected SKU is not valid to create Shipplan"}
      return
    end
    opts = {
      ship_to_country_code: "US",
      label_prep_preference: "SELLER_LABEL"
    }
    client = MWS.fulfillment_inbound_shipment(marketplace: user.mws_market_place_id,
      merchant_id: user.seller_id,
      aws_access_key_id: $mws_developer_id,
      auth_token: user.mws_access_token,
      aws_secret_access_key: $mws_developer_secret)

    begin
      result = client.create_inbound_shipment_plan(user.warehouse_address, inbound_shipment_plan_request_items,opts).parse
    rescue Exception => error
      render json: {'status'=>false, 'msg'=>error.message}
      return
    end
    unless result["InboundShipmentPlans"]["member"].nil?
      unless result["InboundShipmentPlans"]["member"].kind_of?(Array)
        members = [result["InboundShipmentPlans"]["member"]]
      else
        members = result["InboundShipmentPlans"]["member"]
      end
      members.each do |shipment|
        destination_fulfillment_center_id = shipment["DestinationFulfillmentCenterId"]
        shipplan_id = shipment["ShipmentId"]
        ship_address = shipment["ShipToAddress"]

        shipplan = user.inbound_shipplans.create(
          shipplan_id: shipplan_id,
          shipment_name: "#{user.first_name[0..2]}_#{Time.now.strftime("%m_%d_%Y")}_#{destination_fulfillment_center_id}",
          postal_code: user.warehouse_address[:postal_code],
          name: user.warehouse_address[:name],
          city: user.warehouse_address[:city],
          country_code: user.warehouse_address[:country_code],
          state_or_province_code: user.warehouse_address[:state_or_province_code],
          address_line2: user.warehouse_address[:address_line2],
          address_line1: user.warehouse_address[:address_line1],
          are_cases_required: false,
          destination_fulfillment_center_id:  destination_fulfillment_center_id,
          label_prep_type: "SELLER_LABEL")
        if shipplan.nil?
          next
        end
        if shipment["Items"].kind_of?(Array)
          items = shipment["Items"]
        else
          items = [shipment["Items"]]
        end
        items.each do |item|
          fnsku = item["member"]["FulfillmentNetworkSKU"]
          seller_sku = item["member"]["SellerSKU"]
          quantity = item["member"]["Quantity"].to_i
          plan_item = outbound_stage.inbound_shipplan_items.create(
            inbound_shipplan_id: shipplan.id,
            box_qty: quantity * outbound_stage.pending_po.vendoritem.packcount / outbound_stage.case_qty,
            case_qty: outbound_stage.case_qty,
            ship_qty: quantity,
          )
          unless plan_item.nil?
            outbound_stage.stage_status = "SHIPPLAN"
            outbound_stage.save
          end
          sku = SellerSku.find_by sku: seller_sku
          unless sku.nil?
            sku.fnsku = fnsku
            sku.inbound_destination = destination_fulfillment_center_id
            sku.save
          end
        end
      end
    end

    render json: {'status'=>true}

  end

  def finalize_shipment
    @transport_content = TransportContent.new
    @package = @transport_content.packages.build
  end

  def get_previous_address
    stage_id = params[:id]
    user = @outbound_shipment&.pending_po&.vendoritem&.vendor&.user
    unless user.blank?
      render json: {'status'=>true, 'data'=>user}
    else
      render json: {'status'=>false}
    end
  end
  def get_box_information
    shipments = OutboundShipment.pending_box_info(params[:asin])
    render json: {'aaData' => shipments.presence || []}
  end

  def print_mix_2d_barcode
    @label_data = ""
    @asin_info = []
    @quantity = 0
    @outbound_shipment_data = params[:outbound_shipment]
    @outbound_shipment_data.keys.each do |index|
      next if @outbound_shipment_data[index][:quantity_to_be_boxed].to_i < 1
      shipment = OutboundShipment.find_by(shipment_id: @outbound_shipment_data[index][:shipment_id])
      inbound_shipment = InboundShipment.find_by(shipment_id: @outbound_shipment_data[index][:shipment_id])
      if shipment
        already_boxed_quantity = shipment.boxed_quantity
        shipment.update(boxed_quantity: @outbound_shipment_data[index][:quantity_to_be_boxed].to_i + already_boxed_quantity)
        if inbound_shipment
          @customer_name = inbound_shipment.try(:user).try(:full_name)
          inbound_shipment.update(mix_label_charges: inbound_shipment.mix_label_charges + 0.22)
        end
        shipment_str = "AMZN,PO:#{shipment.shipment_id},"
        @label_data += shipment_str
        @label_data += "ASIN:#{@outbound_shipment_data[index][:asin]},QTY:#{@outbound_shipment_data[index][:quantity_to_be_boxed]},"
        @asin_info << {asin: @outbound_shipment_data[index][:asin], quantity: @outbound_shipment_data[index][:quantity_to_be_boxed]}
        @quantity += @outbound_shipment_data[index][:quantity_to_be_boxed].to_i
      end
    end
    p @label_data, @asin_info, @customer_name
    if @asin_info.length == 0
      redirect_to outbound_shipments_path, notice: "Please enter quantities to be boxed!"
    else
      pdf = WickedPdf.new.pdf_from_string(
        render_to_string(:template => 'outbound_shipments/2d_label_v1.html.erb', :layout => 'layouts/pdf.html.erb'),
        page_size: 'Letter',
        :page_height => 60,
        :page_width => 60,
        :margin => {
          top: 1,
            bottom:0,
            left:0,
            right:0
          },
      )
      send_data pdf, type: 'application/pdf', filename: "#{'sticker'}.pdf"
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_outbound_shipment
      @outbound_shipment = OutboundShipment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def outbound_shipment_params
      params.fetch(:outbound_shipment, {})
    end

    def parse_date_for_amazon date
      date.to_date.strftime("%Y-%m-%d")
    end
end
